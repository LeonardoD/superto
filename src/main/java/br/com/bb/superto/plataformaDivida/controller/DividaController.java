package br.com.bb.superto.plataformaDivida.controller;

import br.com.bb.superto.controller.ArchiveController;
import br.com.bb.superto.controller.GenericController;
import br.com.bb.superto.dao.OperacaoDAO;
import br.com.bb.superto.model.Archive;
import br.com.bb.superto.model.Funcionarios;
import br.com.bb.superto.model.Operacao;
import br.com.bb.superto.model.UserNotLogged;
import br.com.bb.superto.plataformaDivida.dao.DividasDAO;
import br.com.bb.superto.plataformaDivida.model.Convenio;
import br.com.bb.superto.plataformaDivida.model.Divida;
import br.com.bb.superto.plataformaDivida.model.StatusAverbacao;
import br.com.bb.superto.security.UserSecurityUtils;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Administrador
 */
@ManagedBean(name = "dividaController")
@ViewScoped
public class DividaController extends GenericController<Divida> implements java.io.Serializable {

    private List<Divida> dividasDevolvidas;
    private List<Divida> pendentes;
    private List<Divida> dividasPrefixo;
    private List<String[]> listUploadForm;
    private List<String[]> listUploadDlg;
    private List<Archive> listAnexos;
    private Archive archiveSelect;
    private boolean opSucess;
    private int idConvenio;

    private Convenio convenio;

    public Convenio getConvenio() {
        if (convenio == null) {
            convenio = new Convenio();
        }
        return convenio;
    }

    public void setConvenio(Convenio convenio) {
        this.convenio = convenio;
    }

    
    
    @PostConstruct
    public void init() {
        listUploadForm = new ArrayList<>();
        listUploadDlg = new ArrayList<>();
        listAnexos = new ArrayList<>();
        opSucess = false;
    }

    public int getIdConvenio() {
        try{
           return selected.getIdConvenio().getID();
        }
        catch(Exception ex){
            return 0;
        }
    }

    public void setIdConvenio(int idConvenio) {
        this.idConvenio = idConvenio;
    }

    public List<Divida> getDividasDevolvidas() {
        dividasDevolvidas = new DividasDAO().getDividasDevolvidoPrefixo(value.getPrefixo());
        return dividasDevolvidas;
    }

    public void setDividasDevolvidas(List<Divida> dividasDevolvidas) {
        this.dividasDevolvidas = dividasDevolvidas;
    }

    public List<Divida> getPendentes() {
        pendentes = new DividasDAO().getDividasPendentesPrefixo(value.getPrefixo());
        return pendentes;
    }

    public void setPendentes(List<Divida> pendentes) {
        this.pendentes = pendentes;
    }

    public List<Divida> getDividasPrefixo() {
        dividasPrefixo = new DividasDAO().getDividasPrefixo(value.getPrefixo());
        return dividasPrefixo;
    }

    public void setDividasPrefixo(List<Divida> dividasPrefixo) {
        this.dividasPrefixo = dividasPrefixo;
    }

    public Archive getArchiveSelect() {
        return archiveSelect;
    }

    public void setArchiveSelect(Archive archiveSelect) {
        this.archiveSelect = archiveSelect;
    }

    public List<String[]> getListUploadForm() {
        return listUploadForm;
    }

    public void setListUploadForm(List<String[]> listUploadForm) {
        this.listUploadForm = listUploadForm;
    }

    public List<String[]> getListUploadDlg() {
        return listUploadDlg;
    }

    public void setListUploadDlg(List<String[]> listUploadDlg) {
        this.listUploadDlg = listUploadDlg;
    }

    public List<Archive> getListAnexos() {
        return listAnexos;
    }

    public void setListAnexos(List<Archive> listAnexos) {
        this.listAnexos = listAnexos;
    }

    public boolean isOpSucess() {
        return opSucess;
    }

    public void setOpSucess(boolean opSucess) {
        this.opSucess = opSucess;
    }

    public void fileUploadListenerForm(FileUploadEvent event) {
        try {
            UploadedFile file = event.getFile();
            Archive archive = new ArchiveController().add(file);
            String[] fileStatus = new String[3];
            if (archive != null) {
                fileStatus[0] = archive.getName();
                fileStatus[1] = "glyphicon glyphicon-ok";
                fileStatus[2] = "glyphicon glyphicon-file";
                listUploadForm.add(fileStatus);
                listAnexos.add(archive);
            } else {
                fileStatus[0] = file.getFileName().substring(0, file.getFileName().length());
                fileStatus[1] = "glyphicon glyphicon-remove";
                listUploadForm.add(fileStatus);
            }
            RequestContext.getCurrentInstance().update("listAnexos");
        } catch (UserNotLogged | IOException e) {
            System.err.println("Erro no Upload dos Arquivos: " + e.getMessage());
        }
    }

    public void fileUploadListenerDialog(FileUploadEvent event) {
        try {
            UploadedFile file = event.getFile();
            Archive archive = new ArchiveController().add(file);
            String[] fileStatus = new String[3];
            if (archive != null) {
                fileStatus[0] = archive.getName();
                fileStatus[1] = "glyphicon glyphicon-ok";
                fileStatus[2] = "glyphicon glyphicon-file";
                listUploadDlg.add(fileStatus);
                listAnexos.add(archive);
            } else {
                fileStatus[0] = file.getFileName().substring(0, file.getFileName().length());
                fileStatus[1] = "glyphicon glyphicon-remove";
                listUploadDlg.add(fileStatus);
            }
        } catch (UserNotLogged | IOException e) {
            System.err.println("Erro no Upload dos Arquivos: " + e.getMessage());
        }
    }

    @Override
    public void edit() {
        selected.setStatusAverbacao(StatusAverbacao.AVERBADA);
        selected.setResponsavel(selected.getLinhaCredito());

        if(selected.getIdConvenio().getID() == 1 || selected.getIdConvenio().getID() == 2 || selected.getIdConvenio().getID() == 3)
        {
                selected.setResponsavel("1970");
        }
        else{
                selected.setResponsavel("8517");
        }
        Operacao operacao = getNovaOperacao();

        try {
     
            Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
            Type dividaType = new TypeToken<Divida>() {
            }.getType();
            String jsonOut = gson.toJson(selected, dividaType);
            operacao.setJsonObj(jsonOut);
        } catch (Exception e) {
            System.out.println("Errro <<<<<<<<<<<<<<< " + e.getMessage() + " >>>>>>>>>>>>>>>>");
        }

        operacao = new OperacaoDAO().makePersistent(operacao);
        selected.getOperacaoModificacao().add(operacao);
        for (Archive novoAnexo : listAnexos) {
            selected.getAnexos().add(novoAnexo);
        }
        new DividasDAO().update(selected);
        System.out.print("Convenio final: " + selected.getConvenio());
        opSucess = true;
        listAnexos.clear();
        listUploadDlg.clear();
        Logger.getLogger(DividaController.class.getName()).log(Level.SEVERE, "Atualização de averbação concluida com sucesso!");
    }

    @Override
    public void delete() {
        if (selected != null) {
            selected.setStatusAverbacao(StatusAverbacao.DELETADO);
            Operacao operacao = getNovaOperacao();
            operacao.setJsonObj(selected.toString());
            operacao = new OperacaoDAO().makePersistent(operacao);
            selected.getOperacaoModificacao().add(operacao);
            opSucess = new DividasDAO().makePersistent(selected) != null;
        }
    }

    /**
     *
     */
    @Override
    public void add() {

        if (value.getCodigoSoliticatacao() != null) {
            value.setStatusAverbacao(StatusAverbacao.SOLICITADO);
        } else {
            value.setStatusAverbacao(StatusAverbacao.AVERBADA);
        }

        Operacao operacao = getNovaOperacao();
        try {
            Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
            Type dividaType = new TypeToken<Divida>() {
            }.getType();
            String jsonOut = gson.toJson(value, dividaType);
            operacao.setJsonObj(jsonOut);
        } catch (Exception e) {
            System.out.println("Errro <<<<<<<<<<<<<<< " + e.getMessage() + " >>>>>>>>>>>>>>>>");
        }
        value.setDatamudanca(new Date());

        value.setOperacaoCriacao(new OperacaoDAO().save(operacao));
        value.setOperacaoModificacao(new ArrayList<>());
        value.getOperacaoModificacao().add(new OperacaoDAO().save(operacao));
        value.setAnexos(new ArrayList<>());
        listAnexos.forEach(arquivo -> value.getAnexos().add(arquivo));
        value.setIdConvenio(getConvenio());
        value.setConvenio(getConvenio().getLabel());
        value.setResponsavel(getConvenio().getResponsavel());
        value.setOrdemprioridade(2);
        opSucess = new DividasDAO().save(value) != null;
        listAnexos.clear();
        listUploadForm.clear();
        clearValue();

    }

    public String verificarBackGroundColor(StatusAverbacao averbacao) {
        if (averbacao == StatusAverbacao.AVERBADA) {
            return "none";
        } else if (averbacao == StatusAverbacao.CONFERIDA) {
            return "#feeb9c";
        } else if (averbacao == StatusAverbacao.DEVOLVIDO) {
            return "#ffc8ce";
        } else if (averbacao == StatusAverbacao.LIBERADA) {
            return "#c6efcd";
        } else if (averbacao == StatusAverbacao.PENDENTE) {
            return "none";
        }
        return "none";
    }

    public String verificarTextColor(StatusAverbacao averbacao) {
        if (averbacao == StatusAverbacao.AVERBADA) {
            return "none";
        } else if (averbacao == StatusAverbacao.CONFERIDA) {
            return "#917219";
        } else if (averbacao == StatusAverbacao.DEVOLVIDO) {
            return "#8c2c37";
        } else if (averbacao == StatusAverbacao.LIBERADA) {
            return "#297b2c";
        } else if (averbacao == StatusAverbacao.PENDENTE) {
            return "none";
        }
        return "none";
    }
    private String UrlConvenio;
    private String ConvenioNome;
    private String UrlImg;

    public String getUrlConvenio() {
        return UrlConvenio;
    }

    public void setUrlConvenio(String UrlConvenio) {
        this.UrlConvenio = UrlConvenio;
    }

    public String getConvenioNome() {
        return ConvenioNome;
    }

    public void setConvenioNome(String ConvenioNome) {
        this.ConvenioNome = ConvenioNome;
    }

    public String getUrlImg() {
        return UrlImg;
    }

    public void setUrlImg(String UrlImg) {
        this.UrlImg = UrlImg;
    }

    public void onConvenioChange() throws UserNotLogged {
        if (convenio != null) {
            ConvenioNome = convenio.getNome().toUpperCase();
            //.replace(" ", "");
            UrlConvenio = "/resources/arquivos/cartilhasDeConveniados/" + ConvenioNome + ".pdf";
            UrlImg = "/resources/arquivos/cartilhasDeConveniados/logoConveniados/" + ConvenioNome + ".jpg";

        } else {
            UrlConvenio = "";
            ConvenioNome = "";
            UrlImg = "";
        }

        value99 = verificaUsuarioConvenio();
    }

    public void limpaObs() {
        selected.setObservacaoLiberacao(null);
    }

    /* SOLICITAÇÃO  */
    private boolean verificaCodigo = true;

    public boolean isVerificaCodigo() {
        return verificaCodigo;
    }

    public void setVerificaCodigo(boolean verificaCodigo) {
        this.verificaCodigo = verificaCodigo;
    }
    private boolean value99;

    public boolean isValue99() {
        return value99;
    }

    public void setValue99(boolean value99) {
        this.value99 = value99;
    }

    public boolean verificaUsuarioConvenio() throws UserNotLogged {
        Funcionarios user = UserSecurityUtils.getUserFuncionario().getFuncionario();
        if (user.getPrefixo() == 1867) { //1867
            return true;
        } else {
            return false;
        }
    }

}
