/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.plataformaDivida.dao;

import br.com.bb.superto.dao.GenericHibernateDAO;
import br.com.bb.superto.plataformaDivida.model.Convenio;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import java.io.Serializable;
import java.util.List;

/**
 * @author Administrador
 */
public class ConvenioDAO extends GenericHibernateDAO<Convenio, Serializable> {

    public List<Convenio> getConvenios() {
        Criteria criteria = getSession().createCriteria(getPersistentClass());
        criteria.add(Restrictions.eq("ativo", true));
        List<Convenio> convenios = criteria.list();

        try {
            if (convenios == null || convenios.isEmpty()) {
                return null;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return convenios;
    }
   

    public Convenio getConvenioByLabel(String label) {
        Criteria criteria = getSession().createCriteria(getPersistentClass());
        criteria.add(Restrictions.eq("label", label));
        Convenio convenio = (Convenio) criteria.uniqueResult();
        if (convenio == null) {
            System.out.println("NULL");
            return null;
        }
        return convenio;
    }
}
