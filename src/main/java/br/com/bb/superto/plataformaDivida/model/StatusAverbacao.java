/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.plataformaDivida.model;

/**
 * @author ViniciusXPS
 */
public enum StatusAverbacao {

    AVERBADA("averbada", "Averbada"),
    CONFERIDA("conferida", "Conferida"),
    LIBERADA("liberada", "Liberada"),
    DEVOLVIDO("devolvido", "Devolvido"),
    PENDENTE("pendente", "Pendente"),
    DELETADO("deletado", "Deletado"),
    SOLICITADO("solicitado", "Solicitado");

    private final String action;
    private final String label;

    private StatusAverbacao(String action, String label) {
        this.action = action;
        this.label = label;
    }

    public String getAction() {
        return action;
    }

    public String getLabel() {
        return label;
    }
}
