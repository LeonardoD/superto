/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.plataformaDivida.controller;

import br.com.bb.superto.controller.GenericController;
import br.com.bb.superto.plataformaDivida.dao.LinhaDeCreditoDAO;
import br.com.bb.superto.plataformaDivida.model.LinhaDeCredito;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.util.List;

/**
 * @author Administrador
 */
@ManagedBean
@ViewScoped
public class LinhaDeCreditoController extends GenericController<LinhaDeCredito> {

    private List<LinhaDeCredito> listLinhaDeCredito;
    private List<LinhaDeCredito> allLinhaDeCredito;

    public LinhaDeCreditoController() {
    }

    public List<LinhaDeCredito> getListLinhaDeCredito() {
        listLinhaDeCredito = new LinhaDeCreditoDAO().getLinhasDeCreditos();
        return listLinhaDeCredito;
    }

    public void setListLinhaDeCredito(List<LinhaDeCredito> listLinhaDeCredito) {
        this.listLinhaDeCredito = listLinhaDeCredito;
    }

    public List<LinhaDeCredito> getAllLinhaDeCredito() {
        allLinhaDeCredito = new LinhaDeCreditoDAO().findAll();
        return allLinhaDeCredito;
    }

    public void setAllLinhaDeCredito(List<LinhaDeCredito> allLinhaDeCredito) {
        this.allLinhaDeCredito = allLinhaDeCredito;
    }

    @Override
    public void edit() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void delete() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void add() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
