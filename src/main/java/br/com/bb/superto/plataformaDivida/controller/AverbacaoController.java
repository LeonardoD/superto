package br.com.bb.superto.plataformaDivida.controller;

import br.com.bb.superto.controller.GenericController;
import br.com.bb.superto.dao.OperacaoDAO;
import br.com.bb.superto.model.Archive;
import br.com.bb.superto.model.Operacao;
import br.com.bb.superto.plataformaDivida.dao.ConvenioDAO;
import br.com.bb.superto.plataformaDivida.dao.DividasDAO;
import br.com.bb.superto.plataformaDivida.dao.LinhaDeCreditoDAO;
import br.com.bb.superto.plataformaDivida.model.Convenio;
import br.com.bb.superto.plataformaDivida.model.Divida;
import br.com.bb.superto.plataformaDivida.model.LinhaDeCredito;
import br.com.bb.superto.plataformaDivida.model.ListaConveniosAverbacao;
import br.com.bb.superto.plataformaDivida.model.StatusAverbacao;
import br.com.bb.superto.plataformaDivida.model.responsavelAverbacao;
import com.csvreader.CsvReader;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import org.hibernate.HibernateException;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;
import java.io.*;
import java.lang.reflect.Type;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Administrador
 */
@ManagedBean(name = "averbacaoController")
@ViewScoped
public class AverbacaoController extends GenericController<Divida> implements Serializable {

    private ArrayList<Divida> listaDePedidosAverbacao = new ArrayList<Divida>();
    private ArrayList<Divida> listaDePedidosFilter;
    private List<Divida> prioridades;
    private List<Divida> lista = new ArrayList<Divida>();
    private List<String> conveniosStr = new ArrayList<String>();
    private List<Archive> listaDeArquivos;
    private UploadedFile uploadedFile;
    private StreamedContent file;
    private String convenio;
    private String responsavel;
    private String numConvenio;
    private String nomeLinhaDeCredito;
    private String numeroLinhaDeCredito;
    private ArrayList<LinhaDeCredito> listLinhaCredito;
    private ArrayList<Convenio> listConvenios;
    private ArrayList<Convenio> conveniosSelecionados;
    private String label;
    private Integer id_linha;
    private Integer id_convenio;
    private boolean opSucess;    
    private boolean outro; // outro
    private boolean numop; // Numero de Operação Inválido
    private boolean valpar; // Valor da Parcela Inválido
    private boolean prazocar; // Prazo de Carência Incorreto
    private boolean convenioinc; // Convênio Incorreto
    private boolean compaverb; // Ausência do Comprovante de Averbação / Comprovante Ilegível
    private boolean cliesanc; // Problemas na ANC / Limite de Crédito
    private boolean opexcluida; // Operação Excluída
    private boolean mescomp; // Mês de Competência Inválido
    private boolean ehDevolvida;
    private String motivos;
    private Integer tamanhoObs;
    private String tempo;
    private Divida AcSelecionado = new Divida(); 
    
    public void limpaVar(){
        selected.setOutro(false);
        selected.setNumop(false);
        selected.setValpar(false);
        selected.setPrazocar(false);
        selected.setConvenioinc(false);
        selected.setCompaverb(false);
        selected.setCliesanc(false);
        selected.setOpexcluida(false);
        selected.setMescomp(false);
        selected.setContPendVal(false);
        selected.setNaoConcursado(false);
    }

    public String getMotivos() {
        return motivos;
    }

    public void setMotivos(String motivos) {
        this.motivos = motivos;
    }
    
    public int qtdDias(int month){
        if(month == 0){
            return 31;
        }
        else if(month == 1){
            return 28;
        }
        else if(month == 2 ||month == 4 || month == 6 || month == 7 || month == 9 || month == 11){
            return 31;
        }
        else{
            return 30;
        }
    }

    public String fontWeight(Divida d){
        Date aux = new Date();
        aux.setHours(aux.getHours()-3);
        Date selecionada = d.getDatamudanca();
        
        try{
            if(aux.getHours() > selecionada.getHours() || aux.getDate() < selecionada.getDate() || aux.getDate() > selecionada.getDate()){
                return "bold; color: red;";
            }
            return "normal";
        }
        catch(Exception x){
            System.out.print("Erroq: " + x);
            return "normal";
        }
        
    }
    
    public String getTempo(Divida d) {
        try{
            tempo="";
            Date aux = new Date();
            Date selecionada = d.getDatamudanca();
            System.out.print("Entramos no getTempo");
            if(aux.after(selecionada)){
                System.out.print("aux é depois da data selecionada");

                int a = 0;

                if(aux.getDate() < selecionada.getDate()){
                    int resta = qtdDias(selecionada.getMonth()) - selecionada.getDate();
                    a = resta + aux.getDate();
                }
                else if(aux.getDate() > selecionada.getDate()){
                    a = aux.getDate() - selecionada.getDate();
                }
                
                if(a>0){
                    return a + " dias";
                }
                
                a = 0;
                if(aux.getHours() < selecionada.getHours()){
                    System.out.print("horas de aux é menor que horas de selecionada:");
                    System.out.print("horas de aux:" + aux.getHours());
                    System.out.print("horas de selecionada:" + selecionada.getHours());
                    a = selecionada.getHours() - aux.getHours();
                }
                else if(aux.getHours() > selecionada.getHours()){
                    System.out.print("horas de aux é maior que horas de selecionada:");
                    System.out.print("horas de aux:" + aux.getHours());
                    System.out.print("horas de selecionada:" + selecionada.getHours());
                    a = aux.getMinutes()- selecionada.getMinutes();
                    a = a + 60;
                }
                else{
                    a = aux.getMinutes()- selecionada.getMinutes();
                    tempo = tempo + a +" minutos.";
                    return tempo;
                }

                if(a>60){
                    double result = a / 60;
                    String p = result+"";
                    int o = Integer.parseInt(p.charAt(0)+"");
                    tempo = tempo + o +" horas, ";
                    a = a - (60 * o);
                    tempo = tempo + a +" minutos.";

                    System.out.print("tempo: " + tempo);
                    System.out.print("tempo: " + tempo);
                    System.out.print("tempo: " + tempo);
                    System.out.print("tempo: " + tempo);
                    System.out.print("tempo: " + tempo);
                    System.out.print("tempo: " + tempo);
                }
                else if(a > 0){
                        System.out.print("valor de a: " + a);
                        tempo = tempo + a +" minutos.";    
                    }
                a = 0;
//
//                if(aux.getMinutes() < selecionada.getMinutes()){
//                    a = selecionada.getMinutes() - aux.getMinutes();
//                }
//                else if(aux.getMinutes() > selecionada.getMinutes()){
//                    a = aux.getMinutes() - selecionada.getMinutes();
//                }
//
//                if(a>0){
//                    tempo = tempo + a+" minutos.";
//                }
            }
            if(tempo.equals("")){
                System.out.print("Tempo é null == retorna agora");
                System.out.print("horas de aux:" + aux.getHours());
                System.out.print("horas de selecionada:" + selecionada.getHours());
                tempo = "Agora";
            }
            return tempo;
        }
        catch(Exception ex){
            System.out.print("Erroc: " + ex);
            return " --- ";
        }
    }

    public void setTempo(String tempo) {
        this.tempo = tempo;
    }
    
    public String construtorDeFrase(){
        motivos = "";
        if(selected.isNumop()){
            motivos = "Número de Operação Inválido;"; 
        }
        if(selected.isValpar()){
            motivos = motivos + "Valor da Parcela Inválido;";
        }
        if(selected.isPrazocar()){
            motivos = motivos + "Prazo de Carência Incorreto;";
        }
        if(selected.isConvenioinc()){
            motivos = motivos + "Convênio Incorreto;";
        }
        if(selected.isCompaverb()){
            motivos = motivos + "Ausência do Comprovante de Averbação / Comprovante Ilegível;";
        }
        if(selected.isCliesanc()){
            motivos = motivos + "Problemas na ANC/Limite de Crédito;";
        }
        if(selected.isOpexcluida()){
            motivos = motivos + "Operação Excluída;";
        }
        if(selected.isMescomp()){
            motivos = motivos + "Mês de Competência Inválido;";
        }
        if(selected.isOutro()){
            motivos = motivos + "Outro;"; 
        }
        if(selected.isContPendVal()){
            motivos = motivos + "Contrato Pendente de Validação;";
        }
        if(selected.isNaoConcursado()){
            motivos = motivos + "Temporário/Não Concursado;";
        }
        return motivos;
    }

    public Divida getAcSelecionado() {
        try{
            return AcSelecionado;
        }
        catch(Exception ex){
            return new Divida();
        }
    }

    public void setAcSelecionado(Divida AcSelecionado) {
        this.AcSelecionado = AcSelecionado;
    }
    
    @PostConstruct
    public void init(){
        selected = new Divida();
        listaDeArquivos = new ArrayList<>();
        listLinhaCredito = new ArrayList<>();
        listConvenios = new ArrayList<>();
        opSucess = false;
    }

    public boolean isEhDevolvida() {
        try{
            ehDevolvida = this.selected.getStatusAverbacao().getLabel().equalsIgnoreCase("Devolvido");
            int aux = this.construtorDeFrase().length();
            if(aux == 0 && ehDevolvida){
                return true;
            }
//            selected.setNumop(true);
            this.setTamanhoObs(0);
            return false;    
        }
        catch(Exception ex){
            return false;
        }
    }

    public void setEhDevolvida(boolean ehDevolvida) {
        this.ehDevolvida = ehDevolvida;
    }
    
    public boolean isOutro() {
        return outro;
    }

    public void setOutro(boolean outro) {
        this.outro = outro;
    }

    public boolean isNumop() {
        return numop;
    }

    public void setNumop(boolean numop) {
        this.numop = numop;
    }

    public boolean isValpar() {
        return valpar;
    }

    public void setValpar(boolean valpar) {
        this.valpar = valpar;
    }

    public boolean isPrazocar() {
        return prazocar;
    }

    public void setPrazocar(boolean prazocar) {
        this.prazocar = prazocar;
    }

    public boolean isConvenioinc() {
        return convenioinc;
    }

    public void setConvenioinc(boolean convenioinc) {
        this.convenioinc = convenioinc;
    }

    public boolean isCompaverb() {
        return compaverb;
    }

    public void setCompaverb(boolean compaverb) {
        this.compaverb = compaverb;
    }

    public boolean isCliesanc() {
        return cliesanc;
    }

    public void setCliesanc(boolean cliesanc) {
        this.cliesanc = cliesanc;
    }

    public boolean isOpexcluida() {
        return opexcluida;
    }

    public void setOpexcluida(boolean opexcluida) {
        this.opexcluida = opexcluida;
    }

    public boolean isMescomp() {
        return mescomp;
    }

    public void setMescomp(boolean mescomp) {
        this.mescomp = mescomp;
    }
    
    public List<Divida> getLista() {
        System.out.print("Entramos no get Lista");
        try{
            lista = new ArrayList<Divida>();
            try{
                lista.addAll(this.getAtrasadas());
            }
            catch(Exception ex){
                System.out.print("Erro ao adicionar prioridades na lista: " + ex);
            }
            
            try{
                if(lista.size()>0){
                    return lista;
                }         
            }
            catch(Exception ex){
                System.out.print("Erro ao retornar lista: " + ex);
            }
        }
        catch(Exception ex)
        {
            Logger.getLogger(AverbacaoController.class.getName()).log(Level.SEVERE, "ERRO FOI:", ex);
        }
        return new ArrayList<Divida>();
    }

    public void setLista(List<Divida> lista) {
        this.lista = lista;
    }
    
    public List<Divida> getPrioridades() {
        try
        {
            return new DividasDAO().getTodasOrganizadas();
        }
        catch(Exception ex)
        {
            System.out.print("Erro no método getPrioridades: " + ex);
            return new ArrayList<Divida>();
        }
    }
    
    public List<Divida> getPrioridades2() {
        try
        {
            if(prioridades == null){
                prioridades = new DividasDAO().getTodasOrganizadas();
            }
            return prioridades;
        }
        catch(Exception ex)
        {
            System.out.print("Erro no método getPrioridades: " + ex);
            return new ArrayList<Divida>();
        }
    }
    
//    public List<Divida> getFechadas() {
//        try
//        {
//            return new DividasDAO().getDividasFechadas();
//        }
//        catch(Exception ex)
//        {
//            System.out.print("Erro no método getPrioridades: " + ex);
//            return new ArrayList<Divida>();
//        }
//    }

    public List<Divida> getAtrasadas() {
        try
        {
            atualizaAtrasadas();
            return new DividasDAO().getDividasAtrasadas();
        }
        catch(Exception ex)
        {
            System.out.print("Erro no método getAtrasadas: " + ex);
            return new ArrayList<Divida>();
        }
    }
    
    public List<Divida> getNormais() {
        try
        {
            return new DividasDAO().getDividasNormais();
        }
        catch(Exception ex)
        {
            System.out.print("Erro no método getNormais: " + ex);
            return new ArrayList<Divida>();
        }
    }
    
    public List<Divida> getConf() {
        try
        {
            return new DividasDAO().getConf();
        }
        catch(Exception ex)
        {
            System.out.print("Erro no método getNormais: " + ex);
            return new ArrayList<Divida>();
        }
    }
    
    public List<Divida> getAverb() {
        try
        {
            return new DividasDAO().getOrdem2();
        }
        catch(Exception ex)
        {
            System.out.print("Erro no método getNormais: " + ex);
            return new ArrayList<Divida>();
        }
    }
    public void setPrioridades(List<Divida> prioridades) {
        this.prioridades = prioridades;
    }
    
    
    public ArrayList<Divida> getListaDePedidosAverbacao() {
        listaDePedidosAverbacao = (ArrayList<Divida>) new DividasDAO().getPedidosAverbacao();
        return listaDePedidosAverbacao;
    }

    public void setListaDePedidosAverbacao(ArrayList<Divida> listaDePedidosAverbacao) {
        this.listaDePedidosAverbacao = listaDePedidosAverbacao;
    }

    public List<Archive> getListaDeArquivos() {
        if (selected != null) {
            listaDeArquivos = selected.getAnexos();
        }
        return listaDeArquivos;
    }

    public void setListaDeArquivos(List<Archive> listaDeArquivos) {
        this.listaDeArquivos = listaDeArquivos;
    }

    public ArrayList<Divida> getListaDePedidosFilter() {
        try{
            return listaDePedidosFilter;
        }
        catch(Exception ex){
            System.out.print("yyyyyyyy");
            System.out.print("Erro" + ex);
            return new ArrayList<Divida>();
        }
    }

    public void setListaDePedidosFilter(ArrayList<Divida> listaDePedidosFilter) {
        this.listaDePedidosFilter = listaDePedidosFilter;
    }

    public StreamedContent getFile() {
        return file;
    }

    public void setFile(StreamedContent file) {
        this.file = file;
    }

    public UploadedFile getUploadedFile() {
        return uploadedFile;
    }

    public void setUploadedFile(UploadedFile uploadedFile) {
        this.uploadedFile = uploadedFile;
    }

    public String getConvenio() {
        return convenio;
    }

    public void setConvenio(String convenio) {
        this.convenio = convenio;
    }

    public ArrayList<Convenio> getConveniosSelecionados() {
        return conveniosSelecionados;
    }

    public void setConveniosSelecionados(ArrayList<Convenio> conveniosSelecionados) {
        this.conveniosSelecionados = conveniosSelecionados;
    }

    public String getResponsavel() {
        return responsavel;
    }

    public void setResponsavel(String responsavel) {
        this.responsavel = responsavel;
    }

    public String getNomeLinhaDeCredito() {
        return nomeLinhaDeCredito;
    }

    public void setNomeLinhaDeCredito(String nomeLinhaDeCredito) {
        this.nomeLinhaDeCredito = nomeLinhaDeCredito;
    }

    public String getNumConvenio() {
        return numConvenio;
    }

    public void setNumConvenio(String numConvenio) {
        this.numConvenio = numConvenio;
    }

    public String getNumeroLinhaDeCredito() {
        return numeroLinhaDeCredito;
    }

    public void setNumeroLinhaDeCredito(String numeroLinhaDeCredito) {
        this.numeroLinhaDeCredito = numeroLinhaDeCredito;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Integer getId_linha() {
        return id_linha;
    }

    public void setId_linha(Integer id_linha) {
        this.id_linha = id_linha;
    }

    public Integer getId_convenio() {
        return id_convenio;
    }

    public void setId_convenio(Integer id_convenio) {
        this.id_convenio = id_convenio;
    }

    public ArrayList<LinhaDeCredito> getListLinhaCredito() {
        return listLinhaCredito;
    }

    public void setListLinhaCredito(ArrayList<LinhaDeCredito> listLinhaCredito) {
        this.listLinhaCredito = listLinhaCredito;
    }

    public ArrayList<Convenio> getListConvenios() {
        return listConvenios;
    }

    public void setListConvenios(ArrayList<Convenio> listConvenios) {
        this.listConvenios = listConvenios;
    }

    public List<String> getBrands() {
        return ListaConveniosAverbacao.getBrands();
    }
    public boolean isOpSucess() {
        return opSucess;
    }

    public void setOpSucess(boolean opSucess) {
        this.opSucess = opSucess;
    }

    public void lerArq(FileUploadEvent event) {
        try {
            UploadedFile arq = event.getFile();
            if (arq != null) {
                CsvReader csvReader = new CsvReader(arq.getInputstream(), Charset.forName("utf-8"));
                csvReader.setDelimiter(',');
                csvReader.readHeaders();
                while (csvReader.readRecord()) 
                {
                    Integer idCsv = Integer.parseInt(csvReader.get(0));
                    String status = csvReader.get(7);
                    Divida dividaAtualizada = new DividasDAO().findById(idCsv, true);
                    try {
                        if (dividaAtualizada != null) {
                            
                            for(StatusAverbacao sa : StatusAverbacao.values()){
                                if(sa.getLabel().equalsIgnoreCase(status) || sa.getAction().equalsIgnoreCase(status)){
                                    dividaAtualizada.setStatusAverbacao(sa);
                                }
                            }
//                            dividaAtualizada.setStatusAverbacao(StatusAverbacao.valueOf(status));
                            Operacao operacao = getNovaOperacao();

                            try {
                                Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
                                Type dividaType = new TypeToken<Divida>() {
                                }.getType();
                                String jsonOut = gson.toJson(dividaAtualizada, dividaType);
                                operacao.setJsonObj(jsonOut);
                            } catch (Exception e) {
                                System.out.println("Errro <<<<<<<<<<<<<<< " + e.getMessage() + " >>>>>>>>>>>>>>>>");
                            }
                            operacao = new OperacaoDAO().makePersistent(operacao);
                            dividaAtualizada.getOperacaoModificacao().add(operacao);
                            opSucess = new DividasDAO().makePersistent(dividaAtualizada) != null;
                        }
                    } catch (HibernateException ex) {
                        Logger.getLogger(AverbacaoController.class.getName()).log(Level.SEVERE, "Erro na importação do CSV, favor faver upload do arquivo csv novamente!", ex);
                    }
                }
                Logger.getLogger(AverbacaoController.class.getName()).log(Level.INFO, "Importação dos dados concluída!");
            } else {
                Logger.getLogger(AverbacaoController.class.getName()).log(Level.INFO, "Favor fazer upload do arquivo!");
            }
        } catch (IOException ex) {
            Logger.getLogger(AverbacaoController.class.getName()).log(Level.SEVERE, "Erro CSVReader: " + ex.getMessage());
        }
    }

    public StreamedContent realizarDownload(Archive a) {
        selected.setOutro(false);
        System.out.print("Entramos no método de download");
        String name;
        System.out.println(a.getUrl());
        FileInputStream stream;
        try {
            System.out.println(a.getUrl());
            stream = new FileInputStream("/home" + a.getUrl());
            name = a.getUrl().substring(a.getUrl().lastIndexOf(File.separator), a.getUrl().length());
            file = new DefaultStreamedContent(stream, "", name);
            System.out.print("arquivo recebido com sucesso - 1");
        } catch (FileNotFoundException ex) {
            System.out.println("Erro no Download: " + ex.getMessage());
        }
        System.out.print("proximo passo é retornar...");

        return file;
    }

    public SelectItem[] getAverbacaoStatus() {
        SelectItem[] itens = new SelectItem[StatusAverbacao.values().length];
        int i = 0;
        for (StatusAverbacao s : StatusAverbacao.values()) {
            itens[i++] = new SelectItem(s, s.getLabel());
        }
        return itens;
    }
    
    public List<String> getResponsaveis() {
        List<String> responsaveis = new DividasDAO().buscaResponsaveisDistintos();
        return responsaveis;
    }
    
    public List<String> getResponsaveisNovo() {
        return responsavelAverbacao.getBrands();
    }
    
    
    private String[] responsaveisSelecionados;

    public String[] getResponsaveisSelecionados() {
        return responsaveisSelecionados;
    }

    public void setResponsaveisSelecionados(String[] responsaveisSelecionados) {
        this.responsaveisSelecionados = responsaveisSelecionados;
    }

    public void editPrioridade() {
        try{
            if(selected.isPrioridade()){
                selected.setPrioridade(false);
                selected.setOrdemprioridade(2);
                new DividasDAO().update(selected);
                opSucess = true;
            }else{
                if(selected.getStatusAverbacao().toString().equalsIgnoreCase("AVERBADA")){
                    selected.setPrioridade(true);
                    selected.setOrdemprioridade(0);
                    new DividasDAO().update(selected);
                    opSucess = true;   
                }else{
                    opSucess = false;
                }
            }
        }
        catch(Exception e){
             opSucess = false;
             System.out.println("Errro <<<<<<<<<<<<<<< " + e.getMessage() + " >>>>>>>>>>>>>>>>");
        }
    }
    
    public boolean renderizaTempo(Divida d){
        if(d.getStatusAverbacao().getLabel().equalsIgnoreCase("Averbada") || d.getStatusAverbacao().getLabel().equalsIgnoreCase("Conferida")){
            return true;
        }
        return false;
    }
    
    
    @Override
    public void edit() {
        if (selected != null) {
            try {
                if(selected.getStatusAverbacao().getLabel().equalsIgnoreCase("Conferida") || selected.getStatusAverbacao().getLabel().equalsIgnoreCase("Averbada")){
                    selected.setDatamudanca(new Date());
                }
                else{
                    selected.setDatamudanca(null);
                }
                if(!(selected.getStatusAverbacao().getLabel().equalsIgnoreCase("Devolvido"))){
                    limpaVar();
                }
                else{
                    try{
                        selected.setObservacoes(selected.getObservacoes() + ";" + construtorDeFrase());
                    }
                    catch(Exception ex){
                        System.out.print("Erro: " + ex);
                    }
                }
                Operacao operacao = getNovaOperacao();
                try {
                    Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
                    Type dividaType = new TypeToken<Divida>() {
                    }.getType();
                    String jsonOut = gson.toJson(selected, dividaType);
                    System.out.println(jsonOut + ">>>>>>> JSON AVERBACAO");
                    operacao.setJsonObj(jsonOut);
                } catch (Exception e) {
                    System.out.println("Errro <<<<<<<<<<<<<<< " + e.getMessage() + " >>>>>>>>>>>>>>>>");
                }
                operacao = new OperacaoDAO().makePersistent(operacao);
                selected.getOperacaoModificacao().add(operacao);
                
                if(selected.getStatusAverbacao().toString().equalsIgnoreCase("AVERBADA")){
                    new DividasDAO().update(selected);
                }else{
                    selected.setPrioridade(false);
                    selected.setOrdemprioridade(2);
                    new DividasDAO().update(selected);
                }
     
                opSucess = true;
            } catch (Exception ex) {
                Logger.getLogger(AverbacaoController.class.getName()).log(Level.SEVERE, "Erro na atualização do Status da Averbação!", ex);
            }
        }
    }

    @Override
    public void delete() {
    }

    @Override
    public void add() {
    }

    public void addLinhaDeCredito() {
        try {
            new LinhaDeCreditoDAO().makePersistent(new LinhaDeCredito(numeroLinhaDeCredito, nomeLinhaDeCredito, true));
            opSucess = true;
        } catch (Exception ex) {
            opSucess = false;
        }
        numeroLinhaDeCredito = "";
        nomeLinhaDeCredito = "";
    }

    public void addConvenio() {
        try {
            new ConvenioDAO().makePersistent(new Convenio(numConvenio, convenio, responsavel, true));
            opSucess = true;
        } catch (Exception ex) {
            opSucess = false;
        }
        numConvenio = "";
        convenio = "";
        responsavel = "";
    }

    public void apagarAverbacao() {
        opSucess = new DividasDAO().makeTransient(selected) != null;
    }

    public void ativarLinhas() {
        LinhaDeCredito linha = new LinhaDeCreditoDAO().findById(id_linha, true);
        linha.setAtivo(true);
        opSucess = new LinhaDeCreditoDAO().makePersistent(linha) != null;
    }

    public void desativarLinhas() {
        LinhaDeCredito linha = new LinhaDeCreditoDAO().findById(id_linha, true);
        linha.setAtivo(false);
        opSucess = new LinhaDeCreditoDAO().makePersistent(linha) != null;
    }

    public void ativarConvenios() {
        Convenio c = new ConvenioDAO().findById(id_convenio, true);
        c.setAtivo(true);
        opSucess = new ConvenioDAO().makePersistent(c) != null;
    }

    public void desativarConvenios() {
        Convenio c = new ConvenioDAO().findById(id_convenio, true);
        c.setAtivo(false);
        opSucess = new ConvenioDAO().makePersistent(c) != null;
    }
    
    public String selectedBox(boolean prioridade){
        if (prioridade){
            return "#ffd800";
        }
        return "none";
    }
    
    public String bgColorPrioridade(int prioridade){
        if (prioridade == 0){
            return "#ffd800";
        }
        else if(prioridade == 1)
        {
            return "#666";
        }
        return "none";
    }
    
    public String corDoTexto(int prioridade)
    {
        if(prioridade == 1)
        {
            return "#fff";
        }
        return "#000";
    }
    
    public String verificarBackGroundColor(StatusAverbacao averbacao) {
            if (averbacao == StatusAverbacao.AVERBADA) {
            return "none";
            } else if (averbacao == StatusAverbacao.CONFERIDA) {
            return "#feeb9c";
            } else if (averbacao == StatusAverbacao.DEVOLVIDO) {
            return "#ffc8ce";
            } else if (averbacao == StatusAverbacao.LIBERADA) {
            return "#c6efcd";
            } else if (averbacao == StatusAverbacao.PENDENTE) {
            return "#FFCB99";
            }
            
        return "none";
        }

    public String verificarTextColor(StatusAverbacao averbacao) {
        if (averbacao == StatusAverbacao.AVERBADA) {
            return "none";
        } else if (averbacao == StatusAverbacao.CONFERIDA) {
            return "#917219";
        } else if (averbacao == StatusAverbacao.DEVOLVIDO) {
            return "#8c2c37";
        } else if (averbacao == StatusAverbacao.LIBERADA) {
            return "#297b2c";
        } else if (averbacao == StatusAverbacao.PENDENTE) {
            return "#5C370A";
        }
        return "none";
    }

    public Operacao getUtltimaOperacao(Divida divida) {
        Operacao operacao = null;
        if (divida != null && divida.getOperacaoModificacao().size() > 0) {
            operacao = divida.getOperacaoModificacao().get(divida.getOperacaoModificacao().size() - 1);
        }
        return operacao;
    }

    public Divida dividaFromJson(Operacao opercao) {
        Divida divida = null;
        if (opercao != null) {
            try {
                Gson gson = new Gson();
                divida = gson.fromJson(opercao.getJsonObj(), Divida.class);
            } catch (Exception e) {
                System.out.println("Erroa: " + e.getMessage());
            }
        }
        return divida;
    }

    public List<String> getConveniosStr() {
        
        conveniosStr = new DividasDAO().findConvenios();
        
        for(String s : conveniosStr){
            try{
                String[] aux = s.split("-");
                System.out.print("Aux[1] = " + aux[1]);
                s = aux[1];
            }
            catch(Exception ex){
                System.out.print("Erro ao converter convenio: " + ex);
            }
        }
        return conveniosStr;
    }

    public void setConveniosStr(List<String> conveniosStr) {
        this.conveniosStr = conveniosStr;
    }

    private String[] filtroSelecionados;

    public String[] getFiltroSelecionados() {
        return filtroSelecionados;
    }

    public void setFiltroSelecionados(String[] filtroSelecionados) {
        this.filtroSelecionados = filtroSelecionados;
    }

    public void pesquisaPorFiltro() {
        List<Divida> filtrosuper = new ArrayList<>();
        List<Divida> filtrocsu = new ArrayList<>();

        for (int i = 0; i < filtroSelecionados.length; i++) {
            if (filtroSelecionados[i].equals("8517")) {
                filtrosuper = new DividasDAO().getDividasAverbadas();
            }
            if (filtroSelecionados[i].equals("1967")) {
                filtrocsu = new DividasDAO().getConferidas();
            }
        }

        filtrosuper.addAll(filtrocsu);
        setListaDePedidosAverbacao((ArrayList<Divida>) filtrosuper);

    }
    
    public void limpaObs() {
        selected.setObservacoes(null);
    }

    public void atualizaAtrasadas() {
        
        try{
            List<Divida> listaNormais = this.getNormais();

            for(Divida d : listaNormais)
            {
                Date data = new AverbacaoController().getUtltimaOperacao(d).getDataOperacao();
                Date dataSistema = new Date();
                dataSistema.setHours(dataSistema.getHours() - 2); // o 2 é pq são duas horas de tolerância... caso precise mudar, muda aqui
                if(d.getStatusAverbacao().getLabel().equals("Averbada") || d.getStatusAverbacao().getLabel().equals("averbada"))
                {
                    if(data.getMonth() < dataSistema.getMonth())
                    {
                        d.setOrdemprioridade(1);
                    }
                    else if(data.getDate() < dataSistema.getDate())
                    {
                        d.setOrdemprioridade(1);
                    }
                    else if(data.getHours() < dataSistema.getHours())
                    {
                        d.setOrdemprioridade(1);
                    }
                    else{
                        d.setOrdemprioridade(2);
                    }
                }
                else{
                    if(d.getStatusAverbacao().getLabel().equals("Conferida") || d.getStatusAverbacao().getLabel().equals("conferida")){ 
                       d.setOrdemprioridade(3);
                    }
                    else{
                        d.setOrdemprioridade(4);
                    }
                }
            }
        }
        catch(Exception ex){
            System.out.print("Erro ao atualizar atrasadas: " + ex);
        }

    }

    public Integer getTamanhoObs(Divida d) {
        System.out.print("----------------");
        tamanhoObs = ObsSizeController(d);
        System.out.print("----------------");
        try{
            return tamanhoObs;
        }
        catch(Exception ex){
            return 0;
        }
    }

    public void setTamanhoObs(Integer tamanhoObs) {
        this.tamanhoObs = tamanhoObs;
    }
    
    
    public int ObsSizeController(Divida d){
        try{
            Divida dividaPersistente = new DividasDAO().findById(d.getId(), true);
            System.out.print("Divida Persistente é outro: " + dividaPersistente.isOutro());
            System.out.print("Divida é outro: " + d.isOutro());
             if(dividaPersistente.isOutro()){
                 System.out.print("Devolução persistente = retorno 0");
                 return 0;
            }
             
            int tamanhoMinimo = 10;
            int selecionados = 0;
            if(d.getStatusAverbacao().getLabel().equalsIgnoreCase("Devolvido")){
                if(d.isNumop()){
                    selecionados = selecionados + 1;
                    tamanhoMinimo = tamanhoMinimo + 27;
                }
                if(d.isValpar()){
                    selecionados = selecionados + 1;
                    tamanhoMinimo = tamanhoMinimo + 26;
                }
                if(d.isPrazocar()){
                    selecionados = selecionados + 1;
                    tamanhoMinimo = tamanhoMinimo + 28;
                }
                if(d.isCliesanc()){
                    selecionados = selecionados + 1;
                    tamanhoMinimo = tamanhoMinimo + 37;
                }
                if(d.isConvenioinc()){
                    selecionados = selecionados + 1;
                    tamanhoMinimo = tamanhoMinimo + 19;
                }
                if(d.isCompaverb()){
                    selecionados = selecionados + 1;
                    tamanhoMinimo = tamanhoMinimo + 60;
                }
                if(d.isOpexcluida()){
                    selecionados = selecionados + 1;
                    tamanhoMinimo = tamanhoMinimo + 18;
                }
                if(d.isMescomp()){
                    selecionados = selecionados + 1;
                    tamanhoMinimo = tamanhoMinimo + 28;
                }
                if(d.isContPendVal()){
                    selecionados = selecionados + 1;
                    tamanhoMinimo = tamanhoMinimo + 30;
                }
                if(d.isNaoConcursado()){
                    selecionados = selecionados + 1;
                    tamanhoMinimo = tamanhoMinimo + 25;
                }
                
                if(d.isOutro()){
                    System.out.print("tamanho minimo = " + tamanhoMinimo);
                    System.out.print("Atual: " + d.getObservacaoLiberacao());
                    System.out.print("Atual: " + d.getObservacoes());
                    return tamanhoMinimo;
                }
                else{
                    if(selecionados == 0){
                        System.out.print("Nenhum selecionado = retorna 10");
                        return 10;
                    }
                    
                    System.out.print("algum selecionado = retorna 0");
                    return 0;
                }
            }
            else{
                return 0;
            }
        }
        catch(Exception ex){
            System.out.print("Errob: " + ex);
            return 0;
        }
        
    }
}
