
package br.com.bb.superto.plataformaDivida.model;

/**
 *
 * @author JHEMESON S. MOTA
 */
import java.util.Arrays;
import java.util.List;

public class responsavelAverbacao {
         
    private final static String[] brands;
     
    static {
 
        brands = new String[2];
        brands[0] = "8517";
        brands[1] = "1970";
    }

    private String getRandomBrand() {
        return brands[(int) (Math.random() * 10)];
    }

    public static List<String> getBrands() {
        return Arrays.asList(brands);
    }
}
