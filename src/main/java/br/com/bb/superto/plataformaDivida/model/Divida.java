/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.plataformaDivida.model;

import br.com.bb.superto.model.Archive;
import br.com.bb.superto.model.GenericEntity;
import br.com.bb.superto.model.Operacao;
import com.google.gson.annotations.Expose;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * @author Administrador
 */

@Entity
@Table(name = "divida")
public class Divida implements Serializable, GenericEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Expose
    private Integer ID;

    @Enumerated(EnumType.STRING)
    @Expose
    private StatusAverbacao statusAverbacao;
    
    @NotNull(message = "Valor prefixo não pode estar vazio!")
    @Expose
    private Integer prefixo;

    @NotEmpty(message = "Valor matricula não pode estar vazio!")
    @Expose
    private String matricula;

    @NotEmpty(message = "Valor mciCliente não pode estar vazio!")
    @Expose
    private String mciCliente;

    @NotEmpty(message = "Valor numOperacao não pode estar vazio!")
    @Expose
    private String numOperacao;

    @NotNull(message = "Valor valorTotal não pode estar vazio!")
    @Expose
    private Double valorTotal;

    @NotNull(message = "Valor troco não pode estar vazio!")
    @Expose
    private Double troco;

    @Column(nullable = true, columnDefinition = "TEXT")

    @Expose private String observacoes;
    
    @NotEmpty(message = "Valor linhaCredito não pode estar vazio!")
    @Expose
    private String linhaCredito;

    @NotEmpty(message = "Valor convenio não pode estar vazio!")
    @Expose
    private String convenio;
    
    private String responsavel;
    
    private String codigoSoliticatacao;
    
    private boolean prioridade;
    
    private int ordemprioridade;
    
    private boolean outro; // outro
    private boolean numop; // Numero de Operação Inválido
    private boolean valpar; // Valor da Parcela Inválido
    private boolean prazocar; // Prazo de Carência Incorreto
    private boolean convenioinc; // Convênio Incorreto
    private boolean compaverb; // Ausência do Comprovante de Averbação / Comprovante Ilegível
    private boolean cliesanc; // Problemas na ANC / Limite de Crédito
    private boolean opexcluida; // Operação Excluída
    private boolean mescomp; // Mês de Competência Inválido
    private boolean contpendval; // Contrato Pedente de Validação
    private boolean naoconcursado; // Temporário/Não Concursado
    
    
    @Temporal(TemporalType.TIMESTAMP)
    private Date datamudanca;
    
    
//      ALTER TABLE divida
//      ADD COLUMN contpendval boolean DEFAULT FALSE;
//      ALTER TABLE divida
//      ADD COLUMN naoconcursado boolean DEFAULT FALSE;
    
    @Expose
    @Lob
    private String observacaoLiberacao;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @Fetch(FetchMode.SUBSELECT)
    private List<Archive> anexos;

    @OneToOne
    @JoinColumn(name = "opcriacao_id")
    private Operacao operacaoCriacao;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @Fetch(FetchMode.SUBSELECT)
    private List<Operacao> operacaoModificacao;

    @ManyToOne
    private Convenio idConvenio;
    
    
    public Divida() {
        mciCliente = "";
        numOperacao = "";
        linhaCredito = "";
        convenio = "";
        valorTotal = 0d;
        troco = 0d;
        prefixo = 0;
        observacoes = "";
        prioridade = false;
        outro = false;
        numop = false;
        valpar = false;
        prazocar = false;
        convenioinc = false;
        compaverb = false;
        cliesanc = false;
        opexcluida = false;
        mescomp = false;
        contpendval = false;
        naoconcursado = false;
    }

    /**
     *
     * @return
     */
    @Override
    public Integer getId() {
        return ID;
    }

    public boolean isPrioridade() {
        return prioridade;
    }
    
    public void setPrioridade(boolean prioridade) {
        this.prioridade = prioridade;
    }

    public int getOrdemprioridade() {
        return ordemprioridade;
    }

    public void setOrdemprioridade(int ordemprioridade) {
        this.ordemprioridade = ordemprioridade;
    }

    public void setID(Integer ID) {
        this.ID = ID;
    }

    public String getMciCliente() {
        return mciCliente;
    }

    public void setMciCliente(String mciCliente) {
        this.mciCliente = mciCliente;
    }

    public String getNumOperacao() {
        return numOperacao;
    }

    public void setNumOperacao(String numOperacao) {
        this.numOperacao = numOperacao;
    }

    public double getValorTotal() {
        try{
            return valorTotal;
        }
        catch(Exception ex){
            return 0;
        }
    }

    public void setValorTotal(double valorTotal) {
        this.valorTotal = valorTotal;
    }

    public double getTroco() {
        try{    
            return troco;
        }
        catch(Exception ex){
            return 0;
        }
    }

    public void setTroco(double troco) {
        this.troco = troco;
    }

    public List<Archive> getAnexos() {
        return anexos;
    }

    public void setAnexos(List<Archive> anexos) {
        this.anexos = anexos;
    }

    public StatusAverbacao getStatusAverbacao() {
        return statusAverbacao;
    }

    public void setStatusAverbacao(StatusAverbacao statusAverbacao) {
        this.statusAverbacao = statusAverbacao;
    }

    public Integer getPrefixo() {
        return prefixo;
    }

    public void setPrefixo(Integer prefixo) {
        this.prefixo = prefixo;
    }

    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    public String getObservacoes() {
        return observacoes;
    }

    public void setObservacoes(String observacoes) {
        this.observacoes = observacoes;
    }
    
    public String getLinhaCredito() {
        return linhaCredito;
    }

    public void setLinhaCredito(String linhaCredito) {
        this.linhaCredito = linhaCredito;
    }

    public String getConvenio() {
        try{
            String[] aux = convenio.split("-");
            System.out.print("Aux[1] = " + aux[1]);
            convenio = aux[1];
        }
        catch(Exception ex){
            System.out.print("Erro ao manipular convenio: " + ex);
        }
        return convenio;
    }

    public void setConvenio(String convenio) {
        this.convenio = convenio;
    }

    public Operacao getOperacaoCriacao() {
        return operacaoCriacao;
    }

    public void setOperacaoCriacao(Operacao operacaoCriacao) {
        this.operacaoCriacao = operacaoCriacao;
    }

    public List<Operacao> getOperacaoModificacao() {
        return operacaoModificacao;
    }

    public void setOperacaoModificacao(List<Operacao> operacaoModificacao) {
        this.operacaoModificacao = operacaoModificacao;
    }

    public Convenio getIdConvenio() {
        return idConvenio;
    }

    public void setIdConvenio(Convenio idConvenio) {
        this.idConvenio = idConvenio;
    }
    
    public String getResponsavel() {
        return responsavel;
    }

    public void setResponsavel(String responsavel) {
        this.responsavel = responsavel;
    }

    public String getObservacaoLiberacao() {
        return observacaoLiberacao;
    }

    public void setObservacaoLiberacao(String observacaoLiberacao) {
        this.observacaoLiberacao = observacaoLiberacao;
    }

    public String getCodigoSoliticatacao() {
        return codigoSoliticatacao;
    }

    public void setCodigoSoliticatacao(String codigoSoliticatacao) {
        this.codigoSoliticatacao = codigoSoliticatacao;
    }

    public boolean isOutro() {
        return outro;
    }

    public void setOutro(boolean outro) {
        this.outro = outro;
    }

    public boolean isNumop() {
        return numop;
    }

    public void setNumop(boolean numop) {
        this.numop = numop;
    }

    public boolean isValpar() {
        return valpar;
    }

    public void setValpar(boolean valpar) {
        this.valpar = valpar;
    }

    public boolean isPrazocar() {
        return prazocar;
    }

    public void setPrazocar(boolean prazocar) {
        this.prazocar = prazocar;
    }

    public boolean isConvenioinc() {
        return convenioinc;
    }

    public void setConvenioinc(boolean convenioinc) {
        this.convenioinc = convenioinc;
    }

    public boolean isCompaverb() {
        return compaverb;
    }

    public void setCompaverb(boolean compaverb) {
        this.compaverb = compaverb;
    }

    public boolean isCliesanc() {
        return cliesanc;
    }

    public void setCliesanc(boolean cliesanc) {
        this.cliesanc = cliesanc;
    }

    public boolean isOpexcluida() {
        return opexcluida;
    }

    public void setOpexcluida(boolean opexcluida) {
        this.opexcluida = opexcluida;
    }

    public boolean isMescomp() {
        return mescomp;
    }

    public void setMescomp(boolean mescomp) {
        this.mescomp = mescomp;
    }

    public boolean isContPendVal() {
        return contpendval;
    }

    public void setContPendVal(boolean contPendVal) {
        this.contpendval = contPendVal;
    }

    public boolean isNaoConcursado() {
        return naoconcursado;
    }

    public void setNaoConcursado(boolean naoConcursado) {
        this.naoconcursado = naoConcursado;
    }
    
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 17 * hash + Objects.hashCode(this.ID);
        return hash;
    }

    public Date getDatamudanca() {
        return datamudanca;
    }

    public void setDatamudanca(Date datamudanca) {
        this.datamudanca = datamudanca;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Divida other = (Divida) obj;
        if (!Objects.equals(this.ID, other.ID)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Divida{" + "ID=" + ID + ", statusAverbacao=" + statusAverbacao + ", prefixo=" + prefixo + ", matricula=" + matricula + ", mciCliente=" + mciCliente + ", numOperacao=" + numOperacao + ", valorTotal=" + valorTotal + ", troco=" + troco + ", observacoes=" + observacoes + ", linhaCredito=" + linhaCredito + ", convenio=" + convenio + ", responsavel=" + responsavel + ", codigoSoliticatacao=" + codigoSoliticatacao + ", prioridade=" + prioridade + ", ordemprioridade=" + ordemprioridade + ", outro=" + outro + ", numop=" + numop + ", valpar=" + valpar + ", prazocar=" + prazocar + ", convenioinc=" + convenioinc + ", compaverb=" + compaverb + ", cliesanc=" + cliesanc + ", opexcluida=" + opexcluida + ", mescomp=" + mescomp + ", observacaoLiberacao=" + observacaoLiberacao + ", anexos=" + anexos + ", operacaoCriacao=" + operacaoCriacao + ", operacaoModificacao=" + operacaoModificacao + ", idConvenio=" + idConvenio + '}';
    }

    
}
