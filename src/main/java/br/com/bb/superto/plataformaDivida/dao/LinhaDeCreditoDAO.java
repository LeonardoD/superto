/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.plataformaDivida.dao;

import br.com.bb.superto.dao.GenericHibernateDAO;
import br.com.bb.superto.plataformaDivida.model.LinhaDeCredito;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import java.io.Serializable;
import java.util.List;

/**
 * @author Administrador
 */
public class LinhaDeCreditoDAO extends GenericHibernateDAO<LinhaDeCredito, Serializable> {

    public List<LinhaDeCredito> getLinhasDeCreditos() {
        Criteria criteria = getSession().createCriteria(getPersistentClass());
        criteria.add(Restrictions.eq("ativo", true));
        List<LinhaDeCredito> creditos = criteria.list();

        try {
            if (creditos == null || creditos.isEmpty()) {
                return null;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return creditos;
    }

    public LinhaDeCredito getLinhaDeCreditoByLabel(String label) {
        Criteria criteria = getSession().createCriteria(getPersistentClass());
        criteria.add(Restrictions.eq("label", label));
        LinhaDeCredito credito = (LinhaDeCredito) criteria.uniqueResult();
        if (credito == null) {
            System.out.println("NULL");
            return null;
        }
        return credito;
    }
}
