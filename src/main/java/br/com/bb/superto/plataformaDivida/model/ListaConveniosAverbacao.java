
package br.com.bb.superto.plataformaDivida.model;

/**
 *
 * @author JHEMESON S. MOTA
 */
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

public class ListaConveniosAverbacao {
         
    private final static String[] brands;
     
    static {
 
        brands = new String[14];
        brands[0] = "Assembléia Legislativa do Tocantins";
        brands[1] = "Câmara Municipal de Palmas";
        brands[2] = "Defensoria Pública do Tocantins";
        brands[3] = "Governo Tocantins";
        brands[4] = "IGEPREV";
        brands[5] = "Justiça Federal de Primeiro Grau";
        brands[6] = "Ministério Público do Tocantins";
        brands[7] = "Prefeitura Municipal de Araguaína";
        brands[8] = "Prefeitura Municipal de Gurupi";
        brands[9] = "Prefeitura Municipal de Palmas";
        brands[10] = "Tribunal de Contas do Tocantins";
        brands[11] = "Tribunal Regional Eleitoral";
        brands[12] = "IGEPREV - Migração Governo Tocantins";
        brands[13] = "IMPAR - Instituto de Previdência de Araguaína";
    }

    private String getRandomBrand() {
        return brands[(int) (Math.random() * 10)];
    }

    public static List<String> getBrands() {
        return Arrays.asList(brands);
    }
}
