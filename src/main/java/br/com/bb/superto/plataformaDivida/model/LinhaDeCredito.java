/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.plataformaDivida.model;

import br.com.bb.superto.model.GenericEntity;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * @author Administrador
 */
@Entity
@Table(name = "linha_de_credito")
public class LinhaDeCredito implements Serializable, GenericEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer ID;
    @NotEmpty
    private String numero;
    @NotEmpty
    private String nome;
    @NotEmpty
    private String label;
    private boolean ativo;

    public LinhaDeCredito() {
    }

    public LinhaDeCredito(String numero, String nome, boolean ativo) {
        this.numero = numero;
        this.nome = nome;
        this.label = numero + " " + nome;
        this.ativo = ativo;
    }

    @Override
    public Integer getId() {
        return ID;
    }

    public Integer getID() {
        return ID;
    }

    public void setID(Integer ID) {
        this.ID = ID;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

    public String getLabel() {
        label = numero + " - " + nome;
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 41 * hash + Objects.hashCode(this.ID);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final LinhaDeCredito other = (LinhaDeCredito) obj;
        if (!Objects.equals(this.ID, other.ID)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "LinhaDeCredito{" + "ID=" + ID + ", numero=" + numero + ", nome=" + nome + ", label=" + label + ", ativo=" + ativo + '}';
    }

}
