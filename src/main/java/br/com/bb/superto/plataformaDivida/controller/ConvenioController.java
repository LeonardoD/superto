package br.com.bb.superto.plataformaDivida.controller;

import br.com.bb.superto.controller.GenericController;
import br.com.bb.superto.plataformaDivida.dao.ConvenioDAO;
import br.com.bb.superto.plataformaDivida.model.Convenio;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.io.Serializable;
import java.util.List;

/**
 * @author Administrador
 */
@ManagedBean
@ViewScoped
public class ConvenioController extends GenericController<Convenio> implements Serializable {

    private List<Convenio> listConvenio;
    private List<Convenio> allConvenio;

    public ConvenioController() {
    }

    public List<Convenio> getListConvenio() {
        listConvenio = new ConvenioDAO().getConvenios();
        return listConvenio;
    }

    public void setListConvenio(List<Convenio> listConvenio) {
        this.listConvenio = listConvenio;
    }

    public List<Convenio> getAllConvenio() {
        allConvenio = new ConvenioDAO().findAll();
        return allConvenio;
    }

    public void setAllConvenio(List<Convenio> allConvenio) {
        this.allConvenio = allConvenio;
    }
    
    public Convenio getById(int id){
        return new ConvenioDAO().findById(id, true);
    }

    @Override
    public void edit() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void delete() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void add() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
