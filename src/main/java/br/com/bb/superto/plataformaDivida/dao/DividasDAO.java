/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.plataformaDivida.dao;

import br.com.bb.superto.dao.GenericHibernateDAO;
import br.com.bb.superto.model.ClienteProduto;
import br.com.bb.superto.plataformaDivida.model.Divida;
import br.com.bb.superto.plataformaDivida.model.StatusAverbacao;
import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.criterion.Projections;

/**
 * @author Administrador
 */
public class DividasDAO extends GenericHibernateDAO<Divida, Serializable> {

    public Divida findByMCI(String mci) {
        Criteria criteria = getSession().createCriteria(getPersistentClass());
        criteria.add(Restrictions.eq("mciCliente", mci));
        Divida divida = (Divida) criteria.uniqueResult();
        return divida;
    }
    
//    public List<Divida> getDividasPriorizadas()
//    {
//        Criteria criteria = getSession().createCriteria(getPersistentClass());
//        criteria.addOrder(Order.asc("id"));
//        criteria.add(Restrictions.eq("prioridade", true));
//        List<Divida> dividas = criteria.list();
//
//        try {
//            if (dividas == null || dividas.isEmpty()) {
//                return null;
//            }
//        } catch (Exception ex) {
//            System.out.println("Erro na consulta dos pedidos de averbacao: " + ex.getMessage());
//        }
//        return dividas;
//    }

    public List<Divida> getDividasPriorizadas()
    {
        Criteria criteria = getSession().createCriteria(getPersistentClass());
        criteria.addOrder(Order.asc("id"));
        criteria.add(Restrictions.eq("ordemprioridade", 0));
        List<Divida> dividas = criteria.list();

        try {
            if (dividas == null || dividas.isEmpty()) {
                return null;
            }
        } catch (Exception ex) {
            System.out.println("Erro na consulta dos pedidos de averbacao: " + ex.getMessage());
        }
        return dividas;
    }
    public List<Divida> getTodasOrganizadas()
    {
        try{
            Query q = getSession().createQuery("SELECT d FROM Divida d WHERE d.id > 25150 ORDER BY d.ordemprioridade, d.id DESC");
//            Query q = getSession().createQuery("SELECT d FROM Divida d WHERE d.id > 25150 ORDER BY d.ordemprioridade, d.id DESC");
            return q.list();
        }
        catch(Exception ex){
            return new ArrayList<Divida>();
        }
    }
//    
//    public List<Divida> getDividasPriorizadas()
//    {
//        try{
//            Query q = getSession().createQuery("SELECT d FROM Divida d WHERE d.ordemprioridade = 0 AND d.id > 18500 ORDER BY d.id ASC");
//            return q.list();
//        }
//        catch(Exception ex){
//            return new ArrayList<Divida>();
//        }
//    }
    
    public List<Divida> getDividasAtrasadas()
    {
        try{
            Query q = getSession().createQuery("SELECT d FROM Divida d WHERE (d.ordemprioridade = 0 OR d.ordemprioridade = 1) AND d.id > 19500 ORDER BY d.ordemprioridade, d.id ASC");
            return q.list();
        }
        catch(Exception ex){
            return new ArrayList<Divida>();
        }
    }
    
    public List<Divida> getDividasNormais()
    {
        try{
            Query q = getSession().createQuery("SELECT d FROM Divida d WHERE (d.ordemprioridade = 2 OR d.ordemprioridade = 3 OR d.ordemprioridade = 4) AND d.id > 19500 ORDER BY d.ordemprioridade, d.id DESC");
            return q.list();
        }
        catch(Exception ex){
            return new ArrayList<Divida>();
        }
    }
    
    public List<Divida> getConf()
    {
        try{
            Query q = getSession().createQuery("SELECT d FROM Divida d WHERE (d.ordemprioridade = 3) AND d.id > 19500 ORDER BY d.ordemprioridade, d.id DESC");
            return q.list();
        }
        catch(Exception ex){
            return new ArrayList<Divida>();
        }
    }
    
    public List<Divida> getOrdem2()
    {
        try{
            Query q = getSession().createQuery("SELECT d FROM Divida d WHERE (d.ordemprioridade = 2 OR d.ordemprioridade = 3) AND d.id > 19500 ORDER BY d.ordemprioridade, d.id DESC");
            return q.list();
        }
        catch(Exception ex){
            return new ArrayList<Divida>();
        }
    }
//    
//    public List<Divida> getDividasFechadas()
//    {
//        try{
//            Query q = getSession().createQuery("SELECT d FROM Divida d WHERE d.ordemprioridade = 3 AND d.id > 18500 ORDER BY d.id DESC");
//            return q.list();
//        }
//        catch(Exception ex){
//            return new ArrayList<Divida>();
//        }
//    }
//    public List<Divida> getDividasNaoPriorizadas()
//    {
//        Criteria criteria = getSession().createCriteria(getPersistentClass());
//        criteria.addOrder(Order.desc("id"));
//        criteria.add(Restrictions.eq("prioridade", false));
//        List<Divida> dividas = criteria.list();
//
//        try {
//            if (dividas == null || dividas.isEmpty()) {
//                return null;
//            }
//        } catch (Exception ex) {
//            System.out.println("Erro na consulta dos pedidos de averbacao: " + ex.getMessage());
//        }
//        return dividas;
//    }

    
    public List<Divida> getDividasAverbadas() {
        Criteria criteria = getSession().createCriteria(getPersistentClass());
        criteria.addOrder(Order.desc("id"));
        criteria.add(Restrictions.eq("statusAverbacao", StatusAverbacao.AVERBADA));
        List<Divida> dividas = criteria.list();

        try {
            if (dividas == null || dividas.isEmpty()) {
                return null;
            }
        } catch (Exception ex) {
            System.out.println("Erro na consulta dos pedidos de averbacao: " + ex.getMessage());
        }
        return dividas;
    }

    public List<Divida> getConferidas() {
        Criteria criteria = getSession().createCriteria(getPersistentClass());
        criteria.addOrder(Order.desc("id"));
        criteria.add(Restrictions.eq("statusAverbacao", StatusAverbacao.CONFERIDA));
        List<Divida> dividas = criteria.list();

        try {
            if (dividas == null || dividas.isEmpty()) {
                return null;
            }
        } catch (Exception ex) {
            System.out.println("Erro na consulta dos pedidos de averbacao: " + ex.getMessage());
        }
        return dividas;
    }

    public List<Divida> getDevolvidasOrPendentes() {
        Criteria criteria = getSession().createCriteria(getPersistentClass());
        criteria.addOrder(Order.desc("id"));
        Criterion criterioDevolvido = (Restrictions.eq("statusAverbacao", StatusAverbacao.DEVOLVIDO));
        Criterion criterioPedente = (Restrictions.eq("statusAverbacao", StatusAverbacao.PENDENTE));
        criteria.add(Restrictions.or(criterioDevolvido, criterioPedente));
        List<Divida> dividas = criteria.list();

        try {
            if (dividas == null || dividas.isEmpty()) {
                return null;
            }
        } catch (Exception ex) {
            System.out.println("Erro na consulta dos pedidos de averbacao: " + ex.getMessage());
        }
        return dividas;
    }

    public List<Divida> getLiberadas() {
        Criteria criteria = getSession().createCriteria(getPersistentClass());
        criteria.addOrder(Order.desc("id"));
        criteria.add(Restrictions.eq("statusAverbacao", StatusAverbacao.LIBERADA));
        List<Divida> dividas = criteria.list();

        try {
            if (dividas == null || dividas.isEmpty()) {
                return null;
            }
        } catch (Exception ex) {
            System.out.println("Erro na consulta dos pedidos de averbacao: " + ex.getMessage());
        }
        return dividas;
    }

    public List<Divida> getDividasPrefixo(Integer prefixo) {
        Criteria criteria = getSession().createCriteria(getPersistentClass());
        criteria.addOrder(Order.desc("ID"));
        criteria.add(Restrictions.eq("prefixo", prefixo));
        criteria.add(Restrictions.not(Restrictions.eq("statusAverbacao", StatusAverbacao.DEVOLVIDO)));
        List<Divida> dividas = criteria.list();

        try {
            if (dividas == null || dividas.isEmpty()) {
                return null;
            }
        } catch (Exception ex) {
            System.out.println("Nao deu certo a lista de dividas!!!");
        }
        return dividas;
    }

    public List<Divida> getDividasDevolvidoPrefixo(Integer prefixo) {
        Criteria criteria = getSession().createCriteria(getPersistentClass());
        criteria.addOrder(Order.desc("ID"));
        criteria.add(Restrictions.eq("prefixo", prefixo));
        criteria.add(Restrictions.eq("statusAverbacao", StatusAverbacao.DEVOLVIDO));
        List<Divida> dividas = criteria.list();

        try {
            if (dividas == null || dividas.isEmpty()) {
                return null;
            }
        } catch (Exception ex) {
            System.out.println("Nao deu certo a lista de dividas!!!");
        }
        return dividas;
    }

    public List<Divida> getDividasPendentesPrefixo(Integer prefixo) {
        Criteria criteria = getSession().createCriteria(getPersistentClass());
        criteria.addOrder(Order.asc("ID"));
        criteria.add(Restrictions.eq("prefixo", prefixo));
        criteria.add(Restrictions.eq("statusAverbacao", StatusAverbacao.PENDENTE));
        List<Divida> dividas = criteria.list();

        try {
            if (dividas == null || dividas.isEmpty()) {
                return null;
            }
        } catch (Exception ex) {
            System.out.println("Nao deu certo a lista de dividas!!!");
        }
        return dividas;
    }

    public List<Divida> getPedidosAverbacao() {
        Criteria criteria = getSession().createCriteria(getPersistentClass());
        criteria.addOrder(Order.desc("id"));
        criteria.addOrder(Order.asc("statusAverbacao"));
        return criteria.list();
    }

     public List<Divida> pegaOrdenadaPrioridade() {
        Criteria criteria = getSession().createCriteria(getPersistentClass());
        criteria.addOrder(Order.desc("prioridade"));
        criteria.addOrder(Order.desc("id"));
        return criteria.list();
    }

    public List<String> buscaResponsaveisDistintos() {
        Criteria criteria = getSession().createCriteria(getPersistentClass());
        criteria.setProjection(Projections.distinct(Projections.property("responsavel")));
        return criteria.list();
    }
    
    public List<String> findConvenios() {
        Query q = getSession().createQuery("SELECT d.convenio FROM Divida d GROUP BY d.convenio");
        return q.list();
    }
    
    

}
