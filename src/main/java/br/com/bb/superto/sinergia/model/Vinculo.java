/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.sinergia.model;

/**
 * @author Administrador
 */
public class Vinculo {

    private String produto;
    private SinergiaGrupo grupo;

    public Vinculo(String produto, SinergiaGrupo grupo) {
        this.produto = produto;
        this.grupo = grupo;
    }

    public Vinculo() {
    }

    public String getProduto() {
        return produto;
    }

    public void setProduto(String produto) {
        this.produto = produto;
    }

    public SinergiaGrupo getGrupo() {
        return grupo;
    }

    public void setGrupo(SinergiaGrupo grupo) {
        this.grupo = grupo;
    }

}
