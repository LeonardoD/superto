/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.sinergia.model;

/**
 * @author Administrador
 */
public class Referencia {

    private String dataString;
    private String dataAbrev;

    public Referencia(String dataString, String dataAbrev) {
        this.dataString = dataString;
        this.dataAbrev = dataAbrev;
    }

    public Referencia() {
        this.dataString = "";
        this.dataAbrev = "";
    }

    public String getDataString() {
        return dataString;
    }

    public void setDataString(String dataString) {
        this.dataString = dataString;
    }

    public String getDataAbrev() {
        return dataAbrev;
    }

    public void setDataAbrev(String dataAbrev) {
        this.dataAbrev = dataAbrev;
    }

}
