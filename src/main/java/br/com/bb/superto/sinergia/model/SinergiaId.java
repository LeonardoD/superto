/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.sinergia.model;

import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

/**
 * @author Filipe
 */
@Embeddable
public class SinergiaId implements Serializable {

    private static final Long SerialVersionUID = 1L;
    private Integer prefixo;
    private String refencia;
    private String produto;

    public SinergiaId() {
    }

    public SinergiaId(Integer prefixo, String refencia, String produto) {
        this.prefixo = prefixo;
        this.refencia = refencia;
        this.produto = produto;
    }

    public Integer getPrefixo() {
        return prefixo;
    }

    public void setPrefixo(Integer prefixo) {
        this.prefixo = prefixo;
    }

    public String getRefencia() {
        return refencia;
    }

    public void setRefencia(String refencia) {
        this.refencia = refencia;
    }

    public String getProduto() {
        return produto;
    }

    public void setProduto(String produto) {
        this.produto = produto;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 29 * hash + Objects.hashCode(this.prefixo);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SinergiaId other = (SinergiaId) obj;
        if (!Objects.equals(this.prefixo, other.prefixo)) {
            return false;
        }
        return true;
    }

}
