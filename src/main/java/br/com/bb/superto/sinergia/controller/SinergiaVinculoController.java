package br.com.bb.superto.sinergia.controller;

import br.com.bb.superto.controller.GenericController;
import br.com.bb.superto.sinergia.dao.SinergiaDAO;
import br.com.bb.superto.sinergia.dao.SinergiaGrupoDAO;
import br.com.bb.superto.sinergia.model.Sinergia;
import br.com.bb.superto.sinergia.model.SinergiaGrupo;
import br.com.bb.superto.sinergia.model.Vinculo;
import org.hibernate.HibernateException;
import org.hibernate.exception.ConstraintViolationException;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Administrador
 */
@ManagedBean(name = "sinergiaVinculoController")
@ViewScoped
public class SinergiaVinculoController extends GenericController<Sinergia> implements Serializable {

    private List<Vinculo> listVinculos;
    private String produto;
    private List<SinergiaGrupo> listaGrupos;
    private SinergiaGrupo grupo;
    private List<String> listaProdutos;
    private Sinergia sinergia;
    private List<Sinergia> sinergias;

    @PostConstruct
    public void init() {
        listVinculos = new ArrayList<>();
        listaProdutos = new ArrayList<>();
        grupo = new SinergiaGrupo();
        selected = new Sinergia();
    }

    public String getProduto() {
        return produto;
    }

    public void setProduto(String produto) {
        this.produto = produto;
    }

    public SinergiaGrupo getGrupo() {
        return grupo;
    }

    public void setGrupo(SinergiaGrupo grupo) {
        this.grupo = grupo;
    }

    public List<SinergiaGrupo> getListaGrupos() {
        listaGrupos = new SinergiaGrupoDAO().findAll();
        return listaGrupos;
    }

    public void setListaGrupos(List<SinergiaGrupo> listaGrupos) {
        this.listaGrupos = listaGrupos;
    }

    public List<String> getListaProdutos() {
        listaProdutos = new SinergiaDAO().findProdutosSemGrupo();
        return listaProdutos;
    }

    public void setListaProdutos(List<String> listaProdutos) {
        this.listaProdutos = listaProdutos;
    }

    public List<Sinergia> getSinergias() {
        return sinergias;
    }

    public void setSinergias(List<Sinergia> sinergias) {
        this.sinergias = sinergias;
    }

    public Sinergia getSinergia() {
        return sinergia;
    }

    public void setSinergia(Sinergia sinergia) {
        this.sinergia = sinergia;
    }

    public List<Vinculo> getListVinculos() {
        listVinculos = new SinergiaDAO().findVinculos();
        return listVinculos;
    }

    public void setListVinculos(List<Vinculo> listVinculos) {
        this.listVinculos = listVinculos;
    }

    public void edit(String pProduto, SinergiaGrupo pGrupo) {
        SinergiaDAO sDao = new SinergiaDAO();
        List<Sinergia> s = new SinergiaDAO().findAllByProduto(pProduto);
        try {
            if (pProduto == null || pGrupo == null) {
                throw new NullPointerException();
            }
            if (s != null) {
                for (Sinergia aux : s) {
                    try {
                        aux.setGrupo(pGrupo);
                        sDao.update(aux);
                        addFacesMsg("Produto " + selected.getSinergiaId().getProduto() + " foi Vinculado ao Grupo " + grupo.getDescricao() + " com sucesso.", FacesMessage.SEVERITY_INFO);
                    } catch (HibernateException e) {
                        addFacesMsg("Houve um erro ao realizar o vinculo do produto " + pProduto + " com o grupo " + pGrupo, FacesMessage.SEVERITY_ERROR);
                    }
                }
            }
        } catch (NullPointerException ex) {
            addFacesMsg("Selecione um Vinculo!", FacesMessage.SEVERITY_ERROR);
        }
    }

    @Override
    public void edit() {
    }

    @Override
    public void delete() {
        List<String> errors = new ArrayList<>();
        try {
            if (selected == null) {
                throw new NullPointerException();
            }
            selected = new SinergiaDAO().makeTransient(selected);
            addFacesMsg("Vinculo entre produto " + selected.getSinergiaId().getProduto() + " e grupo " + selected.getGrupo().getDescricao() + " removido com sucesso.", FacesMessage.SEVERITY_INFO);
            selected = null;
        } catch (NullPointerException ex) {
            errors.add("Víncuo não selecionado!");
        } catch (ConstraintViolationException ex) {
            errors.add("Não foi possivel deletar o vínculo selecionado.");
        } finally {
            for (String e : errors) {
                addFacesMsg(e, FacesMessage.SEVERITY_ERROR);
            }
        }
    }

    @Override
    public void add() {
    }

    public void add(String produto, SinergiaGrupo grupo) {

        SinergiaDAO sDao = new SinergiaDAO();
        List<Sinergia> sins;
        sins = sDao.findAllByProduto(produto);
        boolean ok = true;
        if (sins != null) {
            for (Sinergia aux : sins) {
                try {
                    aux.setGrupo(grupo);
                    sDao.update(aux);
                } catch (HibernateException e) {
                    ok = false;
                }
            }
            if (ok) {
                addFacesMsg("Produto '" + produto + "' foi vinculado ao grupo " + grupo.getDescricao() + " com sucesso.", FacesMessage.SEVERITY_INFO);
            } else {
                addFacesMsg("Erro ao vincular produto " + produto + " ao grupo " + grupo.getDescricao(), FacesMessage.SEVERITY_ERROR);
            }
        }
    }
}
