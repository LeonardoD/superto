/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.sinergia.dao;

import br.com.bb.superto.dao.GenericHibernateDAO;
import br.com.bb.superto.sinergia.model.Sinergia;
import br.com.bb.superto.sinergia.model.SinergiaGrupo;
import br.com.bb.superto.sinergia.model.SinergiaId;
import br.com.bb.superto.sinergia.model.Vinculo;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Administrador
 */
public class SinergiaDAO extends GenericHibernateDAO<Sinergia, Serializable> {

    public Sinergia findById(SinergiaId sId) {
        Sinergia sinergia = null;
        try {
            Criteria criteria = getSession().createCriteria(getPersistentClass());
            criteria.add(Restrictions.eq("prefixo", sId.getPrefixo()));
            criteria.add(Restrictions.eq("referencia", sId.getRefencia()));
            criteria.add(Restrictions.eq("produto", sId.getProduto()));
            sinergia = (Sinergia) criteria.uniqueResult();

            if (sinergia == null) {
                return null;
            }
        } catch (HibernateException ex) {
            System.out.println("Nao Deu certo a procura do registro  - Portal Sinergia!!!\n" + ex.getMessage());
        }
        return sinergia;
    }

    public Sinergia findByProduto(String pProduto) {
        Sinergia sinergia = null;
        try {
            Query q = getSession().createQuery("Select a from Sinergia a where a.sinergiaId.produto=:produto LIMIT 1");
            q.setParameter("produto", pProduto);
            sinergia = (Sinergia) q.uniqueResult();
        } catch (HibernateException e) {
            System.out.println("Nao Deu certo a procura de produto já existente - Portal Sinergia!\n" + e.getMessage());
        }

        if (sinergia != null) {
            return sinergia;
        }
        return null;
    }

    public List<Sinergia> findAllByProduto(String produto) {
        List<Sinergia> produtos = null;
        try {
            Query q = getSession().createQuery("Select a from Sinergia a where a.sinergiaId.produto=:produto");
            q.setParameter("produto", produto);
            produtos = (List<Sinergia>) q.list();
        } catch (HibernateException e) {
            System.out.println("Nao Deu certo a procura de todos os produtos - Portal Sinergia!\n" + e.getMessage());
        }
        return produtos;
    }

    public List<Integer> findAllPrefixos() {
        List<Integer> prefixos = null;
        try {
            prefixos = getSession().createQuery("Select distinct(a.sinergiaId.prefixo) from Sinergia a").list();
        } catch (HibernateException e) {
            System.out.println("Nao Deu certo a procura de todos os prefixos- Portal Sinergia!\n" + e.getMessage());
        }
        return prefixos;
    }

    public List<String> findAllReferencias() {
        List<String> referencias = null;
        try {
            referencias = getSession().createQuery("Select distinct(a.sinergiaId.refencia) from Sinergia a").list();
        } catch (HibernateException e) {
            System.out.println("Nao Deu certo a procura de todas as referencias - Portal Sinergia!\n" + e.getMessage());
        }
        return referencias;
    }

    public List<SinergiaGrupo> findAllGruposSinergia(String ref1, String ref2) {
        List<SinergiaGrupo> grupos = new ArrayList<>();
        try {
            Query q = getSession().createSQLQuery("SELECT a.grupo_id FROM sinergia a where (a.refencia=:firstRef OR a.refencia=:secondRef) and (a.grupo_id IS NOT NULL) GROUP BY a.grupo_id, a.refencia ORDER BY a.grupo_id");
            q.setParameter("firstRef", ref1);
            q.setParameter("secondRef", ref2);
            if (q.list().size() > 0) {
                for (int i = 0; i < q.list().size(); i++) {
                    SinergiaGrupo grp = new SinergiaGrupoDAO().findGroupByID((Integer) q.list().get(i));
                    grupos.add(grp);
                }
            }
        } catch (HibernateException e) {
            System.out.println("Nao Deu certo a procura de todos os grupos do sinergia - Portal Sinergia - SinergiaDAO!" + e.getMessage());
        }
        return grupos;
    }

    public List<String> findProdutosSemGrupo() {
        List<String> produtos = null;
        try {
            produtos = getSession().createQuery("Select distinct(a.sinergiaId.produto) from Sinergia a where a.grupo IS NULL").list();
        } catch (HibernateException e) {
            System.out.println("Nao Deu certo a procura de produtos sem grupo vinculado - Portal Sinergia - SinergiaDAO!" + e.getMessage());
        }
        return produtos;
    }

    /*Procura Sinergias com grupo vinculado*/
    public List<Vinculo> findVinculos() {
        List<Vinculo> vinculos = new ArrayList<>();
        List<Object> produtos = null;
        Object[] vetObj;
        try {
            Query q = getSession().createQuery("Select distinct(a.sinergiaId.produto), a.grupo from Sinergia a where a.grupo IS NOT NULL");

            for (int i = 0; i < q.list().size(); i++) {
                Vinculo v = new Vinculo();
                vetObj = (Object[]) q.list().get(i);
                v.setProduto((String) vetObj[0]);
                v.setGrupo((SinergiaGrupo) vetObj[1]);
                vinculos.add(v);
            }

            if (vinculos == null) {
                return null;
            }
        } catch (HibernateException ex) {
            System.out.println("Nao Deu certo a procura de produtos com vinculos - Portal Sinergia - SinergiaDAO!");
            System.out.println(ex.getMessage());
        }
        return vinculos;
    }

    public List<Sinergia> findFirstGrafico(String ref1, String ref2, SinergiaGrupo grupo) {
        List<Sinergia> s = null;
        try {
            Query q1 = getSession().createQuery("Select a from Sinergia a where (a.sinergiaId.refencia=:ref1 OR a.sinergiaId.refencia=:ref2) "
                    + "AND a.grupo=:grupo "
                    + "AND (a.sinergiaId.prefixo=8517 OR a.sinergiaId.prefixo=8592 OR a.sinergiaId.prefixo=8166) ORDER BY a.sinergiaId.prefixo");
            q1.setParameter("ref1", ref1);
            q1.setParameter("ref2", ref2);
            q1.setParameter("grupo", grupo);
            s = q1.list();
            if (s.size() < 6) {
                s = null; // Se não encontrou 2 sinergia por prefixo então não gera gráfico
            }
        } catch (HibernateException e) {
            System.out.println("Erro ao na busca do primeiro gráfico - Portal Sinergia - SinergiaDAO!");
        }
        return s;
    }
}
