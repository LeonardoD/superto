package br.com.bb.superto.sinergia.controller;

import br.com.bb.superto.controller.GenericController;
import br.com.bb.superto.sinergia.dao.SinergiaGrupoDAO;
import br.com.bb.superto.sinergia.model.SinergiaGrupo;
import com.csvreader.CsvReader;
import org.hibernate.HibernateException;
import org.hibernate.exception.ConstraintViolationException;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

import javax.annotation.PostConstruct;
import javax.el.ELException;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.io.IOException;
import java.io.Serializable;
import java.nio.charset.Charset;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Administrador
 */
@ManagedBean(name = "sinergiaGrpController")
@ViewScoped
public class SinergiaGrupoController extends GenericController<SinergiaGrupo> implements Serializable {

    private UploadedFile file;
    private SinergiaGrupo grupo;
    private List<SinergiaGrupo> listGrupos;
    private List<SinergiaGrupo> listGruposImportados;

    @PostConstruct
    public void init() {
        grupo = new SinergiaGrupo();
        selected = new SinergiaGrupo();
        listGrupos = new ArrayList<>();
        listGruposImportados = new ArrayList<>();
    }

    public SinergiaGrupo getGrupo() {
        return grupo;
    }

    public void setGrupo(SinergiaGrupo grupo) {
        this.grupo = grupo;
    }

    public List<SinergiaGrupo> getListGrupos() {
        listGrupos = new SinergiaGrupoDAO().findAll();
        Collections.sort(listGrupos, new Comparator() {
            @Override
            public int compare(Object o1, Object o2) {
                SinergiaGrupo s1 = (SinergiaGrupo) o1;
                SinergiaGrupo s2 = (SinergiaGrupo) o2;
                return s1.getId() < s2.getId() ? -1 : (s1.getId() > s2.getId() ? +1 : 0);
            }
        });
        if (listGrupos == null || listGrupos.isEmpty()) {
            return null;
        }
        return listGrupos;
    }

    public List<SinergiaGrupo> getListGruposImportados() {
        if (this.listGruposImportados == null || this.listGruposImportados.isEmpty()) {
            return null;
        }
        return this.listGruposImportados;
    }

    public void setListGruposImportados(List<SinergiaGrupo> listGruposImportados) {
        this.listGruposImportados = listGruposImportados;
    }

    public UploadedFile getFile() {
        return file;
    }

    public void setFile(UploadedFile file) {
        this.file = file;
    }

    public void fileUploadListener(FileUploadEvent event) {
        file = event.getFile();
        lerArq(); // Lê o arquivo csv que acabou de ser importado
    }

    public void lerArq() {
        try {
            int cont = 0;
            ArrayList<String> grupos = new ArrayList<>();
            listGruposImportados.clear();
            CsvReader csvReader = new CsvReader(file.getInputstream(), Charset.defaultCharset());
            csvReader.setDelimiter(';');
            while (csvReader.readRecord()) {
                System.out.println("[PORTAL SINERGIA] - Lendo linha " + (cont++) + " do arquivo: " + file.getFileName());
                grupos.add(csvReader.get(0));
            }
            HashSet hs = new HashSet();  /* Retira registros duplicados do Array com a descrição dos grupos */

            hs.addAll(grupos);           // 
            grupos.clear();             //
            grupos.addAll(hs);          //

            for (String a : grupos) {
                SinergiaGrupo sExistente;
                SinergiaGrupo grpAux = new SinergiaGrupo();

                grpAux.setDescricao(a);

                sExistente = new SinergiaGrupoDAO().findGroupByDesc(grpAux.getDescricao());
                if (sExistente != null) {
                    System.out.println("Grupo " + grpAux.getDescricao() + " já existe e foi ignorado");
                } else {
                    listGruposImportados.add(grpAux);
                }
            }

        } catch (IOException ex) {
            System.out.println("Erro ao importar grupos de arquivo csv.");
            Logger.getLogger(SinergiaController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void salvarGrupos() {
        try {
            if (!listGruposImportados.isEmpty() || listGruposImportados != null) {
                new SinergiaGrupoDAO().saveList(listGruposImportados);
                addFacesMsg("Grupos Inseridos com Sucesso", FacesMessage.SEVERITY_INFO);
                listGruposImportados = new ArrayList<>();
            }
        } catch (HibernateException e) {
            System.out.println("Erro ao Salvar Grupos de arquivo csv.");
            addFacesMsg("Erro ao Salvar Grupos de arquivo csv!", FacesMessage.SEVERITY_ERROR);
        } finally {
            RequestContext.getCurrentInstance().update("tableRegistros");
            RequestContext.getCurrentInstance().update("grupos");
        }
    }

    public void cancelarImportacao() {
        listGruposImportados = new ArrayList<>();
        RequestContext.getCurrentInstance().update("tableRegistros");
    }

    @Override
    public void edit() {
        List<String> errors = new ArrayList<>();
        try {
            if (selected == null) {
                throw new NullPointerException();
            }
            new SinergiaGrupoDAO().update(selected);
            addFacesMsg("Grupo '" + selected.getDescricao() + "' editado com sucesso.", FacesMessage.SEVERITY_INFO);
            selected = null;
            RequestContext.getCurrentInstance().execute("PF('editarDlg').hide()");
        } catch (NullPointerException ex) {
            errors.add("Selecione um grupo!");
        } finally {
            for (String e : errors) {
                addFacesMsg(e, FacesMessage.SEVERITY_ERROR);
            }
        }
    }

    @Override
    public void delete() {
        List<String> errors = new ArrayList<>();
        try {
            if (selected == null) {
                throw new NullPointerException();
            }
            selected = new SinergiaGrupoDAO().makeTransient(selected);
            addFacesMsg("Grupo '" + selected.getDescricao() + "' removido com sucesso.", FacesMessage.SEVERITY_INFO);
            selected = null;
        } catch (NullPointerException ex) {
            errors.add("Grupo não selecionado.");
        } catch (ConstraintViolationException ex) {
            errors.add("Não foi possivel deletar o grupo selecionado.");
        } finally {
            for (String e : errors) {
                addFacesMsg(e, FacesMessage.SEVERITY_ERROR);
            }
        }
    }

    @Override
    public void add() {
    }

    public void add(SinergiaGrupo grupo) {
        try {
            SinergiaGrupo gExistente = new SinergiaGrupoDAO().findGroupByDesc(grupo.getDescricao());
            if (gExistente == null) {
                new SinergiaGrupoDAO().save(grupo);
                addFacesMsg("Grupo " + grupo.getDescricao() + " inserido com sucesso!", FacesMessage.SEVERITY_INFO);
                RequestContext.getCurrentInstance().execute("PF('addDialog').hide()");
            } else {
                addFacesMsg("Grupo " + grupo.getDescricao() + " já existe!", FacesMessage.SEVERITY_ERROR);
                RequestContext.getCurrentInstance().execute("PF('addDialog').hide()");
            }
        } catch (HibernateException | ELException e) {
            addFacesMsg("Erro ao Incluir Grupo", FacesMessage.SEVERITY_ERROR);
        }
    }
}
