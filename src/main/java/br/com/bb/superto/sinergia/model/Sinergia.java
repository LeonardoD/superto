/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.sinergia.model;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author Administrador
 */
@Entity
@Table(name = "sinergia")
public class Sinergia implements Serializable {

    @EmbeddedId
    private SinergiaId sinergiaId;
    private String bloco;
    private Double orcado;
    private Double observado;

    @OneToOne
    @JoinColumn(nullable = true)
    private SinergiaGrupo grupo;

    public SinergiaId getSinergiaId() {
        return sinergiaId;
    }

    public void setSinergiaId(SinergiaId sinergiaId) {
        this.sinergiaId = sinergiaId;
    }

    public String getBloco() {
        return bloco;
    }

    public void setBloco(String bloco) {
        this.bloco = bloco;
    }

    public Double getOrcado() {
        return orcado;
    }

    public void setOrcado(Double orcado) {
        this.orcado = orcado;
    }

    public Double getObservado() {
        return observado;
    }

    public void setObservado(Double observado) {
        this.observado = observado;
    }

    public SinergiaGrupo getGrupo() {
        return grupo;
    }

    public void setGrupo(SinergiaGrupo grupo) {
        this.grupo = grupo;
    }
}
