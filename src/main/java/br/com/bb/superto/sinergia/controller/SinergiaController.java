package br.com.bb.superto.sinergia.controller;

import br.com.bb.superto.controller.GenericController;
import br.com.bb.superto.sinergia.dao.SinergiaDAO;
import br.com.bb.superto.sinergia.dao.SinergiaGrupoDAO;
import br.com.bb.superto.sinergia.model.Sinergia;
import br.com.bb.superto.sinergia.model.SinergiaGrupo;
import br.com.bb.superto.sinergia.model.SinergiaId;
import br.com.bb.superto.util.Util;
import com.csvreader.CsvReader;
import org.hibernate.NonUniqueObjectException;
import org.hibernate.exception.ConstraintViolationException;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.io.IOException;
import java.io.Serializable;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Administrador
 */
@ManagedBean(name = "sinergiaController")
@ViewScoped
public class SinergiaController extends GenericController<Sinergia> implements Serializable {

    private UploadedFile file;
    private ArrayList<Sinergia> listSinergia;
    private ArrayList<SinergiaGrupo> listGrupos;
    private ArrayList<SinergiaGrupo> listGruposFiltrados;
    private ArrayList<Sinergia> listSemGrupo;
    private Boolean mostraBotaoSalvar;

    @PostConstruct
    public void init() {
        selected = new Sinergia();
        listSinergia = new ArrayList<>();
        listSemGrupo = new ArrayList<>();
    }

    public ArrayList<SinergiaGrupo> getListGrupos() {
        listGrupos = (ArrayList) new SinergiaGrupoDAO().findAll();

        if (listGrupos == null || listGrupos.isEmpty()) {
            return null;
        }

        return listGrupos;
    }

    public ArrayList<SinergiaGrupo> getListGruposFiltrados() {
        return listGruposFiltrados;
    }

    public void setListGruposFiltrados(ArrayList<SinergiaGrupo> listGruposFiltrados) {
        this.listGruposFiltrados = listGruposFiltrados;
    }

    public Boolean getMostraBotaoSalvar() {
        return mostraBotaoSalvar;
    }

    public void setMostraBotaoSalvar(Boolean mostraBotaoSalvar) {
        this.mostraBotaoSalvar = mostraBotaoSalvar;
    }

    public ArrayList<Sinergia> getListSinergia() {
        return listSinergia;
    }

    public void fileUploadListener(FileUploadEvent event) {
        System.out.println("[PORTAL SINERGIA] - Entrou no FileUploadListner");
        file = event.getFile();
        lerArq(); // LÃª o arquivo csv que acabou de ser importado
    }

    public void lerArq() {
        try {
            int cont = 0;
            String sVerifica = "";
            CsvReader csvReader = new CsvReader(file.getInputstream(), Charset.defaultCharset());
            csvReader.setDelimiter(';');
            while (csvReader.readRecord()) {
                System.out.println("[PORTAL SINERGIA] - Lendo linha " + (cont++) + " do arquivo: " + file.getFileName());
                Sinergia sinergia = new Sinergia();
                Sinergia sExistente;
                SinergiaGrupo grupo = new SinergiaGrupo();

                sinergia.setSinergiaId(new SinergiaId(Integer.parseInt(csvReader.get(0)),
                        csvReader.get(1), csvReader.get(2)));

//                sinergia.setBloco(csvReader.get(3));  Informação ainda não vem no arquivo mais poderá vir.
                if (csvReader.get(3).trim().isEmpty() || csvReader.get(3).contains("-")) {
                    sinergia.setOrcado(0.0d);
                } else {
                    sinergia.setOrcado(Util.retirarMascaraDinheiro(csvReader.get(3)));
                }
                if (csvReader.get(4).isEmpty() || csvReader.get(4).contains("-")) {
                    sinergia.setObservado(0.0d);
                } else {
                    sinergia.setObservado(Util.retirarMascaraDinheiro(csvReader.get(4)));
                }

                sExistente = new SinergiaDAO().findByProduto(sinergia.getSinergiaId().getProduto());
                if (sExistente != null && sExistente.getGrupo() != null) {
                    sinergia.setGrupo(sExistente.getGrupo());
                } else if ((sExistente != null && sExistente.getGrupo() == null) || sExistente == null) {
                    listSemGrupo.add(sinergia);
                }

                listSinergia.add(sinergia);

            }
        } catch (IOException ex) {
            Logger.getLogger(SinergiaController.class.getName()).log(Level.SEVERE, null, ex);
            addFacesMsg("Erro ao analisar Arquivo!", FacesMessage.SEVERITY_ERROR);
        } catch (NumberFormatException ex) {
            addFacesMsg("Erro ao analisar Arquivo! Layout do arquivo pode estar incorreto!", FacesMessage.SEVERITY_ERROR);
            RequestContext.getCurrentInstance().update("msg");
        }
    }

    public void save() {
        SinergiaDAO sDao = new SinergiaDAO();
        int cont = 0;

        if (listSinergia != null && listSinergia.size() > 0) {
            for (Sinergia a : listSinergia) {
                try {
                    sDao.save(a);
                } catch (ConstraintViolationException e) {
                    addFacesMsg("Arquivo já foi importado!", FacesMessage.SEVERITY_ERROR);
                } catch (NonUniqueObjectException e) {
//                    System.out.println(e.getMessage());
                    cont++;
                }
            }
            listSinergia = new ArrayList<>();
            if (cont > 0) {
                addFacesMsg("Foi encontrado " + String.valueOf(cont) + " registros duplicados! Todos foram ignorados.", FacesMessage.SEVERITY_ERROR);
            }
        } else {
            addFacesMsg("Primeiro faça o Upload de algum Arquivo!", FacesMessage.SEVERITY_WARN);
        }
//        RequestContext.getCurrentInstance().update("tableRegistros");
//        RequestContext.getCurrentInstance().update("msg");
    }

    @Override
    public void edit() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void delete() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void add() {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}
