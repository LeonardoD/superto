/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.sinergia.controller;

import br.com.bb.superto.controller.GenericController;
import br.com.bb.superto.sinergia.dao.SinergiaDAO;
import br.com.bb.superto.sinergia.model.Referencia;
import br.com.bb.superto.sinergia.model.Sinergia;
import br.com.bb.superto.sinergia.model.SinergiaGrupo;
import org.primefaces.model.chart.BarChartModel;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ValueChangeEvent;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Administrador
 */
@ManagedBean(name = "sinergiaGrafController")
@ViewScoped
public class SinergiaGraficosController extends GenericController<Sinergia> implements Serializable {

    private Integer firstPrefix;
    private Integer secondPrefix;
    private List<Integer> prefixos;
    private String firstRef;
    private String secondRef;
    private List<Referencia> referencias;
    private String valor1;
    private String valor2;
    private SinergiaGrupo grupo;
    private List<SinergiaGrupo> vrGruposSinergia;
    private BarChartModel firstBarModel;
    private Boolean mostraGrafico;
    private String valorSuperTo;
    private String valorDired;
    private String valorBrasil;
    private String tipoGrafico;

    public String getValorSuperTo() {
        return valorSuperTo;
    }

    public void setValorSuperTo(String valorSuperTo) {
        this.valorSuperTo = valorSuperTo;
    }

    public String getValorDired() {
        return valorDired;
    }

    public void setValorDired(String valorDired) {
        this.valorDired = valorDired;
    }

    public String getValorBrasil() {
        return valorBrasil;
    }

    public void setValorBrasil(String valorBrasil) {
        this.valorBrasil = valorBrasil;
    }

    public String getTipoGrafico() {
        return tipoGrafico;
    }

    public void setTipoGrafico(String tipoGrafico) {
        this.tipoGrafico = tipoGrafico;
    }

    public BarChartModel getBarModel() {
        return firstBarModel;
    }

    public Boolean getMostraGrafico() {
        return mostraGrafico;
    }

    public void setMostraGrafico(Boolean mostraGrafico) {
        this.mostraGrafico = mostraGrafico;
    }

    public String getFirstRef() {
        return firstRef;
    }

    public void setFirstRef(String firstRef) {
        this.firstRef = firstRef;
        try {
            atualizaProdutos();
        } catch (ParseException ex) {
            Logger.getLogger(SinergiaGraficosController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public String getSecondRef() {
        return secondRef;
    }

    public void setSecondRef(String secondRef) {
        this.secondRef = secondRef;
    }

    public Integer getFirstPrefix() {
        return firstPrefix;
    }

    public void setFirstPrefix(Integer firstPrefix) {
        this.firstPrefix = firstPrefix;
    }

    public Integer getSecondPrefix() {
        return secondPrefix;
    }

    public void setSecondPrefix(Integer secondPrefix) {
        this.secondPrefix = secondPrefix;
    }

    public List<Integer> getPrefixos() {
        prefixos = new SinergiaDAO().findAllPrefixos();
        return prefixos;
    }

    public void setPrefixos(List<Integer> prefixos) {
        this.prefixos = prefixos;
    }

    public List<Referencia> getReferencias() throws ParseException {
        return referencias;
    }

    public void setReferencias(List<Referencia> referencias) {
        this.referencias = referencias;
    }

    public String getValor1() {
        return valor1;
    }

    public void setValor1(String valor1) {
        this.valor1 = valor1;
    }

    public String getValor2() {
        return valor2;
    }

    public void setValor2(String valor2) {
        this.valor2 = valor2;
    }

    public SinergiaGrupo getGrupo() {
        return grupo;
    }

    public void setGrupo(SinergiaGrupo grupo) {
        this.grupo = grupo;
    }

    public List<SinergiaGrupo> getVrGruposSinergia() {
        return vrGruposSinergia;
    }

    public void setVrGruposSinergia(List<SinergiaGrupo> vrGruposSinergia) {
        this.vrGruposSinergia = vrGruposSinergia;
    }

    @PostConstruct
    public void init() {
        referencias = new ArrayList<>();
        vrGruposSinergia = new ArrayList<>();
        mostraGrafico = false;
        try {
            atualizaReferencias();
        } catch (ParseException ex) {
            Logger.getLogger(SinergiaGraficosController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void atualizaReferencias() throws ParseException {
        List<String> sRefs = new SinergiaDAO().findAllReferencias();

        if (sRefs != null) {
            Date data;
            Referencia ref;
            DateFormat f = DateFormat.getDateInstance(DateFormat.LONG);
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            if (referencias.size() > 0) {
                referencias = new ArrayList<>();
            }
            for (String a : sRefs) {
                data = sdf.parse(a);
                String aux = f.format(data);
                referencias.add(new Referencia(a, aux.substring(5, aux.length())));
            }
        }
    }

    public void atualizaProdutos(ValueChangeEvent event) throws ParseException {
        List<SinergiaGrupo> vrGrupos;
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

        if (firstRef != null && secondRef != null) {

            Date date1 = sdf.parse(firstRef);
            Date date2 = sdf.parse(secondRef);

            if (date1.before(date2)) {
                if (vrGruposSinergia.size() > 0) {
                    vrGruposSinergia = new ArrayList<>();
                }
                vrGrupos = new SinergiaDAO().findAllGruposSinergia(getFirstRef(), getSecondRef());
                if (firstRef.equalsIgnoreCase(secondRef)) {
                    for (SinergiaGrupo grp : vrGrupos) {
                        vrGruposSinergia.add(grp);
                    }
                } else {
                    if (vrGrupos.size() == 2 && vrGrupos.get(0).getId().compareTo(vrGrupos.get(1).getId()) == 0) {
                        vrGruposSinergia.add(vrGrupos.get(0));
                    } else {
                        for (int i = 1; i < vrGrupos.size(); i++) {
                            if (vrGrupos.get(i - 1).equals(vrGrupos.get(i))) {
                                vrGruposSinergia.add(vrGrupos.get(i));
                            }
                        }
                    }
                }
                if (vrGrupos.isEmpty()) {
                    addFacesMsg("Não foi econtrado produtos que tenham as referências informadas", FacesMessage.SEVERITY_WARN);
                }
            } else {
                addFacesMsg("Segundo periodo deve ser maior do que o Primeiro periodo!", FacesMessage.SEVERITY_WARN);
            }
        }
    }

    public void atualizaProdutos() throws ParseException {
        List<SinergiaGrupo> vrGrupos;
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

        Date date1 = sdf.parse(firstRef);
        Date date2 = sdf.parse(secondRef);

        if (firstRef != null && secondRef != null) {

            if (date1.before(date2)) {
                if (vrGruposSinergia.size() > 0) {
                    vrGruposSinergia = new ArrayList<>();
                }
                vrGrupos = new SinergiaDAO().findAllGruposSinergia(getFirstRef(), getSecondRef());
                if (firstRef.equalsIgnoreCase(secondRef)) {
                    for (SinergiaGrupo grp : vrGrupos) {
                        vrGruposSinergia.add(grp);
                    }
                } else {
                    for (int i = 1; i < vrGrupos.size(); i++) {
                        if (vrGrupos.get(i - 1).equals(vrGrupos.get(i))) {
                            vrGruposSinergia.add(vrGrupos.get(i));
                        }
                    }
                }
                if (vrGrupos.isEmpty()) {
                    addFacesMsg("Não foi econtrado produtos que tenham as referências informadas", FacesMessage.SEVERITY_WARN);
                }
            } else {
                addFacesMsg("Segundo periodo deve ser maior do que o Primeiro periodo!", FacesMessage.SEVERITY_WARN);
            }
        }
    }

    public void atualizaRef1(ValueChangeEvent event) {
        if (event != null) {
            setFirstRef(event.getNewValue().toString());
            if (firstRef != null || !firstRef.isEmpty()) {
                try {
                    atualizaProdutos();
                } catch (ParseException ex) {
                    Logger.getLogger(SinergiaGraficosController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

    }

    public void geraGrafico() throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Date date1 = sdf.parse(firstRef);
        Date date2 = sdf.parse(secondRef);
        if (date1.before(date2)) {
            initFirstModel();
            mostraGrafico = true;
        } else {
            addFacesMsg("Segundo periodo deve ser maior do que o Primeiro periodo!", FacesMessage.SEVERITY_WARN);
            mostraGrafico = false;
        }
    }

    private void initFirstModel() {
        Double valS1 = 0d; // valor superto 1
        Double valS2 = 0d; // valor superto 2
        Double valD1 = 0d; // valor dired 1
        Double valD2 = 0d; // valor dired 2
        Double valB1 = 0d; // valor brasil 1
        Double valB2 = 0d; // valor brasil 2

        List<Sinergia> sinergias = new SinergiaDAO().findFirstGrafico(firstRef, secondRef, grupo);
        if (sinergias != null) {
            if (valor1.equalsIgnoreCase("orc") && valor2.equalsIgnoreCase("obs")) {
                for (Sinergia a : sinergias) {
                    if (a.getSinergiaId().getPrefixo() == 8517 && firstRef.equalsIgnoreCase(a.getSinergiaId().getRefencia())) {
                        valS1 = a.getOrcado();
                    } else if (a.getSinergiaId().getPrefixo() == 8517 && secondRef.equalsIgnoreCase(a.getSinergiaId().getRefencia())) {
                        valS2 = a.getObservado();
                    } else if (a.getSinergiaId().getPrefixo() == 8592 && firstRef.equalsIgnoreCase(a.getSinergiaId().getRefencia())) {
                        valD1 = a.getOrcado();
                    } else if (a.getSinergiaId().getPrefixo() == 8592 && secondRef.equalsIgnoreCase(a.getSinergiaId().getRefencia())) {
                        valD2 = a.getObservado();
                    } else if (a.getSinergiaId().getPrefixo() == 8166 && firstRef.equalsIgnoreCase(a.getSinergiaId().getRefencia())) {
                        valB1 = a.getOrcado();
                    } else if (a.getSinergiaId().getPrefixo() == 8166 && secondRef.equalsIgnoreCase(a.getSinergiaId().getRefencia())) {
                        valB2 = a.getObservado();
                    }
                }

                valorSuperTo = "[" + String.valueOf(valS2 / valS1 - 1) + "]";
                valorDired = "[" + String.valueOf(valD2 / valD1 - 1) + "]";
                valorBrasil = "[" + String.valueOf(valB2 / valB1 - 1) + "]";
                tipoGrafico = "['Orçado x Observado']";

            } else if (valor1.equalsIgnoreCase("obs") && valor2.equalsIgnoreCase("orc")) {

                for (Sinergia a : sinergias) {
                    if (a.getSinergiaId().getPrefixo() == 8517 && firstRef.equalsIgnoreCase(a.getSinergiaId().getRefencia())) {
                        valS1 = a.getObservado();
                    } else if (a.getSinergiaId().getPrefixo() == 8517 && secondRef.equalsIgnoreCase(a.getSinergiaId().getRefencia())) {
                        valS2 = a.getOrcado();
                    } else if (a.getSinergiaId().getPrefixo() == 8592 && firstRef.equalsIgnoreCase(a.getSinergiaId().getRefencia())) {
                        valD1 = a.getObservado();
                    } else if (a.getSinergiaId().getPrefixo() == 8592 && secondRef.equalsIgnoreCase(a.getSinergiaId().getRefencia())) {
                        valD2 = a.getOrcado();
                    } else if (a.getSinergiaId().getPrefixo() == 8166 && firstRef.equalsIgnoreCase(a.getSinergiaId().getRefencia())) {
                        valB1 = a.getObservado();
                    } else if (a.getSinergiaId().getPrefixo() == 8166 && secondRef.equalsIgnoreCase(a.getSinergiaId().getRefencia())) {
                        valB2 = a.getOrcado();
                    }
                }
                valorSuperTo = "[" + String.valueOf(valS2 / valS1 - 1) + "]";
                valorDired = "[" + String.valueOf(valD2 / valD1 - 1) + "]";
                valorBrasil = "[" + String.valueOf(valB2 / valB1 - 1) + "]";
                tipoGrafico = "['Observado x Orçado']";

            } else if (valor1.equalsIgnoreCase("orc") && valor2.equalsIgnoreCase("orc")) {

                for (Sinergia a : sinergias) {
                    if (a.getSinergiaId().getPrefixo() == 8517 && firstRef.equalsIgnoreCase(a.getSinergiaId().getRefencia())) {
                        valS1 = a.getOrcado();
                    } else if (a.getSinergiaId().getPrefixo() == 8517 && secondRef.equalsIgnoreCase(a.getSinergiaId().getRefencia())) {
                        valS2 = a.getOrcado();
                    } else if (a.getSinergiaId().getPrefixo() == 8592 && firstRef.equalsIgnoreCase(a.getSinergiaId().getRefencia())) {
                        valD1 = a.getOrcado();
                    } else if (a.getSinergiaId().getPrefixo() == 8592 && secondRef.equalsIgnoreCase(a.getSinergiaId().getRefencia())) {
                        valD2 = a.getOrcado();
                    } else if (a.getSinergiaId().getPrefixo() == 8166 && firstRef.equalsIgnoreCase(a.getSinergiaId().getRefencia())) {
                        valB1 = a.getOrcado();
                    } else if (a.getSinergiaId().getPrefixo() == 8166 && secondRef.equalsIgnoreCase(a.getSinergiaId().getRefencia())) {
                        valB2 = a.getOrcado();
                    }
                }
                valorSuperTo = "[" + String.valueOf(valS2 / valS1 - 1) + "]";
                valorDired = "[" + String.valueOf(valD2 / valD1 - 1) + "]";
                valorBrasil = "[" + String.valueOf(valB2 / valB1 - 1) + "]";
                tipoGrafico = "['Orçado x Orçado']";

            } else if (valor1.equalsIgnoreCase("obs") && valor2.equalsIgnoreCase("obs")) {

                for (Sinergia a : sinergias) {
                    if (a.getSinergiaId().getPrefixo() == 8517 && firstRef.equalsIgnoreCase(a.getSinergiaId().getRefencia())) {
                        valS1 = a.getObservado();
                    } else if (a.getSinergiaId().getPrefixo() == 8517 && secondRef.equalsIgnoreCase(a.getSinergiaId().getRefencia())) {
                        valS2 = a.getObservado();
                    } else if (a.getSinergiaId().getPrefixo() == 8592 && firstRef.equalsIgnoreCase(a.getSinergiaId().getRefencia())) {
                        valD1 = a.getObservado();
                    } else if (a.getSinergiaId().getPrefixo() == 8592 && secondRef.equalsIgnoreCase(a.getSinergiaId().getRefencia())) {
                        valD2 = a.getObservado();
                    } else if (a.getSinergiaId().getPrefixo() == 8166 && firstRef.equalsIgnoreCase(a.getSinergiaId().getRefencia())) {
                        valB1 = a.getObservado();
                    } else if (a.getSinergiaId().getPrefixo() == 8166 && secondRef.equalsIgnoreCase(a.getSinergiaId().getRefencia())) {
                        valB2 = a.getObservado();
                    }
                }
                valorSuperTo = "[" + String.valueOf(valS2 / valS1 - 1) + "]";
                valorDired = "[" + String.valueOf(valD2 / valD1 - 1) + "]";
                valorBrasil = "[" + String.valueOf(valB2 / valB1 - 1) + "]";
                tipoGrafico = "['Observado x Observado']";

            }
        } else {
            addFacesMsg("Não Foi possível gerar o gráfico, algum produto informado não tem sinergia nos períodos informados!", FacesMessage.SEVERITY_WARN);
        }

    }

    @Override
    public void edit() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void delete() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void add() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
