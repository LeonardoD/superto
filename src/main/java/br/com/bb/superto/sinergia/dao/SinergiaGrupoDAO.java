/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.sinergia.dao;

import br.com.bb.superto.dao.GenericHibernateDAO;
import br.com.bb.superto.sinergia.model.SinergiaGrupo;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Restrictions;

import java.io.Serializable;

/**
 * @author Administrador
 */
public class SinergiaGrupoDAO extends GenericHibernateDAO<SinergiaGrupo, Serializable> {

    /*Procura grupo pela Descriçao*/
    public SinergiaGrupo findGroupByDesc(String descricao) {
        SinergiaGrupo sinergiaGrp = null;
        try {
            Criteria criteria = getSession().createCriteria(getPersistentClass());
            criteria.add(Restrictions.eq("descricao", descricao));
            sinergiaGrp = (SinergiaGrupo) criteria.uniqueResult();

            if (sinergiaGrp == null) {
                return null;
            }
        } catch (HibernateException ex) {
            System.out.println("Nao Deu certo a procura de grupo por produto - Portal Sinergia!!!");
        }
        return sinergiaGrp;
    }

    public SinergiaGrupo findGroupByID(Integer id) {
        SinergiaGrupo grupo = null;
        try {
            Criteria criteria = getSession().createCriteria(getPersistentClass());
            criteria.add(Restrictions.eq("id", id));
            grupo = (SinergiaGrupo) criteria.uniqueResult();

            if (grupo == null) {
                return null;
            }
        } catch (HibernateException ex) {
            System.out.println("Nao Deu certo a procura de grupo por id - Portal Sinergia!!!");
        }
        return grupo;
    }

}
