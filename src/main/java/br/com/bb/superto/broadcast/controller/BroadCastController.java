/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.broadcast.controller;

import br.com.bb.superto.broadcast.dao.BroadCastDao;
import br.com.bb.superto.broadcast.model.BroadCast;
import br.com.bb.superto.controller.GenericController;
import br.com.bb.superto.dao.OperacaoDAO;
import br.com.bb.superto.model.Operacao;
import org.primefaces.context.RequestContext;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Administrador
 */
@ManagedBean(name = "broadCastController")
@ViewScoped
public class BroadCastController extends GenericController<BroadCast> {

    private List<BroadCast> broadcasts;
    private boolean opeSucesso;

    public BroadCastController() {
        broadcasts = new ArrayList<>();
    }

    public List<BroadCast> getBroadcasts() {
        List<BroadCast> aux = new BroadCastDao().findBroadCast(15);
        return aux;
    }

    public void setBroadcasts(List<BroadCast> broadcasts) {
        this.broadcasts = broadcasts;
    }

    public boolean isOpeSucesso() {
        return opeSucesso;
    }

    public void setOpeSucesso(boolean opeSucesso) {
        this.opeSucesso = opeSucesso;
    }

    @Override
    public void edit() {
        Operacao operacao = getNovaOperacao();
        OperacaoDAO opDao = new OperacaoDAO();
        selected.getOperacaoModificacao().add(opDao.save(operacao));
        opeSucesso = new BroadCastDao().makePersistent(selected) != null;
        if (opeSucesso) {
            RequestContext.getCurrentInstance().update("painelTable");
        }
        RequestContext.getCurrentInstance().execute("verificaEdit();");
    }

    @Override
    public void delete() {
        opeSucesso = new BroadCastDao().makeTransient(selected) != null;
    }

    @Override
    public void add() {
        try {
            Operacao operacao = getNovaOperacao();
            OperacaoDAO opDao = new OperacaoDAO();
            value.setOperacaoModificacao(new ArrayList<>());
            value.getOperacaoModificacao().add(opDao.save(operacao));
            value.setOperacaoCriacao(opDao.save(operacao));
            opeSucesso = new BroadCastDao().save(value) != null;
            clearValue();
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }

}
