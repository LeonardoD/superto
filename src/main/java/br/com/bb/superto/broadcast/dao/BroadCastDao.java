/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.broadcast.dao;

import br.com.bb.superto.broadcast.model.BroadCast;
import br.com.bb.superto.dao.GenericHibernateDAO;
import org.hibernate.Query;

import java.io.Serializable;
import java.util.List;

/**
 * @author Administrador
 */
public class BroadCastDao extends GenericHibernateDAO<BroadCast, Serializable> {

    public List<BroadCast> findBroadCast(int size) {
        Query q = getSession().createQuery("SELECT a FROM BroadCast a JOIN FETCH a.operacaoModificacao b WHERE a.visivel = TRUE ORDER BY b.dataOperacao DESC");
        q.setMaxResults(size);
        List<BroadCast> aux = (List<BroadCast>) q.list();
        return aux;
    }

}
