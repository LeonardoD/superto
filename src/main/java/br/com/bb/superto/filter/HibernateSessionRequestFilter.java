package br.com.bb.superto.filter;

import br.com.bb.superto.dao.HibernateUtil;
import br.com.bb.superto.util.Util;
import org.apache.log4j.Logger;
import org.hibernate.SessionFactory;
import org.hibernate.StaleObjectStateException;
import org.hibernate.exception.ConstraintViolationException;

import javax.persistence.PersistenceException;
import javax.servlet.*;
import java.io.IOException;
import java.sql.BatchUpdateException;
import java.sql.SQLException;
import org.hibernate.HibernateException;

/**
 * Filtro que incia uma transação na sessão do hibernate quando uma pagina e
 * requisitada.
 *
 * @author Maycon Antonio Junqueira Costa
 * @version 1.0
 */
public class HibernateSessionRequestFilter implements Filter {

    // Objeto que salva os erros/exceptions no arquivo de log.
    static Logger logger = Logger.getLogger(HibernateSessionRequestFilter.class);
    // Instancia que guarda a fabrica de conexões.
    private SessionFactory sessionFactory;

    /**
     *
     * @param request
     * @param response
     * @param chain
     * @throws IOException
     * @throws ServletException
     */
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        try {
            // Session factory retorna a sessão atual e incia uma transação.
            sessionFactory.getCurrentSession().beginTransaction();
            chain.doFilter(request, response);
            // Após o termino da requisação, sicronizamos os dados da memoria com os dados
            // do banco de dados usando o commit.
            sessionFactory.getCurrentSession().getTransaction().commit();
            //} catch(org.postgresql.util.PSQLException eex){

        } catch (PersistenceException e) {
            System.out.println("Entramos");
            Throwable lastCause = e;
            String constraintName = null;
            while (lastCause != null) {
                if (lastCause.toString().startsWith("java.sql.BatchUpdateException")) {
                    BatchUpdateException bu = (BatchUpdateException) lastCause;
                    constraintName = Util.getViolatedConstraintNameExtracter().extractConstraintName(bu.getNextException());
                }
                lastCause = lastCause.getCause();
            }
            if (constraintName != null) {
                throw new ConstraintViolationException("Mensagem", new SQLException(), constraintName);
            }
        } catch (StaleObjectStateException staleEx) {
        } catch (HibernateException | IOException | ServletException ex) {
            System.out.println("-----------------------------------------");
            System.out.println(ex.getMessage());
            System.out.println("-----------------------------------------");
            try {
                // Se houve algum erro com o banco de dados, verifica se a transação está ativada,
                // caso esteja iniamos um roolback, para desfazer as ultimas modificações.
                if (sessionFactory.getCurrentSession().getTransaction().isActive()) {
                    sessionFactory.getCurrentSession().getTransaction().rollback();
                }
            } catch (Throwable rbEx) {
                //   logger.info(rbEx.getMessage(), rbEx);
            }
            //logger.info(ex.getMessage(), ex);
            //throw ex;
        } 
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        //Obtendo a fabrica de conexões do hibernate.
        sessionFactory = HibernateUtil.getSessionFactory();
    }

    @Override
    public void destroy() {
    }
}
