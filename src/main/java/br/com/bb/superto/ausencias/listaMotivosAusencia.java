/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.ausencias;

import java.util.Arrays;
import java.util.List;

/**
 *
 * @author Leonardo
 */
public class listaMotivosAusencia {
    private final static String[] brands;
     
    static {
 
        brands = new String[6];
        brands[0] = "Férias";
        brands[1] = "Treinamento";
        brands[2] = "Abono";
        brands[3] = "Saúde";
        brands[4] = "Evento";
        brands[5] = "Visita";
    }

   
    public static List<String> getBrands() {
        return Arrays.asList(brands);
    }
}