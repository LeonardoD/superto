/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.ausencias;



import br.com.bb.superto.model.GenericEntity;
import br.com.bb.superto.model.Operacao;
import com.google.gson.annotations.Expose;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;
import java.util.List;


/**
 * @author Leonardo
 */

@Entity
@Table(name = "ausencia")
public class Ausencia implements Serializable, GenericEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Expose
    private Integer ID;
        
    @NotNull(message = "Valor prefixo não pode estar vazio!")
    @Expose
    private Integer prefixoSolicitante;

    @NotEmpty(message = "Matricula do solicitante não pode estar vazia!")
    @Expose
    private String solicitanteMatricula;

    @NotEmpty(message = "Nome do solicitante não pode estar vazio!")
    @Expose
    private String solicitanteNome;

    @NotEmpty(message = "Valor numOperacao não pode estar vazio!")
    @Expose
    private String numOperacao;

    @NotNull(message = "Valor valorTotal não pode estar vazio!")
    @Expose
    private String chaveLateral;

    @Temporal(TemporalType.TIMESTAMP)
    private Date inicio;

    @Temporal(TemporalType.TIMESTAMP)
    private Date fim;
    
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataMudanca;
    
    @NotNull(message = "Valor prefixo não pode estar vazio!")
    @Expose
    private Integer prefixoLateral;

    @NotEmpty(message = "Motivo não pode estar vazio!")
    @Expose
    private String motivo;

    @NotEmpty(message = "Valor status não pode estar vazio!")
    @Expose
    private StatusAusencia statusAusencia;
    
    private String telefoneLateral;
    
    private String justificativa;
    
    private String parecer;
    
    private String nomeLateral;  
   
    
    @OneToOne
    @JoinColumn(name = "opcriacao_id")
    private Operacao operacaoCriacao;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @Fetch(FetchMode.SUBSELECT)
    private List<Operacao> operacaoModificacao;

    
    
    public Ausencia() {
        
    }

    @Override
    public Integer getId() {
        return ID;
    }

    public Date getDataMudanca() {
        return dataMudanca;
    }

    public void setDataMudanca(Date dataMudanca) {
        this.dataMudanca = dataMudanca;
    }

    public String getTelefoneLateral() {
        return telefoneLateral;
    }

    public void setTelefoneLateral(String telefoneLateral) {
        this.telefoneLateral = telefoneLateral;
    }
    
    public String getNomeLateral() {
        return nomeLateral;
    }

    public void setNomeLateral(String nomeLateral) {
        this.nomeLateral = nomeLateral;
    }

    public String getSolicitanteNome() {
        return solicitanteNome;
    }

    public void setSolicitanteNome(String nome) {
        this.solicitanteNome = nome;
    }

    public String getChaveLateral() {
        return chaveLateral;
    }

    public void setChaveLateral(String lateral) {
        this.chaveLateral = lateral;
    }

    public Date getInicio() {
        return inicio;
    }

    public void setInicio(Date inicio) {
        this.inicio = inicio;
    }

    public Date getFim() {
        return fim;
    }

    public void setFim(Date fim) {
        this.fim = fim;
    }

    public Integer getPrefixoLateral() {
        return prefixoLateral;
    }

    public void setPrefixoLateral(Integer prefixo) {
        this.prefixoLateral = prefixo;
    }

    public String getMotivo() {
        return motivo;
    }

    public void setMotivo(String motivo) {
        this.motivo = motivo;
    }

    public StatusAusencia getStatusAusencia() {
        return statusAusencia;
    }

    public void setStatusAusencia(StatusAusencia status) {
        this.statusAusencia = status;
    }

    public String getJustificativa() {
        return justificativa;
    }

    public void setJustificativa(String justificativa) {
        this.justificativa = justificativa;
    }

    public String getParecer() {
        return parecer;
    }

    public void setParecer(String parecer) {
        this.parecer = parecer;
    }

   
    public void setID(Integer ID) {
        this.ID = ID;
    }

    public String getNumOperacao() {
        return numOperacao;
    }

    public void setNumOperacao(String numOperacao) {
        this.numOperacao = numOperacao;
    }

    public Integer getPrefixoSolicitante() {
        return prefixoSolicitante;
    }

    public void setPrefixoSolicitante(Integer prefixo) {
        this.prefixoSolicitante = prefixo;
    }

    public String getSolicitanteMatricula() {
        return solicitanteMatricula;
    }

    public void setSolicitanteMatricula(String matricula) {
        this.solicitanteMatricula = matricula;
    }

    public Operacao getOperacaoCriacao() {
        return operacaoCriacao;
    }

    public void setOperacaoCriacao(Operacao operacaoCriacao) {
        this.operacaoCriacao = operacaoCriacao;
    }

    public List<Operacao> getOperacaoModificacao() {
        return operacaoModificacao;
    }

    public void setOperacaoModificacao(List<Operacao> operacaoModificacao) {
        this.operacaoModificacao = operacaoModificacao;
    }    
}