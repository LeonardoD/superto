/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.ausencias;

/**
 *
 * @author Leonardo
 */
public enum StatusAusencia {
    
    PENDENTE("pendente", "Pendente"),
    AUTORIZADO("autorizado", "Autorizado"),
    NEGADO("negado", "Negado");

    private final String action;
    private final String label;

    private StatusAusencia(String action, String label) {
        this.action = action;
        this.label = label;
    }

    public String getAction() {
        return action;
    }

    public String getLabel() {
        return label;
    }
}
