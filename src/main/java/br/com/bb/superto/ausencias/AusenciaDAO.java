/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.ausencias;

import br.com.bb.superto.dao.GenericHibernateDAO;
import java.io.Serializable;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;


/**
 *
 * @author administrador
 */
public class AusenciaDAO extends GenericHibernateDAO<Ausencia, Serializable> {
    
    public List<Ausencia> getAusenciasPrefixo(Integer prefixo) {
        Criteria criteria = getSession().createCriteria(getPersistentClass());
        criteria.addOrder(Order.desc("ID"));
        criteria.add(Restrictions.eq("prefixosolicitante", prefixo));
        List<Ausencia> ausencias = criteria.list();

        try {
            if (ausencias == null || ausencias.isEmpty()) {
                return null;
            }
        } catch (Exception ex) {
            System.out.println("Nao deu certo a lista de dividas!!!");
        }
        return ausencias;
    }
}
