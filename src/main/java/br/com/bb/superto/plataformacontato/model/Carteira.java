package br.com.bb.superto.plataformacontato.model;

import br.com.bb.superto.model.GenericEntity;
import br.com.bb.superto.model.Operacao;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;

/**
 * @author Administrador
 */
@Entity
@Table(name = "carteira")
public class Carteira implements Serializable, GenericEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String nrCarteira;
    private Integer prefixo;
    @OneToOne
    @JoinColumn(name = "opcriacao_id")
    private Operacao operacaoCriacao;
    @OneToMany
    private List<Operacao> operacaoModificacao;

    public Carteira() {
    }

    public Carteira(String nrCarteira, Integer prefixo) {
        this.nrCarteira = nrCarteira;
        this.prefixo = prefixo;

    }

    @Override
    public Integer getId() {
        return id;
    }

    public void setID(Integer ID) {
        this.id = ID;
    }

    public String getNrCarteira() {
        return nrCarteira;
    }

    public void setNrCarteira(String nrCarteira) {
        this.nrCarteira = nrCarteira;
    }

    public Integer getPrefixo() {
        return prefixo;
    }

    public void setPrefixo(Integer prefixo) {
        this.prefixo = prefixo;
    }

    public Operacao getOperacaoCriacao() {
        return operacaoCriacao;
    }

    public void setOperacaoCriacao(Operacao operacaoCriacao) {
        this.operacaoCriacao = operacaoCriacao;
    }

    public List<Operacao> getOperacaoModificacao() {
        return operacaoModificacao;
    }

    public void setOperacaoModificacao(List<Operacao> operacaoModificacao) {
        this.operacaoModificacao = operacaoModificacao;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 83 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Carteira other = (Carteira) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Carteira{" + "id=" + id + ", nrCarteira=" + nrCarteira + ", prefixo=" + prefixo + ", operacaoCriacao=" + operacaoCriacao + ", operacaoModificacao=" + operacaoModificacao + '}';
    }
}
