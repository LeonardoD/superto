/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.plataformacontato.dao;

import br.com.bb.superto.dao.GenericHibernateDAO;
import br.com.bb.superto.plataformacontato.model.Carteira;
import br.com.bb.superto.plataformacontato.model.Contato;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author Administrador
 */
public class ContatoDAO extends GenericHibernateDAO<Contato, Integer> {

    public List<Contato> getContatos(Integer prefixo) {
        Criteria criteria = getSession().createCriteria(getPersistentClass());
        //   criteria.addOrder(Order.asc("nrCarteira"));
        criteria.add(Restrictions.eq("prefixo", prefixo));
        List<Contato> buscarPor = criteria.list();
        try {
            if (buscarPor == null || buscarPor.isEmpty()) {
                return null;
            }
        } catch (Exception er) {
            er.printStackTrace();
        }
        return buscarPor;

    }

    public List<Contato> getTodosContatos() {
        Criteria criteria = getSession().createCriteria(getPersistentClass());
        criteria.addOrder(Order.asc("prefixo"));

        List<Contato> buscarPor = criteria.list();

        try {
            if (buscarPor == null || buscarPor.isEmpty()) {
                return null;
            }
        } catch (Exception er) {
            er.printStackTrace();
        }
        return buscarPor;
    }

    public List<Contato> getContatos(Carteira carteira, Integer prefixo, Date data) {
        Criteria criteria = getSession().createCriteria(getPersistentClass());
        //   criteria.addOrder(Order.asc("nrCarteira"));
        criteria.add(Restrictions.eq("prefixo", prefixo));
        criteria.add(Restrictions.eq("dataContato", data));

        List<Contato> buscarPor = criteria.list();
        List<Contato> lista = new ArrayList<Contato>();
        if (carteira != null) {
            // criteria.add(Restrictions.eq("carteira", carteira));
            //   System.out.println("Tamanho da lista "+buscarPor.size());
            //  for(Contato c:buscarPor)
            // if(c.getCarteira().getNrCarteira().equalsIgnoreCase(carteira.getNrCarteira()) && prefixo==c.getPrefixo() && data.equals(c.getDataContato()))
            //  lista.add(c);
            for (Contato c : buscarPor) {
                System.out.println("Carteira do contato " + c.getCarteira().getNrCarteira());
                if (c.getCarteira().getNrCarteira().equalsIgnoreCase(carteira.getNrCarteira())) {
                    lista.add(c);
                }
            }
        }

        System.out.println("Tamanho da lista " + lista.size());
        try {
            if (buscarPor == null || buscarPor.isEmpty()) {
                return null;
            }
        } catch (Exception er) {
            er.printStackTrace();
        }
        return lista;
    }

}
