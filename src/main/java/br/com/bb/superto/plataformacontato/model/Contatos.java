/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.plataformacontato.model;

import br.com.bb.superto.plataformacontato.report.annotations.ReportColumn;
import br.com.bb.superto.plataformacontato.report.classtypes.Classes;

/**
 * @author Administrador
 */
public class Contatos {

    @ReportColumn(property = "prefixo", title = "Agência", colClass = Classes.INTEGER, groupingCriteria = true)
    public Integer prefixo;

    @ReportColumn(property = "nrCarteira", title = "Carteira", colClass = Classes.STRING)
    public String nrCarteira;
    @ReportColumn(property = "mci", title = "MCI", colClass = Classes.STRING)
    public String mci;
    @ReportColumn(property = "fmatricula", title = "Funcionário", colClass = Classes.STRING)
    public String fmatricula;
    @ReportColumn(property = "status", title = "Status", colClass = Classes.STRING)
    public String status;
    @ReportColumn(property = "dataContato", title = "Data", colClass = Classes.STRING)
    public String dataContato;
    public String produtos;

    public Contatos(Integer prefixo, String nrCarteira, String mci, String fmatricula, String status, String dataContato, String produtos) {
        this.prefixo = prefixo;
        this.nrCarteira = nrCarteira;
        this.mci = mci;
        this.fmatricula = fmatricula;
        this.status = status;
        this.dataContato = dataContato;
        this.produtos = produtos;
    }

    public Integer getPrefixo() {
        return prefixo;
    }

    public void setPrefixo(Integer prefixo) {
        this.prefixo = prefixo;
    }

    public String getNrCarteira() {
        return nrCarteira;
    }

    public void setNrCarteira(String nrCarteira) {
        this.nrCarteira = nrCarteira;
    }

    public String getMci() {
        return mci;
    }

    public void setMci(String mci) {
        this.mci = mci;
    }

    public String getFmatricula() {
        return fmatricula;
    }

    public void setFmatricula(String fmatricula) {
        this.fmatricula = fmatricula;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDataContato() {
        return dataContato;
    }

    public void setDataContato(String dataContato) {
        this.dataContato = dataContato;
    }

    public String getProdutos() {
        return produtos;
    }

    public void setProdutos(String produtos) {
        this.produtos = produtos;
    }

}
