/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.plataformacontato.model;

import br.com.bb.superto.model.GenericEntity;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author Administrador
 */
@Entity
@Table(name = "carteiraMeta")
public class CarteiraMeta implements Serializable, GenericEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer ID;
    private Integer meta;

    public CarteiraMeta() {

    }

    @Override
    public Integer getId() {
        return ID;
    }

    public void setID(Integer ID) {
        this.ID = ID;
    }

    public Integer getMeta() {
        return meta;
    }

    public void setMeta(Integer meta) {
        this.meta = meta;
    }
}
