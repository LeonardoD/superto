/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.plataformacontato.model;

import br.com.bb.superto.model.GenericEntity;

import javax.persistence.*;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * @author Administrador
 */
@Entity
@Table(name = "contato")
public class Contato implements Serializable, GenericEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer ID;

    @OneToOne
    private Carteira carteira;
    private String status;
    private String mci;
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date dataContato;
    private Integer prefixo;
    private String fmatricula;
    private String dataFormatada;

    private ArrayList<String> produtos;
    private int contDiario;

    public Contato() {

    }

    public Contato(Carteira carteira, String mci, Date dataContato, Integer prefixo, String fmatricula, String status) {
        this.carteira = carteira;
        this.mci = mci;
        this.dataContato = dataContato;
        this.prefixo = prefixo;
        this.fmatricula = fmatricula;
        this.contDiario = 0;
        this.status = status;
        System.out.println("Data contato: " + new SimpleDateFormat("dd/MM/yyyy HH:mm").format(dataContato));
        this.dataFormatada = new SimpleDateFormat("dd/MM/yyyy HH:mm").format(dataContato);

    }

    public String getDataFormatada() {
        return dataFormatada;
    }

    public void setDataFormatada(String dataFormatada) {
        this.dataFormatada = dataFormatada;
    }

    public String getTodosOsProdutos() {
        String produtos = " ";
        int i = 0;
        for (i = 0; i < this.produtos.size(); i++) {
            produtos = produtos + this.produtos.get(i) + " - ";
            // System.out.println("Produto "+ prod.getNome());
        }

        return produtos;
    }

    public Carteira getCarteira() {
        return carteira;
    }

    public void setCarteira(Carteira carteira) {
        this.carteira = carteira;
    }

    public String getDataContado() {
        return new SimpleDateFormat("dd/MM/yyyy HH:mm").format(dataContato);
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getContDiario() {
        return contDiario;
    }

    public void setContDiario(int contDiario) {
        this.contDiario = contDiario;
    }

    public ArrayList<String> getProdutos() {
        return produtos;
    }

    public void setProdutos(ArrayList<String> produtos) {
        this.produtos = produtos;
    }

    public void contar() {
        this.contDiario++;
    }

    public String getMci() {
        return mci;
    }

    public void setMci(String mci) {
        this.mci = mci;
    }

    public Date getDataContato() {
        return dataContato;
    }

    public void setDataContato(Date dataContato) {
        this.dataContato = dataContato;
    }

    public Integer getPrefixo() {
        return prefixo;
    }

    public void setPrefixo(Integer prefixo) {
        this.prefixo = prefixo;
    }

    public String getFmatricula() {
        return fmatricula;
    }

    public void setFmatricula(String fmatricula) {
        this.fmatricula = fmatricula;
    }

    public Integer getID() {
        return ID;
    }

    public void setID(Integer ID) {
        this.ID = ID;
    }

    @Override
    public Integer getId() {
        return ID;
    }

}
