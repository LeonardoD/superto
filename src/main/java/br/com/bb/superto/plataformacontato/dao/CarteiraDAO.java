/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.plataformacontato.dao;

import br.com.bb.superto.dao.GenericHibernateDAO;
import br.com.bb.superto.plataformacontato.model.Carteira;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * @author Administrador
 */
public class CarteiraDAO extends GenericHibernateDAO<Carteira, Integer> {

    public Carteira getCarteiraByNr(String nrCarteira, Integer prefixo) {

        Criteria criteria = getSession().createCriteria(getPersistentClass());
        criteria.add(Restrictions.eq("nrCarteira", nrCarteira));
        criteria.add(Restrictions.eq("prefixo", prefixo));
        List<Carteira> buscarPor = criteria.list();
        try {
            if (buscarPor == null || buscarPor.isEmpty()) {
                return null;
            }
        } catch (Exception er) {
            er.printStackTrace();
        }
        return buscarPor.get(0);

    }

    public List<Carteira> getCarteiras(Integer prefixo) {
        Criteria criteria = getSession().createCriteria(getPersistentClass());
        criteria.addOrder(Order.asc("nrCarteira"));
        criteria.add(Restrictions.eq("prefixo", prefixo));
        List<Carteira> buscarPor = criteria.list();
        try {
            if (buscarPor == null || buscarPor.isEmpty()) {
                return null;
            }
        } catch (Exception er) {
            er.printStackTrace();
        }
        return buscarPor;
    }

    public List<Carteira> getCarteirasOrdenadas() {
        Criteria criteria = getSession().createCriteria(getPersistentClass());
        criteria.addOrder(Order.asc("nrCarteira"));
        return criteria.list();
    }
}
