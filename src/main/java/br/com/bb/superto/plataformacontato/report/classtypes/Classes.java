/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.plataformacontato.report.classtypes;

import java.util.Date;

/**
 * @author webcloud
 */
public enum Classes {

    STRING(String.class.getName()),
    DATE(Date.class.getName()),
    INTEGER(Integer.class.getName()),
    FLOAT(Float.class.getName()),
    LONG(Long.class.getName());

    private final String className;

    private Classes(String className) {
        this.className = className;
    }

    public String className() {
        return this.className;
    }
}
