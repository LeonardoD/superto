/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.security;

import br.com.bb.superto.model.UserFuncionario;
import br.com.bb.superto.model.UserNotLogged;
import org.apache.shiro.SecurityUtils;

/**
 * Classe de utilidades para o contexto de segurança.
 *
 * @author Maycon
 * @version 1.0
 */
public class UserSecurityUtils {

    /**
     * Verifica no contexto do Spring Security se o usuario está logado no
     * sistema
     *
     * @return se o usuario está logado
     * @since 1.0
     */
    public static boolean isUserLogged() {
        return SecurityUtils.getSubject().isAuthenticated();
    }

    /**
     * Busca no contexto do Spring Security o obejto usuario que foi inserido no
     * metodo {@link CustomUserDetailsService#loadUserByUsername(java.lang.String)
     * }
     *
     * @return o objeto usuario
     * @throws UserNotLogged
     * @see CustomUserDetailsService#loadUserByUsername(java.lang.String)
     * @since 1.0
     */
    public static UserFuncionario getUserFuncionario() throws UserNotLogged {
        if (isUserLogged()) {
            return (UserFuncionario) SecurityUtils.getSubject().getPrincipal();
        }
        throw new UserNotLogged("User not logged!");

    }

}
