package br.com.bb.superto.security;

import javax.naming.*;
import javax.naming.directory.*;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;

/**
 * @author Renan Alan S. Costa
 */
public class LdapConnection {

    private static final SearchControls searchControls = new SearchControls(SearchControls.SUBTREE_SCOPE, 50, 0, null, false, false);
    private static final SearchControls nullControls = new SearchControls(SearchControls.SUBTREE_SCOPE, 1, 0, new String[0], false, false);
    private static Logger log = Logger.getLogger("LdapConnection_LOG");
    private DirContext ldapContext;
    private Hashtable<String, String> env = new Hashtable<String, String>();
    private String ldapServer;
    private String baseDN; // DN == Nome Distinto
    private String url;
    private String contextFactory;

    public void init() {
        baseDN = "O=BB,C=BR";
        url = "ldap://172.17.77.213:389";
        contextFactory = "com.sun.jndi.ldap.LdapCtxFactory";
    }

    public Boolean connect(String key, String password) {
        try {
            init();
            env.put(Context.INITIAL_CONTEXT_FACTORY, this.contextFactory);
            env.put(Context.PROVIDER_URL, this.url);
            String securityProtocol = null;
            if (securityProtocol != null) {
                env.put(Context.SECURITY_PROTOCOL, securityProtocol); // none
            }
            env.put(Context.SECURITY_AUTHENTICATION, "simple");
            env.put(Context.SECURITY_PRINCIPAL, this.parameter(key));
            env.put(Context.SECURITY_CREDENTIALS, password);
            System.out.println("InitialDirContext(env)");
            this.ldapContext = new InitialDirContext(env);

            System.out.println("CONNECTED!");
            System.out.println("ENV contains " + env.size() + " key value pairs.\n");
            Set<Map.Entry<String, String>> entrySet = env.entrySet();
            Iterator<Map.Entry<String, String>> iterator = entrySet.iterator();
            while (iterator.hasNext()) {
                Map.Entry<String, String> next = iterator.next();
                System.out.println("Key : " + next.getKey());
            }

            return Boolean.TRUE;
        } catch (CommunicationException e) {
            System.out.println(e);
            String cause = e.getCause().getClass().getName();
            if (cause.equals("java.net.UnknownHostException")) {
                System.out.println("The host " + ldapServer + " could not be found");
            } else if (cause.equals("java.net.SocketException")) {
                System.out.println("Unable to connect to host " + ldapServer);
            } else if (cause.equals("javax.net.ssl.SSLHandshakeException")) {
                System.out.println("SSL certificate for LDAP server is not signed by trusted provider");
            } else {
                System.out.println(cause);
            }
            return Boolean.FALSE;
        } catch (AuthenticationException e) {
            System.out.println("The username (bind name) and/or password are not valid");
            return Boolean.FALSE;
        } catch (AuthenticationNotSupportedException e) {
            System.out.println("The authentication method is not supported");
            return Boolean.FALSE;
        } catch (Exception e) {
            System.out.println(e);
            return Boolean.FALSE;
        }

    }

    public NamingEnumeration<SearchResult> search(String filter, boolean retry)
            throws NamingException {
        try {
            // Attempt to perform search            
            System.out.println("Base DN: " + baseDN + "\nFilter: " + filter);
            return ldapContext.search(baseDN, filter, searchControls);
        } catch (ServiceUnavailableException e) {
            // Connection closed due to inactivity            
            if (retry) {
                // Re-open connection & try one more time
                try {
                    ldapContext = new InitialDirContext(env);
                } catch (Exception ee) {
                    log.warning("LDAP connection failed: " + ee.getMessage());
                    return null;
                }
                log.info("Connection to LDAP server re-opened");
                return search(filter, false);
            } else {
                // We've already tried re-opening the connection, so give up now
                log.warning("LDAP search failed: " + e.getMessage());
                return null;
            }
        } catch (CommunicationException e) {
            // Connection closed due to interruption
            if (retry) {
                // Re-open connection & try one more time
                try {
                    ldapContext = new InitialDirContext(env);
                } catch (Exception ee) {
                    //messageLine.setTransientText ("Failed to re-open connection to LDAP server");
                    log.warning("LDAP connection failed: " + ee.getMessage());
                    return null;
                }
                log.info("Connection to LDAP server re-opened");
                return search(filter, false);
            } else {
                // We've already tried re-opening the connection, so give up now
                log.warning("LDAP search failed: " + e.getMessage());
                return null;
            }
        }
    }

    public Boolean entryExists(String name, Attributes attributes) {
        try {
            return ldapContext.search(fullDn(name), attributes).hasMore();
        } catch (NamingException e) {
            log.warning("Error searching for entry\n" + e.getMessage());
            return false;
        }
    }

    public void printSearchEnumeration(NamingEnumeration retEnum) {
        try {
            while (retEnum.hasMore()) {
                SearchResult sr = (SearchResult) retEnum.next();
                System.out.println("SEARCH RESULT:\t" + sr.getName());
                System.out.println("\n\tDN " + sr.getNameInNamespace());

                Attributes attrs = sr.getAttributes();
                formatAttributes(attrs);

            }
        } catch (NamingException e) {
            e.printStackTrace();
        } catch (java.lang.Exception e) {
            e.printStackTrace();
        }
    }

    /*
     * Generic method to format the Attributes .Displays all the multiple values
     * of each Attribute in the Attributes
     */
    public void formatAttributes(Attributes attrs) throws Exception {
        if (attrs == null) {
            System.out.println("This result has no attributes");
        } else {
            try {
                for (NamingEnumeration enumer = attrs.getAll(); enumer.hasMore(); ) {
                    Attribute attrib = (Attribute) enumer.next();
                    System.out.print("\t" + attrib.getID());

                    try {
                        for (NamingEnumeration e = attrib.getAll(); e.hasMore(); ) {
                            System.out.println("\t\t= " + e.next());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } catch (NamingException e) {
                e.printStackTrace();
            }
        }
    }

    private String fullDn(String name) {
        return (name.length() > 0 ? (name + (baseDN.length() > 0 ? ("," + baseDN) : "")) : baseDN);
    }

    public Boolean isFKey(String key) {
        return key.toLowerCase().startsWith("f");
    }

    public Boolean isTKey(String key) {
        return key.toLowerCase().startsWith("t");
    }

    public Boolean isCKey(String key) {
        return key.toLowerCase().startsWith("c");
    }

    public String parameter(String key) {
        String str = null;
        if (this.isFKey(key)) {
            str = "uid=" + key + ",ou=funcionarios,ou=usuarios,ou=acesso,o=bb,c=br";
        } else if (this.isTKey(key)) {
            str = "uid=" + key + ",ou=estagiarios,ou=usuarios,ou=acesso,o=bb,c=br";
        } else if (this.isCKey(key)) {
            str = "uid=" + key + ",ou=contratados,ou=usuarios,ou=acesso,o=bb,c=br";
        }
        return str;
    }
    //===============================================================================================================================================================
}
