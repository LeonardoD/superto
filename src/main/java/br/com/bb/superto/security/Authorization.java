package br.com.bb.superto.security;

import br.com.bb.superto.dao.FuncionariosDao;
import br.com.bb.superto.dao.FuncionariosRolesDAO;
import br.com.bb.superto.dao.GroupMembersDAO;
import br.com.bb.superto.model.*;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

/**
 * @author maycon
 */
public class Authorization extends AuthorizingRealm {

    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection pc) {
        SimpleAuthorizationInfo simpleAuthorizationInfo = new SimpleAuthorizationInfo();
        simpleAuthorizationInfo.addRoles(new HashSet<String>(((UserFuncionario) pc.getPrimaryPrincipal()).getRoles()));
        simpleAuthorizationInfo.addObjectPermissions(((UserFuncionario) pc.getPrimaryPrincipal()).getWildPermissions());
        return simpleAuthorizationInfo;
    }

    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken at) throws AuthenticationException {
        UsernamePasswordToken token = (UsernamePasswordToken) at;
        System.out.println("Login");
        System.out.println("Nome : " + token.getUsername());
        System.out.println("Host2 : " + token.getHost());
        UserFuncionario userFuncionario = new UserFuncionario();
        Funcionarios listaPorMatricula = new FuncionariosDao().getByFmatricula(token.getUsername());
        System.out.println("aqui");
        Funcionarios func = null;
        ArrayList<String> roles = new ArrayList<String>();
        ArrayList<String> permissions = new ArrayList<String>();
        try {
            System.out.println("aqui");
            if (listaPorMatricula == null) {
                System.out.println("Usuario não encontrado em nosso banco de dados.");
                func = new Funcionarios();
                func.setFmatricula(token.getUsername());
                func.setNome("Visitante");
                roles.add("funcionario");
            } else {
                System.out.println("aqui");
                func = listaPorMatricula;
                List<FuncionarioRole> rolesByFuncionario = new FuncionariosRolesDAO().getRolesByFuncionario(func);
                roles.add("funcionario");
                for (FuncionarioRole role : rolesByFuncionario) {
                    if (!roles.contains(role.getRole().getRoleName())) {
                        roles.add(role.getRole().getRoleName());
                        for (RolesPermissions permission : role.getRole().getRolesPermissions()) {
                            if (!permissions.contains(permission.getPermission())) {
                                permissions.add(permission.getPermission());
                            }
                        }
                    }
                }
                List<GroupMembers> groupsByFunc = new GroupMembersDAO().getGroupsByFunc(func);
                for (GroupMembers e : groupsByFunc) {
                    List<GroupRole> groupRoles = e.getGroups().getGroupRoles();
                    for (GroupRole r : groupRoles) {
                        if (!roles.contains(r.getRole().getRoleName())) {
                            roles.add(r.getRole().getRoleName());
                            for (RolesPermissions permission : r.getRole().getRolesPermissions()) {
                                if (!permissions.contains(permission.getPermission())) {
                                    permissions.add(permission.getPermission());
                                }
                            }
                        }
                    }
                }
                for (String hj : roles) {
                    System.out.println("\t" + hj);
                }
            }
            userFuncionario.setFuncionario(func);

            userFuncionario.setRoles(roles);
            userFuncionario.setPermissions(permissions);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return new SimpleAuthenticationInfo(userFuncionario, "naoalterar", func.getFmatricula());
    }
}
