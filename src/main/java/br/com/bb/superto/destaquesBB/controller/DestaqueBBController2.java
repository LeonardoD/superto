/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.destaquesBB.controller;

import br.com.bb.superto.controller.ArchiveController;
import br.com.bb.superto.controller.GenericController;
import br.com.bb.superto.dao.OperacaoDAO;
import br.com.bb.superto.destaquesBB.dao.DestaqueBBDao;
import br.com.bb.superto.destaquesBB.model.DestaqueBB;
import br.com.bb.superto.model.Archive;
import br.com.bb.superto.model.Operacao;
import br.com.bb.superto.model.UserNotLogged;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author administrador
 */
@ManagedBean
@ViewScoped
public class DestaqueBBController2 extends GenericController<DestaqueBB> {

    private String texto;
    private List<DestaqueBB> listaDestaquesBB;
    private int sizeListaDestaques;
    private boolean formSucess;
    private Archive archive;

    public DestaqueBBController2() {
        listaDestaquesBB = new DestaqueBBDao().findDestaquesVisiveis(5);
        formSucess = false;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    public List<DestaqueBB> getListaDestaquesBB() {
        return listaDestaquesBB;
    }

    public void setListaDestaquesBB(List<DestaqueBB> listaDestaquesBB) {
        this.listaDestaquesBB = listaDestaquesBB;
    }

    public boolean isFormSucess() {
        return formSucess;
    }

    public void setFormSucess(boolean formSucess) {
        this.formSucess = formSucess;
    }

    public Archive getArchive() {
        return archive;
    }

    public void setArchive(Archive archive) {
        this.archive = archive;
    }

    public int getSizeListaDestaques() {
        return listaDestaquesBB.size();
    }

    public void setSizeListaDestaques(int sizeListaDestaques) {
        this.sizeListaDestaques = sizeListaDestaques;
    }

    public void fileUploadListener(FileUploadEvent event) {
        try {
            archive = new ArchiveController().add(event.getFile());
            if (archive != null) {
                RequestContext.getCurrentInstance().update("imagemAdd");
                RequestContext.getCurrentInstance().update("imagemEdit");
            }
        } catch (UserNotLogged | IOException ex) {
            Logger.getLogger(DestaqueBBController.class.getName()).log(Level.SEVERE, null, ex);
            addFacesMsg("Erro ao anexar arquivo: " + ex.getMessage(), FacesMessage.SEVERITY_ERROR);
        }
    }

    public void mudaCapaValue() {
        value.setFotoDestaque(archive);
    }

    public void mudaCapaSelected() {
        selected.setFotoDestaque(archive);
    }

    @Override
    public void edit() {
        try {
            Operacao operacao = getNovaOperacao();
            selected.getOperacaoModificacao().add(new OperacaoDAO().save(operacao));
            if (archive != null) {
                selected.setFotoDestaque(archive);
            }
            formSucess = new DestaqueBBDao().makePersistent(selected) != null;
            if (formSucess) {
                RequestContext.getCurrentInstance().update("destaquesTable");
            }
        } catch (Exception e) {
            formSucess = false;
            System.out.println("Erro ao editar: " + e.getMessage());
        }
        RequestContext.getCurrentInstance().execute("verificaEdit();");
    }

    @Override
    public void delete() {
        try {
            formSucess = new DestaqueBBDao().makeTransient(selected) != null;
            if (formSucess) {
                RequestContext.getCurrentInstance().update("destaquesTable");
            }
        } catch (Exception e) {
            formSucess = false;
            System.out.println("Erro ao deletar: " + e.getMessage());
        }
    }

    @Override
    public void add() {
        try {
            Operacao operacao = getNovaOperacao();
            OperacaoDAO opDao = new OperacaoDAO();
            value.setOperacaoModificacao(new ArrayList<>());
            value.getOperacaoModificacao().add(opDao.save(operacao));
            value.setOperacaoCriacao(opDao.save(operacao));
            if (archive != null) {
                value.setFotoDestaque(archive);
            }
            formSucess = new DestaqueBBDao().save(value) != null;
            if (formSucess) {
                clearValue();
                RequestContext.getCurrentInstance().update("addDestaqueBB");
                RequestContext.getCurrentInstance().update("destaquesTable");
                archive = null;
            }
        } catch (Exception e) {
            formSucess = false;
            System.out.println("Erro ao salvar: " + e.getMessage());
        } finally {
            RequestContext.getCurrentInstance().execute("verificaAdd();");
        }
    }

    public String getUltimaDataOperacao(DestaqueBB destaqueBB) {
        if (destaqueBB.getOperacaoModificacao().isEmpty()) {
            return null;
        }
        Operacao op = destaqueBB.getOperacaoModificacao().get(destaqueBB.getOperacaoModificacao().size() - 1);
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/mm/yyyy hh:mm:ss");
        return dateFormat.format(op.getDataOperacao());
    }

}
