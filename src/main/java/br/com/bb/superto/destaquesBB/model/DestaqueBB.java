package br.com.bb.superto.destaquesBB.model;

import br.com.bb.superto.model.Archive;
import br.com.bb.superto.model.GenericEntity;
import br.com.bb.superto.model.Operacao;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;

/**
 * @author Vinicius A. Barros
 */
@Entity
@Table(name = "destaquesbb")
public class DestaqueBB implements Serializable, GenericEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer ID;

    @OneToOne
    private Archive fotoDestaque;

    @NotEmpty(message = "Campo Título não pode estar vazio!")
    private String titulo;

    @NotEmpty(message = "Campo Texto não pode estar vazio!")
    private String texto;
    private boolean visivel;

    @OneToOne
    @JoinColumn(name = "opcriacao_id")
    private Operacao operacaoCriacao;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @Fetch(FetchMode.SUBSELECT)
    private List<Operacao> operacaoModificacao;

    public DestaqueBB() {
        visivel = false;
    }

    @Override
    public Integer getId() {
        return ID;
    }

    public void setID(Integer ID) {
        this.ID = ID;
    }

    public Archive getFotoDestaque() {
        return fotoDestaque;
    }

    public void setFotoDestaque(Archive fotoDestaque) {
        this.fotoDestaque = fotoDestaque;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    public boolean isVisivel() {
        return visivel;
    }

    public void setVisivel(boolean visivel) {
        this.visivel = visivel;
    }

    public Operacao getOperacaoCriacao() {
        return operacaoCriacao;
    }

    public void setOperacaoCriacao(Operacao operacaoCriacao) {
        this.operacaoCriacao = operacaoCriacao;
    }

    public List<Operacao> getOperacaoModificacao() {
        return operacaoModificacao;
    }

    public void setOperacaoModificacao(List<Operacao> operacaoModificacao) {
        this.operacaoModificacao = operacaoModificacao;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.ID);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final DestaqueBB other = (DestaqueBB) obj;
        if (!Objects.equals(this.ID, other.ID)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "DestaqueBB{" + "ID=" + ID + ", fotoDestaque=" + fotoDestaque + ", titulo=" + titulo + ", texto=" + texto + ", visivel=" + visivel + ", operacaoCriacao=" + operacaoCriacao + ", operacaoModificacao=" + operacaoModificacao + '}';
    }

}
