/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.destaquesBB.dao;

import br.com.bb.superto.dao.GenericHibernateDAO;
import br.com.bb.superto.destaquesBB.model.DestaqueBB;
import org.hibernate.Query;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Vinicius A. Barros
 */
public class DestaqueBBDao extends GenericHibernateDAO<DestaqueBB, Serializable> {

    public List<DestaqueBB> findDestaquesVisiveis(int quantidade) {
        ArrayList<DestaqueBB> destaques = null;
        Query q = getSession().createQuery("SELECT a FROM DestaqueBB a WHERE a.visivel = TRUE ORDER BY a.operacaoCriacao.dataOperacao DESC");
        q.setMaxResults(quantidade);
        destaques = (ArrayList<DestaqueBB>) q.list();
        return destaques;
    }
}
