/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.resolve.controller;

import br.com.bb.superto.resolve.dao.ResolveDAO;
import br.com.bb.superto.resolve.model.Resolve;
import org.primefaces.model.chart.CartesianChartModel;
import org.primefaces.model.chart.ChartSeries;
import org.primefaces.model.chart.PieChartModel;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Administrador
 */
@ManagedBean(name = "resolveGrafico")
@ViewScoped
public class Resolve_Estatisticas_Controller implements Serializable {

    int posicao = 0;
    private PieChartModel tortaGráfica;
    private PieChartModel tortaGráficaIN;
    private CartesianChartModel gráficoBarraIN;
    private ArrayList<Prefixos> prefixos;
    private ArrayList<Prefixos> listaINs;

    public Resolve_Estatisticas_Controller() {

        gerarGráficoTorta();
        gerarGráficoTortaINS();

    }

    public CartesianChartModel getGráficoBarraIN() {
        return gráficoBarraIN;
    }

    public void setGráficoBarraIN(CartesianChartModel gráficoBarraIN) {
        this.gráficoBarraIN = gráficoBarraIN;
    }

    public PieChartModel gráficoDinâmico() {
        if (posicao == listaINs.size() - 1) {
            posicao = 0;
        }
        tortaGráficaIN.getData().put("1", listaINs.get(posicao).getQuant());
        tortaGráficaIN.getData().put("2", listaINs.get(posicao + 1).getQuant());
        posicao++;
        return tortaGráficaIN;
    }

    public ArrayList<Prefixos> getListaINs() {
        return listaINs;
    }

    public void setListaINs(ArrayList<Prefixos> listaINs) {
        this.listaINs = listaINs;
    }

    public PieChartModel getTortaGráfica() {
        return tortaGráfica;
    }

    public void setTortaGráfica(PieChartModel tortaGráfica) {
        this.tortaGráfica = tortaGráfica;
    }

    public boolean getPrefixo(String prefixo) {
        for (Prefixos prefixo2 : prefixos) {
            if (prefixo2.getPrefixo().equalsIgnoreCase(prefixo)) {
                return true;
            }
        }
        return false;
    }

    public boolean existeNaLista(String IN) {
        for (Prefixos in : listaINs) {
            if (in.getPrefixo().equalsIgnoreCase(IN)) {
                return true;
            }
        }
        return false;
    }

    public Prefixos getPrefixoC(String prefixo) {
        for (Prefixos prefixo2 : prefixos) {
            if (prefixo2.getPrefixo().equalsIgnoreCase(prefixo)) {
                return prefixo2;
            }
        }
        return null;
    }

    public Prefixos getIN(String IN) {
        for (Prefixos in : listaINs) {
            if (in.getPrefixo().equalsIgnoreCase(IN)) {
                return in;
            }
        }
        return null;
    }

    public int getPrefixoIndice(String prefixo) {
        for (int i = 0; i < prefixos.size(); i++) {
            if (prefixos.get(i).getPrefixo().equalsIgnoreCase(prefixo)) {
                return i;
            }
        }
        return -1;
    }

    public int getINIndice(String IN) {
        for (int i = 0; i < listaINs.size(); i++) {
            if (listaINs.get(i).getPrefixo().equalsIgnoreCase(IN)) {
                return i;
            }
        }
        return -1;
    }

    public void gerarGráficoTorta() {
        List<Resolve> listaResolves = new ResolveDAO().findAll();
        prefixos = new ArrayList<Prefixos>();

        for (Resolve resolvido : listaResolves) {
            String prefixoSelecionado = resolvido.getPrefixo();
            if (!getPrefixo(prefixoSelecionado)) {
                prefixos.add(new Prefixos(prefixoSelecionado, 1));
            } else {
                Prefixos temp = getPrefixoC(prefixoSelecionado);
                temp.setQuant(temp.getQuant() + 1);
                prefixos.set(getPrefixoIndice(prefixoSelecionado), temp);
            }
        }

        tortaGráfica = new PieChartModel();
        for (Prefixos prefixo_Aux : prefixos) {
            tortaGráfica.set(prefixo_Aux.getPrefixo() + "", prefixo_Aux.getQuant());
        }

    }

    public void gerarGráficoTortaINS() {
        List<Resolve> listaresolves = new ResolveDAO().findAll();
        listaINs = new ArrayList<Prefixos>();

        for (Resolve resolve : listaresolves) {
            String inSelecionado = resolve.getNumeroIN();

            if (!existeNaLista(inSelecionado)) {
                listaINs.add(new Prefixos(inSelecionado, 1));
            } else {
                Prefixos temp = getIN(inSelecionado);
                temp.setQuant(temp.getQuant() + 1);
                listaINs.set(getINIndice(inSelecionado), temp);
            }
            gráficoBarraIN = new CartesianChartModel();
            tortaGráficaIN = new PieChartModel();
            tortaGráficaIN.set("1", 20);
            tortaGráficaIN.set("2", 40);
            // tortaGráficaIN.set(listaINs.get(posicao).getPrefixo(), listaINs.get(posicao).getQuant());
            //tortaGráficaIN.set(listaINs.get(posicao+1).getPrefixo(), listaINs.get(posicao+1).getQuant());
            //ArrayList<ChartSeries> serieDados = new ArrayList<ChartSeries>();
            for (Prefixos aux : listaINs) {
                ChartSeries serie = new ChartSeries(aux.getPrefixo());
                serie.set("", aux.getQuant());
                gráficoBarraIN.addSeries(serie);
            }
        }

    }
}
