/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.resolve.dao;

import br.com.bb.superto.dao.GenericHibernateDAO;
import br.com.bb.superto.resolve.model.Resolve;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.hibernate.criterion.Restrictions;

import java.util.List;
import org.hibernate.Query;

/**
 * @author Administrador
 */
public class ResolveDAO extends GenericHibernateDAO<Resolve, Integer> {

    public List<Resolve> findAll() {
        Query q = getSession().createQuery("SELECT a FROM Resolve AS a ORDER BY a.dataResolve DESC");
        return (List<Resolve>) q.list();
    }

    public Resolve getResolveByIN(String IN) {
        List<Resolve> buscarPor = findByCriteria(Restrictions.eq("numero", IN));
        try {
            if (buscarPor == null || buscarPor.isEmpty()) {
                return null;
            }
        } catch (Exception er) {
            er.printStackTrace();
        }
        return buscarPor.get(0);
    }

    public List<Resolve> getResolvesByPrefixo(int prefixo) {
        List<Resolve> buscarPor = findByCriteria(Restrictions.eq("Prefixo", prefixo));
        try {
            if (buscarPor == null || buscarPor.isEmpty()) {
                return null;
            }
        } catch (Exception er) {
            er.printStackTrace();
        }
        return buscarPor;
    }

    public int getResolvesPorMes(Date inicio, Date fim) {
        SimpleDateFormat formato = new SimpleDateFormat("MM/dd/yyyy HH:mm");
        String i = formato.format(inicio);
        String f = formato.format(fim);

        Query q = getSession().createQuery("SELECT COUNT(r.id) FROM Resolve r WHERE r.dataResolve >= \'" + i + "\' AND r.dataResolve <= \'" + f + "\'");
        System.err.println();
        return Integer.parseInt(q.list().get(0).toString());
    }
    
    public List<Resolve> getResolvesPorAno(String i, String f) {
        System.out.print("Entramos no método: Pega Resolver Por Ano");
        Query q = getSession().createQuery("SELECT r FROM Resolve r WHERE r.dataResolve >= \'" + i + "\' AND r.dataResolve <= \'" + f + "\'");
        System.out.print("Pegamos a lista");
        return (List<Resolve>) q.list();
    }
}
