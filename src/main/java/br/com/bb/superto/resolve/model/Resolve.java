/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.resolve.model;

import br.com.bb.superto.model.GenericEntity;
import java.util.Date;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;

/**
 * @author Administrador
 */
@Entity
@Table(name = "resolve")
public class Resolve implements java.io.Serializable, GenericEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer id;
    private String numero;
    private String numeroIN;
    private String nrPesquisa;
    private String prefixo;
    @Length(max = 10000)
    private String descricao;
    @Length(max = 2000)
    private String consulta;
    
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataResolve;
    public Resolve() {
    }
    public Resolve(String nrPesquisa, String prefixo, String numeroIN, String descricao, String consulta, Date data, String nr) {
        this.consulta = consulta;
        this.dataResolve= data;
        this.descricao = descricao;
        this.numeroIN = numeroIN;
        this.nrPesquisa =nrPesquisa;
        this.prefixo = prefixo;
        this.numero =nr;

    }

    @Override
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNrPesquisa() {
        return nrPesquisa;
    }

    public void setNrPesquisa(String nrPesquisa) {
        this.nrPesquisa = nrPesquisa;
    }

    public String getPrefixo() {
        return prefixo;
    }

    public void setPrefixo(String prefixo) {
        this.prefixo = prefixo;
    }

    public String getNumeroIN() {
        return numeroIN;
    }

    public void setNumeroIN(String numeroIN) {
        this.numeroIN = numeroIN;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getConsulta() {
        return consulta;
    }

    public void setConsulta(String consulta) {
        this.consulta = consulta;
    }

    public Date getData() {
        return dataResolve;
    }

    public void setData(Date data) {
        this.dataResolve = data;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

}
