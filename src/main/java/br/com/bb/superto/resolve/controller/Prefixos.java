/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.resolve.controller;

/**
 * @author Administrador
 */
public class Prefixos {

    private String prefixo;
    private Integer quant;

    public Prefixos(String prefixo, Integer quant) {
        setPrefixo(prefixo);
        setQuant(quant);
    }

    public String getPrefixo() {
        return prefixo;
    }

    public void setPrefixo(String prefixo) {
        this.prefixo = prefixo;
    }

    public Integer getQuant() {
        return quant;
    }

    public void setQuant(Integer quant) {
        this.quant = quant;
    }
}
