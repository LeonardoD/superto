/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.resolve.controller;

import br.com.bb.superto.controller.FuncionariosController2;
import br.com.bb.superto.controller.GenericController;
import br.com.bb.superto.controller.ProgressBarController;
import br.com.bb.superto.dao.UserHistoryDao;
import br.com.bb.superto.resolve.dao.ResolveDAO;
import br.com.bb.superto.resolve.model.Resolve;
import br.com.bb.superto.util.FacesUtils;
import com.csvreader.CsvReader;
import org.primefaces.event.CloseEvent;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

import javax.faces.application.FacesMessage;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.faces.context.FacesContext;
import java.io.IOException;
import java.nio.charset.Charset;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.primefaces.context.RequestContext;
import org.primefaces.model.chart.Axis;
import org.primefaces.model.chart.AxisType;
import org.primefaces.model.chart.BarChartModel;
import org.primefaces.model.chart.ChartSeries;

/**
 * @author Administrador
 */

@ViewScoped
@Named("resolveController")
public class ResolveController extends GenericController<Resolve> {

    private ProgressBarController progress;
    private List<Resolve> resolvesFiltrados;
    private List<Resolve> todosOsResolves;
    private UploadedFile uploadedResolves;
    private String fileName;
    private String ano;
    private boolean visivel = false;

    public List<Resolve> getTodosOsResolves() {
        if (todosOsResolves == null) {
            todosOsResolves = new ResolveDAO().findAll();
        }
        return todosOsResolves;
    }

    public void setTodosOsResolves(List<Resolve> todosOsResolves) {
        this.todosOsResolves = todosOsResolves;
    }

    public List<Resolve> getResolvesFiltrados() {
        return resolvesFiltrados;
    }

    public void setResolvesFiltrados(List<Resolve> resolvesFiltrados) {
        this.resolvesFiltrados = resolvesFiltrados;
    }

    public UploadedFile getUploadedResolves() {
        return uploadedResolves;
    }

    public void setUploadedResolves(UploadedFile uploadedResolves) {
        this.uploadedResolves = uploadedResolves;
    }

    public void fileUploadListener(FileUploadEvent event) {
        uploadedResolves = event.getFile();
        fileName = uploadedResolves.getFileName().substring(0, uploadedResolves.getFileName().indexOf("."));
        System.out.println("Nome do arquivo é " + fileName);
    }

    public Integer getProgress() {
        return progress.getCurrentValue();
    }

    public void handleClose(CloseEvent event) {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO,
                event.getComponent().getId() + " closed", "So you don't like nature?");
        facesContext.addMessage(null, message);
    }
    
    public void showMessage() {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "What we do in life", "Echoes in eternity.");
         
        RequestContext.getCurrentInstance().showMessageInDialog(message);
    }
    public void updateResolves() throws ParseException {
        try {
            progress = (ProgressBarController) FacesUtils.getViewAttribute("progressBarController");
            if (progress == null) {
                progress = new ProgressBarController();
            }
            
            ResolveDAO aResolve = new ResolveDAO();
            CsvReader aResolveCSV = new CsvReader(uploadedResolves.getInputstream(), Charset.forName("iso-8859-1"));

            aResolveCSV.setDelimiter(';');

            aResolveCSV.readHeaders();

            progress.setMaxValue(300);

            while (aResolveCSV.readRecord()) {

                String nrPesquisa = aResolveCSV.get(0);
                String prefixo = aResolveCSV.get(1);
                String numeroIN = aResolveCSV.get(2);
                String descricao = aResolveCSV.get(3).replaceAll("§§§", "\n");
                String consulta = aResolveCSV.get(4);
                String data = aResolveCSV.get(5).split(" ")[0];
                String nr = aResolveCSV.get(6);

                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

                Date d = new Date(sdf.parse(data).getTime());

                Resolve resolve = aResolve.getResolveByIN(nr);

                if (resolve != null) {
                    resolve.setNumero(nr);
                    resolve.setData(d);
                    resolve.setDescricao(descricao);
                    resolve.setNumeroIN(numeroIN);
                    resolve.setPrefixo(prefixo);
                    resolve.setConsulta(consulta);
                    resolve.setNrPesquisa(nrPesquisa);
                    aResolve.makePersistent(resolve);
                } else {
                    resolve = new Resolve(nrPesquisa, prefixo, numeroIN, descricao, consulta, d, nr);
                    new ResolveDAO().save(resolve);
                }
                progress.setCurrentValue(progress.getCurrentValue() + 1);
            }

            aResolveCSV.close();

            addFacesMsg("Resolves atualizados com sucesso.", FacesMessage.SEVERITY_INFO);
        } catch (IOException ex) {
            Logger.getLogger(FuncionariosController2.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public void edit() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void delete() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void add() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    //*************** GRÁFICO DE RESOLVE ***********
    private String chartYear;
    private final ArrayList<String> listaAnos = new ArrayList<>();
    private BarChartModel barModelResolve;
    private int QtdResolvesAno;

    public int getQtdResolvesAno() {
        return QtdResolvesAno;
    }

    public void setQtdResolvesAno(int QtdResolvesAno) {
        this.QtdResolvesAno = QtdResolvesAno;
    }

    public String getChartYear() {
        if (chartYear == null) {
            setChartYear(2014 + "");
        }
        return chartYear;
    }

    public void setChartYear(String chartYear) {
        this.chartYear = chartYear;
    }

    public List<String> getListaAnos() {
        listaAnos.clear();
        Date day = new Date();
        @SuppressWarnings("LocalVariableHidesMemberVariable")
        int ano = (day.getYear()) + (1900);
        System.out.println("Ano atual dentro do getListaAnos(): " + ano);
        for (int i = 2014; i < ano; i++) {
            listaAnos.add(i + "");
        }
        listaAnos.add(ano + "");
        System.err.println("<<<<<<<<<<<Lista anos:" + listaAnos);
        return listaAnos;
    }

    public BarChartModel getBarModelResolves() {
        if (barModelResolve == null) {
            barModelResolve = new BarChartModel();
        }
        createBarModelResolves();
        return barModelResolve;
    }

    private void createBarModelResolves() {
        barModelResolve = resolvesPorAno();

        barModelResolve.setTitle("Resolves:");

        Axis xAxis = barModelResolve.getAxis(AxisType.X);
        xAxis.setLabel("Meses");

        Axis yAxis = barModelResolve.getAxis(AxisType.Y);
        yAxis.setLabel("Qtd. Resolves");

    }

    private BarChartModel resolvesPorAno() {
        BarChartModel modelFuncs = new BarChartModel();
        Date day = new Date();
        ChartSeries dadosAcessos = new ChartSeries();
        Calendar calendario = GregorianCalendar.getInstance();
        modelFuncs.setShowPointLabels(true);
        modelFuncs.setZoom(true);

        day.setYear(Integer.parseInt(chartYear) - 1900);
        day.setDate(01);

        for (int i = 0; i < 12; i++) {
            day.setMonth(i);
            Date dayFinal = new Date();
            dayFinal.setTime(day.getTime());
            calendario.setTime(day);
            dayFinal.setDate(calendario.getActualMaximum(Calendar.DAY_OF_MONTH) - 1);
            QtdResolvesAno = new ResolveDAO().getResolvesPorMes(day, dayFinal);
            dadosAcessos.set(i + 1, QtdResolvesAno);
        }

        modelFuncs.addSeries(dadosAcessos);
        return modelFuncs;
    }

    public String getAno() {
        return ano;
    }

    public void setAno(String ano) {
        this.ano = ano;
    }
    
    public boolean isVisivel(){
        return visivel;
    }
    
    public void setVisivel(boolean visivel) {
        this.visivel = visivel;
    }

    public void deletaResolvesPorAno() {
        System.out.print("Entramos no método deletaResolvesPorAno");
        String dateI = "";
        String dateF = "";
        
//        
        if(ano != null){
            System.out.print("Ano não é nulo");
            dateI = "01/01/"+ano; // produção
            dateF = "12/12/"+ano; // produção  
        }
        else{
            System.out.print("Ano é nulo");
        }
        
        System.out.print("Data de inicio: " + dateI);
        System.out.print("Data de Fim: " + dateF);
        
        List<Resolve> resolvesDoAno = new ArrayList<Resolve>();
        
        try{
            System.out.print("Vamos pegar a lista");
            resolvesDoAno = new ResolveDAO().getResolvesPorAno(dateI, dateF);
            System.out.print("Pegamos a lista");

        }
        catch(Exception ex){
            System.out.print("Erro ao pegar lista: " + resolvesDoAno);
        }
        
        System.out.print("Lista de Resolves do Ano foi declarada, abaixo o valor do primeiro item");
        System.out.print("-----------" + resolvesDoAno.toString());
        
        try{
            System.out.print("Entramos no try");
            ResolveDAO excluindo = new ResolveDAO();
            boolean sucesso = true;
            System.out.print("Variável excluindo declarada");
            System.out.print("Próximo passo == entrar no for");
            
            for(Resolve r: resolvesDoAno){
                try{
                    excluindo.makeTransient(r);    
                }
                catch(Exception ex){
                    sucesso = false;
                    System.out.print("Erro: " + ex);
                }
            }
            System.out.print("Saimos do for... sucesso: " + sucesso);
        }
        catch(Exception ex){
            System.out.print("Erro: " + ex);
        }
    }
}
