/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.painelonline.model;

import br.com.bb.superto.model.Agencias;
import br.com.bb.superto.model.Produto;

import javax.persistence.Embeddable;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Objects;

/**
 * @author administrador
 */
@Embeddable
public class PainelOnlineID implements Serializable {

    @NotNull(message = "Produto não pode ser nulo!")
    @OneToOne
    private Produto produto;
    @NotNull(message = "Agência não pode ser nula!")
    @OneToOne
    private Agencias agencia;

    public Produto getProduto() {
        return produto;
    }

    public void setProduto(Produto produto) {
        this.produto = produto;
    }

    public Agencias getAgencia() {
        return agencia;
    }

    public void setAgencia(Agencias agencia) {
        this.agencia = agencia;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 47 * hash + Objects.hashCode(this.produto);
        hash = 47 * hash + Objects.hashCode(this.agencia);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PainelOnlineID other = (PainelOnlineID) obj;
        if (!Objects.equals(this.produto, other.produto)) {
            return false;
        }
        if (!Objects.equals(this.agencia, other.agencia)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "PainelOnlineID{" + "produto=" + produto + ", agencia=" + agencia + '}';
    }
}
