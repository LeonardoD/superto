/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.painelonline.dao;

import br.com.bb.superto.controller.LoginController;
import br.com.bb.superto.dao.AgenciasDao;
import br.com.bb.superto.dao.GenericHibernateDAO;
import br.com.bb.superto.dao.ProdutoDao;
import br.com.bb.superto.model.Agencias;
import br.com.bb.superto.model.Funcionarios;
import br.com.bb.superto.model.Produto;
import br.com.bb.superto.painelonline.model.PainelOnline;
import org.hibernate.Query;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author administrador
 */
public class PainelOnlineDao extends GenericHibernateDAO<PainelOnline, Serializable> {

    //    public List<PainelOnline> findPaineisVisiveis(int quantidade) {
//        ArrayList<PainelOnline> paineis = null;
//        Query q = getSession().createQuery("SELECT a FROM PainelOnline a JOIN FETCH a.operacaoModificacao b WHERE a.visible = TRUE ORDER BY b.dataOperacao DESC");
//        q.setMaxResults(quantidade);
//        paineis = (ArrayList<PainelOnline>) q.list();
//        return paineis;
//    }
    public List<PainelOnline> findPaineisByProduto(Produto pProduto) {
        List<PainelOnline> aux;

        Query q = getSession().createQuery("SELECT a FROM PainelOnline a WHERE a.id.produto = :produto");
        q.setParameter("produto", pProduto);
        aux = q.list();

        return aux;
    }

    /*Busca um painelonline de acordo com o produto passado por parametro e pela agencia do funcionario logado */
    public PainelOnline findPainelByProdutoPrefixo(Produto pProduto) {
        PainelOnline aux;
        Funcionarios func = new LoginController().getFuncionario();
        Agencias ag = new AgenciasDao().getAgenciaByPrefixo(func.getPrefixo());

        Query q = getSession().createQuery("SELECT a FROM PainelOnline a WHERE a.id.produto = :produto AND a.id.agencia = :agencia");
        q.setParameter("produto", pProduto);
        q.setParameter("agencia", ag);
        aux = (PainelOnline) q.uniqueResult();

        return aux;
    }

    public List<Produto> findProdutosEmPaineisVisiveis() {
        List<Integer> ids;
        List<Produto> aux = new ArrayList<>();
        Query q = getSession().createQuery("SELECT a.id.produto.id FROM PainelOnline a WHERE a.visible = TRUE GROUP BY a.id.produto.id");
        ids = q.list();
        for (Integer id : ids) {
            aux.add(new ProdutoDao().findById(id, true));
        }

        return aux;
    }

    public List<Produto> findProdutosEmPaineis() {
        List<Integer> ids;
        List<Produto> aux = new ArrayList<>();
        Query q = getSession().createQuery("SELECT a.id.produto.id FROM PainelOnline a WHERE a.visible = FALSE GROUP BY a.id.produto.id");
        ids = q.list();
        for (Integer id : ids) {
            aux.add(new ProdutoDao().findById(id, true));
        }

        return aux;
    }
}
