package br.com.bb.superto.painelonline.controller;

import br.com.bb.superto.controller.GenericController;
import br.com.bb.superto.model.Produto;
import br.com.bb.superto.painelonline.dao.PainelOnlineDao;
import br.com.bb.superto.painelonline.model.PainelOnline;
import org.hibernate.HibernateException;
import org.primefaces.context.RequestContext;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author administrador
 */
@ManagedBean
@ViewScoped
public class PainelOnlineController extends GenericController<PainelOnline> {

    private List<Produto> listaPaineisVisiveis;
    private List<Produto> listaPaineis;
    private List<PainelOnline> listaPaineisIndex;
    private Produto produtoSelecionado;
    private boolean opeSucesso;

    public List<Produto> getListaPaineis() {
        if (listaPaineis == null) {
            listaPaineis = new PainelOnlineDao().findProdutosEmPaineis();
        }
        return listaPaineis;
    }

    public void setListaPaineis(List<Produto> listaPaineis) {
        this.listaPaineis = listaPaineis;
    }

    public boolean isOpeSucesso() {
        return opeSucesso;
    }

    public void setOpeSucesso(boolean opeSucesso) {
        this.opeSucesso = opeSucesso;
    }

    public List<Produto> getListaPaineisVisiveis() {
        listaPaineisVisiveis = new PainelOnlineDao().findProdutosEmPaineisVisiveis();
        return listaPaineisVisiveis;
    }

    public void setListaPaineisVisiveis(List<Produto> listaPaineisVisiveis) {
        this.listaPaineisVisiveis = listaPaineisVisiveis;
    }

    public List<PainelOnline> getListaPaineisIndex() {
        List<Produto> produtosAtivos = new PainelOnlineDao().findProdutosEmPaineisVisiveis();
        listaPaineisIndex = new ArrayList<>();

        for (Produto p : produtosAtivos) {
            listaPaineisIndex.add(new PainelOnlineDao().findPainelByProdutoPrefixo(p));
        }
        return listaPaineisIndex;
    }

    public void setListaPaineisIndex(List<PainelOnline> listaPaineisIndex) {
        this.listaPaineisIndex = listaPaineisIndex;
    }

    public Produto getProdutoSelecionado() {
        return produtoSelecionado;
    }

    public void setProdutoSelecionado(Produto produtoSelecionado) {
        this.produtoSelecionado = produtoSelecionado;
        RequestContext.getCurrentInstance().update("btn-add-painel");
    }

    public boolean limitePaineis(int limite) {
        List<Produto> produtosAtivos = new PainelOnlineDao().findProdutosEmPaineisVisiveis();
        if (produtosAtivos.size() == limite) {
            produtoSelecionado = null;
            RequestContext.getCurrentInstance().execute("mostraError('oops!','Limite de Paineis Visíveis Excedido! Desative um Painel para poder incluir outro!')");
            return true;
        }
        return false;
    }

    public List<Produto> completeProdutos(String query) {
        List<Produto> allProdutos = getListaPaineis();
        List<Produto> produtosFiltrados = new ArrayList<>();

        for (Produto prod : allProdutos) {
            if (prod.getDescricao().toLowerCase().contains(query.toLowerCase())) {
                produtosFiltrados.add(prod);
            }
        }
        return produtosFiltrados;
    }

    @Override
    public void edit() {
        try {
            List<PainelOnline> paineis = new PainelOnlineDao().findPaineisByProduto(produtoSelecionado);
            for (PainelOnline a : paineis) {
                a.setVisible(false);
            }
            opeSucesso = new PainelOnlineDao().makePersistentList(paineis) != null;
        } catch (HibernateException e) {
            opeSucesso = false;
            Logger.getLogger(PainelOnlineController.class.getName()).log(Level.SEVERE, null, e);
        }
        produtoSelecionado = null;
        listaPaineis = null;
    }

    @Override
    public void delete() {
    }

    @Override
    public void add() {
        try {
            if (!limitePaineis(5)) {
                List<PainelOnline> paineis = new PainelOnlineDao().findPaineisByProduto(produtoSelecionado);
                for (PainelOnline a : paineis) {
                    a.setVisible(true);
                }
                opeSucesso = new PainelOnlineDao().makePersistentList(paineis) != null;
                RequestContext.getCurrentInstance().update("alertas");
                RequestContext.getCurrentInstance().execute("verificaAdd();");
            }
        } catch (HibernateException e) {
            opeSucesso = false;
            Logger.getLogger(PainelOnlineController.class.getName()).log(Level.SEVERE, null, e);
        }
        produtoSelecionado = null;
        listaPaineis = null;
        RequestContext.getCurrentInstance().update("menus");
        RequestContext.getCurrentInstance().update("addDialog");
    }
}
