/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.painelonline.controller;

import br.com.bb.superto.controller.GenericController;
import br.com.bb.superto.dao.AgenciasDao;
import br.com.bb.superto.dao.OperacaoDAO;
import br.com.bb.superto.dao.ProdutoDao;
import br.com.bb.superto.model.Agencias;
import br.com.bb.superto.model.Operacao;
import br.com.bb.superto.model.Produto;
import br.com.bb.superto.painelonline.dao.PainelOnlineDao;
import br.com.bb.superto.painelonline.model.PainelOnline;
import br.com.bb.superto.painelonline.model.PainelOnlineID;
import com.csvreader.CsvReader;
import org.hibernate.HibernateException;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author filipe
 */
@ManagedBean(name = "painelImportarController")
@ViewScoped
public class PainelImportarController extends GenericController<PainelOnline> {

    private UploadedFile uploadedFile;
    private String newProduto;
    private Produto produto;
    private boolean opeSucesso;

    public UploadedFile getUploadedFile() {
        return uploadedFile;
    }

    public void setUploadedFile(UploadedFile uploadedFile) {
        this.uploadedFile = uploadedFile;
    }

    public String getNewProduto() {
        return newProduto;
    }

    public void setNewProduto(String newProduto) {
        this.newProduto = newProduto;
    }

    public Produto getProduto() {
        return produto;
    }

    public void setProduto(Produto produto) {
        this.produto = produto;
    }

    public boolean isOpeSucesso() {
        return opeSucesso;
    }

    public void setOpeSucesso(boolean opeSucesso) {
        this.opeSucesso = opeSucesso;
    }

    public void addProduto() {
        produto = new Produto();
        produto.setDescricao(newProduto);
        produto = new ProdutoDao().save(produto);
        if (produto == null) {
            addFacesMsg("Erro ao incluir o produto!", FacesMessage.SEVERITY_FATAL);
        } else {
            RequestContext.getCurrentInstance().execute("PF('addProdutoDialog').hide()");
        }
    }

    public List<Produto> completeProdutos(String query) {
        List<Produto> allProdutos = new ProdutoDao().findAll();
        List<Produto> produtosFiltrados = new ArrayList<>();

        for (Produto prod : allProdutos) {
            if (prod.getDescricao().toLowerCase().contains(query.toLowerCase())) {
                produtosFiltrados.add(prod);
            }
        }
        return produtosFiltrados;
    }

    public void fileUploadListener(FileUploadEvent event) {
        uploadedFile = event.getFile();
        RequestContext.getCurrentInstance().update("btn-salvar-painel");
    }

    public void lerArq() {
        if (uploadedFile != null) {
            try {
                int aux = 0;
                List<PainelOnline> paineisOnlineAdicionar = new ArrayList<>();
                List<PainelOnline> paineisOnlineAtualizar = new ArrayList<>();

                CsvReader csvReader = new CsvReader(uploadedFile.getInputstream(), Charset.defaultCharset());
                csvReader.readHeaders();
                csvReader.setDelimiter(';');

                while (csvReader.readRecord()) {
                    aux++;
                    PainelOnline po;
                    PainelOnlineID id = new PainelOnlineID();
                    Agencias ag = new AgenciasDao().getAgenciaByPrefixo(Integer.parseInt(csvReader.get(0)));
                    if (ag != null) {
                        id.setAgencia(ag);
                        id.setProduto(produto);
                        po = new PainelOnlineDao().findById(id, true);
                        if (po != null) {
                            po.setValor(csvReader.get(1));
                            po.setLigacoes(Integer.parseInt(csvReader.get(2)));
                            String atingimento = csvReader.get(3).replace("-", "0");
                            atingimento = atingimento.replace(",", ".");
                            po.setAtingimento(Double.parseDouble(atingimento));
                            paineisOnlineAtualizar.add(po);
                        } else {
                            po = new PainelOnline();
                            po.setId(id);
                            po.setValor(csvReader.get(1));
                            po.setLigacoes(Integer.parseInt(csvReader.get(2)));
                            String atingimento = csvReader.get(3).replace("-", "0");
                            atingimento = atingimento.replace(",", ".");
                            po.setAtingimento(Double.parseDouble(atingimento));
                            paineisOnlineAdicionar.add(po);
                        }
                    }
                }
                if (paineisOnlineAdicionar.size() > 0) {
                    add(paineisOnlineAdicionar);
                } else if (paineisOnlineAtualizar.size() > 0) {
                    edit(paineisOnlineAtualizar);
                }
                produto = null;
            } catch (IOException ex) {
                Logger.getLogger(PainelImportarController.class.getName()).log(Level.SEVERE, null, ex);
                addFacesMsg("Erro ao analisar Arquivo!", FacesMessage.SEVERITY_ERROR);
            } catch (NumberFormatException ex) {
                addFacesMsg("Erro ao analisar Arquivo! Layout do arquivo pode estar incorreto!", FacesMessage.SEVERITY_ERROR);
            }
        } else {
            addFacesMsg("Upload do arquivo csv é obrigatório!", FacesMessage.SEVERITY_ERROR);
        }
    }

    @Override
    public void edit() {
    }

    public void edit(List<PainelOnline> pListPainelOnlines) {
        try {
            Operacao operacao = getNovaOperacao();
            operacao = new OperacaoDAO().save(operacao);
            for (PainelOnline p : pListPainelOnlines) {
                p.getOperacaoModificacao().add(operacao);
            }
            List<PainelOnline> vrListAtualizados = new PainelOnlineDao().makePersistentList(pListPainelOnlines);
            opeSucesso = vrListAtualizados.size() == pListPainelOnlines.size();
            newProduto = "";
            uploadedFile = null;
            produto = null;
            RequestContext.getCurrentInstance().update("alertas");
            RequestContext.getCurrentInstance().execute("verificaAdd();");
        } catch (Exception e) {
            opeSucesso = false;
            Logger.getLogger(PainelOnlineController.class.getName()).log(Level.SEVERE, "Erro ao atualizar Paineis:\n", e);
        }
    }

    @Override
    public void delete() {
    }

    @Override
    public void add() {
    }

    public void add(List<PainelOnline> pListPainelOnlines) {
        Operacao operacao = getNovaOperacao();
        OperacaoDAO opDao = new OperacaoDAO();
        operacao = opDao.save(operacao);
        try {
            for (PainelOnline p : pListPainelOnlines) {
                p.setOperacaoCriacao(operacao);
                p.setOperacaoModificacao(new ArrayList<>());
                p.getOperacaoModificacao().add(operacao);
            }
            List<PainelOnline> vrListAdicionados = new PainelOnlineDao().saveListReturn(pListPainelOnlines);

            // Verifica se a lista passado por parametro foi totalmente incluída
            opeSucesso = vrListAdicionados.size() == pListPainelOnlines.size();
            newProduto = "";
            uploadedFile = null;
            produto = null;
            RequestContext.getCurrentInstance().update("alertas");
            RequestContext.getCurrentInstance().execute("verificaAdd();");
        } catch (HibernateException e) {
            opeSucesso = false;
            Logger.getLogger(PainelOnlineController.class.getName()).log(Level.SEVERE, null, e);
        }
    }
}
