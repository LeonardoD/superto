/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.inadimplencia.model;

import br.com.bb.superto.model.GenericEntity;
import br.com.bb.superto.model.Operacao;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author vinicius
 */
@Entity
@Table(name = "mensagens_inadimplencia")
public class MensagensInadimplencia implements GenericEntity, Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer Id;

    private String comentarioSuper;
    private String comentarioAgencia;

    @ManyToOne
    @JoinColumn(name = "ciclo_id")
    private CicloInadimplencia cicloInadimplencia;

    @OneToOne
    @JoinColumn(name = "op_super_id")
    private Operacao operacaoSuper;

    @OneToOne
    @JoinColumn(name = "op_agencia_id")
    private Operacao operacaoAgencia;

    public MensagensInadimplencia() {
    }

    public MensagensInadimplencia(String comentarioSuper, String comentarioAgencia, Operacao operacaoSuper, Operacao operacaoAgencia) {
        this.comentarioSuper = comentarioSuper;
        this.comentarioAgencia = comentarioAgencia;
        this.operacaoSuper = operacaoSuper;
        this.operacaoAgencia = operacaoAgencia;
    }

    @Override
    public Integer getId() {
        return Id;
    }

    public void setId(Integer Id) {
        this.Id = Id;
    }

    public String getComentarioSuper() {
        return comentarioSuper;
    }

    public void setComentarioSuper(String comentarioSuper) {
        this.comentarioSuper = comentarioSuper;
    }

    public String getComentarioAgencia() {
        return comentarioAgencia;
    }

    public void setComentarioAgencia(String comentarioAgencia) {
        this.comentarioAgencia = comentarioAgencia;
    }

    public Operacao getOperacaoSuper() {
        return operacaoSuper;
    }

    public void setOperacaoSuper(Operacao operacaoSuper) {
        this.operacaoSuper = operacaoSuper;
    }

    public Operacao getOperacaoAgencia() {
        return operacaoAgencia;
    }

    public void setOperacaoAgencia(Operacao operacaoAgencia) {
        this.operacaoAgencia = operacaoAgencia;
    }

    public CicloInadimplencia getCicloInadimplencia() {
        return cicloInadimplencia;
    }

    public void setCicloInadimplencia(CicloInadimplencia cicloInadimplencia) {
        this.cicloInadimplencia = cicloInadimplencia;
    }

}
