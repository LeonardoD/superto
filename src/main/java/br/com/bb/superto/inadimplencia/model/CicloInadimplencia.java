package br.com.bb.superto.inadimplencia.model;

import br.com.bb.superto.model.*;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Objects;

/**
 * @author vinicius
 */
@Entity
@Table(name = "ciclo_inadimplencia")
public class CicloInadimplencia implements Serializable, GenericEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @OneToOne
    @JoinColumn(name = "cliente_id")
    private Cliente cliente;

    @OneToOne
    @JoinColumn(name = "op_entrada_id")
    private Operacao dataEntrada;

    @OneToOne
    @JoinColumn(name = "op_saida_id")
    private Operacao dataSaida;

    private String prefixo;
    private String matriculaGerente;
    private String nomeGerenteRelacionamento;

    @OneToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "op_envionotificacao_id")
    private Operacao envioNotificacao;

    @Temporal(TemporalType.DATE)
    private Date previsaoRegularizacao;

    @OneToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "op_notificacao_id")
    private Operacao opEnvioNotificacao;

    @Enumerated(EnumType.STRING)
    private Mercado mercado;

    private BigDecimal atraso;
    private BigDecimal arrasto;

    public CicloInadimplencia() {
    }

    @Override
    public Integer getId() {
        return id;
    }

    public void setID(Integer id) {
        this.id = id;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Operacao getDataEntrada() {
        return dataEntrada;
    }

    public void setDataEntrada(Operacao dataEntrada) {
        this.dataEntrada = dataEntrada;
    }

    public Operacao getDataSaida() {
        return dataSaida;
    }

    public void setDataSaida(Operacao dataSaida) {
        this.dataSaida = dataSaida;
    }

    public String getMatriculaGerente() {
        return matriculaGerente;
    }

    public String getPrefixo() {
        return prefixo;
    }

    public void setPrefixo(String prefixo) {
        this.prefixo = prefixo;
    }

    public void setMatriculaGerente(String matriculaGerente) {
        this.matriculaGerente = matriculaGerente;
    }

    public String getNomeGerenteRelacionamento() {
        return nomeGerenteRelacionamento;
    }

    public void setNomeGerenteRelacionamento(String nomeGerenteRelacionamento) {
        this.nomeGerenteRelacionamento = nomeGerenteRelacionamento;
    }

    public Operacao getEnvioNotificacao() {
        return envioNotificacao;
    }

    public void setEnvioNotificacao(Operacao envioNotificacao) {
        this.envioNotificacao = envioNotificacao;
    }

    public Date getPrevisaoRegularizacao() {
        return previsaoRegularizacao;
    }

    public void setPrevisaoRegularizacao(Date previsaoRegularizacao) {
        this.previsaoRegularizacao = previsaoRegularizacao;
    }

    public Operacao getOpEnvioNotificacao() {
        return opEnvioNotificacao;
    }

    public void setOpEnvioNotificacao(Operacao opEnvioNotificacao) {
        this.opEnvioNotificacao = opEnvioNotificacao;
    }

    public Mercado getMercado() {
        return mercado;
    }

    public void setMercado(Mercado mercado) {
        this.mercado = mercado;
    }

    public BigDecimal getAtraso() {
        return atraso;
    }

    public void setAtraso(BigDecimal atraso) {
        this.atraso = atraso;
    }

    public BigDecimal getArrasto() {
        return arrasto;
    }

    public void setArrasto(BigDecimal arrasto) {
        this.arrasto = arrasto;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 11 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CicloInadimplencia other = (CicloInadimplencia) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

}
