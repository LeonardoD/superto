/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.inadimplencia.dao;

import br.com.bb.superto.dao.GenericHibernateDAO;
import br.com.bb.superto.inadimplencia.model.CicloInadimplencia;
import br.com.bb.superto.inadimplencia.model.MensagensInadimplencia;

import java.io.Serializable;
import java.util.List;
import org.hibernate.Query;

/**
 * @author vinicius
 */
public class MensagensInadimplenteDAO extends GenericHibernateDAO<MensagensInadimplencia, Serializable> {

    public MensagensInadimplencia getUtimaMensagem(CicloInadimplencia ciclo) {
        Query q = getSession().createQuery("SELECT m FROM MensagensInadimplencia m WHERE m.cicloInadimplencia = :ciclo ORDER BY m.Id DESC");
        q.setParameter("ciclo", ciclo);
        List<MensagensInadimplencia> msgList = (List<MensagensInadimplencia>) q.list();
        return (msgList != null && !msgList.isEmpty()) ? msgList.get(0) : null;
    }

    public List<MensagensInadimplencia> findByCicloInadimplenciaList(CicloInadimplencia ciclo) {
        Query q = getSession().createQuery("SELECT m FROM MensagensInadimplencia m WHERE m.cicloInadimplencia = :ciclo ORDER BY m.Id DESC");
        q.setParameter("ciclo", ciclo);
        return (List<MensagensInadimplencia>) q.list();
    }
}
