/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.inadimplencia.dao;

import br.com.bb.superto.dao.GenericHibernateDAO;
import br.com.bb.superto.inadimplencia.model.Inadimplente;
import org.hibernate.Query;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @author vinicius
 */
public class InadimplenteDAO extends GenericHibernateDAO<Inadimplente, Serializable> {

    public List<Inadimplente> getInadimplentesByPrefixo(String prefixo) {
        Query q = getSession().createQuery("SELECT i FROM Inadimplente i WHERE i.prefixo = :prefixo");
        q.setParameter("prefixo", prefixo);
        List<Inadimplente> listInadimplentes = (List<Inadimplente>) q.list();
        return listInadimplentes;
    }

    public List<Inadimplente> getEmTempoRegular(String prefixo) {
        Query q;
        if (prefixo.equals("8517")) {
            q = getSession().createQuery("SELECT i FROM Inadimplente i WHERE i.previsaoRegularizacao >= :dataAtual");
            q.setParameter("dataAtual", new Date());
        } else {
            q = getSession().createQuery("SELECT i FROM Inadimplente i WHERE i.previsaoRegularizacao >= :dataAtual AND i.prefixo = :prefixo");
            q.setParameter("dataAtual", new Date());
            q.setParameter("prefixo", prefixo);
        }
        List<Inadimplente> listInadimplentes = (List<Inadimplente>) q.list();
        return listInadimplentes;
    }

    public List<Inadimplente> getEmAtraso(String prefixo) {
        Query q;
        if (prefixo.equals("8517")) {
            q = getSession().createQuery("SELECT i FROM Inadimplente i WHERE i.previsaoRegularizacao < :dataAtual");
            q.setParameter("dataAtual", new Date());
        } else {
            q = getSession().createQuery("SELECT i FROM Inadimplente i WHERE i.previsaoRegularizacao < :dataAtual AND i.prefixo = :prefixo");
            q.setParameter("dataAtual", new Date());
            q.setParameter("prefixo", prefixo);
        }
        List<Inadimplente> listInadimplentes = (List<Inadimplente>) q.list();
        return listInadimplentes;
    }

    public Inadimplente findByCliente(Integer mci) {
        Query q = getSession().createQuery("SELECT i FROM Inadimplente i WHERE i.clienteInadimplente.mci = :mci");
        q.setParameter("mci", mci);
        return (Inadimplente) q.uniqueResult();
    }
}
