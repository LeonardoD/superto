package br.com.bb.superto.inadimplencia.controller;

import br.com.bb.superto.controller.GenericController;
import br.com.bb.superto.controller.LoginController;
import br.com.bb.superto.dao.ClienteDao;
import br.com.bb.superto.dao.OperacaoDAO;
import br.com.bb.superto.inadimplencia.dao.CicloInadimplenciaDao;
import br.com.bb.superto.inadimplencia.dao.InadimplenteDAO;
import br.com.bb.superto.inadimplencia.dao.MensagensInadimplenteDAO;
import br.com.bb.superto.inadimplencia.model.CicloInadimplencia;
import br.com.bb.superto.inadimplencia.model.Inadimplente;
import br.com.bb.superto.inadimplencia.model.MensagensInadimplencia;
import br.com.bb.superto.inadimplencia.model.StatusInadimplencia;
import br.com.bb.superto.model.Cliente;
import br.com.bb.superto.model.Mercado;
import br.com.bb.superto.util.Util;
import com.csvreader.CsvReader;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import org.primefaces.event.SelectEvent;

/**
 * @author vinicius
 */
@ManagedBean(name = "inadimplenteController")
@ViewScoped
public class InadimplenteController extends GenericController<Inadimplente> {

    private List<Inadimplente> listInadSuper;
    private List<Inadimplente> listaDeInadFilter;
    private List<MensagensInadimplencia> lisMsgAdimplente;
    private List<Inadimplente> listInadAg;
    private List<CicloInadimplencia> cicloInadimplenciaByCliente;
    private String mensagemSuper;
    private String mensagemAgencia;
    private String dataPrevisaoRegularizacao;
    private Integer prefixoLogin;
    private boolean opSucess;
    private boolean checked;
    private Integer mciClienteBuscado;
    private CicloInadimplencia cicloInadimplenciaSelected;

    @PostConstruct
    public void init() {
        listaDeInadFilter = new ArrayList<>();
        prefixoLogin = new LoginController().getFuncionario().getPrefixo();
    }

    public List<Inadimplente> getListInadSuper() {
        listInadSuper = new InadimplenteDAO().findAll();
        return listInadSuper;
    }

    public void setListInadSuper(List<Inadimplente> listInadSuper) {
        this.listInadSuper = listInadSuper;
    }

    public List<Inadimplente> getListInadEmTempoRegular() {
        return new InadimplenteDAO().getEmTempoRegular(Util.normalizarPrefixo(prefixoLogin.toString()));
    }

    public List<Inadimplente> getListInadEmAtraso() {
        return new InadimplenteDAO().getEmAtraso(Util.normalizarPrefixo(prefixoLogin.toString()));
    }

    public List<CicloInadimplencia> getListHistoricoInadimplencia() {
        return new CicloInadimplenciaDao().historicoInadimplente(Util.normalizarPrefixo(prefixoLogin.toString()));
    }

    public List<Inadimplente> getListaDeInadFilter() {
        return listaDeInadFilter;
    }

    public void setListaDeInadFilter(List<Inadimplente> listaDeInadFilter) {
        this.listaDeInadFilter = listaDeInadFilter;
    }

    public List<Inadimplente> getListInadAg() {
        listInadAg = new InadimplenteDAO().getInadimplentesByPrefixo(Util.normalizarPrefixo(prefixoLogin.toString()));
        return listInadAg;
    }

    public void setListInadAg(List<Inadimplente> listInadAg) {
        this.listInadAg = listInadAg;
    }

    public List<CicloInadimplencia> getCicloInadimplenciaByCliente() {
        cicloInadimplenciaByCliente = new CicloInadimplenciaDao().findByMci(mciClienteBuscado);
        return cicloInadimplenciaByCliente;
    }

    public Integer getMciClienteBuscado() {
        return mciClienteBuscado;
    }

    public void setMciClienteBuscado(Integer mciClienteBuscado) {
        this.mciClienteBuscado = mciClienteBuscado;
    }

    public void setCicloInadimplenciaByCliente(List<CicloInadimplencia> cicloInadimplenciaByCliente) {
        this.cicloInadimplenciaByCliente = cicloInadimplenciaByCliente;
    }

    public boolean isOpSucess() {
        return opSucess;
    }

    public void setOpSucess(boolean opSucess) {
        this.opSucess = opSucess;
    }

    public Integer getPrefixoLogin() {
        return prefixoLogin;
    }

    public void setPrefixoLogin(Integer prefixoLogin) {
        this.prefixoLogin = prefixoLogin;
    }

    public List<MensagensInadimplencia> getLisMsgAdimplente() {
        if (selected != null) {
            CicloInadimplencia cicloInadimplencia = new CicloInadimplenciaDao().findByCliente(selected.getClienteInadimplente());
            lisMsgAdimplente = new MensagensInadimplenteDAO().findByCicloInadimplenciaList(cicloInadimplencia);
        }
        return lisMsgAdimplente;
    }

    public Inadimplente getInadimplenteSelected() {
        return selected;
    }

    public void setInadimplenteSelected(Inadimplente selected) {
        this.selected = selected;
    }

    public void setLisMsgAdimplente(List<MensagensInadimplencia> lisMsgAdimplente) {
        this.lisMsgAdimplente = lisMsgAdimplente;
    }

    public String getDataPrevisaoRegularizacao() {
        return dataPrevisaoRegularizacao;
    }

    public void setDataPrevisaoRegularizacao(String dataPrevisaoRegularizacao) {
        this.dataPrevisaoRegularizacao = dataPrevisaoRegularizacao;
    }

    public String getMensagemSuper() {
        return mensagemSuper;
    }

    public void setMensagemSuper(String mensagemSuper) {
        this.mensagemSuper = mensagemSuper;
    }

    public String getMensagemAgencia() {
        return mensagemAgencia;
    }

    public void setMensagemAgencia(String mensagemAgencia) {
        this.mensagemAgencia = mensagemAgencia;
    }

    public void onRowSelect(SelectEvent event) {
        selected = (Inadimplente) event.getObject();
    }

    public void onRowUnselect() {
        selected = null;
    }

    public CicloInadimplencia getCicloInadimplenciaSelected() {
        return cicloInadimplenciaSelected;
    }

    public void setCicloInadimplenciaSelected(CicloInadimplencia cicloInadimplenciaSelected) {
        this.cicloInadimplenciaSelected = cicloInadimplenciaSelected;
    }

    public void fileUploadListener(FileUploadEvent event) {
        UploadedFile uploadedFile = event.getFile();
        lerArquivo(uploadedFile);
        FacesContext context = FacesContext.getCurrentInstance();
        if (opSucess) {
            context.addMessage(null, new FacesMessage("Sucesso", "Importação dos dados realizada com sucesso!"));
        } else {
            context.addMessage(null, new FacesMessage("Erro", "Ocorreu "));
        }
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public SelectItem[] getInadimplenciaStatus() {
        SelectItem[] itens = new SelectItem[StatusInadimplencia.values().length];
        int i = 0;
        for (StatusInadimplencia s : StatusInadimplencia.values()) {
            itens[i++] = new SelectItem(s, s.getLabel());
        }
        return itens;
    }

    public void verificarInadimplencia(List<Inadimplente> listCSV) {

        List<Inadimplente> inadimplentesBD = new InadimplenteDAO().findAll();

        //Remove os inadimplentes presentes na base de dados
        listCSV.stream().forEach(baseCsv -> {
            inadimplentesBD.removeIf(i -> Objects.equals(i.getClienteInadimplente().getMci(), baseCsv.getClienteInadimplente().getMci()));
        });

        for (Inadimplente inadimplente : inadimplentesBD) {

            CicloInadimplencia cicloFechamento = new CicloInadimplenciaDao().findByCliente(inadimplente.getClienteInadimplente());
            if (cicloFechamento != null) {
                cicloFechamento.setPrefixo(inadimplente.getPrefixo());
                cicloFechamento.setMatriculaGerente(inadimplente.getMatriculaGerente());
                cicloFechamento.setNomeGerenteRelacionamento(inadimplente.getNomeGerenteRelacionamento());
                cicloFechamento.setAtraso(inadimplente.getArrasto());
                cicloFechamento.setArrasto(inadimplente.getArrasto());
                cicloFechamento.setEnvioNotificacao(inadimplente.getEnvioNotificacao());
                cicloFechamento.setPrevisaoRegularizacao(inadimplente.getPrevisaoRegularizacao());
                cicloFechamento.setMercado(inadimplente.getMercado());

                cicloFechamento.setDataSaida(new OperacaoDAO().makePersistent(getNovaOperacao()));
                new CicloInadimplenciaDao().update(cicloFechamento);
            }
        }
        new InadimplenteDAO().removeAll(inadimplentesBD);
    }

    public void lerArquivo(UploadedFile uploadedFile) {
        try {
            List<Inadimplente> listInadCSV = new ArrayList<>();

            CsvReader csvReader = new CsvReader(uploadedFile.getInputstream(), Charset.forName("iso-8859-1"));
            csvReader.setDelimiter(';');
            csvReader.readHeaders();

            while (csvReader.readRecord()) {

                Integer prefixo = Integer.parseInt(csvReader.get(0).trim());
                Integer mci = Integer.parseInt(csvReader.get(1).trim());
                String atraso = csvReader.get(2).trim();
                String arrasto = csvReader.get(3).trim();
                String chaveGerente = csvReader.get(4).trim();
                String nomeGerente = csvReader.get(5).trim();
                String mercado = csvReader.get(6).trim();

                Inadimplente inadExitente = new InadimplenteDAO().findByCliente(mci);

                if (inadExitente != null) {
                    inadExitente.setPrefixo(Util.normalizarPrefixo("" + prefixo));
                    inadExitente.setMercado(Mercado.valueOf(mercado));
                    inadExitente.setAtraso(BigDecimal.valueOf(Util.retirarMascaraDinheiro(atraso)));
                    inadExitente.setArrasto(BigDecimal.valueOf(Util.retirarMascaraDinheiro(arrasto)));
                    opSucess = new InadimplenteDAO().makePersistent(inadExitente) != null;
                    listInadCSV.add(inadExitente);

                } else {

                    Inadimplente clienteInadimplente = new Inadimplente();
                    clienteInadimplente.setStatusInadimplencia(StatusInadimplencia.Novo);
                    Cliente cliente = new ClienteDao().findByMci(mci);
                    CicloInadimplencia cicloInadimplencia = new CicloInadimplencia();

                    if (cliente != null) {

                        clienteInadimplente.setClienteInadimplente(cliente);
                        cicloInadimplencia.setCliente(cliente);

                    } else {

                        Cliente novoCliente = new Cliente();
                        novoCliente.setMci(mci);
                        novoCliente.setPrefixo(Util.normalizarPrefixo("" + prefixo));
                        new ClienteDao().makePersistent(novoCliente);
                        clienteInadimplente.setClienteInadimplente(novoCliente);
                        cicloInadimplencia.setCliente(novoCliente);

                    }

                    cicloInadimplencia.setDataEntrada(new OperacaoDAO().makePersistent(getNovaOperacao()));
                    cicloInadimplencia.setPrefixo(Util.normalizarPrefixo("" + prefixo));
                    cicloInadimplencia.setMercado(Mercado.valueOf(mercado));
                    cicloInadimplencia.setArrasto(BigDecimal.valueOf(Util.retirarMascaraDinheiro(atraso)));
                    cicloInadimplencia.setAtraso(BigDecimal.valueOf(Util.retirarMascaraDinheiro(arrasto)));
                    cicloInadimplencia.setMatriculaGerente(chaveGerente);
                    cicloInadimplencia.setNomeGerenteRelacionamento(nomeGerente);
                    new CicloInadimplenciaDao().makePersistent(cicloInadimplencia);

                    clienteInadimplente.setPrefixo(Util.normalizarPrefixo("" + prefixo));
                    clienteInadimplente.setMercado(Mercado.valueOf(mercado));
                    clienteInadimplente.setAtraso(BigDecimal.valueOf(Util.retirarMascaraDinheiro(atraso)));
                    clienteInadimplente.setArrasto(BigDecimal.valueOf(Util.retirarMascaraDinheiro(arrasto)));
                    clienteInadimplente.setMatriculaGerente(chaveGerente);
                    clienteInadimplente.setNomeGerenteRelacionamento(nomeGerente);

                    opSucess = new InadimplenteDAO().makePersistent(clienteInadimplente) != null;
                    listInadCSV.add(clienteInadimplente);

                }
            }
            verificarInadimplencia(listInadCSV);
        } catch (IOException ex) {
            Logger.getLogger(InadimplenteController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void edit() {

    }

    public void fazerComentario() {
        FacesContext context = FacesContext.getCurrentInstance();
        if (selected != null) {
            if (mensagemAgencia != null) {
                editAgencia();
            } else if (mensagemSuper != null) {
                editSuper();
            }
            if (opSucess) {
                context.addMessage(null, new FacesMessage("Sucesso", "Operação de cobrança realizada com sucesso!"));
            } else {
                context.addMessage(null, new FacesMessage("Erro", "Ocorreu um erro na operação!"));
            }
            mensagemAgencia = mensagemSuper = dataPrevisaoRegularizacao = null;
        }
    }

    public void editSuper() {
        try {
            CicloInadimplencia cicloInadimplenciaEmAberto = new CicloInadimplenciaDao().findByCliente(selected.getClienteInadimplente());
            MensagensInadimplencia ultimaMensagem = new MensagensInadimplenteDAO().getUtimaMensagem(cicloInadimplenciaEmAberto);
            if (cicloInadimplenciaEmAberto != null) {
                if (ultimaMensagem != null) {
                    if (ultimaMensagem.getComentarioSuper() != null) {
                        MensagensInadimplencia msg = new MensagensInadimplencia();
                        msg.setComentarioSuper(mensagemSuper);
                        msg.setOperacaoSuper(new OperacaoDAO().makePersistent(getNovaOperacao()));
                        msg.setCicloInadimplencia(cicloInadimplenciaEmAberto);
                        new MensagensInadimplenteDAO().save(msg);
                        opSucess = true;
                    } else {
                        ultimaMensagem.setComentarioSuper(mensagemSuper);
                        ultimaMensagem.setOperacaoSuper(new OperacaoDAO().makePersistent(getNovaOperacao()));
                        ultimaMensagem.setCicloInadimplencia(cicloInadimplenciaEmAberto);
                        new MensagensInadimplenteDAO().update(ultimaMensagem);
                        opSucess = true;
                    }
                } else {
                    MensagensInadimplencia msg = new MensagensInadimplencia();
                    msg.setComentarioSuper(mensagemSuper);
                    msg.setOperacaoSuper(new OperacaoDAO().makePersistent(getNovaOperacao()));

                    msg.setCicloInadimplencia(cicloInadimplenciaEmAberto);
                    new MensagensInadimplenteDAO().update(msg);
                    opSucess = true;
                }
                selected.setStatusInadimplencia(StatusInadimplencia.Super);
                new InadimplenteDAO().update(selected);
                opSucess = true;
            }
        } catch (Exception e) {
            System.out.println("Erro: " + e.getMessage() + ">>>>>>>>>>>>>>>>>>>>>>>>>>>>");
            opSucess = false;
        }
        mensagemSuper = null;
    }

    public void editAgencia() {
        try {
            FacesContext context = FacesContext.getCurrentInstance();
            if (selected.getPrevisaoRegularizacao() != null || selected.getNotificadoMutuarioCobrigado() != null || selected.getRenegociacaoFormalizacaoContabilizacao() != null || selected.getPropostaGecor() != null
                    || selected.isSemPerspectiva() || selected.isEmNegociacaoComCliente() || selected.isNenhumaDasOpcoes()) {
                CicloInadimplencia cicloInadimplenciaEmAberto = new CicloInadimplenciaDao().findByCliente(selected.getClienteInadimplente());
                MensagensInadimplencia ultimaMensagem = new MensagensInadimplenteDAO().getUtimaMensagem(cicloInadimplenciaEmAberto);
                if (cicloInadimplenciaEmAberto != null) {
                    if (ultimaMensagem != null) {
                        if (ultimaMensagem.getComentarioAgencia() != null) {
                            MensagensInadimplencia msg = new MensagensInadimplencia();
                            msg.setComentarioAgencia(mensagemAgencia);
                            msg.setOperacaoAgencia(new OperacaoDAO().makePersistent(getNovaOperacao()));
                            msg.setCicloInadimplencia(cicloInadimplenciaEmAberto);
                            opSucess = new MensagensInadimplenteDAO().makePersistent(msg) != null;
                        } else {
                            ultimaMensagem.setComentarioAgencia(mensagemAgencia);
                            ultimaMensagem.setOperacaoAgencia(new OperacaoDAO().makePersistent(getNovaOperacao()));
                            ultimaMensagem.setCicloInadimplencia(cicloInadimplenciaEmAberto);
                            opSucess = new MensagensInadimplenteDAO().makePersistent(ultimaMensagem) != null;
                        }
                    } else {
                        MensagensInadimplencia msg = new MensagensInadimplencia();
                        msg.setComentarioAgencia(mensagemAgencia);
                        msg.setOperacaoAgencia(new OperacaoDAO().makePersistent(getNovaOperacao()));

                        msg.setCicloInadimplencia(cicloInadimplenciaEmAberto);
                        opSucess = new MensagensInadimplenteDAO().makePersistent(msg) != null;
                    }

                    if (selected.isNenhumaDasOpcoes()) {
                        selected.setSemPerspectiva(false);
                        selected.setEmNegociacaoComCliente(false);
                    }
                    selected.setStatusInadimplencia(StatusInadimplencia.Agencia);
                    new InadimplenteDAO().update(selected);
                    opSucess = true;
                }
            } else {
                context.addMessage(null, new FacesMessage("Erro", "Favor informar alguma data de previsão de regularização!"));
            }
        } catch (Exception e) {
            System.out.println("Erro: " + e.getMessage() + ">>>>>>>>>>>>>>>>>>>>>>>>>>>>");
            opSucess = false;
        }
        mensagemAgencia = null;
    }

    @Override
    public void delete() {
        if (selected != null) {
            CicloInadimplencia cicloFechamento = new CicloInadimplenciaDao().findByCliente(selected.getClienteInadimplente());
            cicloFechamento.setPrefixo(selected.getPrefixo());
            cicloFechamento.setMatriculaGerente(selected.getMatriculaGerente());
            cicloFechamento.setNomeGerenteRelacionamento(selected.getNomeGerenteRelacionamento());
            cicloFechamento.setAtraso(selected.getArrasto());
            cicloFechamento.setArrasto(selected.getArrasto());
            cicloFechamento.setEnvioNotificacao(selected.getEnvioNotificacao());
            cicloFechamento.setPrevisaoRegularizacao(selected.getPrevisaoRegularizacao());
            cicloFechamento.setMercado(selected.getMercado());
            cicloFechamento.setDataSaida(new OperacaoDAO().makePersistent(getNovaOperacao()));
            new CicloInadimplenciaDao().makePersistent(cicloFechamento);
            opSucess = new InadimplenteDAO().makeTransient(selected) != null;
        }
    }

    @Override
    public void add() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public String verificarBackGroundColor(StatusInadimplencia status) {
        if (status == StatusInadimplencia.Novo) {
            return "#feeb9c";
        } else if (status == StatusInadimplencia.Agencia) {
            return "#c6efcd";
        } else if (status == StatusInadimplencia.Super) {
            return "#c6efcd";
        }
        return "none";
    }

    public String verificarTextColor(StatusInadimplencia status) {
        if (status == StatusInadimplencia.Novo) {
            return "#917219";
        } else if (status == StatusInadimplencia.Agencia) {
            return "#297b2c";
        } else if (status == StatusInadimplencia.Super) {
            return "#c6efcd";
        }
        return "none";
    }
}
