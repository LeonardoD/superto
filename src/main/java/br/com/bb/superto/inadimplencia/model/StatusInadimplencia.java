package br.com.bb.superto.inadimplencia.model;

/**
 *
 * @author vinicius
 */
public enum StatusInadimplencia {

    Novo("Novo", "Novo"),
    Agencia("Agência", "Agência"),
    Super("Super", "Super");

    private final String action;
    private final String label;

    private StatusInadimplencia(String action, String label) {
        this.action = action;
        this.label = label;
    }

    public String getAction() {
        return action;
    }

    public String getLabel() {
        return label;
    }
}
