package br.com.bb.superto.inadimplencia.model;

import br.com.bb.superto.model.*;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * @author vinicius
 */
@Entity
@Table(name = "inadimplente")
public class Inadimplente implements Serializable, GenericEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer Id;

    private String prefixo;
    private String matriculaGerente;
    private String nomeGerenteRelacionamento;
    private boolean nenhumaDasOpcoes;
    private boolean previsaoDeRegularizacao;
    private boolean propostaDaGecor;
    private boolean notificadomutuarioEcoobrigados;
    private boolean renegociacaoemFormalizacaoContabilizacao;
    private boolean semPerspectiva;
    private boolean emNegociacaoComCliente;

    @OneToOne
    @JoinColumn(name = "op_envionotificacao_id")
    private Operacao envioNotificacao;

    @OneToOne
    @JoinColumn(name = "cliente_id")
    private Cliente clienteInadimplente;

    @Temporal(TemporalType.DATE)
    private Date previsaoRegularizacao;

    @Temporal(TemporalType.DATE)
    private Date notificadoMutuarioCobrigado;

    @Temporal(TemporalType.DATE)
    private Date renegociacaoFormalizacaoContabilizacao;

    @Temporal(TemporalType.DATE)
    private Date propostaGecor;

    @OneToOne
    @JoinColumn(name = "op_notificacao_id")
    private Operacao opEnvioNotificacao;

    @Enumerated(EnumType.STRING)
    private Mercado mercado;

    @Enumerated(EnumType.STRING)
    private StatusInadimplencia statusInadimplencia;

    private BigDecimal atraso;
    private BigDecimal arrasto;

    public Inadimplente() {
    }

    @Override
    public Integer getId() {
        return Id;
    }

    public void setId(Integer Id) {
        this.Id = Id;
    }

    public Operacao getEnvioNotificacao() {
        return envioNotificacao;
    }

    public void setEnvioNotificacao(Operacao envioNotificacao) {
        this.envioNotificacao = envioNotificacao;
    }

    public Cliente getClienteInadimplente() {
        return clienteInadimplente;
    }

    public void setClienteInadimplente(Cliente clienteInadimplente) {
        this.clienteInadimplente = clienteInadimplente;
    }

    public Date getPrevisaoRegularizacao() {
        return previsaoRegularizacao;
    }

    public void setPrevisaoRegularizacao(Date previsaoRegularizacao) {
        this.previsaoRegularizacao = previsaoRegularizacao;
    }

    public Mercado getMercado() {
        return mercado;
    }

    public void setMercado(Mercado mercado) {
        this.mercado = mercado;
    }

    public BigDecimal getAtraso() {
        return atraso;
    }

    public void setAtraso(BigDecimal atraso) {
        this.atraso = atraso;
    }

    public BigDecimal getArrasto() {
        return arrasto;
    }

    public void setArrasto(BigDecimal arrasto) {
        this.arrasto = arrasto;
    }

    public Operacao getOpEnvioNotificacao() {
        return opEnvioNotificacao;
    }

    public void setOpEnvioNotificacao(Operacao opEnvioNotificacao) {
        this.opEnvioNotificacao = opEnvioNotificacao;
    }

    public String getMatriculaGerente() {
        return matriculaGerente;
    }

    public void setMatriculaGerente(String matriculaGerente) {
        this.matriculaGerente = matriculaGerente;
    }

    public String getNomeGerenteRelacionamento() {
        return nomeGerenteRelacionamento;
    }

    public void setNomeGerenteRelacionamento(String nomeGerenteRelacionamento) {
        this.nomeGerenteRelacionamento = nomeGerenteRelacionamento;
    }

    public String getPrefixo() {
        return prefixo;
    }

    public void setPrefixo(String prefixo) {
        this.prefixo = prefixo;
    }

    public boolean isNenhumaDasOpcoes() {
        return nenhumaDasOpcoes;
    }

    public void setNenhumaDasOpcoes(boolean nenhumaDasOpcoes) {
        this.nenhumaDasOpcoes = nenhumaDasOpcoes;
    }

    public StatusInadimplencia getStatusInadimplencia() {
        return statusInadimplencia;
    }

    public void setStatusInadimplencia(StatusInadimplencia statusInadimplencia) {
        this.statusInadimplencia = statusInadimplencia;
    }

    public Date getNotificadoMutuarioCobrigado() {
        return notificadoMutuarioCobrigado;
    }

    public void setNotificadoMutuarioCobrigado(Date notificadoMutuarioCobrigado) {
        this.notificadoMutuarioCobrigado = notificadoMutuarioCobrigado;
    }

    public Date getRenegociacaoFormalizacaoContabilizacao() {
        return renegociacaoFormalizacaoContabilizacao;
    }

    public void setRenegociacaoFormalizacaoContabilizacao(Date renegociacaoFormalizacaoContabilizacao) {
        this.renegociacaoFormalizacaoContabilizacao = renegociacaoFormalizacaoContabilizacao;
    }

    public Date getPropostaGecor() {
        return propostaGecor;
    }

    public void setPropostaGecor(Date propostaGecor) {
        this.propostaGecor = propostaGecor;
    }

    public boolean isPrevisaoDeRegularizacao() {
        return previsaoDeRegularizacao;
    }

    public void setPrevisaoDeRegularizacao(boolean previsaoDeRegularizacao) {
        this.previsaoDeRegularizacao = previsaoDeRegularizacao;
    }

    public boolean isPropostaDaGecor() {
        return propostaDaGecor;
    }

    public void setPropostaDaGecor(boolean propostaDaGecor) {
        this.propostaDaGecor = propostaDaGecor;
    }

    public boolean isNotificadomutuarioEcoobrigados() {
        return notificadomutuarioEcoobrigados;
    }

    public void setNotificadomutuarioEcoobrigados(boolean notificadomutuarioEcoobrigados) {
        this.notificadomutuarioEcoobrigados = notificadomutuarioEcoobrigados;
    }

    public boolean isRenegociacaoemFormalizacaoContabilizacao() {
        return renegociacaoemFormalizacaoContabilizacao;
    }

    public void setRenegociacaoemFormalizacaoContabilizacao(boolean renegociacaoemFormalizacaoContabilizacao) {
        this.renegociacaoemFormalizacaoContabilizacao = renegociacaoemFormalizacaoContabilizacao;
    }

    public boolean isSemPerspectiva() {
        return semPerspectiva;
    }

    public void setSemPerspectiva(boolean semPerspectiva) {
        this.semPerspectiva = semPerspectiva;
    }

    public boolean isEmNegociacaoComCliente() {
        return emNegociacaoComCliente;
    }

    public void setEmNegociacaoComCliente(boolean emNegociacaoComCliente) {
        this.emNegociacaoComCliente = emNegociacaoComCliente;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + Objects.hashCode(this.Id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Inadimplente other = (Inadimplente) obj;
        if (!Objects.equals(this.Id, other.Id)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Inadimplente{" + "Id=" + Id + ", prefixo=" + prefixo + ", matriculaGerente=" + matriculaGerente + ", nomeGerenteRelacionamento=" + nomeGerenteRelacionamento + ", nenhumaDasOpcoes=" + nenhumaDasOpcoes + ", previsaoDeRegularizacao=" + previsaoDeRegularizacao + ", propostaDaGecor=" + propostaDaGecor + ", notificadomutuarioEcoobrigados=" + notificadomutuarioEcoobrigados + ", renegociacaoemFormalizacaoContabilizacao=" + renegociacaoemFormalizacaoContabilizacao + ", semPerspectiva=" + semPerspectiva + ", emNegociacaoComCliente=" + emNegociacaoComCliente + ", envioNotificacao=" + envioNotificacao + ", clienteInadimplente=" + clienteInadimplente + ", previsaoRegularizacao=" + previsaoRegularizacao + ", notificadoMutuarioCobrigado=" + notificadoMutuarioCobrigado + ", renegociacaoFormalizacaoContabilizacao=" + renegociacaoFormalizacaoContabilizacao + ", propostaGecor=" + propostaGecor + ", opEnvioNotificacao=" + opEnvioNotificacao + ", mercado=" + mercado + ", statusInadimplencia=" + statusInadimplencia + ", atraso=" + atraso + ", arrasto=" + arrasto + '}';
    }

}
