/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.inadimplencia.dao;

import br.com.bb.superto.dao.GenericHibernateDAO;
import br.com.bb.superto.inadimplencia.model.CicloInadimplencia;
import br.com.bb.superto.model.Cliente;
import org.hibernate.Query;

import java.io.Serializable;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

/**
 * @author Administrador
 */
public class CicloInadimplenciaDao extends GenericHibernateDAO<CicloInadimplencia, Serializable> {

    public List<CicloInadimplencia> historicoInadimplente(String prefixo) {
        Query q;
        if (prefixo.equals("8517")) {
            return (List<CicloInadimplencia>) getSession().createQuery("FROM CicloInadimplencia").list();
        } else {
            q = getSession().createQuery("SELECT c FROM CicloInadimplencia c WHERE c.cliente.prefixo = :prefixo");
            q.setParameter("prefixo", prefixo);
        }
        List<CicloInadimplencia> historicoCliente = (List<CicloInadimplencia>) q.list();
        return historicoCliente;
    }

    public CicloInadimplencia findByCliente(Cliente cliente) {
        Query q = getSession().createQuery("SELECT c FROM CicloInadimplencia c WHERE c.cliente = :cliente AND c.dataSaida IS NULL");
        q.setParameter("cliente", cliente);
        List<CicloInadimplencia> listCi = q.list();
        CicloInadimplencia ci = null;
        if (listCi.size() >= 1) {
            ci = (CicloInadimplencia) q.list().get(q.list().size() - 1);
        }
        return ci;
    }

    public List<CicloInadimplencia> findByMci(Integer mci) {
        Query q = getSession().createQuery("SELECT c FROM CicloInadimplencia c WHERE c.cliente.mci = :mci ORDER BY c.dataEntrada DESC");
        q.setParameter("mci", mci);
        List<CicloInadimplencia> listCicloInadimplencias = (List<CicloInadimplencia>) q.list();
        return listCicloInadimplencias;
    }

    public CicloInadimplencia findByClienteCri(Cliente cliente) {
        Criteria c = getSession().createCriteria(getPersistentClass());
        c.add(Restrictions.eq("cliente", cliente)).add(Restrictions.isNull("dataSaida"));
        return (CicloInadimplencia) c.uniqueResult();
    }
}
