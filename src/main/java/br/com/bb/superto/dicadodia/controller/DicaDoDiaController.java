/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.dicadodia.controller;

import br.com.bb.superto.controller.ArchiveController;
import br.com.bb.superto.controller.GenericController;
import br.com.bb.superto.dao.OperacaoDAO;
import br.com.bb.superto.destaquesBB.controller.DestaqueBBController;
import br.com.bb.superto.dicadodia.dao.DicaDoDiaDao;
import br.com.bb.superto.dicadodia.model.DicaDoDia;
import br.com.bb.superto.model.Archive;
import br.com.bb.superto.model.Operacao;
import br.com.bb.superto.model.UserNotLogged;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author administrador
 */
@ManagedBean
@ViewScoped
public class DicaDoDiaController extends GenericController<DicaDoDia> {

    private List<DicaDoDia> dicasDoDia;
    private boolean opeSucesso;
    private Archive imagem;

    public DicaDoDiaController() {
        opeSucesso = false;
    }

    public Archive getImagem() {
        return imagem;
    }

    public void setImagem(Archive imagem) {
        this.imagem = imagem;
    }

    public List<DicaDoDia> getDicasDoDia() {
        if (dicasDoDia == null) {
            return new DicaDoDiaDao().getLastDicasdoDia();
        }
        return dicasDoDia;
    }

    public void setDicasDoDia(List<DicaDoDia> dicasDoDia) {
        this.dicasDoDia = dicasDoDia;
    }

    public boolean isOpeSucesso() {
        return opeSucesso;
    }

    public void setOpeSucesso(boolean opeSucesso) {
        this.opeSucesso = opeSucesso;
    }

    public void fileUploadListener(FileUploadEvent event) {
        try {
            imagem = new ArchiveController().add(event.getFile());
            if (imagem != null) {
                RequestContext.getCurrentInstance().update("imagemAdd");
                RequestContext.getCurrentInstance().update("imagemEdit");
            }
        } catch (UserNotLogged | IOException ex) {
            Logger.getLogger(DestaqueBBController.class.getName()).log(Level.SEVERE, null, ex);
            addFacesMsg("Erro ao anexar arquivo: " + ex.getMessage(), FacesMessage.SEVERITY_ERROR);
        }
    }

    public void mudaImagem() {
        if (value != null) {
            value.setImagem(imagem);
        }
        if (selected != null) {
            selected.setImagem(imagem);
        }
    }

    @Override
    public void edit() {
        Operacao operacao = getNovaOperacao();
        selected.getOperacaoModificacao().add(new OperacaoDAO().save(operacao));
        opeSucesso = new DicaDoDiaDao().makePersistent(selected) != null;
        if (opeSucesso) {
            RequestContext.getCurrentInstance().update("dicasTable");
        }
    }

    @Override
    public void delete() {
        opeSucesso = new DicaDoDiaDao().makeTransient(selected) != null;
    }

    @Override
    public void add() {
        if (value.getImagem() != null) {
            Operacao operacao = getNovaOperacao();
            OperacaoDAO opDao = new OperacaoDAO();
            try {
                value.setOperacaoCriacao(opDao.save(operacao));
                value.setOperacaoModificacao(new ArrayList<>());
                value.getOperacaoModificacao().add(opDao.save(operacao));
                opeSucesso = new DicaDoDiaDao().save(value) != null;
                RequestContext.getCurrentInstance().update("dicasTable");
                RequestContext.getCurrentInstance().execute("verificaAdd();");
                clearValue();
                RequestContext.getCurrentInstance().update("addDialog");
            } catch (Exception e) {
                opeSucesso = false;
                Logger
                        .getLogger(DicaDoDia.class
                                .getName()).log(Level.SEVERE, "Erro ao adiconar Dica-do-dia.", "");
            }
        } else {
            addFacesMsg("Erro ao incluir dica! Verifique se fez o Upload da Imagem!", FacesMessage.SEVERITY_ERROR);
        }
    }
}
