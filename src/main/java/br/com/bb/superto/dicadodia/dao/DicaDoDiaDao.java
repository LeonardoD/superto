/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.dicadodia.dao;

import br.com.bb.superto.dao.GenericHibernateDAO;
import br.com.bb.superto.dicadodia.model.DicaDoDia;
import org.hibernate.Query;

import java.io.Serializable;
import java.util.Calendar;
import java.util.List;

/**
 * @author administrador
 */
public class DicaDoDiaDao extends GenericHibernateDAO<DicaDoDia, Serializable> {

    public List<DicaDoDia> getLastDicasdoDia() {
        Calendar hoje = Calendar.getInstance();
        Query q = getSession().createQuery("SELECT a FROM DicaDoDia a WHERE a.visivel = TRUE AND a.dataExpiracao > CURRENT_TIMESTAMP ORDER BY a.dataExpiracao DESC");

        return q.list();
    }
}
