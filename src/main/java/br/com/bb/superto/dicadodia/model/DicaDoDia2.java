/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.dicadodia.model;

import br.com.bb.superto.model.Archive;
import br.com.bb.superto.model.Mercado;
import br.com.bb.superto.model.Operacao;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @author administrador
 */
@Entity
@Table(name = "dica_do_dia2")
public class DicaDoDia2 implements Serializable {

    @Enumerated(value = EnumType.STRING)
    Mercado mercado;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @OneToOne
    @JoinColumn(name = "archive_id")
    private Archive imagem;
    @NotNull(message = "Duração em dias é obrigatória!")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataExpiracao;
    private boolean visivel;

    @OneToOne
    @JoinColumn(name = "opcriacao_id")
    private Operacao operacaoCriacao;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @Fetch(FetchMode.SUBSELECT)
    private List<Operacao> operacaoModificacao;

    public DicaDoDia2() {
        this.visivel = false;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Archive getImagem() {
        return imagem;
    }

    public void setImagem(Archive imagem) {
        this.imagem = imagem;
    }

    public boolean isVisivel() {
        return visivel;
    }

    public void setVisivel(boolean visivel) {
        this.visivel = visivel;
    }

    public Operacao getOperacaoCriacao() {
        return operacaoCriacao;
    }

    public void setOperacaoCriacao(Operacao operacaoCriacao) {
        this.operacaoCriacao = operacaoCriacao;
    }

    public List<Operacao> getOperacaoModificacao() {
        return operacaoModificacao;
    }

    public void setOperacaoModificacao(List<Operacao> operacaoModificacao) {
        this.operacaoModificacao = operacaoModificacao;
    }

    public Date getDataExpiracao() {
        return dataExpiracao;
    }

    public void setDataExpiracao(Date dataExpiracao) {
        this.dataExpiracao = dataExpiracao;
    }

    public Mercado getMercado() {
        return mercado;
    }

    public void setMercado(Mercado mercado) {
        this.mercado = mercado;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DicaDoDia)) {
            return false;
        }
        DicaDoDia other = (DicaDoDia) object;
        //if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
        //    return false;
        //}
        return true;
    }

    @Override
    public String toString() {
        return "DicaDoDia{" + "id=" + id + ", imagem=" + imagem + ", dataExpiracao=" + dataExpiracao + ", mercado=" + mercado + ", visivel=" + visivel + ", operacaoCriacao=" + operacaoCriacao + ", operacaoModificacao=" + operacaoModificacao + '}';
    }
}
