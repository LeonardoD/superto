/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.model;

/**
 * @author maycon
 */
public enum Actions {

    ALL("*", "Todos"),
    EDIT("editar", "Editar"),
    VIEW("visualizar", "Visualizar"),
    DELETE("deletar", "Deletar"),
    ADD("adicionar", "Adicionar"),
    COMMENTS("comentar", "comentar");

    private final String action;
    private final String label;

    private Actions(String action, String label) {
        this.action = action;
        this.label = label;
    }

    public String getAction() {
        return action;
    }

    public String getLabel() {
        return label;
    }
}
