/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.model;

import javax.persistence.*;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Classe em modelo orientado a objeto para o modelo relacional da tabela
 * archive.
 *
 * @author Maycon Antonio Junqueira Costa
 * @version 1.0
 */
@Entity
@Table(name = "archive")
public class Archive implements java.io.Serializable, GenericEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String name;
    private String radomName;
    private String url;
    private String mimeType;
    private long archiveSize;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date uploadDate;
    private String uploadHour;

    /**
     * Referencia ao funcionario que fez o upload do arquivo.
     */
    @ManyToOne
    private Funcionarios userUploaded;

    public Archive() {
    }

    @Override
    public Integer getId() {
        return this.id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }

    public long getArchiveSize() {
        return archiveSize;
    }

    public void setArchiveSize(long archiveSize) {
        this.archiveSize = archiveSize;
    }

    public Date getUploadDate() {
        return uploadDate;
    }

    public void setUploadDate(Date uploadDate) {
        this.uploadDate = uploadDate;
    }

    public String getUploadDateFormat() {
        return new SimpleDateFormat("dd/MM/yyyy").format(uploadDate);
    }

    public Funcionarios getUserUploaded() {
        return userUploaded;
    }

    public void setUserUploaded(Funcionarios userUploaded) {
        this.userUploaded = userUploaded;
    }

    public String getRadomName() {
        return radomName;
    }

    public void setRadomName(String radomName) {
        this.radomName = radomName;
    }

    public String getUploadHour() {
        return uploadHour;
    }

    public void setUploadHour(String uploadHour) {
        this.uploadHour = uploadHour;
    }

    @Override
    public String toString() {
        return "Archive{" + "id=" + id + ", name=" + name + ", radomName=" + radomName + ", url=" + url + ", mimeType=" + mimeType + ", archiveSize=" + archiveSize + ", uploadDate=" + uploadDate + ", uploadHour=" + uploadHour + ", userUploaded=" + userUploaded + '}';
    }

}
