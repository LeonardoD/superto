/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.model;

import javax.persistence.*;

/**
 * @author maycon
 */
@Entity
@Table(name = "roles_permissions")
public class RolesPermissions implements java.io.Serializable, GenericEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer id;
    String permission;
    @ManyToOne(fetch = FetchType.LAZY)
    Roles role;

    public RolesPermissions() {
        this.id = Integer.SIZE;
    }

    public RolesPermissions(String permission, Roles role) {
        this.id = Integer.SIZE;
        this.permission = permission;
        this.role = role;
    }

    @Override
    public Integer getId() {
        return this.id;
    }

    public String getPermission() {
        return permission;
    }

    public void setPermission(String permission) {
        this.permission = permission;
    }

    public Roles getRole() {
        return role;
    }

    public void setRole(Roles role) {
        this.role = role;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 29 * hash + (this.permission != null ? this.permission.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final RolesPermissions other = (RolesPermissions) obj;
        if ((this.permission == null) ? (other.permission != null) : !this.permission.equals(other.permission)) {
            return false;
        }
        return true;
    }
}
