/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.model;

import javax.persistence.*;

/**
 * @author maycon
 */
@Entity
@Table(name = "like_entity")
public class Like implements java.io.Serializable, GenericEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private Integer sourceId;
    @ManyToOne
    private Funcionarios user;
    @Enumerated(value = EnumType.STRING)
    private Mercado category;
    private int likeType;

    @Override
    public Integer getId() {
        return this.id;
    }

    public Integer getSourceId() {
        return sourceId;
    }

    public void setSourceId(Integer sourceId) {
        this.sourceId = sourceId;
    }

    public Funcionarios getUser() {
        return user;
    }

    public void setUser(Funcionarios user) {
        this.user = user;
    }

    public Mercado getCategory() {
        return category;
    }

    public void setCategory(Mercado category) {
        this.category = category;
    }

    public int getLikeType() {
        return likeType;
    }

    public void setLikeType(int likeType) {
        this.likeType = likeType;
    }

}
