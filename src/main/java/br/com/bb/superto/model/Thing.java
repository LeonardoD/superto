package br.com.bb.superto.model;

import com.google.gson.annotations.Expose;
import javax.persistence.*;

@Entity
@Table(name = "thing")
public class Thing implements java.io.Serializable, GenericEntity {
    @Expose
    private String cpf;
    @Expose
    private String banco;
    @Expose
    private String prazo;
    @Expose
    private String parcela;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Expose
    private Integer ID;

    public Thing() {
    }

    public Thing(String cpf, String banco, String prazo, String parcela) {
        this.cpf = cpf;
        this.banco = banco;
        this.prazo = prazo;
        this.parcela = parcela;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getBanco() {
        return banco;
    }

    public void setBanco(String banco) {
        this.banco = banco;
    }

    public String getPrazo() {
        return prazo;
    }

    public void setPrazo(String prazo) {
        this.prazo = prazo;
    }

    public String getParcela() {
        return parcela;
    }

    public void setParcela(String parcela) {
        this.parcela = parcela;
    }

    @Override
    public String toString() {
        return cpf + ";" + banco + ";" + prazo + ";" + parcela + ";";
    }

    public Integer getID() {
        return ID;
    }

    public void setID(Integer ID) {
        this.ID = ID;
    }    
    
    @Override
    public Integer getId() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}