/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.model;

import javax.persistence.*;

/**
 * @author maycon
 */
@Entity
@Table(name = "func_role")
public class FuncionarioRole implements java.io.Serializable, GenericEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer id;

    @ManyToOne
    Roles role;

    @ManyToOne
    Funcionarios funcionario;

    public FuncionarioRole() {
        this.id = Integer.SIZE;
    }

    public FuncionarioRole(Roles role, Funcionarios funcionario) {
        this.id = Integer.SIZE;
        this.role = role;
        this.funcionario = funcionario;
    }

    @Override
    public Integer getId() {
        return this.id;
    }

    public Roles getRole() {
        return role;
    }

    public void setRole(Roles role) {
        this.role = role;
    }

    public Funcionarios getFuncionario() {
        return funcionario;
    }

    public void setFuncionario(Funcionarios funcionario) {
        this.funcionario = funcionario;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 89 * hash + (this.role != null ? this.role.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final FuncionarioRole other = (FuncionarioRole) obj;
        if (this.id != other.id && (this.id == null || !this.id.equals(other.id))) {
            return false;
        }
        if (this.role != other.role && (this.role == null || !this.role.equals(other.role))) {
            return false;
        }
        return true;
    }

}
