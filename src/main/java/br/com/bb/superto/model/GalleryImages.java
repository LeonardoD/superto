/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.model;

import javax.persistence.*;

/**
 * @author maycon
 */
@Entity
@Table(name = "gallery_images")
public class GalleryImages implements java.io.Serializable, GenericEntity {

    @ManyToOne(cascade = CascadeType.REMOVE)
    Gallery gallery;
    @ManyToOne
    Image image;
    Integer posicao;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(columnDefinition = "boolean default false")
    private boolean capa;

    public GalleryImages() {
    }

    @Override
    public Integer getId() {
        return id;
    }

    public Gallery getGallery() {
        return gallery;
    }

    public void setGallery(Gallery gallery) {
        this.gallery = gallery;
    }

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }

    public Integer getPosicao() {
        return posicao;
    }

    public void setPosicao(Integer posicao) {
        this.posicao = posicao;
    }

    public boolean isCapa() {
        return capa;
    }

    public void setCapa(boolean capa) {
        this.capa = capa;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 71 * hash + (this.id != null ? this.id.hashCode() : 0);
        hash = 71 * hash + (this.gallery != null ? this.gallery.hashCode() : 0);
        hash = 71 * hash + (this.image != null ? this.image.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final GalleryImages other = (GalleryImages) obj;
        if (this.id != other.id && (this.id == null || !this.id.equals(other.id))) {
            return false;
        }
        if (this.gallery != other.gallery && (this.gallery == null || !this.gallery.equals(other.gallery))) {
            return false;
        }
        if (this.image != other.image && (this.image == null || !this.image.equals(other.image))) {
            return false;
        }
        return true;
    }

}
