/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.model;

/**
 * @author Administrador
 */
public enum SubCategoria {

    //PF 
    creditoPF("Crédito PF"),
    imob("Imobiliário"),
    segurPF("Seguridade PF"),
    gov("Governo"),
    gestConv("Gestão de Convênios"),
    //PJ
    credPJG("Crédito PJ - Giro"),
    credPJI("Crédito PJ - Investimento"),
    limCred("Limite de Crédito"),
    segurPJ("Seguridade PJ"),
    gestCart("Gestão de Carteiras"),
    cobr("Cobrança"),
    dicasPJ("Dicas PJ"),
    relacionamento("Relacionamento"),
    sinergia("Sinergia"),
    financiamentoAProducao("Financiamento a Produção"),
    comex("Comex"),
    credPJRecebiveis("Crédito PJ Recebíveis"),
    cartoes("Cartões"),
    kitAberturaDeContasPJ("Kit de Abertura de Contas PJ"),
    demaisArq("Demais Arquivos"),
    //Agro
    mpo("MPO"),
    caf("CAF"),
    fies("FIES"),
    pronaf("PRONAF"),
    planDRS("Planos DRS"),
    inadAGRO("Inadimplência AGRO/DRS"),
    credAGRO("Crédito AGRO"),
    cartAgro("Cartão AGRO"),
    segurRural("Seguros Rurais"),
    abcDCA("ABC/DCA"),
    //ADM
    logistica("Logística/Segurança"),
    rating("Rating"),
    adimplencia("Adimplência"),
    funcionalismo("Funcionalismo/Demandas Judiciais"),
    atendimento("Atendimento/Comunicação"),

    //GEATI
    icc("ICC"),
    qualiCred("Qualidade do Crédito"),
    regulFGO("Regularização FGO"),
    ajui("Ajuizamento"),
    inscDAU("Inscrição em DAU"),
    renegDivid("Renegociação de Dívidas"),
    //NUCOM
    lob("LOB"),
    imprensa("Imprensa"),
    patroc("Patrocínio"),
    //Atendimento
    atend("Atendimento"),
    contMov("Contas não Movimentadas"),
    siner("Sinergia"),
    ambienc("Ambiência"),
    avali("Avaliação do Atendimento"),
    //Administrativo
    capacit("Capacitação"),
    administ("Despesas Administrativas"),
    prestCont("Prestação de Contas"),
    procurac("Procurações"),
    judici("Demandas Judiciais"),
    //Rede
    postal("Banco Postal"),
    coban("COBAN MAIS BB"),
    equip("Equipamentos"),
    orfix("ORFIX"),
    sng("SNG"),
    taa("TAA");

    private final String type;

    SubCategoria(String type) {
        this.type = type;
    }

    private String type() {
        return this.type;
    }

    public String getType() {
        return this.type;
    }

    public String asString() {
        return this.type;
    }

}
