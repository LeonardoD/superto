/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.model;

import com.google.gson.annotations.Expose;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

/**
 *
 * @author administrador
 */
@Entity
@Table(name = "clientelob")
public class ClienteLob implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Expose
    private Integer mci;
    
    @Expose
    private String prefixo;
    
    @Expose
    private String numeroCarteira;
    
    @Expose
    private String nome;
    
    @Expose
    private boolean lob;
    
    @Expose
    private boolean contatado;
    
    @Expose
    private String telefone;
    
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    @Expose
    private Date dataContatado;
    @Expose
    private boolean lobAceita;
    
    @Expose
    private String lobMotivo;
    
    @Expose
    private boolean compareceu;
    
    @Expose
    private boolean protocolou;
    
    @Expose
    private String obs;

    @OneToOne
    @JoinColumn(name = "opcriacao_id")
    private Operacao operacaoCriacao;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @Fetch(FetchMode.SUBSELECT)
    private List<Operacao> operacaoModificacao;

    public Integer getMci() {
        return mci;
    }

    public void setMci(Integer mci) {
        this.mci = mci;
    }

    public String getPrefixo() {
        return prefixo;
    }

    public void setPrefixo(String prefixo) {
        this.prefixo = prefixo;
    }

    public String getNumeroCarteira() {
        return numeroCarteira;
    }

    public void setNumeroCarteira(String numeroCarteira) {
        this.numeroCarteira = numeroCarteira;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public boolean isLob() {
        return lob;
    }

    public void setLob(boolean lob) {
        this.lob = lob;
    }

    public boolean isContatado() {
        return contatado;
    }

    public void setContatado(boolean contatado) {
        this.contatado = contatado;
    }

    public Date getDataContatado() {
        return dataContatado;
    }

    public void setDataContatado(Date dataContatado) {
        this.dataContatado = dataContatado;
    }

    public boolean isLobAceita() {
        return lobAceita;
    }

    public void setLobAceita(boolean lobAceita) {
        this.lobAceita = lobAceita;
    }

    public String getLobMotivo() {
        return lobMotivo;
    }

    public void setLobMotivo(String lobMotivo) {
        this.lobMotivo = lobMotivo;
    }

    public boolean isCompareceu() {
        return compareceu;
    }

    public void setCompareceu(boolean compareceu) {
        this.compareceu = compareceu;
    }

    public boolean isProtocolou() {
        return protocolou;
    }

    public void setProtocolou(boolean protocolou) {
        this.protocolou = protocolou;
    }

    public String getObs() {
        return obs;
    }

    public void setObs(String obs) {
        this.obs = obs;
    }

    public Operacao getOperacaoCriacao() {
        return operacaoCriacao;
    }

    public void setOperacaoCriacao(Operacao operacaoCriacao) {
        this.operacaoCriacao = operacaoCriacao;
    }

    public List<Operacao> getOperacaoModificacao() {
        return operacaoModificacao;
    }

    public void setOperacaoModificacao(List<Operacao> operacaoModificacao) {
        this.operacaoModificacao = operacaoModificacao;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }
    
    @Override
    public int hashCode() {
        int hash = 5;
        hash = 43 * hash + Objects.hashCode(this.mci);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ClienteLob other = (ClienteLob) obj;
        if (!Objects.equals(this.mci, other.mci)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ClienteLob{" + "mci=" + mci + ", prefixo=" + prefixo + ", numeroCarteira=" + numeroCarteira + ", nome=" + nome + ", telefone=" + telefone + ", lob=" + lob + ", contatado=" + contatado + ", dataContatado=" + dataContatado + ", lobAceita=" + lobAceita + ", lobMotivo=" + lobMotivo + ", compareceu=" + compareceu + ", protocolou=" + protocolou + ", obs=" + obs + '}';
    }

}
