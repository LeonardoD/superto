/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.model;

import javax.persistence.*;
import java.util.Date;

/**
 * @author maycon
 */
@Entity
@Table(name = "relato")
public class Relatos implements GenericEntity, java.io.Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String relato;
    @OneToOne
    private Funcionarios funcionario;
    private boolean aprovado;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date dataEnvio;

    public Relatos() {
    }

    public Relatos(String relato, Funcionarios funcionario, boolean aprovado) {
        this.relato = relato;
        this.funcionario = funcionario;
        this.aprovado = aprovado;
    }

    @Override
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRelato() {
        return relato;
    }

    public void setRelato(String relato) {
        this.relato = relato;
    }

    public Funcionarios getFuncionario() {
        return funcionario;
    }

    public void setFuncionario(Funcionarios funcionario) {
        this.funcionario = funcionario;
    }

    public boolean isAprovado() {
        return aprovado;
    }

    public void setAprovado(boolean aprovado) {
        this.aprovado = aprovado;
    }

    public Date getDataEnvio() {
        return dataEnvio;
    }

    public void setDataEnvio(Date dataEnvio) {
        this.dataEnvio = dataEnvio;
    }
}
