package br.com.bb.superto.model;

import com.google.gson.annotations.Expose;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Classe em modelo orientado a objeto para o modelo relacional da tabela cargo.
 *
 * @author Jhemeson Silva Mota
 * @version 1.0
 */
@Entity
@Table(name = "corridaMaluca")
public class corridaMaluca implements java.io.Serializable, GenericEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Expose
    private Integer id;
    
    @Expose
    private double pct;
    
    @Expose
    private String nome;

    public corridaMaluca() {
    }

    public corridaMaluca(Integer id, double pct, String nome) {
        this.id = id;
        this.pct = pct;
        this.nome = nome;
    }

    @Override
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public double getPct() {
        return pct;
    }
    
    public double getPosix(){
        double aux =0;
        
        if(pct>=100){
            aux = 100 * 8;
        }
        else{
            aux = pct * 8;
        }
        //quanto mais próximo do 100% maior é o carro
        // filtro do responsável do averbação parece não estar funcionando
        return aux;
    }
    
    public double getWidth(){
        double aux =0;
        
        if(pct < 10){
            aux = 120;
        }
        else if(pct < 20){
            aux = 130;
        }
        else if(pct < 30){
            aux = 140;
        }
        else if(pct < 40){
            aux = 150;
        }
        else if(pct < 50){
            aux = 160;
        }
        else if(pct < 60){
            aux = 170;
        }
        else if(pct < 70){
            aux = 180;
        }
        else if(pct < 80){
            aux = 190;
        }
        else if(pct < 90){
            aux = 200;
        }
        else{
            aux = 210;
        }
        return aux;
    }

    public void setPct(double pct) {
        this.pct = pct;
    }

    public String getNome() {
        System.out.print("Chegamos no GetNome");
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof corridaMaluca)) {
            return false;
        }
        corridaMaluca other = (corridaMaluca) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.bb.superto.model.corridaMaluca[ id=" + id + " ]";
    }
    
}
