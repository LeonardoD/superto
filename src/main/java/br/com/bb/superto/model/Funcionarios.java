/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.model;

import br.com.bb.superto.dao.AgenciasDao;
import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * @author maycon
 */
@Entity
@Table(name = "funcionarios")
public class Funcionarios implements java.io.Serializable, GenericEntity {

    @Id
    private Integer matriculaNum;
    private String matricula;
    private String nome;
    private String fmatricula;
    private Integer prefixo;
    private String nomeGuerra;
    @Column(nullable = false, name = "Status", columnDefinition = "boolean default true")
    private boolean status;

    @ManyToOne(fetch = FetchType.EAGER)
    private Cargos codCargo;

    @Temporal(javax.persistence.TemporalType.DATE)
    private Date dataNascimento;

    @Temporal(javax.persistence.TemporalType.DATE)
    private Date inicioBanco;

    private String grauInstrucao;
    private String estadoCivil;
    private String funcao;

    @Enumerated(EnumType.STRING)
    private Sexo sexo;

    private Long cpf;
    private String cpfNumero;

    @Enumerated(value = EnumType.STRING)
    private Mercado mercado;

    private String assuntosConducao;
    private String foneTrabalho;
    private String foneResidencial;
    private String celular;
    private String foneFax;
    private String emailPessoal;
    private String endereco;
    private String enderecoAlternativo;
    private String atendimentoPf;
    private String atendimentoPj;

    @OneToOne(cascade = CascadeType.REMOVE)
    private Avatar avatar;
    
    @ManyToMany
    @JoinTable(name="assunto_funcionario")
    private List<Assunto> assuntos;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
    
    public Funcionarios() {
        
    }
    
    public Funcionarios(Integer matriculaNum, Integer prefixo, String Fmatricula, String nome, String nomeGuerra, String Funcao, boolean status){
        this.matriculaNum = matriculaNum;
        this.prefixo = prefixo;
        this.fmatricula = Fmatricula;
        this.nome = nome;
        this.nomeGuerra =  nomeGuerra;
        this.funcao =  Funcao;
        this.status = status;
    }

    public Funcionarios(Integer matriculaNum, String matricula, String nome,
                        String fmatricula, Integer prefixo, String nomeGuerra, Cargos codCargo,
                        Date dataNascimento, Date inicioBanco, String grauInstrucao, String estadoCivil,
                        String funcao, Sexo sexo, String cpfNumero, String foneTrabalho, String foneResidencial,
                        String celular, String foneFax, String emailPessoal, String endereco,
                        String enderecoAlternativo, String atendimentoPf, String atendimentoPj) {
        this.matriculaNum = matriculaNum;
        this.matricula = matricula;
        this.nome = nome;
        this.fmatricula = fmatricula;
        this.prefixo = prefixo;
        this.nomeGuerra = nomeGuerra;
        this.codCargo = codCargo;
        this.dataNascimento = dataNascimento;
        this.inicioBanco = inicioBanco;
        this.grauInstrucao = grauInstrucao;
        this.estadoCivil = estadoCivil;
        this.funcao = funcao;
        this.sexo = sexo;
        this.cpfNumero = cpfNumero;
        this.foneTrabalho = foneTrabalho;
        this.foneResidencial = foneResidencial;
        this.celular = celular;
        this.foneFax = foneFax;
        this.emailPessoal = emailPessoal;
        this.endereco = endereco;
        this.enderecoAlternativo = enderecoAlternativo;
        this.atendimentoPf = atendimentoPf;
        this.atendimentoPj = atendimentoPj;
    }

    public String getCpfNumero() {
        return cpfNumero;
    }

    public void setCpfNumero(String cpfNumero) {
        this.cpfNumero = cpfNumero;
    }

    public String getAssuntosConducao() {
        return assuntosConducao;
    }

    public void setAssuntosConducao(String assuntosConducao) {
        this.assuntosConducao = assuntosConducao;
    }

    public Mercado getMercado() {
        return mercado;
    }

    public void setMercado(Mercado mercado) {
        this.mercado = mercado;
    }

    public Integer getMatriculaNum() {
        return matriculaNum;
    }

    public void setMatriculaNum(Integer matriculaNum) {
        this.matriculaNum = matriculaNum;
    }

    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getFmatricula() {
        return fmatricula;
    }

    public void setFmatricula(String fmatricula) {
        this.fmatricula = fmatricula;
    }

    public Integer getPrefixo() {
        return prefixo;
    }

    public void setPrefixo(Integer prefixo) {
        this.prefixo = prefixo;
    }

    public String getNomeGuerra() {
        return nomeGuerra;
    }

    public void setNomeGuerra(String nomeGuerra) {
        this.nomeGuerra = nomeGuerra;
    }

    public Cargos getCodCargo() {
        return codCargo;
    }

    public void setCodCargo(Cargos codCargo) {
        this.codCargo = codCargo;
    }

    public Date getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(Date dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    public Date getInicioBanco() {
        return inicioBanco;
    }

    public void setInicioBanco(Date inicioBanco) {
        this.inicioBanco = inicioBanco;
    }

    public String getGrauInstrucao() {
        return grauInstrucao;
    }

    public void setGrauInstrucao(String grauInstrucao) {
        this.grauInstrucao = grauInstrucao;
    }

    public String getEstadoCivil() {
        return estadoCivil;
    }

    public void setEstadoCivil(String estadoCivil) {
        this.estadoCivil = estadoCivil;
    }

    public String getFuncao() {
        return funcao;
    }

    public void setFuncao(String funcao) {
        this.funcao = funcao;
    }

    public Sexo getSexo() {
        return sexo;
    }

    public void setSexo(Sexo sexo) {
        this.sexo = sexo;
    }

    public Long getCpf() {
        return cpf;
    }

    public void setCpf(Long cpf) {
        this.cpf = cpf;
    }

    public String getFoneTrabalho() {
        return foneTrabalho;
    }

    public void setFoneTrabalho(String foneTrabalho) {
        this.foneTrabalho = foneTrabalho;
    }

    public String getFoneResidencial() {
        return foneResidencial;
    }

    public void setFoneResidencial(String foneResidencial) {
        this.foneResidencial = foneResidencial;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public String getFoneFax() {
        return foneFax;
    }

    public void setFoneFax(String foneFax) {
        this.foneFax = foneFax;
    }

    public String getEmailPessoal() {
        return emailPessoal;
    }

    public void setEmailPessoal(String emailPessoal) {
        this.emailPessoal = emailPessoal;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public String getEnderecoAlternativo() {
        return enderecoAlternativo;
    }

    public void setEnderecoAlternativo(String enderecoAlternativo) {
        this.enderecoAlternativo = enderecoAlternativo;
    }

    public String getAtendimentoPf() {
        return atendimentoPf;
    }

    public void setAtendimentoPf(String atendimentoPf) {
        this.atendimentoPf = atendimentoPf;
    }

    public String getAtendimentoPj() {
        return atendimentoPj;
    }

    public void setAtendimentoPj(String atendimentoPj) {
        this.atendimentoPj = atendimentoPj;
    }

    public Avatar getAvatar() {
        return avatar;
    }

    public void setAvatar(Avatar avatar) {
        this.avatar = avatar;
    }

    public String getURL() {
        return avatar.getImagem().getUrl();
    }

    public List<Assunto> getAssuntos() {
        return assuntos;
    }

    public void setAssuntos(List<Assunto> assuntos) {
        this.assuntos = assuntos;
    }
    

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 29 * hash + (this.matriculaNum != null ? this.matriculaNum.hashCode() : 0);
        hash = 29 * hash + (this.nome != null ? this.nome.hashCode() : 0);
        hash = 29 * hash + (this.fmatricula != null ? this.fmatricula.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Funcionarios other = (Funcionarios) obj;
        if (this.matriculaNum != other.matriculaNum && (this.matriculaNum == null || !this.matriculaNum.equals(other.matriculaNum))) {
            return false;
        }
        return true;
    }

    @Override
    public Integer getId() {
        return matriculaNum;
    }

    @Override
    public String toString() {
        return "Funcionarios{" + "matriculaNum=" + matriculaNum + ", matricula=" + matricula + ", nome=" + nome + ", fmatricula=" + fmatricula + ", prefixo=" + prefixo + ", nomeGuerra=" + nomeGuerra + ", codCargo=" + codCargo + ", dataNascimento=" + dataNascimento + ", inicioBanco=" + inicioBanco + ", grauInstrucao=" + grauInstrucao + ", estadoCivil=" + estadoCivil + ", funcao=" + funcao + ", sexo=" + sexo + ", cpf=" + cpf + ", cpfNumero=" + cpfNumero + ", mercado=" + mercado + ", assuntosConducao=" + assuntosConducao + ", foneTrabalho=" + foneTrabalho + ", foneResidencial=" + foneResidencial + ", celular=" + celular + ", foneFax=" + foneFax + ", emailPessoal=" + emailPessoal + ", endereco=" + endereco + ", enderecoAlternativo=" + enderecoAlternativo + ", atendimentoPf=" + atendimentoPf + ", atendimentoPj=" + atendimentoPj + ", avatar=" + avatar + '}';
    }
    
        
    private Agencias agen;




	public String pegaNomeAgencia(Integer prefixo) {
            AgenciasDao bdAgencia = new AgenciasDao();
            agen = bdAgencia.getAgenciaByPrefixo(prefixo);
            return agen.getNomeAgencia();
    	}  
}
