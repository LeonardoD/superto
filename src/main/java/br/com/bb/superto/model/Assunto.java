/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.model;

import java.util.List;
import java.util.Objects;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

/**
 *
 * @author administrador
 */
@Entity
public class Assunto implements java.io.Serializable, GenericEntity {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer ID;
    private String descricao;

    @JoinColumn(name = "id_mercado", referencedColumnName = "id")
    @ManyToOne(optional = false, cascade = CascadeType.ALL)
    private MercadoGeral mercadogeral;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "assunto")
    private List<ProdutoGeral> produtos;    
    
    @ManyToMany(mappedBy = "assuntos")
    private List<Funcionarios> funcionarios;

    public List<Funcionarios> getFuncionarios() {
        return funcionarios;
    }

    public void setFuncionarios(List<Funcionarios> funcionarios) {
        this.funcionarios = funcionarios;
    }    
   
    @Override
    public Integer getId() {
        return ID;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public List<ProdutoGeral> getProdutos() {
        return produtos;
    }

    public void setProdutos(List<ProdutoGeral> produtos) {
        this.produtos = produtos;
    }

    public MercadoGeral getMercadogeral() {
        return mercadogeral;
    }

    public void setMercadogeral(MercadoGeral mercadogeral) {
        this.mercadogeral = mercadogeral;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.ID);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Assunto other = (Assunto) obj;
        if (!Objects.equals(this.ID, other.ID)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Assunto{" + "ID=" + ID + ", descricao=" + descricao + ", mercadogeral=" + mercadogeral + ", produtos=" + produtos + '}';
    }
    
}
