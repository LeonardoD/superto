package br.com.bb.superto.model;

import com.google.gson.annotations.Expose;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "produtoseguridade")
public class ProdutoSeguridade implements java.io.Serializable, GenericEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Expose
    private Integer codProduto;
    
    @Expose
    private String nomeProduto;
    
    @Expose
    private String Bloco;
    
    public ProdutoSeguridade() {
    }

    public ProdutoSeguridade(Integer codProduto, String nomeProduto, String Bloco) {
        this.codProduto = codProduto;
        this.nomeProduto = nomeProduto;
        this.Bloco = Bloco;
    }

    public ProdutoSeguridade(String nomeProduto, String Bloco) {
        this.nomeProduto = nomeProduto;
        this.Bloco = Bloco;
    }

    public Integer getCodProduto() {
        return codProduto;
    }

    public void setCodProduto(Integer codProduto) {
        this.codProduto = codProduto;
    }

    public String getNomeProduto() {
        return nomeProduto;
    }

    public void setNomeProduto(String nomeProduto) {
        this.nomeProduto = nomeProduto;
    }

    public String getBloco() {
        return Bloco;
    }

    public void setBloco(String Bloco) {
        this.Bloco = Bloco;
    }
   
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codProduto != null ? codProduto.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof ProdutoSeguridade)) {
            return false;
        }
        ProdutoSeguridade other = (ProdutoSeguridade) object;
        if ((this.codProduto == null && other.codProduto != null) || (this.codProduto != null && !this.codProduto.equals(other.codProduto))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.bb.superto.model.clienteSeguridade[ id=" + codProduto + " ]";
    }
    
    public String getInfo()
    {
        return codProduto + " - " + nomeProduto;
    }

    @Override
    public Integer getId() {
        return codProduto;
    }
    
}
