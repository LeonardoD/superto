/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.model;

/**
 * Interface que define um metodo que retorna um objeto do tipo
 *
 * @author maycon
 * @see Funcionarios {@link Funcionarios#celular}
 */
public interface FuncionarioInterface {

    public Funcionarios getFuncionario();
}
