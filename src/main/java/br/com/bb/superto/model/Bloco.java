package br.com.bb.superto.model;

import com.google.gson.annotations.Expose;
import javax.persistence.*;

/**
 * Classe em modelo orientado a objeto para o modelo relacional da tabela.
 *
 * @author Jhemeson S. Mota
 * @version 1.0
 */
@Entity
@Table(name = "blocos")
public class Bloco implements java.io.Serializable, GenericEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Expose
    private Integer id;
    
    @Expose
    private String nome;

    public Bloco() {
    }
    

    @Override
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Bloco(Integer id, String nome) {
        this.id = id;
        this.nome = nome;
    }

    public Bloco(String nome) {
        this.nome = nome;
    }

    
    @Override
    public String toString() {
        return "Bloco{" + "id=" + id + ", nome=" + nome + '}';
    }
    

}
