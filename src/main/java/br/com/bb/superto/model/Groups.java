/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.model;

import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author maycon
 */
@Entity
@Table(name = "groups")
public class Groups implements java.io.Serializable, GenericEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer id;
    @NotEmpty(message = "Campo nome do Grupo não pode estar vazio!")
    String groupName;
    @OneToMany(mappedBy = "group", fetch = FetchType.LAZY)
    private List<GroupRole> groupRoles;

    public Groups() {
    }

    public Groups(String groupName) {
        this.groupName = groupName;
    }

    @Override
    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public List<GroupRole> getGroupRoles() {
        return groupRoles;
    }

    public void setGroupRoles(List<GroupRole> groupRoles) {
        this.groupRoles = groupRoles;
    }

    public List<Roles> getRoles() {
        List<Roles> roles = new ArrayList<>();
        groupRoles.stream().forEach((r) -> {
            roles.add(r.getRole());
        });
        return roles;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 41 * hash + (this.groupName != null ? this.groupName.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Groups other = (Groups) obj;
        if ((this.groupName == null) ? (other.groupName != null) : !this.groupName.equals(other.groupName)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return this.id + "";
    }

}
