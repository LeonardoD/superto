package br.com.bb.superto.model;

import br.com.bb.superto.dao.ClienteProdutoDao;
import com.google.gson.annotations.Expose;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Classe em modelo orientado a objeto para o modelo relacional da tabela cargo.
 *
 * @author Jhemeson Silva Mota
 * @version 1.0
 */
@Entity
@Table(name = "clienteSeguridade")
public class clienteSeguridade implements java.io.Serializable, GenericEntity {

    @Id
    @Expose
    private Integer mci;
    
    @Expose
    private String numCarteira;
    
    @Expose
    private String telefone;
    
    @Expose
    private String prefixo;
    
    @Expose
    private String nomeCliente;

    
    public clienteSeguridade() {
    }

    public clienteSeguridade(Integer mci, String numCarteira, String telefone, String prefixo, String nomeCliente) {
        this.mci = mci;
        this.numCarteira = numCarteira;
        this.telefone = telefone;
        this.prefixo = prefixo;
        this.nomeCliente = nomeCliente;
    }

    public Integer getMci() {
        return mci;
    }

    public void setMci(Integer mci) {
        this.mci = mci;
    }

    public String getNumCarteira() {
        return numCarteira;
    }

    public void setNumCarteira(String numCarteira) {
        this.numCarteira = numCarteira;
    }

    public String getTelefone() {
        try{
            return (telefone.replace(";", "; "));
        }
        catch(Exception ex){
            System.out.print("Erro: " + ex);
            return "";
        }
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getPrefixo() {
        return prefixo;
    }

    public void setPrefixo(String prefixo) {
        this.prefixo = prefixo;
    }

    public String getNomeCliente() {
        return nomeCliente;
    }

    public void setNomeCliente(String nomeCliente) {
        this.nomeCliente = nomeCliente;
    }
    
    public boolean possuiVincendos(int mci){
        return new ClienteProdutoDao().PossuiVincendos(mci);
    }
   
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (mci != null ? mci.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof clienteSeguridade)) {
            return false;
        }
        clienteSeguridade other = (clienteSeguridade) object;
        if ((this.mci == null && other.mci != null) || (this.mci != null && !this.mci.equals(other.mci))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.bb.superto.model.clienteSeguridade[ id=" + mci + " ]";
    }

    @Override
    public Integer getId() {
        return mci;
    }
    
}
