/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.model;

import java.util.List;

/**
 * @author maycon
 */
public class OrganogramaList {

    Funcionarios func;
    List<OrganogramaList> childrens;

    public OrganogramaList() {
    }

    public OrganogramaList(Funcionarios func) {
        this.func = func;
    }

    public Funcionarios getFunc() {
        return func;
    }

    public void setFunc(Funcionarios func) {
        this.func = func;
    }

    public List<OrganogramaList> getChildrens() {
        return childrens;
    }

    public void setChildrens(List<OrganogramaList> childrens) {
        this.childrens = childrens;
    }

}
