/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.model;

import org.apache.shiro.authz.Permission;
import org.apache.shiro.authz.permission.WildcardPermission;

import java.util.ArrayList;
import java.util.List;

/**
 * @author maycon
 */
public class UserFuncionario implements java.io.Serializable {

    UserHistory userHistory;
    List<Permission> prms;
    private Funcionarios funcionario;
    private List<String> roles;
    private List<String> permissions;

    public UserFuncionario() {
        roles = new ArrayList<>();
        permissions = new ArrayList<>();
    }

    public UserFuncionario(Funcionarios funcionario, List<String> roles, List<String> permissions) {
        this.funcionario = funcionario;
        this.roles = roles;
        this.permissions = permissions;
    }

    public Funcionarios getFuncionario() {
        return funcionario;
    }

    public void setFuncionario(Funcionarios funcionario) {
        this.funcionario = funcionario;
    }

    public List<String> getRoles() {
        return roles;
    }

    public void setRoles(List<String> roles) {
        this.roles = roles;
    }

    public List<String> getPermissions() {
        return permissions;
    }

    public void setPermissions(List<String> permissions) {
        this.permissions = permissions;
    }

    public List<Permission> getWildPermissions() {
        if (prms == null) {
            prms = new ArrayList<>();
            for (String pr : permissions) {
                prms.add(new WildcardPermission(pr));
            }
        }
        return prms;
    }

    public UserHistory getUserHistory() {
        return userHistory;
    }

    public void setUserHistory(UserHistory userHistory) {
        this.userHistory = userHistory;
    }

}
