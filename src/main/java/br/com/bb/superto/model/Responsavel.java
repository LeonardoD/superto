/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.model;

import javax.persistence.*;

/**
 * @author Administrador
 */
@Entity
@Table(name = "responsavel")
public class Responsavel implements java.io.Serializable, GenericEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer id;
    Funcionarios funcionarioResponsavel;
    @Enumerated(value = EnumType.STRING)
    SubCategoria subcategoria;

    public Responsavel() {
    }

    public Responsavel(Funcionarios funcionario, SubCategoria subCategoria) {
        this.funcionarioResponsavel = funcionario;
        this.subcategoria = subCategoria;
    }

    @Override
    public Integer getId() {
        return id;
    }

    public Funcionarios getFuncionarioResponsavel() {
        return funcionarioResponsavel;
    }

    public void setFuncionarioResponsavel(Funcionarios funcionarioResponsavel) {
        this.funcionarioResponsavel = funcionarioResponsavel;
    }

    public SubCategoria getSubcategoria() {
        return subcategoria;
    }

    public void setSubcategoria(SubCategoria subcategoria) {
        this.subcategoria = subcategoria;
    }
}
