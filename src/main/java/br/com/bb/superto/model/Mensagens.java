/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.model;

import javax.persistence.*;
import java.util.Date;

/**
 * @author maycon
 */
@Entity
@Table(name = "mensagens")
public class Mensagens implements GenericEntity, java.io.Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne(fetch = FetchType.LAZY)
    private Funcionarios remetente;

    @ManyToOne(fetch = FetchType.LAZY)
    private Funcionarios destinatario;
    private String mensagem;
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date dataDeEnvio;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date dataDeCriacao;
    private boolean visualizada;
    private boolean despachada;

    @Enumerated(value = EnumType.STRING)
    private MensagemType tipo;

    public Mensagens() {
        id = Integer.SIZE;
    }

    @Override
    public Integer getId() {
        return id;
    }

    public Funcionarios getRemetente() {
        return remetente;
    }

    public void setRemetente(Funcionarios remetente) {
        this.remetente = remetente;
    }

    public Funcionarios getDestinatario() {
        return destinatario;
    }

    public void setDestinatario(Funcionarios destinatario) {
        this.destinatario = destinatario;
    }

    public String getMensagem() {
        return mensagem;
    }

    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }

    public Date getDataDeEnvio() {
        return dataDeEnvio;
    }

    public void setDataDeEnvio(Date dataDeEnvio) {
        this.dataDeEnvio = dataDeEnvio;
    }

    public boolean isDespachada() {
        return despachada;
    }

    public void setDespachada(boolean despachada) {
        this.despachada = despachada;
    }

    public Date getDataDeCriacao() {
        return dataDeCriacao;
    }

    public void setDataDeCriacao(Date dataDeCriacao) {
        this.dataDeCriacao = dataDeCriacao;
    }

    public boolean isVisualizada() {
        return visualizada;
    }

    public void setVisualizada(boolean visualizada) {
        this.visualizada = visualizada;
    }

    public MensagemType getTipo() {
        return tipo;
    }

    public void setTipo(MensagemType tipo) {
        this.tipo = tipo;
    }

}
