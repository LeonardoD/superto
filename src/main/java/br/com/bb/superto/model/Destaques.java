/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.model;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

/**
 * @author maycon
 */
@Entity
@Table(name = "destaques")
public class Destaques implements java.io.Serializable, GenericEntity {

    @Enumerated(value = EnumType.STRING)
    Mercado mercado;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String titulo;
    private String descricao;
    private boolean visivel;
    @OneToOne
    @JoinColumn(name = "noticia_id")
    private News noticia;
    @OneToOne
    @JoinColumn(name = "image_id")
    private Image imagem;
    @OneToOne
    @JoinColumn(name = "opcriacao_id")
    private Operacao operacaoCriacao;
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @Fetch(FetchMode.SUBSELECT)
    private List<Operacao> operacaoModificacao;
    
    @Column(columnDefinition = "TEXT")
    private String url;


    public Destaques() {
    }

    @Override
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public News getNoticia() {
        return noticia;
    }

    public void setNoticia(News noticia) {
        this.noticia = noticia;
        this.noticia.setDestaque(this);
    }

    public Image getImagem() {
        return imagem;
    }

    public void setImagem(Image imagem) {
        this.imagem = imagem;
    }

    public boolean isVisivel() {
        return visivel;
    }

    public void setVisivel(boolean visivel) {
        this.visivel = visivel;
    }

    public Operacao getOperacaoCriacao() {
        return operacaoCriacao;
    }

    public void setOperacaoCriacao(Operacao operacaoCriacao) {
        this.operacaoCriacao = operacaoCriacao;
    }

    public List<Operacao> getOperacaoModificacao() {
        return operacaoModificacao;
    }

    public void setOperacaoModificacao(List<Operacao> operacaoModificacao) {
        this.operacaoModificacao = operacaoModificacao;
    }

    public Mercado getMercado() {
        return mercado;
    }

    public void setMercado(Mercado mercado) {
        this.mercado = mercado;
    }
    
    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }    

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 37 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Destaques other = (Destaques) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Destaques{" + "id=" + id + ", titulo=" + titulo + ", descricao=" + descricao + ", visivel=" + visivel + ", noticia=" + noticia + ", imagem=" + imagem + ", operacaoCriacao=" + operacaoCriacao + ", operacaoModificacao=" + operacaoModificacao + ", mercado=" + mercado + '}';
    }

}
