/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * @author administrador
 */
@Entity
@Table(name = "operacao")
public class Operacao implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer ID;

    @Temporal(TemporalType.TIMESTAMP)
    private Date dataOperacao;

    @Column(columnDefinition = "TEXT")
    private String jsonObj;

    @OneToOne
    private Funcionarios autor;

    @OneToOne
    @JoinColumn(name = "agencia_id")
    private Agencias agenciaDeOrigem;

    public Operacao() {
    }

    public Integer getID() {
        return ID;
    }

    public void setID(Integer ID) {
        this.ID = ID;
    }

    public Date getDataOperacao() {
        return dataOperacao;
    }

    public void setDataOperacao(Date dataOperacao) {
        this.dataOperacao = dataOperacao;
    }

    public Funcionarios getAutor() {
        return autor;
    }

    public void setAutor(Funcionarios autor) {
        this.autor = autor;
    }

    public Agencias getAgencias() {
        return agenciaDeOrigem;
    }

    public void setAgencias(Agencias agenciaDeOrigem) {
        this.agenciaDeOrigem = agenciaDeOrigem;
    }

    public String getJsonObj() {
        return jsonObj;
    }

    public void setJsonObj(String jsonObj) {
        this.jsonObj = jsonObj;
    }

    public Agencias getAgenciaDeOrigem() {
        return agenciaDeOrigem;
    }

    public void setAgenciaDeOrigem(Agencias agenciaDeOrigem) {
        this.agenciaDeOrigem = agenciaDeOrigem;
    }
}
