/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.model;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

/**
 * @author maycon
 */
@Entity
@Table(name = "news")
public class News implements java.io.Serializable, GenericEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String title;
    private String urlTitle;
    private String subtitle;
    @Column(columnDefinition = "TEXT")
    private String text;


    @OneToOne
    @JoinColumn(name = "opcriacao_id")
    private Operacao operacaoCriacao;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @Fetch(FetchMode.SUBSELECT)
    private List<Operacao> operacaoModificacao;

    @Enumerated(value = EnumType.STRING)
    private Mercado mercado;

    private boolean visible;

    @OneToOne
    @JoinColumn(name = "image_id")
    private Image capa;

    @OneToOne(mappedBy = "noticia")
    private Destaques destaque;

    private int qtVisualizacoes;
    
    public News() {
        this.qtVisualizacoes = 1;
    }

    @Override
    public Integer getId() {
        return this.id;
    }

    public int getQtVisualizacoes() {
        return qtVisualizacoes;
    }

    public void setQtVisualizacoes() {
        this.qtVisualizacoes = this.qtVisualizacoes + 1;
        System.out.println("Qt Visualizações =" + this.qtVisualizacoes);
    }
    
    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrlTitle() {
        return urlTitle;
    }

    public void setUrlTitle(String urlTitle) {
        this.urlTitle = urlTitle;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Mercado getMercado() {
        return mercado;
    }

    public void setMercado(Mercado mercado) {
        this.mercado = mercado;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public Image getCapa() {
        return capa;
    }

    public void setCapa(Image capa) {
        this.capa = capa;
    }

    public Destaques getDestaque() {
        return destaque;
    }

    public void setDestaque(Destaques destaque) {
        this.destaque = destaque;
    }

    public Operacao getOperacaoCriacao() {
        return operacaoCriacao;
    }

    public void setOperacaoCriacao(Operacao operacaoCriacao) {
        this.operacaoCriacao = operacaoCriacao;
    }

    public List<Operacao> getOperacaoModificacao() {
        return operacaoModificacao;
    }

    public void setOperacaoModificacao(List<Operacao> operacaoModificacao) {
        this.operacaoModificacao = operacaoModificacao;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 67 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final News other = (News) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "News{" + "id=" + id + ", title=" + title + ", subtitle=" + subtitle + ", text=" + text + ", operacaoCriacao=" + operacaoCriacao + ", operacaoModificacao=" + operacaoModificacao + ", category=" + mercado + ", visible=" + visible + ", capa=" + capa + ", destaque=" + destaque + '}';
    }

}
