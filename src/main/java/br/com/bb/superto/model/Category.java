/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.model;

/**
 * @author administrador
 */
public enum Category {

    NEWS("NEWS", "NEWS");

    private final String action;
    private final String label;

    Category(String action, String label) {
        this.action = action;
        this.label = label;
    }

    public String getAction() {
        return action;
    }

    public String getLabel() {
        return label;
    }
}
