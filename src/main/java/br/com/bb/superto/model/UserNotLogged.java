package br.com.bb.superto.model;

/**
 * @author Maycon Antonio Junqueira Costa
 * @version 1.0
 */
public class UserNotLogged extends Exception {

    public UserNotLogged(String message) {
        super(message);
    }

}
