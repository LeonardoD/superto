/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.model;

/**
 * @author maycon
 */
public enum MensagemType {

    ATENCAO("attention", "Atenção"),
    SUCESSO("success", "Sucesso"),
    INFORMACAO("information", "Informação"),
    PERIGO("warning", "Problema");

    private final String value;
    private final String label;

    private MensagemType(String value, String label) {
        this.value = value;
        this.label = label;
    }

    public String getValue() {
        return value;
    }

    public String getLabel() {
        return label;
    }
}
