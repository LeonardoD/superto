package br.com.bb.superto.model;

import com.google.gson.annotations.Expose;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

/**
 * Classe em modelo orientado a objeto para o modelo relacional da tabela.
 *
 * @author Jhemeson Silva Mota
 * @version 1.0
 */
@Entity
@Table(name = "clienteproduto")
public class ClienteProduto implements java.io.Serializable, GenericEntity {

    @Expose
    private Integer mci;
    
    @Expose
    private Integer codproduto;
    
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    @Expose
    private Date datavencimento;
    
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    @Expose
    private Date datainiciovigencia;
    
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    @Expose
    private Date datacancelamento;

    @Expose
    private String descricaocancelamento;
    
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    @Expose
    private Date vistoporultimo;
    
    @Expose
    private boolean contatado;
    
    @Expose
    private boolean contratado;
    
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    @Expose
    private Date datacontatado;
    
    @Expose
    private boolean cancelado;
    
    @Expose
    private boolean status;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Expose
    private Integer id;
    
    @Expose
    private Double premio;
    
    @Expose
    private Integer prefixo;
    
    @Expose
    private Integer proposta;
    
    @Expose
    private boolean manual;
    
    @OneToOne
    @JoinColumn(name = "opcriacao_id")
    private Operacao operacaoCriacao;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @Fetch(FetchMode.SUBSELECT)
    private List<Operacao> operacaoModificacao;

    private String dataInicioVigenciaFormatada;
    
    
    public ClienteProduto() {
    }

    public ClienteProduto(Integer mci, Integer codproduto, Date datavencimento, Date datainiciovigencia, Date datacancelamento, String descricaocancelamento, Date vistoporultimo, boolean contatado, boolean contratado, Date datacontatado, boolean cancelado, boolean status, Integer id, Double premio, Integer prefixo, Integer proposta, Operacao operacaoCriacao, List<Operacao> operacaoModificacao) {
        this.mci = mci;
        this.codproduto = codproduto;
        this.datavencimento = datavencimento;
        this.datainiciovigencia = datainiciovigencia;
        this.datacancelamento = datacancelamento;
        this.descricaocancelamento = descricaocancelamento;
        this.vistoporultimo = vistoporultimo;
        this.contatado = contatado;
        this.contratado = contratado;
        this.datacontatado = datacontatado;
        this.cancelado = cancelado;
        this.status = status;
        this.id = id;
        this.premio = premio;
        this.prefixo = prefixo;
        this.proposta = proposta;
        this.operacaoCriacao = operacaoCriacao;
        this.operacaoModificacao = operacaoModificacao;
    }

    public boolean isManual() {
        return manual;
    }

    public void setManual(boolean manual) {
        this.manual = manual;
    }
    
    

    public Integer getMci() {
        return mci;
    }

    public void setMci(Integer mci) {
        this.mci = mci;
    }

    public Integer getCodproduto() {
        return codproduto;
    }

    public Integer getProposta() {
        return proposta;
    }

    public void setProposta(Integer proposta) {
        this.proposta = proposta;
    }

    
    public void setCodproduto(Integer codproduto) {
        this.codproduto = codproduto;
    }

    public Date getDatavencimento() {
        return datavencimento;
    }

    public void setDatavencimento(Date datavencimento) {
        this.datavencimento = datavencimento;
    }

    public Date getDatainiciovigencia() throws ParseException {
        return datainiciovigencia;
    }

    public String getDataInicioVigenciaFormatada() {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        dataInicioVigenciaFormatada = sdf.format(this.datainiciovigencia);
        return dataInicioVigenciaFormatada;
    }

    public void setDataInicioVigenciaFormatada(String dataInicioVigenciaFormatada) {
        this.dataInicioVigenciaFormatada = dataInicioVigenciaFormatada;
    }
    
    public void setDatainiciovigencia(Date datainiciovigencia) {
        this.datainiciovigencia = datainiciovigencia;
    }

    public Date getDatacancelamento() {
        return datacancelamento;
    }

    public void setDatacancelamento(Date datacancelamento) {
        this.datacancelamento = datacancelamento;
    }

    public String getDescricaocancelamento() {
        return descricaocancelamento;
    }

    public void setDescricaocancelamento(String descricaocancelamento) {
        this.descricaocancelamento = descricaocancelamento;
    }

    public Date getVistoporultimo() {
        return vistoporultimo;
    }

    public void setVistoporultimo(Date vistoporultimo) {
        this.vistoporultimo = vistoporultimo;
    }

    public boolean isContatado() {
        return contatado;
    }

    public void setContatado(boolean contatado) {
        this.contatado = contatado;
    }

    public boolean isContratado() {
        return contratado;
    }

    public void setContratado(boolean contratado) {
        this.contratado = contratado;
    }

    public Date getDatacontatado() {
        if(datacontatado == null){
            return new Date();
        }
        else{
            return datacontatado;        
        }
    }

    public void setDatacontatado(Date datacontatado) {
        this.datacontatado = datacontatado;
    }

    public boolean isCancelado() {
        return cancelado;
    }

    public void setCancelado(boolean cancelado) {
        this.cancelado = cancelado;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public Double getPremio() {
        return premio;
    }

    public void setPremio(Double premio) {
        this.premio = premio;
    }

    public Integer getPrefixo() {
        return prefixo;
    }

    public void setPrefixo(Integer prefixo) {
        this.prefixo = prefixo;
    }

    public Operacao getOperacaoCriacao() {
        return operacaoCriacao;
    }

    public void setOperacaoCriacao(Operacao operacaoCriacao) {
        this.operacaoCriacao = operacaoCriacao;
    }

    public List<Operacao> getOperacaoModificacao() {
        return operacaoModificacao;
    }

    public void setOperacaoModificacao(List<Operacao> operacaoModificacao) {
        this.operacaoModificacao = operacaoModificacao;
    }
    
    

    public ClienteProduto(Integer mci, Integer codproduto, Date datavencimento, Date datainiciovigencia, Date datacancelamento, String descricaocancelamento, Date vistoporultimo, boolean contatado, boolean contratado, Date datacontatado, boolean cancelado, boolean status, Integer id, Double premio, Integer prefixo) {
        this.mci = mci;
        this.codproduto = codproduto;
        this.datavencimento = datavencimento;
        this.datainiciovigencia = datainiciovigencia;
        this.datacancelamento = datacancelamento;
        this.descricaocancelamento = descricaocancelamento;
        this.vistoporultimo = vistoporultimo;
        this.contatado = contatado;
        this.contratado = contratado;
        this.datacontatado = datacontatado;
        this.cancelado = cancelado;
        this.status = status;
        this.id = id;
        this.premio = premio;
        this.prefixo = prefixo;
    }

   
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof ClienteProduto)) {
            return false;
        }
        ClienteProduto other = (ClienteProduto) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.bb.superto.model.clienteSeguridade[ id=" + id + " ]";
    }

    @Override
    public Integer getId() {
        return id;
    }
    
}
