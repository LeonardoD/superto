/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.model;

import org.primefaces.model.DefaultStreamedContent;

import javax.faces.context.FacesContext;
import javax.persistence.*;
import javax.servlet.ServletContext;
import java.io.InputStream;
import java.util.List;
import java.util.Objects;

/**
 * @author maycon
 */
@Entity
@Table(name = "reports")
public class Reports implements java.io.Serializable, GenericEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String title;
    private String description;
    @Enumerated(value = EnumType.STRING)
    private Mercado mercado;
    @Enumerated(value = EnumType.STRING)
    private SubCategoria subCategoria;
    private boolean visible;
    @OneToOne
    private Archive report;
    @OneToOne
    @JoinColumn(name = "opcriacao_id")
    private Operacao operacaoCriacao;
    @OneToMany
    private List<Operacao> operacaoModificacao;

    public Reports() {
    }

    @Override
    public Integer getId() {
        return this.id;
    }

    public SubCategoria getSubCategoria() {
        return subCategoria;
    }

    public void setSubCategoria(SubCategoria subCategoria) {
        this.subCategoria = subCategoria;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Mercado getMercado() {
        return mercado;
    }

    public void setMercado(Mercado mercado) {
        this.mercado = mercado;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public Archive getReport() {
        return report;
    }

    public void setReport(Archive report) {
        this.report = report;
    }

    public Operacao getOperacaoCriacao() {
        return operacaoCriacao;
    }

    public void setOperacaoCriacao(Operacao operacaoCriacao) {
        this.operacaoCriacao = operacaoCriacao;
    }

    public List<Operacao> getOperacaoModificacao() {
        return operacaoModificacao;
    }

    public void setOperacaoModificacao(List<Operacao> operacaoModificacao) {
        this.operacaoModificacao = operacaoModificacao;
    }

    public DefaultStreamedContent getFileStream() {
        InputStream stream = ((ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext()).getResourceAsStream("/" + this.report.getUrl());
        return new DefaultStreamedContent(stream, this.report.getMimeType(), this.report.getName());
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 61 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Reports other = (Reports) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Reports{" + "id=" + id + ", title=" + title + ", description=" + description + ", Category=" + mercado + ", subCategoria=" + subCategoria + ", visible=" + visible + ", report=" + report + ", operacaoCriacao=" + operacaoCriacao + ", operacaoModificacao=" + operacaoModificacao + '}';
    }

}
