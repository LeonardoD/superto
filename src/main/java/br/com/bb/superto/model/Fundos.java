package br.com.bb.superto.model;

import com.google.gson.annotations.Expose;

import javax.persistence.*;
import java.util.Objects;

/**
 * @author Jhemeson S. Mota
 */

@Entity
@Table(name = "fundos")
public class Fundos implements java.io.Serializable, GenericEntity {

    @Id
    @Expose
    private Integer id;
    
    @Expose
    private String nomefundo;

    @Expose
    private String tipo;
    
    @Expose
    private boolean situacao;
    
    @Expose
    private String familia;
    
    @Expose
    private String segmento;
    
    @Expose
    private String nivelRel;
    
    @Expose
    private double taxaadm;
    
    @Expose
    private double valorcota;
    
    @Expose
    private double patrimonio;
    
    @Expose
    private double rentabilidadedia;
    
    @Expose
    private double rentabilidademes;
    
    @Expose
    private double rentabilidadeano;
    
    @Expose
    private double aplicacaoinicial;
    
    @Expose
    private double aplicacaosubs;
    
    @Expose
    private double resgateminimo;
    
    @Expose
    private double saldominimo;
    
    @Expose
    private String cotaaplicacao;
    
    @Expose
    private String debitoaplicacao;
    
    @Expose
    private String cotaresgate;
    
    @Expose
    private String creditoResgate;
    
    @Expose
    private boolean movautoaplicavao;
    
    @Expose
    private boolean movautoresgate;

    public Fundos() {
    }

    public Fundos(Integer id, String nomeFundo, String tipo, boolean situacao, String familia, String segmento, String nivelRel, double taxaadm, double valorcota, double patrimonio, double rentabilidadedia, double rentabilidademes, double rentabilidadeano, double aplicacaoinicial, double aplicacaosubs, double resgateminimo, double saldominimo, String cotaaplicacao, String debitoaplicacao, String cotaresgate, String creditoResgate, boolean movautoaplicavao, boolean movautoresgate) {
        this.id = id;
        this.nomefundo = nomeFundo;
        this.tipo = tipo;
        this.situacao = situacao;
        this.familia = familia;
        this.segmento = segmento;
        this.nivelRel = nivelRel;
        this.taxaadm = taxaadm;
        this.valorcota = valorcota;
        this.patrimonio = patrimonio;
        this.rentabilidadedia = rentabilidadedia;
        this.rentabilidademes = rentabilidademes;
        this.rentabilidadeano = rentabilidadeano;
        this.aplicacaoinicial = aplicacaoinicial;
        this.aplicacaosubs = aplicacaosubs;
        this.resgateminimo = resgateminimo;
        this.saldominimo = saldominimo;
        this.cotaaplicacao = cotaaplicacao;
        this.debitoaplicacao = debitoaplicacao;
        this.cotaresgate = cotaresgate;
        this.creditoResgate = creditoResgate;
        this.movautoaplicavao = movautoaplicavao;
        this.movautoresgate = movautoresgate;
    }

    public String getNomefundo() {
        return nomefundo;
    }

    public void setNomefundo(String nomeFundo) {
        this.nomefundo = nomeFundo;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public boolean isSituacao() {
        return situacao;
    }

    public void setSituacao(boolean situacao) {
        this.situacao = situacao;
    }

    public String getFamilia() {
        return familia;
    }

    public void setFamilia(String familia) {
        this.familia = familia;
    }

    public String getSegmento() {
        return segmento;
    }

    public void setSegmento(String segmento) {
        this.segmento = segmento;
    }

    public String getNivelRel() {
        return nivelRel;
    }

    public void setNivelRel(String nivelRel) {
        this.nivelRel = nivelRel;
    }

    public double getTaxaadm() {
        return taxaadm;
    }

    public void setTaxaadm(double taxaadm) {
        this.taxaadm = taxaadm;
    }

    public double getValorcota() {
        return valorcota;
    }

    public void setValorcota(double valorcota) {
        this.valorcota = valorcota;
    }

    public double getPatrimonio() {
        return patrimonio;
    }

    public void setPatrimonio(double patrimonio) {
        this.patrimonio = patrimonio;
    }

    public double getRentabilidadedia() {
        return rentabilidadedia;
    }

    public void setRentabilidadedia(double rentabilidadedia) {
        this.rentabilidadedia = rentabilidadedia;
    }

    public double getRentabilidademes() {
        return rentabilidademes;
    }

    public void setRentabilidademes(double rentabilidademes) {
        this.rentabilidademes = rentabilidademes;
    }

    public double getRentabilidadeano() {
        return rentabilidadeano;
    }

    public void setRentabilidadeano(double rentabilidadeano) {
        this.rentabilidadeano = rentabilidadeano;
    }

    public double getAplicacaoinicial() {
        return aplicacaoinicial;
    }

    public void setAplicacaoinicial(double aplicacaoinicial) {
        this.aplicacaoinicial = aplicacaoinicial;
    }

    public double getAplicacaosubs() {
        return aplicacaosubs;
    }

    public void setAplicacaosubs(double aplicacaosubs) {
        this.aplicacaosubs = aplicacaosubs;
    }

    public double getResgateminimo() {
        return resgateminimo;
    }

    public void setResgateminimo(double resgateminimo) {
        this.resgateminimo = resgateminimo;
    }

    public double getSaldominimo() {
        return saldominimo;
    }

    public void setSaldominimo(double saldominimo) {
        this.saldominimo = saldominimo;
    }

    public String getCotaaplicacao() {
        return cotaaplicacao;
    }

    public void setCotaaplicacao(String cotaaplicacao) {
        this.cotaaplicacao = cotaaplicacao;
    }

    public String getDebitoaplicacao() {
        return debitoaplicacao;
    }

    public void setDebitoaplicacao(String debitoaplicacao) {
        this.debitoaplicacao = debitoaplicacao;
    }

    public String getCotaresgate() {
        return cotaresgate;
    }

    public void setCotaresgate(String cotaresgate) {
        this.cotaresgate = cotaresgate;
    }

    public String getCreditoResgate() {
        return creditoResgate;
    }

    public void setCreditoResgate(String creditoResgate) {
        this.creditoResgate = creditoResgate;
    }

    public boolean isMovautoaplicavao() {
        return movautoaplicavao;
    }

    public void setMovautoaplicavao(boolean movautoaplicavao) {
        this.movautoaplicavao = movautoaplicavao;
    }

    public boolean isMovautoresgate() {
        return movautoresgate;
    }

    public void setMovautoresgate(boolean movautoresgate) {
        this.movautoresgate = movautoresgate;
    }
    
    @Override
    public int hashCode() {
        int hash = 5;
        hash = 67 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Fundos other = (Fundos) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    @Override
    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Fundos{" + "id=" + id + ", nomeFundo=" + nomefundo + ", tipo=" + tipo + ", situacao=" + situacao + ", familia=" + familia + ", segmento=" + segmento + ", nivelRel=" + nivelRel + ", taxaadm=" + taxaadm + ", valorcota=" + valorcota + ", patrimonio=" + patrimonio + ", rentabilidadedia=" + rentabilidadedia + ", rentabilidademes=" + rentabilidademes + ", rentabilidadeano=" + rentabilidadeano + ", aplicacaoinicial=" + aplicacaoinicial + ", aplicacaosubs=" + aplicacaosubs + ", resgateminimo=" + resgateminimo + ", saldominimo=" + saldominimo + ", cotaaplicacao=" + cotaaplicacao + ", debitoaplicacao=" + debitoaplicacao + ", cotaresgate=" + cotaresgate + ", creditoResgate=" + creditoResgate + ", movautoaplicavao=" + movautoaplicavao + ", movautoresgate=" + movautoresgate + '}';
    }
    
    public String getInfo(){
        return id + " - " + nomefundo;
    }
}
