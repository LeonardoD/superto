/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.model;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

/**
 * @author maycon
 */
@Entity
@Table(name = "videos")
public class Videos implements java.io.Serializable, GenericEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String title;
    private String description;
    @Enumerated(value = EnumType.STRING)
    private Mercado Category;
    private boolean visible;
    @OneToOne
    private Archive video;
    @OneToOne
    @JoinColumn(name = "opcriacao_id")
    private Operacao operacaoCriacao;
    @OneToMany
    private List<Operacao> operacaoModificacao;

    public Videos() {
    }

    @Override
    public Integer getId() {
        return this.id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public br.com.bb.superto.model.Mercado getCategory() {
        return Category;
    }

    public void setCategory(br.com.bb.superto.model.Mercado Category) {
        this.Category = Category;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public Archive getVideo() {
        return video;
    }

    public void setVideo(Archive video) {
        this.video = video;
    }

    public Operacao getOperacaoCriacao() {
        return operacaoCriacao;
    }

    public void setOperacaoCriacao(Operacao operacaoCriacao) {
        this.operacaoCriacao = operacaoCriacao;
    }

    public List<Operacao> getOperacaoModificacao() {
        return operacaoModificacao;
    }

    public void setOperacaoModificacao(List<Operacao> operacaoModificacao) {
        this.operacaoModificacao = operacaoModificacao;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 67 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Videos other = (Videos) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Videos{" + "id=" + id + ", title=" + title + ", description=" + description + ", Category=" + Category + ", visible=" + visible + ", video=" + video + ", operacaoCriacao=" + operacaoCriacao + ", operacaoModificacao=" + operacaoModificacao + '}';
    }

}
