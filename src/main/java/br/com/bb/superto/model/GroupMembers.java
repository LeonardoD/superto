/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.model;

import javax.persistence.*;

/**
 * @author maycon
 */
@Entity
@Table(name = "group_members")
public class GroupMembers implements java.io.Serializable, GenericEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @ManyToOne
    private Funcionarios funcionario;
    @ManyToOne
    private Groups groups;

    public GroupMembers() {
    }

    public GroupMembers(Funcionarios funcionario, Groups groups) {
        this.id = Integer.SIZE;
        this.funcionario = funcionario;
        this.groups = groups;
    }

    @Override
    public Integer getId() {
        return this.id;
    }

    public Funcionarios getFuncionario() {
        return funcionario;
    }

    public void setFuncionario(Funcionarios funcionario) {
        this.funcionario = funcionario;
    }

    public Groups getGroups() {
        return groups;
    }

    public void setGroups(Groups groups) {
        this.groups = groups;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 97 * hash + (this.id != null ? this.id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final GroupMembers other = (GroupMembers) obj;
        if (this.id != other.id && (this.id == null || !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

}
