package br.com.bb.superto.model;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

/**
 * @author Jhemeson S. Mota
 */
@Entity
@Table(name = "curiosidades")
public class Curiosidade implements java.io.Serializable, GenericEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String title;
    @Column(columnDefinition = "TEXT")
    private String text;

    @OneToOne
    @JoinColumn(name = "opcriacao_id")
    private Operacao operacaoCriacao;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @Fetch(FetchMode.SUBSELECT)
    private List<Operacao> operacaoModificacao;

    private boolean visible;

    @OneToOne
    @JoinColumn(name = "image_id")
    private Image capa;

//    @OneToOne(mappedBy = "noticia")
//    private Destaques destaque;

    public Curiosidade() {
    }

    @Override
    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public Image getCapa() {
        return capa;
    }

    public void setCapa(Image capa) {
        this.capa = capa;
    }
    
    public Operacao getOperacaoCriacao() {
        return operacaoCriacao;
    }

    public void setOperacaoCriacao(Operacao operacaoCriacao) {
        this.operacaoCriacao = operacaoCriacao;
    }

    public List<Operacao> getOperacaoModificacao() {
        return operacaoModificacao;
    }

    public void setOperacaoModificacao(List<Operacao> operacaoModificacao) {
        this.operacaoModificacao = operacaoModificacao;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 67 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Curiosidade other = (Curiosidade) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Curiosidade{" + "id=" + id + ", title=" + title + ", text=" + text + ", operacaoCriacao=" + operacaoCriacao + ", operacaoModificacao=" + operacaoModificacao + ", visible=" + visible + ", capa=" + capa + '}';
    }
}
