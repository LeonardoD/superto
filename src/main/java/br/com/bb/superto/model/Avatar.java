/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.model;

import java.util.Objects;
import javax.persistence.*;

/**
 * @author maycon
 */
@Entity
@Table(name = "avatar")
public class Avatar implements java.io.Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @ManyToOne(cascade = CascadeType.REMOVE)
    @JoinColumn(name = "avatar_id")
    private Archive imagem;

    public Avatar() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Archive getImagem() {
        return imagem;
    }

    public void setImagem(Archive imagem) {
        this.imagem = imagem;
    }
    
    @Override
    public int hashCode() {
        int hash = 5;
        hash = 89 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Avatar other = (Avatar) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Avatar{" + "id=" + id + ", imagem=" + imagem + '}';
    }
}
