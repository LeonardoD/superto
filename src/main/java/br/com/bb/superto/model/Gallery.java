/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.model;

import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * @author maycon
 */
@Entity
@Table(name = "gallery")
public class Gallery implements java.io.Serializable, GenericEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @NotEmpty(message = "Campo título não pode estar vazio!")
    private String title;
    @NotEmpty(message = "Campo descrição não pode estar vazio!")
    @Size(max = 128)
    private String description;

    @OneToOne
    @JoinColumn(name = "opcriacao_id")
    private Operacao operacaoCriacao;
    @OneToMany
    private List<Operacao> operacaoModificacao;

    private boolean visible;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date publicationDate;

    @Enumerated(value = EnumType.STRING)
    private Mercado category;

    @OneToMany(mappedBy = "gallery", cascade = CascadeType.REMOVE)
    private List<GalleryImages> images;

    public Gallery() {
    }

    @Override
    public Integer getId() {
        return this.id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public Date getPublicationDate() {
        return publicationDate;
    }

    public void setPublicationDate(Date publicationDate) {
        this.publicationDate = publicationDate;
    }

    public List<GalleryImages> getImages() {
        return images;
    }

    public void setImages(List<GalleryImages> images) {
        this.images = images;
    }

    public Mercado getCategory() {
        return category;
    }

    public void setCategory(Mercado category) {
        this.category = category;
    }

    public Operacao getOperacaoCriacao() {
        return operacaoCriacao;
    }

    public void setOperacaoCriacao(Operacao operacaoCriacao) {
        this.operacaoCriacao = operacaoCriacao;
    }

    public List<Operacao> getOperacaoModificacao() {
        return operacaoModificacao;
    }

    public void setOperacaoModificacao(List<Operacao> operacaoModificacao) {
        this.operacaoModificacao = operacaoModificacao;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 31 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Gallery other = (Gallery) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Gallery{" + "id=" + id + ", title=" + title + ", description=" + description + ", operacaoCriacao=" + operacaoCriacao + ", operacaoModificacao=" + operacaoModificacao + ", visible=" + visible + ", publicationDate=" + publicationDate + ", category=" + category + ", images=" + images + '}';
    }

}
