/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.model;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;

/**
 *
 * @author administrador
 */

@Entity
@Table(name = "macro")
public class Macro implements java.io.Serializable, GenericEntity{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id; 

    private String Nome; 
    private String Relatorio; 
    @Column(columnDefinition = "TEXT")
    private String InstrucoesMacro; //Arquivo IIM?????
    private String Motivo; 
    private String URL;
    
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date dataCadastro; 
    private String ChaveSolicitante; 

    public Macro() {
    }

    public Macro(Integer id, String Nome, String Relatorio, String InstrucoesMacro, String Motivo, String URL, Date dataCadastro, String ChaveSolicitante) {
        this.id = id;
        this.Nome = Nome;
        this.Relatorio = Relatorio;
        this.InstrucoesMacro = InstrucoesMacro;
        this.Motivo = Motivo;
        this.URL = URL;
        this.dataCadastro = dataCadastro;
        this.ChaveSolicitante = ChaveSolicitante;
    }
    
    public Macro(String Nome, String Relatorio, String Motivo, Date dataCadastro, String ChaveSolicitante, String url) {
        this.Nome = Nome;
        this.Relatorio = Relatorio;
        this.Motivo = Motivo;
        this.dataCadastro = dataCadastro;
        this.ChaveSolicitante = ChaveSolicitante;
        this.URL = url;
    }

    public String getURL() {
        return URL;
    }

    public void setURL(String URL) {
        this.URL = URL;
    }
    
    @Override
    public Integer getId() {
        return id;
    }

    public String getMotivo() {
        return Motivo;
    }

    public void setMotivo(String Motivo) {
        this.Motivo = Motivo;
    }

    
    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return Nome;
    }

    public void setNome(String Nome) {
        this.Nome = Nome;
    }

    public String getRelatorio() {
        return Relatorio;
    }

    public void setRelatorio(String Relatorio) {
        this.Relatorio = Relatorio;
    }

    public String getInstrucoesMacro() {
        return InstrucoesMacro;
    }

    public void setInstrucoesMacro(String InstrucoesMacro) {
        this.InstrucoesMacro = InstrucoesMacro;
    }

    public Date getDataCadastro() {
        return dataCadastro;
    }

    public void setDataCadastro(Date dataCadastro) {
        this.dataCadastro = dataCadastro;
    }

    public String getChaveSolicitante() {
        return ChaveSolicitante;
    }

    public void setChaveSolicitante(String ChaveSolicitante) {
        this.ChaveSolicitante = ChaveSolicitante;
    }
    
    
}
