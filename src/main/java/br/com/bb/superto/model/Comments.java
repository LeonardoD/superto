/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.model;

import javax.persistence.*;
import java.util.Date;

/**
 * Classe em modelo orientado a objeto para o modelo relacional da tabela
 * comments.
 *
 * @author Maycon Antonio Junqueira Costa
 * @version 1.0
 */
@Entity
@Table(name = "comments")

public class Comments implements java.io.Serializable, GenericEntity {

    /**
     * Identificador unico do comentario.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    private Integer id;

    /**
     * Id referente a tabela do comentario.
     */
    private Integer sourceId;

    private String text;

    /**
     * Referencia ao funcioario que fez o comentario.
     */
    @ManyToOne
    private Funcionarios author;
    @Temporal(javax.persistence.TemporalType.DATE)

    private Date datePublication;

    /**
     * Define se o comentario será visivel.
     */
    private boolean visible;

    /**
     * Categoria define a tabela no qual o comentario pertence.
     */
    @Enumerated(value = EnumType.STRING)
    private Category category;

    /**
     * Campo referente a resposta do comentario.
     */
    @ManyToOne
    private Comments parent;

    public Comments() {
    }

    @Override
    public Integer getId() {
        return this.id;
    }

    public Integer getSourceId() {
        return sourceId;
    }

    public void setSourceId(Integer sourceId) {
        this.sourceId = sourceId;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Funcionarios getAuthor() {
        return author;
    }

    public void setAuthor(Funcionarios author) {
        this.author = author;
    }

    public Date getDatePublication() {
        return datePublication;
    }

    public void setDatePublication(Date datePublication) {
        this.datePublication = datePublication;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Comments getParent() {
        return parent;
    }

    public void setParent(Comments parent) {
        this.parent = parent;
    }

}
