package br.com.bb.superto.model;

import java.util.Date;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

/**
 * @author Jhemeson S. Mota
 */
@Entity
@Table(name = "demanda")
public class Demanda implements java.io.Serializable, GenericEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String title;
    private String cliente;
    @Column(columnDefinition = "TEXT")
    private String text;

    private Date dataCadastro;
    private Date dataInicio;
    private Date dataFinalizado;

    public Demanda() {
    }

    public Demanda(String title, String cliente, String text, Date dataCadastro, Date dataInicio, Date dataFinalizado) {
        this.id = 0;
        this.title = title;
        this.cliente = cliente;
        this.text = text;
        this.dataCadastro = dataCadastro;
        this.dataFinalizado = dataFinalizado;
        this.dataInicio = dataInicio;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Date getDataCadastro() {
        return dataCadastro;
    }

    public void setDataCadastro(Date dataCadastro) {
        this.dataCadastro = dataCadastro;
    }

    public Date getDataFinalizado() {
        return dataFinalizado;
    }

    public void setDataFinalizado(Date dataFinalizado) {
        this.dataFinalizado = dataFinalizado;
    }

    public Date getDataInicio() {
        return dataInicio;
    }

    public void setDataInicio(Date dataInicio) {
        this.dataInicio = dataInicio;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 67 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Demanda other = (Demanda) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Demanda{" + "id=" + id + ", title=" + title + ", cliente=" + cliente + ", text=" + text + ", dataCadastro=" + dataCadastro + ", dataFinalizado=" + dataFinalizado + '}';
    }

}
