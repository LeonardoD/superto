package br.com.bb.superto.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Classe em modelo orientado a objeto para o modelo relacional da tabela cargo.
 *
 * @author Maycon Antonio Junqueira Costa
 * @version 1.0
 */
@Entity
@Table(name = "cargos")
public class Cargos implements java.io.Serializable, GenericEntity {

    @Id
    private Integer codCargo;

    private String funcao;
    private String descricao;
    private boolean administrador;
    private boolean pf;
    private boolean pj;
    private boolean agro;
    private boolean comissionado;
    private boolean governo;

    public Cargos() {
    }

    public Integer getCodCargo() {
        return codCargo;
    }

    public void setCodCargo(Integer codCargo) {
        this.codCargo = codCargo;
    }

    public String getFuncao() {
        return funcao;
    }

    public void setFuncao(String funcao) {
        this.funcao = funcao;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public boolean isAdministrador() {
        return administrador;
    }

    public void setAdministrador(boolean administrador) {
        this.administrador = administrador;
    }

    public boolean isPf() {
        return pf;
    }

    public void setPf(boolean pf) {
        this.pf = pf;
    }

    public boolean isPj() {
        return pj;
    }

    public void setPj(boolean pj) {
        this.pj = pj;
    }

    public boolean isAgro() {
        return agro;
    }

    public void setAgro(boolean agro) {
        this.agro = agro;
    }

    public boolean isComissionado() {
        return comissionado;
    }

    public void setComissionado(boolean comissionado) {
        this.comissionado = comissionado;
    }

    public boolean isGoverno() {
        return governo;
    }

    public void setGoverno(boolean governo) {
        this.governo = governo;
    }

    @Override
    public Integer getId() {
        return codCargo;
    }
}
