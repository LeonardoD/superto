package br.com.bb.superto.model;

import com.google.gson.annotations.Expose;
import java.io.Serializable;
import java.util.Objects;
import java.util.logging.Logger;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author maycon
 */
@Entity
@Table(name = "cliente")
public class Cliente implements Serializable {

    @Id
    @Expose
    private Integer mci;
    
    @Expose
    private String prefixo;
    
    @Expose
    private Integer numCarteira;
    
    @Expose
    private String telefone;
    
    @Expose
    private String nomeCliente;

    public Integer getNumCarteira() {
        return numCarteira;
    }

    public void setNumCarteira(Integer numCarteira) {
        this.numCarteira = numCarteira;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getNomeCliente() {
        return nomeCliente;
    }

    public void setNomeCliente(String nomeCliente) {
        this.nomeCliente = nomeCliente;
    }

    public Cliente() {
    }

    public Integer getMci() {
        return mci;
    }

    public void setMci(Integer mci) {
        this.mci = mci;
    }

    public String getPrefixo() {
        return prefixo;
    }

    public void setPrefixo(String prefixo) {
        this.prefixo = prefixo;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 67 * hash + Objects.hashCode(this.mci);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Cliente other = (Cliente) obj;
        if (!Objects.equals(this.mci, other.mci)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Cliente{" + "mci=" + mci + ", prefixo=" + prefixo + ", numCarteira=" + numCarteira + ", telefone=" + telefone + ", nomeCliente=" + nomeCliente + '}';
    }
    
}
