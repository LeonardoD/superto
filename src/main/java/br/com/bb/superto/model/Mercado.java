/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.model;

/**
 * @author maycon
 */
public enum Mercado {

    SUPER("SUPER", "SUPER"), ADM("ADM", "ADM"), PF("PF", "PF"), GOV("GOV", "GOV"),
    PJ("PJ", "PJ"), DS("DS", "DS"), AGRO("AGRO", "AGRO"), REGIONAL("REGIONAL", "REGIONAL"), ECOA("ECOA", "ECOA");

    private final String action;
    private final String label;

    Mercado(String action, String label) {
        this.action = action;
        this.label = label;
    }

    public String getAction() {
        return action;
    }

    public String getLabel() {
        return label;
    }

}
