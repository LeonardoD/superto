/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.model;

import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * @author maycon
 */
@Entity
@Table(name = "app_modules")
public class AppModules implements java.io.Serializable, GenericEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer id;
    @NotNull(message = "Nome da aplicação não pode estar em branco.")
    @NotEmpty(message = "Nome da aplicação não pode estar em branco.")
    @Column(unique = true)
    String name;

    public AppModules() {
    }

    public AppModules(String name) {
        this.id = Integer.SIZE;
        this.name = name;
    }

    @Override
    public Integer getId() {
        return this.id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
