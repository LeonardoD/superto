/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.model;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author maycon
 */
@Entity
@Table(name = "organograma")
public class Organograma implements Serializable, GenericEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @ManyToOne
    private Funcionarios func;
    @ManyToOne
    private Organograma parent;

    @Override
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Funcionarios getFunc() {
        return func;
    }

    public void setFunc(Funcionarios func) {
        this.func = func;
    }

    public Organograma getParent() {
        return parent;
    }

    public void setParent(Organograma parent) {
        this.parent = parent;
    }

    @Override
    public String toString() {
        return "Organograma{" + "id=" + id + ", func=" + func + ", parent=" + parent + '}';
    }

}
