package br.com.bb.superto.model;

import com.google.gson.annotations.Expose;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 *
 * @author Jhemeson Silva Mota
 * @version 1.0
 */
@Entity
@Table(name = "margemdisponivel")
public class ClienteMD implements java.io.Serializable, GenericEntity {

    @Id
    @Expose
    private Integer mci;
    
    @Expose
    private String orgao;
    
    @Expose
    private double margem;

    @Expose
    private double consig;
    
    @Expose
    private double trocomax;
    
    @Expose
    private double salario;
    
    @Expose
    private boolean focosalario;
    
    @Expose
    private boolean contatado;
    
    @Expose
    private String cpf;
    
    public ClienteMD() {
    }

    public ClienteMD(Integer mci, String orgao, double margem, double consig, double trocomax, double salario, boolean focosalario, boolean contatado) {
        this.mci = mci;
        this.orgao = orgao;
        this.margem = margem;
        this.consig = consig;
        this.trocomax = trocomax;
        this.salario = salario;
        this.focosalario = focosalario;
        this.contatado = contatado;
    }

    public ClienteMD(Integer mci, String orgao, double margem) {
        this.mci = mci;
        this.orgao = orgao;
        this.margem = margem;
        this.contatado = false;
    }    
    
    public ClienteMD(Integer mci, String orgao, double margem, boolean contatado) {
        this.mci = mci;
        this.orgao = orgao;
        this.margem = margem;
        this.contatado = contatado;
    }    

    public Integer getMci() {
        return mci;
    }

    public void setMci(Integer mci) {
        this.mci = mci;
    }

    public String getOrgao() {
        return orgao;
    }

    public void setOrgao(String orgao) {
        this.orgao = orgao;
    }

    public double getMargem() {
        return margem;
    }

    public void setMargem(double margem) {
        this.margem = margem;
    }

    public double getConsig() {
        return consig;
    }

    public void setConsig(double consig) {
        this.consig = consig;
    }

    public double getTrocomax() {
        return trocomax;
    }

    public void setTrocomax(double trocomax) {
        this.trocomax = trocomax;
    }

    public double getSalario() {
        return salario;
    }

    public void setSalario(double salario) {
        this.salario = salario;
    }

    public boolean isFocosalario() {
        return focosalario;
    }

    public void setFocosalario(boolean focosalario) {
        this.focosalario = focosalario;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (mci != null ? mci.hashCode() : 0);
        return hash;
    }

    public boolean isContatado() {
        return contatado;
    }

    public void setContatado(boolean contatado) {
        this.contatado = contatado;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof ClienteMD)) {
            return false;
        }
        ClienteMD other = (ClienteMD) object;
        if ((this.mci == null && other.mci != null) || (this.mci != null && !this.mci.equals(other.mci))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.bb.superto.model.clientemd[ id=" + mci + " ]";
    }

    @Override
    public Integer getId() {
        return mci;
    }
    
}
