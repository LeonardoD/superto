/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.model;

import javax.persistence.*;
import java.util.Date;

/**
 * @author maycon
 */
@Entity
@Table(name = "user_history")
public class UserHistory implements java.io.Serializable, GenericEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne
    private Funcionarios user;
    private String history;
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date loginDate;
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date logoutDate;

    @Override
    public Integer getId() {
        return this.id;
    }

    public Funcionarios getUser() {
        return user;
    }

    public void setUser(Funcionarios user) {
        this.user = user;
    }

    public String getHistory() {
        return history;
    }

    public void setHistory(String history) {
        this.history = history;
    }

    public Date getLoginDate() {
        return loginDate;
    }

    public void setLoginDate(Date loginDate) {
        this.loginDate = loginDate;
    }

    public Date getLogoutDate() {
        return logoutDate;
    }

    public void setLogoutDate(Date logoutDate) {
        this.logoutDate = logoutDate;
    }
}
