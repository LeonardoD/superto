package br.com.bb.superto.model;

import javax.persistence.*;

/**
 * Classe em modelo orientado a objeto para o modelo relacional da tabela
 * agencia.
 *
 * @author Maycon Antonio Junqueira Costa
 * @version 1.0
 */
@Entity
@Table(name = "agencias")
public class Agencias implements java.io.Serializable, GenericEntity {

    @Id
    private Integer prefixo;
    private String rede;
    private String nomeAgencia;
    private Integer metaseguridade;
    private Integer feitoseguridade;

    @OneToOne
    @JoinColumn(name = "funcionario_id")
    private Funcionarios gerenteAgencia;

    public Agencias() {
    }

    public Agencias(Integer prefixo, String rede, String nomeAgencia) {
        this.prefixo = prefixo;
        this.rede = rede;
        this.nomeAgencia = nomeAgencia;
    }

    public Integer getMetaseguridade() {
        try{
            return metaseguridade;
        }
        catch(Exception e){
            return 0;
        }
    }

    public void setMetaseguridade(Integer metaseguridade) {
        this.metaseguridade = metaseguridade;
    }

    public Integer getFeitoseguridade() {
        return feitoseguridade;
    }

    public void setFeitoseguridade(Integer feitoseguridade) {
        this.feitoseguridade = feitoseguridade;
    }

    @Override
    public Integer getId() {
        return prefixo;
    }

    public Integer getPrefixo() {
        return prefixo;
    }

    public void setPrefixo(Integer prefixo) {
        this.prefixo = prefixo;
    }

    public String getRede() {
        return rede;
    }

    public void setRede(String rede) {
        this.rede = rede;
    }

    public String getNomeAgencia() {
        return nomeAgencia;
    }
    
    public String pegaNomeAgencia(Integer pre) {

            return nomeAgencia;
    }

    public void setNomeAgencia(String nomeAgencia) {
        this.nomeAgencia = nomeAgencia;
    }

    public Funcionarios getGerenteAgencia() {
        return gerenteAgencia;
    }

    public void setGerenteAgencia(Funcionarios gerenteAgencia) {
        this.gerenteAgencia = gerenteAgencia;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 37 * hash + (this.prefixo != null ? this.prefixo.hashCode() : 0);
        hash = 37 * hash + (this.rede != null ? this.rede.hashCode() : 0);
        hash = 37 * hash + (this.nomeAgencia != null ? this.nomeAgencia.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Agencias other = (Agencias) obj;
        if (this.prefixo != other.prefixo && (this.prefixo == null || !this.prefixo.equals(other.prefixo))) {
            return false;
        }
        if ((this.rede == null) ? (other.rede != null) : !this.rede.equals(other.rede)) {
            return false;
        }
        if ((this.nomeAgencia == null) ? (other.nomeAgencia != null) : !this.nomeAgencia.equals(other.nomeAgencia)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Agencias{" + "prefixo=" + prefixo + ", rede=" + rede + ", nomeAgencia=" + nomeAgencia + '}';
    }
}
