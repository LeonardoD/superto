/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.model;

/**
 * @author maycon
 */
public enum Sexo {

    MASCULINO("Masculino"),
    FEMININO("Feminino");

    private final String type;

    Sexo(String type) {
        this.type = type;
    }

    private String type() {
        return this.type;
    }

    public String getType() {
        return this.type;
    }

    public String asString() {
        return this.type;
    }

}
