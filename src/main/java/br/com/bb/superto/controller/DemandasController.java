package br.com.bb.superto.controller;

import br.com.bb.superto.dao.DemandasDao;
import br.com.bb.superto.model.Demanda;
import java.util.Date;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.validation.ConstraintViolationException;

/**
 *
 * @author Jheemeson S. Mota
 */

@ManagedBean(name = "demandasController")
@ViewScoped
public class DemandasController extends GenericController<Demanda> implements java.io.Serializable {

    private boolean opSucesso;
    private List<Demanda> listaDePedidosFilter;
    private String id; // Id para buscar a demanda
    private String title;
    private Date dataCad;
    private Date dataIn;
    private Date dataFinal;
    private String desc;
    private String clie;
    private boolean finalizado;
    private boolean iniciada;
    private String status;
    
    public DemandasController() {
        opSucesso = false;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getDataCad() {
        return dataCad;
    }

    public void setDataCad(Date dataCad) {
        this.dataCad = dataCad;
    }

    public Date getDataFinal() {
        return dataFinal;
    }

    public void setDataFinal(Date dataFinal) {
        this.dataFinal = dataFinal;
    }
    
    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getClie() {
        return clie;
    }

    public void setClie(String clie) {
        this.clie = clie;
    }
    
    public List<Demanda> getAll() {
        return new DemandasDao().findAll();
    }
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    
    public boolean isOpSucesso() {
        return opSucesso;
    }

    public void setOpSucesso(boolean opSucesso) {
        this.opSucesso = opSucesso;
    }

    public List<Demanda> getListaDePedidosFilter() {
        return listaDePedidosFilter;
    }

    public void setListaDePedidosFilter(List<Demanda> listaDePedidosFilter) {
        this.listaDePedidosFilter = listaDePedidosFilter;
    }

    public Date getDataIn() {
        return dataIn;
    }

    public void setDataIn(Date dataIn) {
        this.dataIn = dataIn;
    }

    public boolean isFinalizado() {
        try{
            this.finalizado = selected.getDataFinalizado()!=null;
            return finalizado;
        }
        catch(Exception ex){
            return false;
        }
    }

    public void setFinalizado(boolean finalizado) {
        this.finalizado = finalizado;
        selected.setDataFinalizado(new Date());
        opSucesso = new DemandasDao().makePersistent(selected) != null;
    }

    public boolean isIniciada() {
        try{
            this.iniciada = selected.getDataInicio()!=null;
            return iniciada;
        }
        catch(Exception ex){
            return false;
        }
    }

    public void setIniciada(boolean iniciada) {
        this.iniciada = iniciada;
        selected.setDataInicio(new Date());
        opSucesso = new DemandasDao().makePersistent(selected) != null;
    }

    public String getStatus() {
        return status;
    }
    
    public String pegarStatus(Demanda d) {
        status = "Desconhecido";
        if(d.getDataInicio()!=null && d.getDataFinalizado() != null){
            status = "Finalizada";
        }
        else if(d.getDataInicio() == null && d.getDataFinalizado() == null){
            status = "Em Espera";
        }
        else if(d.getDataInicio() != null && d.getDataFinalizado() == null){
            status = "Iniciada";
        }
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    
    //concatenar motivos de devolução nas observações... ou colocar eles no histórico de averbação
    // no lugar do 'X' agora o btn off-label vai ser vazio
    // ao mudar o status para qqr outro (de devolvida para estado X), os campos de motivos serão setados false
    
    @Override
    public void edit() {
        try {
            System.out.println("<<<<<<<<<<<< Antes da operação: "+selected.getTitle()+">>>>>>>>>>>>>>>");
            if (selected != null) {                
                opSucesso = new DemandasDao().makePersistent(selected) != null;
                System.out.print("***");
                System.out.print("***");
                System.out.print("***");
                System.out.print("Sucesso: " + opSucesso);
            }
            
        } catch (Exception ex) {
            System.out.println("Erro ao editar Demanda: " + ex.getMessage());
        }
    } 
    
    @Override
    public void delete() {
        try {
            if (selected == null) {
                throw new NullPointerException();
            }
            opSucesso = new DemandasDao().makeTransient(selected) != null;
            opSucesso = true;
            selected = null;
        } catch (NullPointerException | ConstraintViolationException ex) {
            System.out.println("Erro ao deletar Demanda: " + ex.getMessage());
        }
    }

    @Override
    public void add() {
        System.out.print("Entramos no método de adição");
        System.out.print("Entramos no método de adição");
        System.out.print("Entramos no método de adição");
        System.out.print("Entramos no método de adição");
        try {
            System.out.print("Entramos no try");
            dataCad = new Date();
            dataFinal = null;
            dataIn = null;
            Demanda d = new Demanda(title, clie, desc, dataCad, dataIn, dataFinal);
            System.out.print("criamos demanda d com o titulo: " + title + ", e o id: " + id);
            opSucesso = new DemandasDao().save(d) != null;
            System.out.print("Salvamos d: " + opSucesso);
        } catch (Exception ex) {
            System.out.println("Erro ao salvar a demanda: " + ex.getMessage());
        }
    }
}
