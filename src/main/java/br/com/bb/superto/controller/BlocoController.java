/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.controller;

import br.com.bb.superto.dao.BlocoDao;
import br.com.bb.superto.dao.ProdutoSeguridadeDao;
import br.com.bb.superto.model.Bloco;
import br.com.bb.superto.model.ClienteProduto;
import br.com.bb.superto.model.ProdutoSeguridade;
import br.com.bb.superto.model.clienteSeguridade;
import java.util.ArrayList;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.util.List;

/**
 * @author Jhemeson s. Mota
 */
@ManagedBean(name = "blocoController")
@ViewScoped
public class BlocoController extends GenericController<Bloco> {

    private UploadedFile file;
    private String fileName;
    private boolean opSucesso;
    private List<Bloco> listBlocos;
    private String nomeBloco;
    private Integer id;

    public BlocoController() {
    }

    public String getNomeBloco() {
        return nomeBloco;
    }

    public void setNomeBloco(String nomeBloco) {
        this.nomeBloco = nomeBloco;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public UploadedFile getFile() {
        return file;
    }
    public List<Bloco> getListBlocos() {
        listBlocos = new BlocoDao().findAll();
        return listBlocos;
    }

    public void setFile(UploadedFile file) {
        this.file = file;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public List<Bloco> getBlocos() {
        List <Bloco> listBlocosAux = new BlocoDao().findTodos();        
        return listBlocosAux;
    }
    
    public List<String> getNomeBlocos() {
        List <Bloco> listBlocosAux = new BlocoDao().findTodos();
        List <String> listStringAux = new ArrayList<String>();
        for(Bloco b : listBlocosAux)
        {
            listStringAux.add(b.getNome());
        }
        return listStringAux;
    }
    
    public Bloco getBlocoByNome(String nome){
        return new BlocoDao().findByNome(nome);
    }
    public void fileUploadListener(FileUploadEvent event) {
        file = event.getFile();
    }

    public boolean isOpSucesso() {
        return opSucesso;
    }

    public void setOpSucesso(boolean opSucesso) {
        this.opSucesso = opSucesso;
    }
    
    public boolean habilitado(int qtd){
        if(qtd > 0){
            return true;
        }
        else{
            return false;
        }
    }
    
    public boolean tbRender(clienteSeguridade cs){
        boolean possui = false;
        for(Bloco b : this.getBlocos()){
            if(this.totalProdutos(cs, b)>0){
                possui = true;
            }
        }
        return possui;
    }
    
    public int totalProdutos(clienteSeguridade c, Bloco b)
    {
        try
        {
            List <ClienteProduto> cp;
            cp = new ClienteProdutoController().getItensFiltradosMci(c.getMci());
            int cont = 0;
            try
            {
                for(ClienteProduto aux : cp)
                {
                    ProdutoSeguridade ps;
                    ps = new ProdutoSeguridadeDao().findByCodProduto(aux.getCodproduto());
                    if(ps.getBloco().equals(b.getNome()) && aux.isContatado() == false)
                    {
                        cont = cont +1;
                    }
                }
                return cont;
            }
            catch(Exception e)
            {
                return 0;
            }  
        }
        catch(Exception ex)
        {
            return 0;
        }
        
    }
  
    public double totalValores(clienteSeguridade c, Bloco b)
    {
        try
        {
            List <ClienteProduto> cp;
            cp = new ClienteProdutoController().getItensFiltradosMci(c.getMci());
            double cont = 0;
            try
            {
                for(ClienteProduto aux : cp)
                {
                    ProdutoSeguridade ps;
                    ps = new ProdutoSeguridadeDao().findByCodProduto(aux.getCodproduto());
                    if(ps.getBloco().equals(b.getNome()) && aux.isContatado() == false)
                    {
                        cont = cont + aux.getPremio();
                    }
                }
                return cont;
            }
            catch(Exception e)
            {
                return 0;
            }  
        }
        catch(Exception ex)
        {
            return 0;
        }
        
    }

    @Override
    public void edit() {
        if (selected != null) {
            new BlocoDao().makePersistent(selected);
            addFacesMsg("Bloco editado com sucesso", FacesMessage.SEVERITY_INFO);
        }
    }

    @Override
    public void delete() {
        if (selected != null) {
            opSucesso = new BlocoDao().makeTransient(selected) != null;
        }
    }

    @Override
    public void add() {
        if (value != null) {
                opSucesso = new BlocoDao().save(value) != null;
        }
    }
    
    public void addBloco() {
        System.out.print("Entramos no metodo addBloco");
        try {
            Bloco aux = new Bloco(nomeBloco);
            new BlocoDao().makePersistent(aux);
            opSucesso = true;
        } catch (Exception ex) {
            System.out.print("Entramos no catch = " + ex);
            opSucesso = false;
        }
        id = null;
        nomeBloco = null;
    }
}
