/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.controller;

import br.com.bb.superto.dao.UserHistoryDao;
import br.com.bb.superto.model.UserHistory;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;

/**
 *
 * @author administrador
 */
@ManagedBean(name = "AcessosController")
@ViewScoped
public class AcessosController implements java.io.Serializable {

    private Integer tipoPesquisa;
    private List<UserHistory> lista;
    private String pesquisa = " ";
    private String filtro = "Funcionário";
    private Date dataInicio;
    private Date dataFim;

//    public void acessosDoDia() {
//        lista.clear();
//        setLista(new UserHistoryDao().getAcessosDoDia());
//    }

    public void acessosdoDia() {
        lista.clear();
        setLista(new UserHistoryDao().getTodosAcessosDoDia());
    }

    public boolean temAcesso(Integer prefixo)
    {
        String aux = prefixo + "";
        if(aux.equals("1886") || aux.equals("1867") || aux.equals("8517") || aux.equals("5067") || aux.equals("794"))
        {
            return true;
        }
        return false;
    }

    public Integer getTipoPesquisa() {
        return tipoPesquisa;
    }

    public void setTipoPesquisa(Integer tipoPesquisa) {
        this.tipoPesquisa = tipoPesquisa;
    }

    public List<UserHistory> getLista() {
        if (lista == null) {
            lista = new ArrayList<>();
        }
        return lista;
    }

    public void setLista(List<UserHistory> lista) {
        this.lista = lista;
    }

    public String getPesquisa() {
        return pesquisa;
    }

    public void setPesquisa(String pesquisa) {
        this.pesquisa = pesquisa;
    }

    public String getFiltro() {
        return filtro;
    }

    public void setFiltro(String filtro) {
        this.filtro = filtro;
    }

    public String mudarFiltro(ActionEvent event) {
        if (tipoPesquisa == 0) {
            filtro = "Funcionário";
        } else {
            filtro = "Prefixo";
        }
        return filtro;
    }

    public Date getDataInicio() {
        return dataInicio;
    }

    public void setDataInicio(Date dataInicio) {
        this.dataInicio = dataInicio;
    }

    public Date getDataFim() {
        return dataFim;
    }

    public void setDataFim(Date dataFim) {
        this.dataFim = dataFim;
    }
}
