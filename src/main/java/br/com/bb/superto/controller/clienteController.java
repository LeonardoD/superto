package br.com.bb.superto.controller;

import br.com.bb.superto.dao.ClienteDao;
import br.com.bb.superto.dao.ClienteSeguridadeDao;
import br.com.bb.superto.dao.OperacaoDAO;
import br.com.bb.superto.dao.ProdutoSeguridadeDao;
import br.com.bb.superto.model.Cliente;
import br.com.bb.superto.model.ClienteProduto;
import br.com.bb.superto.model.Funcionarios;
import br.com.bb.superto.model.Operacao;
import br.com.bb.superto.model.ProdutoSeguridade;
import br.com.bb.superto.model.UserNotLogged;
import br.com.bb.superto.model.clienteSeguridade;
import br.com.bb.superto.security.UserSecurityUtils;
import com.csvreader.CsvReader;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import java.io.IOException;
import java.lang.reflect.Type;
import java.nio.charset.Charset;
import java.util.ArrayList;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.context.FacesContext;import org.hibernate.HibernateException;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;

/**
 * @author Jhemeson
 */
@ManagedBean(name = "clienteController")
@ViewScoped
public class clienteController extends GenericController<clienteSeguridade> {

    private List<clienteSeguridade> listVinc;
    private List<clienteSeguridade> listVencCanc;
    private List<clienteSeguridade> lista;
    private List<clienteSeguridade> listaComfinfo;
    private List<clienteSeguridade> listaComCMD;
    private List<clienteSeguridade> listaFilter;
    private List<clienteSeguridade> listaSemfinfo;
    private List<clienteSeguridade> listaFiltrada;
    private ClienteDao clienteDao;
    private Funcionarios func;
    private boolean opSucesso;
    private UploadedFile uploadedFile;
    private StreamedContent file;
    private int mci;
    private String nomeCliente;
    private String telefone;
    private String prefixo;
    private String carteira;

    public clienteController() {
        this.getListSeguridade();
    }   
    
    public String getPrefixo() {
        return prefixo;
    }

    public void setPrefixo(String prefixo) {
        this.prefixo = prefixo;
    }

    public List<clienteSeguridade> getListaFiltrada() {
        return listaFiltrada;
    }

    public void setListaFiltrada(List<clienteSeguridade> listaFiltrada) {
        this.listaFiltrada = listaFiltrada;
    }
    
    public clienteSeguridade findByMci(Integer mci)
    {
        return new ClienteSeguridadeDao().findById(mci, true);
    }
    
    public String getCarteira() {
        return carteira;
    }

    public void setCarteira(String carteira) {
        this.carteira = carteira;
    }
    
    private List<clienteSeguridade> listClienteSeguridade;
    private List<Cliente> clientes;

    public UploadedFile getUploadedFile() {
        return uploadedFile;
    }

    public void setUploadedFile(UploadedFile uploadedFile) {
        this.uploadedFile = uploadedFile;
    }

    public StreamedContent getFile() {
        return file;
    }

    public void setFile(StreamedContent file) {
        this.file = file;
    }

    public int getMci() {
        return mci;
    }

    public void setMci(int mci) {
        this.mci = mci;
    }

    public String getNomeCliente() {
        return nomeCliente;
    }

    public void setNomeCliente(String nomeCliente) {
        this.nomeCliente = nomeCliente;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }
    
    public List<clienteSeguridade> getListSeguridade() {
        if(listClienteSeguridade == null){
            listClienteSeguridade = new ClienteDao().findAllSeguridade();
        }
        return listClienteSeguridade;
    }
    
    public String pegaBlocosPorPessoa(int mci) {
        List<ClienteProduto> lista = new ArrayList<ClienteProduto>();
        lista = new ClienteProdutoController().getItensFiltradosMci(mci);
        String strBuilder = "";
        for(ClienteProduto cp : lista)
        {
            try
            {
                ProdutoSeguridade ps = new ProdutoSeguridadeDao().findByCodProduto(cp.getCodproduto());
                if((strBuilder.contains(ps.getBloco())) == false)
                {
                    if(strBuilder.equals(""))
                    {
                        strBuilder = strBuilder + ps.getBloco();
                    }
                    else
                    {
                        strBuilder = strBuilder + " - " + ps.getBloco();
                    }
                }
            }
            catch(Exception ex)
            {
                System.out.print("Erro Encontrado ao tentar pegar Blocos por Pessoa: " + ex);
            }
        }
        return strBuilder;
    }
    
    public clienteSeguridade getOneManList(Integer mci) {
        try{
            clienteSeguridade aux2 = new ClienteSeguridadeDao().findById(mci, true);
            return aux2;
        }
        catch(Exception e){
            clienteSeguridade aux = new clienteSeguridade();
            return aux;
        }
    }
 
    public List<clienteSeguridade> getListSeguridadeVinc() {
        int pref = new LoginController().getFuncionario().getPrefixo();
        if(pref == 8517){
            listVinc = new ClienteSeguridadeDao().findWithProdVincSuper();            
        }
        else{
            listVinc = new ClienteSeguridadeDao().findWithProdVinc(pref);                        
        }
        return listVinc;
    }
    
    public List<clienteSeguridade> getListSeguridadeVencCanc() {
        int pref = new LoginController().getFuncionario().getPrefixo();
        List<clienteSeguridade> listaAux = null;
        if(pref == 8517){
            listVencCanc = new ClienteSeguridadeDao().findWithProdVencSuper();
            listaAux = new ClienteSeguridadeDao().findWithProdCancSuper();
        }
        else{
            listVencCanc = new ClienteSeguridadeDao().findWithProdVenc(pref);
            listaAux = new ClienteSeguridadeDao().findWithProdCanc(pref);
        }
        listVencCanc.addAll(listaAux);
        return listVencCanc;
    }
    
    private List<clienteSeguridade> listClienteVincendos;

    public void setListClienteVincendos(List<clienteSeguridade> listClienteVincendos) {
        this.listClienteVincendos = listClienteVincendos;
    }
    
    public List<Cliente> getClientes() {
        return new ClienteDao().findByPrefixo("5743");
    }
    
    public void setClientes(List<Cliente> clientes) {
        this.clientes = clientes;
    }

    public List<clienteSeguridade> getLista() {
        lista = new ClienteSeguridadeDao().findAll();
        return lista;
    }

    public void setLista(List<clienteSeguridade> lista) {
        this.lista = lista;
    }

    public List<clienteSeguridade> getListaComfinfo() {
        listaComfinfo = new ClienteSeguridadeDao().findComInfo();
        return listaComfinfo;
    }
    
    public List<clienteSeguridade> getListaComCMD() {
        int prefixo = 0;
        prefixo = new LoginController().getFuncionario().getPrefixo();
        if(prefixo == 8517){    
            listaComCMD = new ClienteSeguridadeDao().findComCMD();
        }
        else{
            listaComCMD = new ClienteSeguridadeDao().findComCMDByPrefixo(prefixo+"");
        }
        return listaComCMD;
    }

    public List<clienteSeguridade> getListaFilter() {
        try{
            System.out.print("Entramos no listaFilter: " + listaFilter.toString());
            return listaFilter;
        }
        catch(Exception ex){
            System.out.print("Erro ao retornar lista filter: " + ex);
            return this.listaComCMD;
        }
    }

    public void setListaFilter(List<clienteSeguridade> listaFilter) {
        this.listaFilter = listaFilter;
    }
    
    public void setListaComCMD(List<clienteSeguridade> listaComCMD) {
        this.listaComCMD = listaComCMD;
    }
    
    public void setListaComfinfo(List<clienteSeguridade> listaComfinfo) {
        this.listaComfinfo = listaComfinfo;
    }
    
    public List<clienteSeguridade> getListaSemfinfo() {
        listaSemfinfo = new ClienteSeguridadeDao().findSemInfo();
        return listaSemfinfo;
    }

    public void setListaSemfinfo(List<clienteSeguridade> listaSemfinfo) {
        this.listaSemfinfo = listaSemfinfo;
    }

    public void clientes() {
        pegarFuncionario();
        if (func.getPrefixo() == 8517) {
            setClientes(new ClienteDao().findAll());
        } else {
            setClientes(new ClienteDao().findByPrefixo(func.getPrefixo().toString()));
        }
    }
 
    public void pegarFuncionario() {
        try {
            func = UserSecurityUtils.getUserFuncionario().getFuncionario();
        } catch (UserNotLogged ex) {
            Logger.getLogger(ClienteLobController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @Override
    public void edit() {
        try {
            
            Cliente c = new ClienteDao().findByMci(selected.getMci());
            c.setNomeCliente(selected.getNomeCliente());
            c.setNumCarteira(Integer.parseInt(selected.getNumCarteira()));
            c.setPrefixo(selected.getPrefixo());
            c.setTelefone(selected.getTelefone());
            opSucesso = new ClienteDao().makePersistent(c) != null;
            setClientes(null);
        } catch (Exception e) {
            System.out.println("Erro ao editar Cliente:" + e.getMessage());
        }
    }

    public String onRowSelect(SelectEvent event) {

        setSelected((clienteSeguridade) event.getObject());
        FacesMessage msg = new FacesMessage("Cliente Selecionado", ((clienteSeguridade) event.getObject()).getNomeCliente());
        FacesContext.getCurrentInstance().addMessage(null, msg);
        
        return "erro";
    }

    public ClienteDao getClienteDao() {
        if (this.clienteDao == null) {
            clienteDao = new ClienteDao();
        }
        return clienteDao;
    }
    
    public void lerArq(FileUploadEvent event) {
        try {
            UploadedFile arq = event.getFile();
            if (arq != null) {
                CsvReader csvReader = new CsvReader(arq.getInputstream(), Charset.forName("utf-8"));
                csvReader.setDelimiter(';');
                csvReader.readHeaders();
                while (csvReader.readRecord()) {
                    Integer Mci = Integer.parseInt(csvReader.get(0));
                    String Nome = csvReader.get(1);
                    String telefone = csvReader.get(2);
                    String carteira = csvReader.get(3);
                    String prefixo = csvReader.get(4);
                    clienteSeguridade clienteAtualizado = new ClienteSeguridadeDao().findById(Mci, true);
                    try {
                        if (clienteAtualizado != null) {
                            clienteAtualizado.setPrefixo(prefixo);
                            clienteAtualizado.setNumCarteira(carteira);
                            clienteAtualizado.setNomeCliente(Nome);
                            clienteAtualizado.setMci(Mci);
                            clienteAtualizado.setTelefone(telefone);

                            Operacao operacao = getNovaOperacao();
                            try {
                                Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
                                Type clienteType = new TypeToken<clienteSeguridade>() {
                                }.getType();
                                String jsonOut = gson.toJson(clienteAtualizado, clienteType);
                                operacao.setJsonObj(jsonOut);
                            } catch (Exception e) {
                                System.out.println("Errro <<<<<<<<<<<<<<< " + e.getMessage() + " >>>>>>>>>>>>>>>>");
                            }
                            operacao = new OperacaoDAO().makePersistent(operacao);
                            opSucesso = new ClienteSeguridadeDao().makePersistent(clienteAtualizado) != null;
                        } 
                        else {
                            clienteSeguridade clienteNovo = new clienteSeguridade();
                            clienteNovo.setMci(Mci);
                            clienteNovo.setPrefixo(prefixo);
                            clienteNovo.setNumCarteira(carteira);
                            clienteNovo.setNomeCliente(Nome);
                            clienteNovo.setTelefone(telefone);

                            Operacao operacao = getNovaOperacao();
                            try {
                                Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
                                Type clienteType = new TypeToken<clienteSeguridade>() {
                                }.getType();
                                String jsonOut = gson.toJson(clienteNovo, clienteType);
                                operacao.setJsonObj(jsonOut);
                            } catch (Exception e) {
                                System.out.println("Errro <<<<<<<<<<<<<<< " + e.getMessage() + " >>>>>>>>>>>>>>>>");
                            } 
                            opSucesso = new ClienteSeguridadeDao().save(clienteNovo) != null;
                        }

                    } catch (HibernateException ex) {
                        Logger.getLogger(clienteSeguridade.class.getName()).log(Level.SEVERE, "Erro na importação do CSV, favor fazer upload do arquivo csv novamente!", ex);
                    }
                }

                FacesMessage msg = new FacesMessage("SUCESSO!", "Base de Dados importada com sucesso!");
                FacesContext.getCurrentInstance().addMessage(null, msg);
                Logger.getLogger(clienteSeguridade.class.getName()).log(Level.INFO, "Importação dos dados concluída!");
            } else {
                FacesMessage msg = new FacesMessage("ATENÇÃO!", "Favor fazer upload do arquivo!");
                FacesContext.getCurrentInstance().addMessage(null, msg);
                Logger.getLogger(clienteSeguridade.class.getName()).log(Level.INFO, "Favor fazer upload do arquivo!");
            }
        } catch (IOException ex) {
            Logger.getLogger(clienteSeguridade.class.getName()).log(Level.SEVERE, "Erro CSVReader: " + ex.getMessage());
        }
    }

    @Override
    public void delete() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void add() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
