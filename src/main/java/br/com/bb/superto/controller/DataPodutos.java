/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.controller;

import br.com.bb.superto.model.ClienteProduto;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author administrador
 */
class DataPodutos {
    private Date data;
    private List<ClienteProduto> lista = new ArrayList<ClienteProduto>(); 
    
    public void addCP(ClienteProduto cp)
    {
        lista.add(cp);
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public List<ClienteProduto> getLista() {
        return lista;
    }

    public void setLista(List<ClienteProduto> lista) {
        this.lista = lista;
    }
    
    
    
    @Override
    public String toString() {
        return "DataProdutos{" + "data=" + data + ", lista=" + lista + '}';
    }
    
}
