package br.com.bb.superto.controller;

import br.com.bb.superto.dao.ProdutoDao;
import br.com.bb.superto.model.Produto;
import com.csvreader.CsvReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

/**
 * @author Vinicius Aires Barros
 * @email viniciusaires7@gmail.com
 * @git www.github.com.br/viniciusaires7
 */
@ManagedBean
@ViewScoped
public class ProdutoController extends GenericController<Produto> {

    private List<Produto> listProdutos;
    private Object arquivo;
    private boolean opSucesso;

    public ProdutoController() {
    }

    public List<Produto> getListProdutos() {
        listProdutos = new ProdutoDao().findAll();
        return listProdutos;
    }

    public void setListProdutos(List<Produto> listProdutos) {
        this.listProdutos = listProdutos;
    }

    public boolean isOpSucesso() {
        return opSucesso;
    }

    public void setOpSucesso(boolean opSucesso) {
        this.opSucesso = opSucesso;
    }

    public void fileUploadListner(FileUploadEvent event) {
        lerarquivo(event.getFile());
    }

    private void lerarquivo(UploadedFile file) {
        try {
            CsvReader csvReader = new CsvReader(file.getInputstream(), Charset.forName("UTF-8"));
            csvReader.setDelimiter(';');
            csvReader.readHeaders();
            while (csvReader.readRecord()) {
                Integer idProduto = Integer.parseInt(csvReader.get(0));
                String descricaoProduto = csvReader.get(1);
                String apelidoProduto = csvReader.get(2);
                if (apelidoProduto == null || apelidoProduto.isEmpty()) {
                    apelidoProduto = descricaoProduto;
                }
                Produto p = new ProdutoDao().findById(idProduto, true);
                if (p == null) {
                    Produto novoProduto = new Produto();
                    novoProduto.setId(idProduto);
                    novoProduto.setDescricao(descricaoProduto);
                    novoProduto.setApelido(apelidoProduto);
                    new ProdutoDao().save(novoProduto);
                } else {
                    p.setDescricao(descricaoProduto);
                    p.setApelido(apelidoProduto);
                    new ProdutoDao().makePersistent(p);
                }
            }
            opSucesso = true;
        } catch (IOException ex) {
            Logger.getLogger(ProdutoController.class.getName()).log(Level.SEVERE, null, ex);
            opSucesso = false;
        }
    }

    @Override
    public void edit() {
        String msg = "";
        try {
            new ProdutoDao().makePersistent(selected);
            opSucesso = true;
            msg = "Produto editado com sucesso";
        } catch (Exception ex) {
            Logger.getLogger(ProdutoController.class.getName()).log(Level.SEVERE, null, ex);
            opSucesso = false;
            msg = "Erro ao editar o produto!";
        } finally{
            RequestContext.getCurrentInstance().update("alertas");
            RequestContext.getCurrentInstance().execute("mostraAlerta('editDialog','"+msg+"')");
        }
    }

    @Override
    public void delete() {
        try {
            new ProdutoDao().makeTransient(selected);
        } catch (Exception ex) {
        }
    }

    @Override
    public void add() {
        try {
            new ProdutoDao().save(value);
        } catch (Exception ex) {
        }
    }

}
