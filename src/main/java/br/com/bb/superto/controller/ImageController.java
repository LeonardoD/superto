/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.controller;

import br.com.bb.superto.dao.ImageDao;
import br.com.bb.superto.model.Archive;
import br.com.bb.superto.model.Image;
import br.com.bb.superto.model.UserNotLogged;
import br.com.bb.superto.util.Pagination;
import br.com.bb.superto.util.Util;
import org.hibernate.exception.ConstraintViolationException;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author maycon
 */
@ManagedBean(name = "imageController")
@ViewScoped
public class ImageController extends GenericController<Image> implements java.io.Serializable {

    List<Image> listaDeFotos;
    private UploadedFile file;
    private Pagination pagImages;
    private List<Image> imagensFiltradas;
    private HashMap<Image, UploadedFile> mapFotos;

    public ImageController() {
        mapFotos = new HashMap<>();
        listaDeFotos = new ArrayList<>();
    }

    public UploadedFile getFile() {
        return file;
    }

    public void setFile(UploadedFile file) {
        this.file = file;
    }

    public HashMap<Image, UploadedFile> getListaFotos() {
        return mapFotos;
    }

    public void setListaFotos(HashMap<Image, UploadedFile> listaFotos) {
        this.mapFotos = listaFotos;
    }

    public HashMap<Image, UploadedFile> getMapFotos() {
        return mapFotos;
    }

    public void setMapFotos(HashMap<Image, UploadedFile> mapFotos) {
        this.mapFotos = mapFotos;
    }

    public List<Image> getListaDeFotos() {
        return listaDeFotos;
    }

    public void setListaDeFotos(List<Image> listaDeFotos) {
        this.listaDeFotos = listaDeFotos;
    }

    public Pagination getPagImages() {
        if (pagImages == null) {
            pagImages = new Pagination(10) {
                @Override
                public List<Image> getPageDataModel() {
                    return new ImageDao().getImages(getPageFirstItem(), getPageSize(), false, null);
                }
            };
            pagImages.setRowCount(new ImageDao().getRowCount());
        }
        return pagImages;
    }

    public List<Image> getImagensFiltradas() {
        return imagensFiltradas;
    }

    public void setImagensFiltradas(List<Image> imagensFiltradas) {
        this.imagensFiltradas = imagensFiltradas;
    }

    public List<Image> getImagens() {
        return new ImageDao().findAll();
    }

    public void fileUploadListener(FileUploadEvent event) {
        file = event.getFile();
        value.setTitle(file.getFileName().substring(0, file.getFileName().lastIndexOf(".")));
    }

    public void fileUploadAll(FileUploadEvent event) {
        try {
            Archive archive = new ArchiveController().add(event.getFile());
            Image image = new Image();
            image.setVisible(true);
            if (archive != null) {
                image.setArchive(archive);
                mapFotos.put(image, event.getFile());
            }
        } catch (UserNotLogged | IOException e) {
            System.out.println("Erro ao anexar foto " + event.getFile().getFileName() + " : " + e.getMessage());
        }

    }

    @Override
    public void edit() {
        List<String> errors = new ArrayList<>();
        try {
            if (selected == null) {
                throw new NullPointerException();
            }
            selected = new ImageDao().makePersistent(selected);
            addFacesMsg("Imagem '" + selected.getTitle() + "' editada com sucesso.", FacesMessage.SEVERITY_INFO);
        } catch (NullPointerException ex) {
            errors.add("Selecione uma imagem.");
        } finally {
            for (String e : errors) {
                addFacesMsg(e, FacesMessage.SEVERITY_ERROR);
            }
        }
    }

    @Override
    public void delete() {
        List<String> errors = new ArrayList<>();
        try {
            if (selected == null) {
                throw new NullPointerException();
            }
            ImageDao imageDao = new ImageDao();
            selected = imageDao.makeTransient(selected);
            imageDao.flush();
            addFacesMsg("Imagem '" + selected.getTitle() + "' removida com sucesso.", FacesMessage.SEVERITY_INFO);
            selected = null;
        } catch (NullPointerException ex) {
            errors.add("Imagem não selecionada.");
        } catch (ConstraintViolationException ex) {
            errors.add("Não foi possivel deletar a imagem selecionada.");
        } finally {
            for (String e : errors) {
                addFacesMsg(e, FacesMessage.SEVERITY_ERROR);
                System.out.println("e");
            }

        }
    }

    public void upload() {
        if (file != null) {
            System.out.println("Nome do arquivo\n\t" + file.getFileName());
        } else {
            System.out.println("Null");
        }
    }

    public void addAll() {
        try {
            listaDeFotos.addAll(mapFotos.keySet());
            new ImageDao().saveList(listaDeFotos);
            mapFotos.keySet().forEach(image -> criarThumbnailFromArchive(mapFotos.get(image), image.getArchive()));
        } catch (Exception e) {
            System.out.println("Erro: " + e.getMessage());
        }
    }

    @Override
    public void add() {
        try {
            Archive archive = new ArchiveController().add(file);
            value.setArchive(archive);
            value = new ImageDao().makePersistent(value);
            addFacesMsg("Imagem '" + value.getTitle() + "' salva com sucesso!", FacesMessage.SEVERITY_INFO);
            criarThumbnailFromArchive(file, archive);
        } catch (FileNotFoundException | UserNotLogged ex) {
            Logger.getLogger(ImageController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ImageController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void criarThumbnailFromArchive(UploadedFile file, Archive archive) {
        try {
            String path = Util.getMediaPath();
            String url = "thumbnail" + File.separator + archive.getRadomName() + "_tb" + ".jpg";
            byte[] byteData = file.getContents();
            byte[] f = createThumbnail(byteData, 160);

            try (FileOutputStream fos = new FileOutputStream(new File(path + url))) {
                fos.write(f);
            }
        } catch (IOException ex) {
            Logger.getLogger(ImageController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private byte[] createThumbnail(byte[] orig, int maxDim) {

        try {
            ImageIcon imageIcon = new ImageIcon(orig);
            java.awt.Image inImage = imageIcon.getImage();
            double scale = (double) maxDim / (double) inImage.getWidth(null);
            double scale2 = (double) 120 / (double) inImage.getHeight(null);

            int scaledW = (int) (scale * inImage.getWidth(null));
            int scaledH = (int) (scale2 * inImage.getHeight(null));

            BufferedImage outImage = new BufferedImage(scaledW, scaledH, BufferedImage.TYPE_INT_RGB);
            AffineTransform tx = new AffineTransform();

            if (scale < 1.0d) {
                tx.scale(scale, scale2);
            }

            Graphics2D g2d = outImage.createGraphics();
            g2d.drawImage(inImage, tx, null);
            g2d.dispose();

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ImageIO.write(outImage, "JPG", baos);
            byte[] bytesOut = baos.toByteArray();

            return bytesOut;
        } catch (IOException e) {
            System.out.println("Erro: " + e.getMessage());
        }
        return null;

    }
}
