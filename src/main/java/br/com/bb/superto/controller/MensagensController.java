/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.controller;

import br.com.bb.superto.dao.FuncionariosDao;
import br.com.bb.superto.dao.MensagemDao;
import br.com.bb.superto.model.Funcionarios;
import br.com.bb.superto.model.Mensagens;
import br.com.bb.superto.model.UserNotLogged;
import br.com.bb.superto.security.UserSecurityUtils;
import br.com.bb.superto.util.Pagination;
import br.com.bb.superto.util.Util;
import org.primefaces.event.UnselectEvent;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author maycon
 */
@ManagedBean(name = "mensagensController")
@ViewScoped
public class MensagensController extends GenericController<Mensagens> implements java.io.Serializable {

    public List<Funcionarios> funcionariosSelecionados;
    private Pagination pagMensagens;
    private FuncionariosDao funcDao;
    private String querySearch = "Pesquisar...";

    public MensagensController() {
    }

    public String getQuerySearch() {
        return querySearch;
    }

    public void setQuerySearch(String querySearch) {
        this.querySearch = querySearch;
    }

    public Pagination getPagMensagens() {
        if (pagMensagens == null) {
            pagMensagens = new Pagination(10) {

                @Override
                public List<Mensagens> getPageDataModel() {
                    try {
                        System.out.println(this.getQuery());
                        setRowCount(new MensagemDao().rowCount(UserSecurityUtils.getUserFuncionario().getFuncionario(), getQuery()));
                        return new MensagemDao().getMensagens(UserSecurityUtils.getUserFuncionario().getFuncionario(), getPageFirstItem(), getPageSize(), getQuery());
                    } catch (UserNotLogged ex) {
                        Logger.getLogger(MensagensController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    return null;
                }

            };
        }
        return pagMensagens;
    }

    @Override
    public void edit() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void delete() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void add() {
        MensagemDao msgDao = new MensagemDao();
        for (Funcionarios func : funcionariosSelecionados) {
            try {
                Mensagens msg = new Mensagens();
                msg.setDataDeCriacao(Util.getDate());
                msg.setDataDeEnvio(Util.getDate());
                msg.setDespachada(true);
                msg.setDestinatario(func);
                msg.setMensagem(value.getMensagem());
                msg.setTipo(value.getTipo());
                msg.setVisualizada(false);
                msg.setRemetente(UserSecurityUtils.getUserFuncionario().getFuncionario());
                msgDao.save(msg);
            } catch (UserNotLogged ex) {
                Logger.getLogger(MensagensController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        addFacesMsg("Mensagens enviadas com sucesso!", FacesMessage.SEVERITY_INFO);
    }

    public List<Funcionarios> getFuncionariosSelecionados() {
        return funcionariosSelecionados;
    }

    public void setFuncionariosSelecionados(List<Funcionarios> funcionariosSelecionados) {
        this.funcionariosSelecionados = funcionariosSelecionados;
    }

    public List<Funcionarios> complete(String query) {
        return getFuncionariosDao().pesquisarFuncionarios(query.toUpperCase());
    }

    public FuncionariosDao getFuncionariosDao() {
        if (funcDao == null) {
            funcDao = new FuncionariosDao();
        }
        return funcDao;
    }

    public List<Mensagens> mensagensNaoLidas(Integer qtd, boolean all) {
        try {
            return new MensagemDao().getMensagensNaoLidas(UserSecurityUtils.getUserFuncionario().getFuncionario(), qtd, all);
        } catch (UserNotLogged ex) {
            Logger.getLogger(MensagensController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public void handleUnselect(UnselectEvent event) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Unselected:" + event.getObject().toString(), null);

        FacesContext.getCurrentInstance().addMessage(null, message);
    }

    public void pesquisar() {
        System.out.println(querySearch);
        getPagMensagens().setQuery(querySearch);
    }

    public void pesquisarLimpar() {
        getPagMensagens().setQuery(null);
    }

}
