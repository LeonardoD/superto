/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.controller;

import br.com.bb.superto.dao.ProdutoSeguridadeDao;
import br.com.bb.superto.model.Cliente;
import br.com.bb.superto.model.ProdutoSeguridade;
import java.util.Collections;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.util.List;
import javax.faces.context.FacesContext;
import org.primefaces.event.SelectEvent;

/**
 * @author Jhemeson S. Mota
 */
@ManagedBean(name = "produtoSeguridadeController")
@ViewScoped
public class ProdutoSeguridadeController extends GenericController<ProdutoSeguridade> {

    private List<ProdutoSeguridade> produtos;
    private ProdutoSeguridade produto;
    private ProdutoSeguridadeDao psDao;
    private boolean opSucesso;
    private String nomeProduto;
    private Integer codProduto;
    private String bloco;
    private List<String> blocos;

    public List<String> getBlocos() {
        blocos.add("Bloco a");
        blocos.add("Bloco b");
        blocos.add("Bloco c");
        return blocos;
    }

    public void setBlocos(List<String> blocos) {
        this.blocos = blocos;
    }
    private boolean opSucess;

    public ProdutoSeguridade getProduto() {
        return produto;
    }

    public void setProduto(ProdutoSeguridade produto) {
        this.produto = produto;
    }

    public ProdutoSeguridadeDao getPsDao() {
        return psDao;
    }

    public void setPsDao(ProdutoSeguridadeDao psDao) {
        this.psDao = psDao;
    }

    public boolean isOpSucesso() {
        return opSucesso;
    }

    public void setOpSucesso(boolean opSucesso) {
        this.opSucesso = opSucesso;
    }

    public String getNomeProduto() {
        return nomeProduto;
    }

    public void setNomeProduto(String nomeProduto) {
        this.nomeProduto = nomeProduto;
    }

    public Integer getCodProduto() {
        return codProduto;
    }

    public void setCodProduto(Integer codProduto) {
        this.codProduto = codProduto;
    }

    public String getBloco() {
        return bloco;
    }

    public void setBloco(String bloco) {
        this.bloco = bloco;
    }
    
    
    public List<ProdutoSeguridade> getProdutos() {
        produtos = new ProdutoSeguridadeDao().findLista();
        return produtos;
    }
    
    public String getProdutosByCod(Integer cod) {
        try{
            produto = new ProdutoSeguridadeDao().findByCodProduto(cod);
            return produto.getNomeProduto();
        }
        catch(Exception ex){
            System.out.print("Erro ao pegar os produtos por código: " + ex);
        }
        return null;
    }
    
    public String getBlocoProdutoByCod(Integer cod) {
        try
        {
            produto= new ProdutoSeguridadeDao().findByCodProduto(cod);
            return produto.getBloco();
        }
        catch(Exception ex)
        {
            System.out.print("Erro: " + ex);
        }
        return "";
    }
    
    @Override
    public void edit() {
        try {
        } catch (Exception e) {
            System.out.println("Erro ao editar:" + e.getMessage());
        }
    }

    public String onRowSelect(SelectEvent event) {

        setSelected((ProdutoSeguridade) event.getObject());
        FacesMessage msg = new FacesMessage("Cliente Selecionado", ((Cliente) event.getObject()).getNomeCliente());
        FacesContext.getCurrentInstance().addMessage(null, msg);
        
        return "erro";
    }

    @Override
    public void delete() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void add() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void addProduto() {
        System.out.print("Entramos no metodo addProduto");
        try {
            ProdutoSeguridade aux = new ProdutoSeguridade();
            aux.setBloco(bloco);
            aux.setCodProduto(codProduto);
            aux.setNomeProduto(nomeProduto);
            new ProdutoSeguridadeDao().makePersistent(aux);
            opSucess = true;
        } catch (Exception ex) {
            opSucess = false;
        }
        codProduto = null;
        nomeProduto = null;
        bloco = null;
    }
}
