package br.com.bb.superto.controller;

import br.com.bb.superto.dao.CuriosidadesDao;
import br.com.bb.superto.dao.NewsDao;
import br.com.bb.superto.dao.OperacaoDAO;
import br.com.bb.superto.model.Archive;
import br.com.bb.superto.model.Curiosidade;
import br.com.bb.superto.model.Mercado;
import br.com.bb.superto.model.Image;
import br.com.bb.superto.model.News;
import br.com.bb.superto.model.Operacao;
import br.com.bb.superto.model.UserNotLogged;
import br.com.bb.superto.util.FacesUtils;
import br.com.bb.superto.util.Pagination;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.validation.ConstraintViolationException;
import org.primefaces.event.FileUploadEvent;

/**
 *
 * @author Jheemeson S. Mota
 */
@ManagedBean(name = "curiosidadesController")
@ViewScoped
public class CuriosidadesController extends GenericController<Curiosidade> implements java.io.Serializable {

    private Pagination pagNews;
    private Mercado setorFilter;
    private String categoriaFilter;
    private Archive novaCapa;
    private boolean opSucesso;
    private List<Curiosidade> listaCuriosidades;
    private int sizeListaNoticias;
    private String id; // Id para buscar a curiosidade

    public CuriosidadesController() {
        opSucesso = false;
    }

    public Mercado getSetorFilter() {
        return setorFilter;
    }

    public void setSetorFilter(Mercado setorFilter) {
        this.setorFilter = setorFilter;
    }

    public String getCategoriaFilter() {
        return categoriaFilter;
    }

    public void setCategoriaFilter(String categoriaFilter) {
        this.categoriaFilter = categoriaFilter;
    }

    public List<Curiosidade> getAll() {
        return new CuriosidadesDao().findAll();
    }

    public List<Curiosidade> getAtivos() {
        return new CuriosidadesDao().getAtivos();
    }
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void adicionarCapa(Image image) {
        value.setCapa(image);
    }

    public void uploadCapa() {
        ImageController imageController = (ImageController) FacesUtils.getViewAttribute("imageController");
        imageController.add();
        adicionarCapa(imageController.getValue());
    }

    public boolean isOpSucesso() {
        return opSucesso;
    }

    public void setOpSucesso(boolean opSucesso) {
        this.opSucesso = opSucesso;
    }

    @Override
    public void edit() {
        try {
            System.out.println("<<<<<<<<<<<< Antes da operação: "+selected+">>>>>>>>>>>>>>>");
            if (selected != null) {
                selected.getOperacaoModificacao().add(new OperacaoDAO().save(getNovaOperacao()));
                
                System.out.println("<<<<<<<<<<<< Depois da operação: "+selected+">>>>>>>>>>>>>>>");
                opSucesso = new CuriosidadesDao().makePersistent(selected) != null;
            }
            
        } catch (Exception ex) {
            System.out.println("Erro ao editar noticia: " + ex.getMessage());
        }
    }

    public void editSel(Curiosidade c) {
        try {
            System.out.println("<<<<<<<<<<<< Antes da operação: "+ c +">>>>>>>>>>>>>>>");
            if (c != null) {
                c.getOperacaoModificacao().add(new OperacaoDAO().save(getNovaOperacao()));
                
                System.out.println("<<<<<<<<<<<< Depois da operação: "+ c +">>>>>>>>>>>>>>>");
                opSucesso = new CuriosidadesDao().makePersistent(c) != null;
            }
            
        } catch (Exception ex) {
            System.out.println("Erro ao editar noticia: " + ex.getMessage());
        }
    }
    
    @Override
    public void delete() {
        try {
            if (selected == null) {
                throw new NullPointerException();
            }
            opSucesso = new CuriosidadesDao().makeTransient(selected) != null;
            opSucesso = true;
            selected = null;
        } catch (NullPointerException | ConstraintViolationException ex) {
            System.out.println("Erro ao deletar curiosidade: " + ex.getMessage());
        }
    }

    @Override
    public void add() {
        System.out.print("Entramos no método de adição");
        try {
            Operacao op = getNovaOperacao();
            value.setOperacaoCriacao(new OperacaoDAO().save(op));
            value.setOperacaoModificacao(new ArrayList<>());
            value.getOperacaoModificacao().add(new OperacaoDAO().save(op));
            opSucesso = new CuriosidadesDao().save(value) != null;
            value = new Curiosidade();
        } catch (Exception ex) {
            System.out.println("Erro ao salvar a noticia: " + ex.getMessage());
        }
    }

    public void fileUploadCapa(FileUploadEvent event) {
        try {
            novaCapa = new ArchiveController().add(event.getFile());
        } catch (UserNotLogged | IOException e) {
            addFacesMsg("Falha no Upload da Capa:  '" + e.getMessage(), FacesMessage.SEVERITY_ERROR);
        }
    }

}
