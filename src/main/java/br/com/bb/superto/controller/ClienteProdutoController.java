/*
 * author: Jhemeson
 */
package br.com.bb.superto.controller;
import br.com.bb.superto.dao.AgenciasDao;
import br.com.bb.superto.dao.BlocoDao;
import br.com.bb.superto.dao.ClienteProdutoDao;
import br.com.bb.superto.dao.ClienteSeguridadeDao;
import br.com.bb.superto.dao.NewsDao;
import br.com.bb.superto.dao.OperacaoDAO;
import br.com.bb.superto.dao.ProdutoSeguridadeDao;
import br.com.bb.superto.dao.UserHistoryDao;
import br.com.bb.superto.model.Agencias;
import br.com.bb.superto.model.Bloco;
import br.com.bb.superto.model.ClienteLob;
import br.com.bb.superto.model.ClienteProduto;
import br.com.bb.superto.model.Operacao;
import br.com.bb.superto.model.ProdutoSeguridade;
import br.com.bb.superto.model.UserNotLogged;
import br.com.bb.superto.model.clienteSeguridade;
import br.com.bb.superto.security.UserSecurityUtils;
import com.csvreader.CsvReader;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import java.io.IOException;
import java.lang.reflect.Type;
import java.nio.charset.Charset;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.context.FacesContext;
import javax.validation.ConstraintViolationException;
import org.hibernate.HibernateException;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultScheduleEvent;
import org.primefaces.model.ScheduleEvent;
import org.primefaces.model.ScheduleModel;
import org.primefaces.model.UploadedFile;

/**
 * @author Jhemeson S. Mota
 */
@ManagedBean(name = "ClienteProdutoController")
@ViewScoped
public class ClienteProdutoController extends GenericController<ClienteProduto> {

    private List<ClienteProduto> itens;
    private List<ClienteProduto> itensListaDoDia;
    private List<ClienteProduto> itensFiltrados;
    private List<ClienteProduto> itensFiltradosMci;
    private List<ClienteProduto> listaAdicionada = new ArrayList<ClienteProduto>();
    private String possuiCancelados;
    private String possuiVencidos;
    private String possuiNaoContatados;
    private boolean possuiVincendos;
    private boolean possuiVincendosNaSemana;
    private ClienteProduto item;
    private boolean opSucess;
    private UploadedFile file;
    private ClienteProduto selectedRow;
    private List<ClienteProduto> selectedRows;
    private boolean selecionado;
    private ClienteProduto modelo;
    private Integer codProduto;
    private Integer numProposta;
    private Date dataInicioVigencia;
    private double premio;
    private boolean contratado;
    private boolean renderizado;
    private Date dataEvento;
    private String logSubida = "Não Subido";
    private boolean habilitaVizualisar = false;
    
    public String getLogSubida() {
        return logSubida;
    }

    public void setLogSubida(String logSubida) {
        this.logSubida = logSubida;
    }
    
    public Date getDataEvento() {
        return dataEvento;
    }

    public void setDataEvento(Date dataEvento) {
        System.out.print("Era: " +      this.dataEvento);
        System.out.print("Virou: " + dataEvento);
        this.dataEvento = dataEvento;
    }
    
    public Integer getCodProduto() {
        return codProduto;
    }

    public boolean isOpSucess() {
        return opSucess;
    }

    public void setOpSucess(boolean opSucess) {
        this.opSucess = opSucess;
    }

    public boolean isContratado() {
        return contratado;
    }

    public boolean isRenderizado() {
        return renderizado;
    }

    public void setRenderizado(boolean renderizado) {
        this.renderizado = renderizado;
    }
    
    public boolean pegaRenderizado(ClienteProduto c)
    {
        renderizado = c.isContratado();
        return renderizado;
    }
    
    public void setContratado(boolean contratado) {
        this.contratado = contratado;
    }

    public void setCodProduto(Integer codProduto) {
        this.codProduto = codProduto;
    }

    public List<ClienteProduto> getListaAdicionada() {
        return listaAdicionada;
    }

    public void setListaAdicionada(List<ClienteProduto> listaAdicionada) {
        this.listaAdicionada = listaAdicionada;
    }

    public Integer getNumProposta() {
        return numProposta;
    }

    public void setNumProposta(Integer numProposta) {
        this.numProposta = numProposta;
    }

    public Date getDataInicioVigencia() {
        return dataInicioVigencia;
    }

    public void setDataInicioVigencia(Date dataInicioVigencia) {
        this.dataInicioVigencia = dataInicioVigencia;
    }

    public double getPremio() {
        return premio;
    }

    public void setPremio(double premio) {
        this.premio = premio;
    }

    public ClienteProduto getModelo() {
        return modelo;
    }

    public void setModelo(ClienteProduto modelo) {
        this.modelo = modelo;
    }
    
    public ClienteProduto pegaModelo(List <ClienteProduto> listModelo)
    {
        int i = 0;
        for(ClienteProduto cp : listModelo)
        {
            if(i == 0)
            {
                return cp;
            }
        }
        return null;
    }

    public boolean isSelecionado() {
        return selecionado;
    }

    public void setSelecionado(boolean selecionado) {
        this.selecionado = selecionado;
    }

    public ClienteProduto getSelectedRow() {
        return selectedRow;
    }

    public void setSelectedRow(ClienteProduto selectedRow) {
        this.selectedRow = selectedRow;
    }

    public List<ClienteProduto> getSelectedRows() {
        return selectedRows;
    }

    public boolean isHabilitaVizualisar() {
        List<ClienteProduto> cp = new ArrayList<ClienteProduto>();
        cp = this.selectedRows;
        try{
            if(cp.size()>0){
                habilitaVizualisar = true;
            }
            else{
                habilitaVizualisar = false;
            }
        }
        catch(Exception ex){
            System.out.print("Erro: " + ex);
            habilitaVizualisar = false;
        }
        finally{
            return habilitaVizualisar;
        }
    }

    public void setHabilitaVizualisar(boolean habilitaVizualisar) {
        this.habilitaVizualisar = habilitaVizualisar;
    }
    
    public void setSelectedRows(List<ClienteProduto> selectedRows) {
        this.selectedRows = selectedRows;
    }
    
    public boolean addProd(ClienteProduto p)
    {
        selectedRows.add(selected);
        try
        {
            for(ClienteProduto aux: selectedRows)
            {
                if(aux.getId() == p.getId())
                {
                    return true;
                }
            }
        }
        catch(Exception ex)
        {
            System.out.print("Exception ex (Método Add Produto na lista) : " + ex);
        }
        return false;
    }

    public List<ClienteProduto> getItensFiltrados(int pref) {
        itensFiltrados.clear();
        for(ClienteProduto c : this.getItens()){
            if(c.getPrefixo().equals(pref)){
                itensFiltrados.add(c);
            }
        }
        return itensFiltrados;
    }

    public List<ClienteProduto> getItensFiltradosMci(int mci) {
        itensFiltradosMci = new ClienteProdutoDao().findByMci(mci);
        return itensFiltradosMci;
    }
    
    public String pegaDataFim(ClienteProduto cp)
    {
        try
        {
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            if(cp.getDatavencimento() == null)
            {
                String data = sdf.format(cp.getDatacancelamento());
                return data;
            }
            else
            {
                String data = sdf.format(cp.getDatavencimento());
                return data;
            }
        }
        catch(Exception ex)
        {
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            System.out.print("Erro ao retornar data Fim: " + ex);
            String data = sdf.format(cp.getDatavencimento());
            return data;
        }
    }
    
    public String pegaEstado(ClienteProduto cp)
    {
        try
        {
            Date aux = new Date();
            if(cp.getDatavencimento() != null)
            {
                if(cp.getDatavencimento().after(aux))
                {
                    return "Vincendos";
                }
                else
                {
                    return "Vencidos";
                }
            }
            else
            {
                   return "Cancelados";
            }
        }
        catch(Exception ex)
        {
            System.out.print("Erro ao retornar data Fim: " + ex);
            return "";
        }
    }
    
    public List<ClienteProduto> pegaProdutosByBlocoAndCliente(int mci, String b)
    {
        itensFiltradosMci = new ClienteProdutoDao().findByMci(mci);
        List <ClienteProduto> aux = new ClienteProdutoDao().findByMci(mci);
        
        for(ClienteProduto cp : itensFiltradosMci)
        {
            ProdutoSeguridade ps;
            try
            {
                ps = new ProdutoSeguridadeDao().findByCodProduto(cp.getCodproduto());
            }
            catch(Exception ex)
            {
                ps = new ProdutoSeguridade();
            }
            if(ps.getBloco().equalsIgnoreCase(b))
            {
            }
            else
            {
                try
                {
                   aux.remove(cp);
                }
                catch(Exception ex)
                {
                    aux.remove(cp);
                }   
            }
        }
        return aux;
    }
    
    public List<ClienteProduto> pegaProdutosIguais(List<ClienteProduto> lista, List<ClienteProduto> lista2)
    {       
        Integer codPrim = null;
        List<DataPodutos> dates = new ArrayList<DataPodutos>();
        List<DataPodutos> datesCanc = new ArrayList<DataPodutos>();
        List<ClienteProduto> listaRetorno = new ArrayList<ClienteProduto>();
        System.out.print("CheckPoint 1");
        System.out.print("CheckPoint 1");
        System.out.print("CheckPoint 1");
        for(ClienteProduto cp : lista)
        {
            Date dateAux = new Date();
            Date CancAux = dateAux;
            Date comp = CancAux;
            if(cp.getDatavencimento()==null)
            {
                CancAux = cp.getDatacancelamento();
            }
            else
            {
                dateAux = cp.getDatavencimento();
            }

            if(dates == null) 
            {
                // caso não dê certo, testar com dates.size() == 0;
                if(dateAux != comp)
                {
                    DataPodutos dp = new DataPodutos();
                    dp.setData(dateAux);
                    dates.add(dp);
                }
            }
            else
            {
                boolean existe = false;
                for(DataPodutos dt : dates)
                {
                    if(dateAux == dt.getData())
                    {
                        existe = true;
                    }
                }
                if(existe == false)
                {
                    DataPodutos dp = new DataPodutos();
                    dp.setData(dateAux);
                    dates.add(dp);
                }
            }

            if(datesCanc == null) 
            {
                // caso não dê certo, testar com dates.size() == 0;
                if(CancAux != comp)
                {
                    DataPodutos dp = new DataPodutos();
                    dp.setData(CancAux);
                    dates.add(dp);
                    datesCanc.add(dp);
                }
            }
            else
            {
                boolean exist = false;
                for(DataPodutos dt : dates)
                {
                    if(CancAux == dt.getData())
                    {
                        exist = true;
                    }
                }
                if(exist == false)
                {
                    DataPodutos dp = new DataPodutos();
                    dp.setData(CancAux);
                    dates.add(dp);
                    datesCanc.add(dp);
                }
            }
        }

        System.out.print("CheckPoint 2");
        System.out.print("CheckPoint 2");
        System.out.print("CheckPoint 2");
        
        for(DataPodutos dp : dates)
        {
            for(ClienteProduto cp : lista)
            {
                if(dp.getLista() == null)
                {
                    if(cp.getDatavencimento() == dp.getData())
                    {
                        dp.addCP(cp);
                    }
                    else if(cp.getDatacancelamento() == dp.getData())
                    {
                        dp.addCP(cp);
                    }
                }
                else
                {
                    boolean existe = false;
                    for(ClienteProduto cc : dp.getLista())
                    {
                        if(cc.getCodproduto() == cp.getCodproduto())
                        {
                            existe = true;
                        }
                    }
                    if(existe == false)
                    {
                        if(cp.getDatavencimento() == dp.getData())
                        {
                            dp.addCP(cp);
                        }
                        else if(cp.getDatacancelamento() == dp.getData())
                        {
                            dp.addCP(cp);
                        }
                    }
                }
            }
        }
        
        System.out.print("CheckPoint 3");
        System.out.print("CheckPoint 3");
        System.out.print("CheckPoint 3");
        
        for(DataPodutos dp : dates)
        {
            System.out.print("Entramos no for dos dates: " + dp.getData());
            for(ClienteProduto cp : dp.getLista())
            {
                System.out.print("Entramos no for dos dates.cps: " + cp.getCodproduto());
                System.out.print("Tamanho da lista é: " + listaRetorno.size());
                if(listaRetorno.size()<1)
                {
                    System.out.print("Lista era vazia, estamos adicionando: " + cp.getCodproduto());
                    listaRetorno.add(cp);
                }
                else
                {
                    System.out.print("Lista não era vazia");
                    boolean existe2 = false;
                    for(ClienteProduto xx : listaRetorno)
                    {
                        System.out.print("Entramos no for xx: listaRetorno: xx ==" + xx.getCodproduto());
                        System.out.print("Entramos no for xx: listaRetorno: cp ==" + cp.getCodproduto());
                        System.out.print("Entramos no for xx: listaRetorno: xx.data ==" + xx.getDatavencimento());
                        System.out.print("Entramos no for xx: listaRetorno: cp.data ==" + cp.getDatavencimento());
                        
                        SimpleDateFormat formato = new SimpleDateFormat("MM/dd/yyyy"); //produção
                        String i = null;
                        String f = null;
                        
                        String g = null;
                        String j = null;
                        if(xx.getDatavencimento() != null && cp.getDatavencimento() != null)
                        {
                            i = formato.format(xx.getDatavencimento());
                            f = formato.format(cp.getDatavencimento());
                        }
                        else if(xx.getDatacancelamento() != null && cp.getDatacancelamento() != null)
                        {
                            g = formato.format(xx.getDatacancelamento());
                            j = formato.format(cp.getDatacancelamento());
                        }
                        System.out.print("Entramos no for xx: listaRetorno: xx.data ==" + i);
                        System.out.print("Entramos no for xx: listaRetorno: cp.data ==" + f);
                        
                        if(i != null)
                        {
                            System.out.print("i não é nulo");
                            System.out.print(i.equals(f));
                            if(xx.getCodproduto() == cp.getCodproduto() && i.equals(f))
                            {
                                System.out.print("Existe um produto xx que é igual o cp");
                                existe2 = true;
                            }
                        }
                        else if(g != null)
                        {
                            System.out.print("g não é nulo");
                             System.out.print(g.equals(j));
                            if(xx.getCodproduto() == cp.getCodproduto() && g.equals(j))
                            {
                                System.out.print("Existe um produto xx que é igual o cp");
                                existe2 = true;
                            }
                        }
                        else{
                            System.out.print("i e g são nulos");
                        }
                    }
                    System.out.print("Saímos do for, agora é conferir se ele existe, e adicionar");
                    System.out.print("existe? " + existe2);
                    if(existe2 == false)
                    {
                        System.out.print("Estamos adicionando: " + cp.getCodproduto());
                        listaRetorno.add(cp);
                    }
                }
            }
        }
        
        System.out.print("CheckPoint 4");
        System.out.print("CheckPoint 4");
        System.out.print("CheckPoint 4");
        
        return listaRetorno;
    }
    public List<ClienteProduto> refinaLista(int mci, String b)
    {
        List <ClienteProduto> aux = this.pegaProdutosByBlocoAndCliente(mci, b);
        List <ClienteProduto> aux2 = this.pegaProdutosByBlocoAndCliente(mci, b);
        // até aqui, ele pega todos os produtos deste cliente e bloco corretamente
        
        List <ClienteProduto> aux3 = this.pegaProdutosIguais(aux, aux2);
        return aux3;
    }
    
//    public List<ClienteProduto> refinaListaDia()
//    {
////        itens = new ClienteProdutoDao().findAll();
//        List <ClienteProduto> aux =  new ClienteProdutoDao().findAll();
//        List <ClienteProduto> aux2 =  new ArrayList<ClienteProduto>();
//        
//        for(ClienteProduto cp : aux)
//        {
//            if(cp.getDatavencimento() != null)
//            {
//                if(cp.getDatavencimento().equals(dataEvento))
//                {
//                    aux2.add(cp);
//                }
//            }
//            else
//            {
//                if(cp.getDatacancelamento().equals(dataEvento))
//                {
//                    aux2.add(cp);
//                }
//            }
//        }
//        return aux2;
//    }

    public List<ClienteProduto> pegaItensListaDoDia(Object dia) {
        System.out.print("Dia: " + dia);
        Date auxiliar = new Date();
        auxiliar = (Date) dia;
        System.out.print("Entramos no metodo de pegar a lista");
        System.out.print("Dia: " + dia);
        List <ClienteProduto> aux =  new ClienteProdutoDao().findAll();
        System.out.print("aux recebeu todos");
        itensListaDoDia = new ArrayList<ClienteProduto>();
        System.out.print("Limpamos a lista do dia");
        for(ClienteProduto cp : aux)
        {
            System.out.print("Entramos no for dos cps");
            if(cp.getDatavencimento() != null)
            {
                System.out.print("Entramos no if data vencimento != null");
                SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                System.out.print("Definimos SDF"); 
                String data = sdf.format(cp.getDatavencimento());
                System.out.print("Convertemos data de vencimento: " + data);
                System.out.print("Próx.: converter data do evento");
                System.out.print("data do evento: " + dia);
                String data2 = sdf.format(dia);
                System.out.print("Convertemos data de evento"); 
                System.out.print("Data vencimento: " + data);
                System.out.print("Data Evento: " + data2);
                if(data.equals(data2))
                {
                    System.out.print("Entramos no if data vencimento == dataEvento");
                    itensListaDoDia.add(cp);
                    System.out.print("adicionamos cp na lista");  
                }
            }
            else
            {
                System.out.print("Entramos no IF DATA vencimento == null");
                if(cp.getDatacancelamento().equals(dia))
                {
                    System.out.print("Entramos no IF DATA cancelamento == data do evento");   
                    itensListaDoDia.add(cp);
                    System.out.print("adicionamos cp na lista");   
                }
            }
        }
        System.out.print("Fim do For!!! Proximo passo == retorno");  
        return itensListaDoDia;
    }

    public List<ClienteProduto> getItensListaDoDia() {
        return itensListaDoDia;
    }
    
    public void setItensListaDoDia(List<ClienteProduto> itensListaDoDia) {
        this.itensListaDoDia = itensListaDoDia;
    }
    
    
    
    public int getQtItensByMciAndBloco(Bloco b, ClienteProduto cp){
        itensFiltradosMci = new ClienteProdutoDao().findByMci(cp.getMci());
        int cont = 0;
        for(ClienteProduto cc : itensFiltradosMci)
        {
            try
            {
                if(cp.getDatavencimento() == null)
                {
                    if(cc.getDatacancelamento().equals(cp.getDatacancelamento()) && cc.getCodproduto() == cp.getCodproduto() && cp.isContatado() == false)
                    {
                        cont = cont + 1;
                    }
                }
                else
                {
                    if(cc.getDatavencimento().equals(cp.getDatavencimento()) && cc.getCodproduto() == cp.getCodproduto() && cp.isContatado() == false)
                    {
                        cont = cont + 1;
                    }
                }
            }
            catch(Exception ex)
            {
                System.out.print("Erro: " + ex);
            }
        }
        return cont;
    }
    
    public int getTotalItensByMciAndBloco(Bloco b, ClienteProduto cp){
        itensFiltradosMci = new ClienteProdutoDao().findByMci(cp.getMci());
        int cont = 0;
        for(ClienteProduto cc : itensFiltradosMci)
        {
            try
            {
                if(cp.getDatavencimento() == null)
                {
                    if(cc.getDatacancelamento().equals(cp.getDatacancelamento()) && cc.getCodproduto() == cp.getCodproduto())
                    {
                        cont = cont + 1;
                    }
                }
                else
                {
                    if(cc.getDatavencimento().equals(cp.getDatavencimento()) && cc.getCodproduto() == cp.getCodproduto())
                    {
                        cont = cont + 1;
                    }
                }
            }
            catch(Exception ex)
            {
                System.out.print("Erro: " + ex);
            }
        }
        return cont;
    }
    
    public double getValorItensByMciAndBloco(Bloco b, ClienteProduto cp){
        itensFiltradosMci = new ClienteProdutoDao().findByMci(cp.getMci());
        double cont = 0;
        for(ClienteProduto cc : itensFiltradosMci)
        {
            try
            {
                if(cp.getDatavencimento() == null)
                {
                    if(cc.getDatacancelamento().equals(cp.getDatacancelamento()) && cc.getCodproduto() == cp.getCodproduto() && cp.isContatado() == false)
                    {
                        cont = cont + cp.getPremio();
                    }
                }
                else
                {
                    if(cc.getDatavencimento().equals(cp.getDatavencimento()) && cc.getCodproduto() == cp.getCodproduto() && cp.isContatado() == false)
                    {
                        cont = cont + cp.getPremio();
                    }
                }
            }
            catch(Exception ex)
            {
                System.out.print("Erro: " + ex);
            }
        }
        return cont;
    }


    public boolean isPossuiCancelados(int mci) {        
        return new ClienteProdutoDao().PossuiCancelados(mci);
    }

    public String getPossuiNaoContatados(int mci) {
        possuiNaoContatados = new ClienteProdutoDao().PossuiNaoContatados(mci);
        return possuiNaoContatados;
    }

    public boolean VincAteh30dias(int mci) {
        System.out.print("ENTRAMOS NO MÉTODO VincAteh30dias da Classe ClienteProdutoController");
        try
        {
            boolean aux;
            System.out.print("Declaramos a variável booleana aux");
            System.out.print("Próx linha: aux = new ClienteProdutoDao().VincAteh30dias(mci)");
            aux = new ClienteProdutoDao().VincAteh30dias(mci);
            System.out.print("aux recebeu o valor de vincAteh30dias --- Proximo passo REtornar aux");
            return aux;   
        }
        catch(Exception e)
        {
            System.out.print("Erro: " + e);
            return false;
        }
    }
    
    public boolean VincsMes(Integer mci) {
        Date dtIni = new Date();
        Date dtFim = new Date();
        Calendar calendario = GregorianCalendar.getInstance();
        calendario.setTime(dtFim);
        dtIni.setDate(01);
        dtFim.setDate(calendario.getActualMaximum(Calendar.DAY_OF_MONTH)-1);
        return new ClienteProdutoDao().VincsPorIntervaloDeDatas(dtIni, dtFim, mci);
    }
    
    public boolean VencsMes(Integer mci) {
        Date dtIni = new Date();
        Date dtFim = new Date();
        Calendar calendario = GregorianCalendar.getInstance();
        calendario.setTime(dtFim);
        dtIni.setDate(01);
        dtFim.setDate(calendario.getActualMaximum(Calendar.DAY_OF_MONTH)-1);
        return new ClienteProdutoDao().VencsPorIntervaloDeDatas(dtIni, dtFim, mci);
    }
    
    public boolean Vincs3Meses(Integer mci) {
        Date dtIni = new Date();
        Date dtFim = new Date();
        Calendar calendario = GregorianCalendar.getInstance();
        calendario.setTime(dtFim);
        dtIni.setDate(01);
        dtFim.setMonth(dtFim.getMonth()+2);
        dtFim.setDate(calendario.getActualMaximum(Calendar.DAY_OF_MONTH)-1);
        return new ClienteProdutoDao().VincsPorIntervaloDeDatas(dtIni, dtFim, mci);
    }

    
    public int getQtNaoContatados(int mci) {
        return new ClienteProdutoDao().QTNaoContatados(mci);
    }
    
    public double getPremNaoContatados(int mci) {
        double totalPremio = 0;
        try
        {
            totalPremio = new ClienteProdutoDao().PremNaoContatados(mci);
        }
        catch(Exception ex)
        {
            totalPremio = 0;
        }
        return totalPremio;
    }
    public void setPossuiNaoContatados(String possuiContatados) {
        this.possuiNaoContatados = possuiContatados;
    }

    public UploadedFile getFile() {
        return file;
    }

    public void setFile(UploadedFile file) {
        this.file = file;
    }
    
    
    public Date getHoraAtual(){
        Date date = new Date();
        return date;
    }
    
    public boolean isPossuiVencidos(int mci) {
        return new ClienteProdutoDao().PossuiVencidos(mci);
    }
    
    public boolean isPossuiVincendos(int mci) {
        return new ClienteProdutoDao().PossuiVincendos(mci);
    }
    
    public boolean isPossuiVincendosNaSemana(int mci) {
        System.out.print("********************************");
        possuiVincendosNaSemana = new ClienteProdutoDao().PossuiVincendos7Dias(mci);
        System.out.print("======================= " + possuiVincendosNaSemana + "============");
        System.out.print("********************************");
        return possuiVincendosNaSemana;
    }

    public ClienteProduto getByCodProduto(int cod){
        item = new ClienteProdutoDao().findByCodProduto(cod);
        return item;
    }

    public void setItensFiltrados(List<ClienteProduto> itensFiltrados) {
        this.itensFiltrados = itensFiltrados;
    }
    private boolean opSucesso;

    public List<ClienteProduto> getItens() {
        itens = new ClienteProdutoDao().findAll();
        return itens;
    }

    public void setItens(List<ClienteProduto> itens) {
        this.itens = itens;
    }

    public String qtVencDia(Date dt)
    {
        int prefixo = new LoginController().getFuncionario().getPrefixo();
        int vinc = (int) new ClienteProdutoDao().qtVincVenc(0, dt, prefixo);
        int vincFeitos = (int) new ClienteProdutoDao().qtVincVenc(1, dt, prefixo);
        
        Date data = new Date();
        if(data.after(dt))
        {
            if(vinc == 1)
            {
                return ("Vencido: " + vincFeitos + "/" + vinc);
            }
            return ("Vencidos: " + vincFeitos + "/" + vinc);
        }
        else
        {
            if(vinc == 1)
            {
                return ("Vincendo: " + vincFeitos + "/" + vinc);
            }
            return ("Vincendos: " + vincFeitos + "/" + vinc);
        }
    }
    
    public String qtCancDia(Date dt)
    {
        int prefixo = new LoginController().getFuncionario().getPrefixo();
        int canc = (int) new ClienteProdutoDao().qtCanc(0, dt, prefixo);
        int cancFeitos = (int) new ClienteProdutoDao().qtCanc(1, dt, prefixo);
        
        if(canc != 0)
        {
            if(canc == 1)
            {
                return ("Cancelado: " + cancFeitos + "/" + canc);
            }
            return ("Cancelados: " + cancFeitos + "/" + canc);
        }
        return "";
    }
    
    @Override
    public void edit() {
        if (selected != null) {
            try {
                Operacao operacao = getNovaOperacao();
                try
                {
                    Integer pref = UserSecurityUtils.getUserFuncionario().getFuncionario().getPrefixo();
                    Agencias ad = new AgenciasDao().getAgenciaByPrefixo(pref);
                    ad.setFeitoseguridade(ad.getFeitoseguridade()+1);
                }
                catch(Exception ex)
                {
                    System.out.print("Erro ao setar quantidade de feitos para agencia: " + ex);
                }
                try {
                    Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
                    Type ClienteProdutoType = new TypeToken<ClienteProduto>() {
                    }.getType();
                    String jsonOut = gson.toJson(selected, ClienteProdutoType);
                    operacao.setJsonObj(jsonOut);
                } catch (Exception e) {
                    System.out.println("Errro <<<<<<<<<<<<<<< " + e.getMessage() + " >>>>>>>>>>>>>>>>");
                }
                operacao = new OperacaoDAO().makePersistent(operacao);
                selected.getOperacaoModificacao().add(operacao);
                new ClienteProdutoDao().update(selected);
                opSucess = true;
                FacesMessage msg = new FacesMessage("SUCESSO!", "Alterações Realizadas!");
                FacesContext.getCurrentInstance().addMessage(null, msg);
            } catch (Exception ex) {
                Logger.getLogger(ClienteLobController.class.getName()).log(Level.SEVERE, "Erro na atualização do Cliente!", ex);
            }
        }
    }
    
    public void editFromMultipleDialog2(List <ClienteProduto> ps, ClienteProduto p) throws UserNotLogged 
    {
        System.out.print("Entramos no método de edição");
        contratado = p.isContratado();
        itensFiltradosMci = new ClienteProdutoDao().findByMci(p.getMci());
        List <ClienteProduto> itensAux = new ClienteProdutoDao().findByMci(p.getMci());
        try
        {
            Integer pref = UserSecurityUtils.getUserFuncionario().getFuncionario().getPrefixo();
            Agencias ad = new AgenciasDao().getAgenciaByPrefixo(pref);
            ad.setFeitoseguridade(ad.getFeitoseguridade()+1);
        }
        catch(Exception ex)
        {
            System.out.print("Erro ao setar quantidade de feitos para agencia: " + ex);
        }
        
        for(ClienteProduto cc : itensFiltradosMci)
        {
            try
            {
                    if(p.getDatavencimento() == null)
                    {
                        if(cc.getDatacancelamento().equals(p.getDatacancelamento()) && cc.getCodproduto() == p.getCodproduto())
                        {
                            System.out.print("Entramos no if");
                        }
                        else
                        {
                            System.out.print("Entramos no else");
                            System.out.print("itensAux irá remover: " + cc.getMci() + " - " + cc.getCodproduto() + " - " + cc.getId());
                            itensAux.remove(cc);
                        }
                    }
                    else
                    {
                        if(cc.getDatavencimento().equals(p.getDatavencimento()) && cc.getCodproduto() == p.getCodproduto())
                        {
                            System.out.print("Entramos no if");
                        }
                        else
                        {
                            System.out.print("Entramos no else");
                            System.out.print("itensAux irá remover: " + cc.getMci() + " - " + cc.getCodproduto() + " - " + cc.getId());
                            itensAux.remove(cc);
                        }
                    }
            }
            catch(Exception ex)
            {
                    System.out.print("Erro: " + ex);
            }
        }
        for(ClienteProduto cAux : ps)
                    {
                        System.out.print("Entramos no segundo FOR");
                        contratado = p.isContratado();
                        if(cAux != null)
                        {
                            System.out.print("Entramos no if cAux != null");
                            try
                            {
                                System.out.print("Entramos no try");
                                cAux.setContatado(p.isContatado());
                                System.out.print("cAux contatado? " + cAux.isContatado());
                                cAux.setContratado(p.isContratado());
                                System.out.print("cAux contratado? " + cAux.isContratado());
                                cAux.setDatacontatado(p.getDatacontatado());
                                System.out.print("cAux data Contatado? " + cAux.getDatacontatado());
                                for(ClienteProduto cc : itensAux)
                                {
                                    System.out.print("Entramos no FOR dentro do segundo FOR");
                                        try
                                        {
                                            System.out.print("Entramos no try dentro do  FOR dentro do segundo FOR");
                                            cc.setContatado(p.isContatado());
                                            cc.setContratado(p.isContratado());
                                            cc.setDatacontatado(p.getDatacontatado());
                                        }
                                        catch(Exception ex)
                                        {
                                            System.out.print("Erro no Método de Edição : " + ex);
                                        }
                                }

                                Operacao operacao = getNovaOperacao();
                                try 
                                {
                                    System.out.print("Entramos no try de Gson");
//                                    Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
                                    Type ClienteProdutoType = new TypeToken<ClienteProduto>() {
                                    }.getType();
//                                    String jsonOut = gson.toJson(cAux, ClienteProdutoType);
                                    operacao.setJsonObj("");
                                    System.out.print("Saimos do try de Gson");
                                } 
                                catch (Exception e) 
                                {
                                    System.out.println("Errro <<<<<<<<<<<<<<< " + e.getMessage() + " >>>>>>>>>>>>>>>>");
                                }
                                System.out.print("Prox: makePersistent");
                                operacao = new OperacaoDAO().makePersistent(operacao);
                                System.out.print("operação virou persistent");
                                cAux.getOperacaoModificacao().add(operacao);
                                System.out.print("Próx: Dar Update no cAux");
                                new ClienteProdutoDao().update(cAux);
                                System.out.print("Demos Update no cAux");
                                opSucess = true;
                                FacesMessage msg = new FacesMessage("SUCESSO!", "Alterações Realizadas!");
                                FacesContext.getCurrentInstance().addMessage(null, msg);
                            }
                            catch(Exception ex)
                            {
                                System.out.print("Erro no Método de Edição : " + ex);
                            }
                        }
                    }  
    }
    
    public String onRowSelect(SelectEvent event) {
        setSelected((ClienteProduto) event.getObject());
        FacesMessage msg = new FacesMessage("Cliente_Produto Selecionado", ((ClienteProduto) event.getObject()).getCodproduto()+"");
        FacesContext.getCurrentInstance().addMessage(null, msg);
        
        return "erro";
    }
    
    public String btnSalvar(ClienteProduto cpc)
    {
        System.out.print("***************Entramos no método de salvar ************");
        System.out.print("*************** cpc é contatado: " + cpc.isContatado());
        if(cpc.isContatado())
        {
            System.out.print("*************** cpc é contatado!");
            System.out.print("*************** cpc foi contratado: " + cpc.isContratado());
            if(cpc.isContratado())
            {
                System.out.print("*************** cpc foi contratado! ");
                return "PF('novoNegocio').show()";
            }
            else
            {
                System.out.print("*************** cpc não foi contratado! ");
                return "PF('MultiSelect').hide()";
            }
        }
        System.out.print("***************Nem entramos no if************");
        return "";
    }
    
    

    @Override
    public void delete() {
        System.out.print("Entramos no método de exclusão");
        try {
            System.out.print("Entramos no try");
            if (selected == null) {
                System.out.print("Selected = nulo");
                throw new NullPointerException();
            }   
            System.out.print("Selected não é nulo");
            listaAdicionada.remove(selected);
            opSucess = new ClienteProdutoDao().makeTransient(selected) != null;    
            System.out.print("Apaguei, Biiirl");
            opSucess = true;
            selected = null;
        } catch (NullPointerException | ConstraintViolationException ex) {
            System.out.println("Erro ao deletar noticia: " + ex.getMessage());
        }
    }

    @Override
    public void add() {
        System.out.print("***************************************");
    }
    
    public void addInside(Integer mci, Integer prefixo) {
        if(premio > 0)
        {
            try {
            ClienteProduto axl = new ClienteProduto();
            axl.setCodproduto(codProduto);
            axl.setProposta(numProposta);
            axl.setDatainiciovigencia(dataInicioVigencia);
            axl.setPremio(premio);
            axl.setPrefixo(prefixo);
            axl.setMci(mci);
            axl.setManual(true);
            Date aux = new Date();
            axl.setDatavencimento(aux);
            axl.getDatavencimento().setYear(2020-1900);
            axl.setContratado(true);
            new ClienteProdutoDao().makePersistent(axl);
            listaAdicionada.add(axl);
            opSucess = true;
            }
            catch (Exception ex) {
                System.out.print("******************* ERRO = " + ex);
                opSucess = false;
            }
        }
        
        codProduto = null;
        numProposta = null;
        dataInicioVigencia = null;
        premio = 0;
    }

    public void limpaLista()
    {
        listaAdicionada = null;
        listaAdicionada = new ArrayList<ClienteProduto>();
    }

    public boolean lerArq(FileUploadEvent event) {
        try {
            System.out.print("Entramos no arquivo");
            UploadedFile arq = event.getFile();
            System.out.print("arq recebeu o arquivo");
            if (arq != null) {
                System.out.print("Entramos no if");
                CsvReader csvReader = new CsvReader(arq.getInputstream(), Charset.forName("utf-8"));
                csvReader.setDelimiter(',');
                csvReader.readHeaders();
                System.out.print("csv lido");
                while (csvReader.readRecord()) {
                    System.out.print("Entramos no while");
                    
                  
                Integer prefixo = Integer.parseInt(csvReader.get(0));
                System.out.print("prefixo Definido = " + prefixo);
                Integer numCarteira = Integer.parseInt(csvReader.get(1));
                System.out.print("numCarteira definido = " + numCarteira);
                String Bloco = csvReader.get(2);
                System.out.print("Bloco definido = " + Bloco);
                String Produto = csvReader.get(3);
                System.out.print("Produto definido = " + Produto);
                Integer proposta = Integer.parseInt(csvReader.get(4));
                System.out.print("proposta definida = " + proposta);
                Integer mci = Integer.parseInt(csvReader.get(5));
                System.out.print("mci definido = " + mci);
                String datainicio = csvReader.get(6);
                System.out.print("data inicio definida = " + datainicio);
                String datafim = csvReader.get(7);
                System.out.print("data fim definida = " + datafim);
                String premioStr = csvReader.get(8);
                if(premioStr.contains(".")){
                        premioStr = premioStr.replace(".", "");
                }
                double premio = Double.parseDouble(premioStr);
                System.out.print("premio Definido = " + premio);
                clienteSeguridade cliente = new clienteSeguridade();
                try
                {
                    cliente = new ClienteSeguridadeDao().findById(mci, true);
                    System.out.print("Ultima linha do try -+-+- Cliente definido: " + cliente.getMci());
                }
                catch(Exception ex)
                {
                    cliente = new clienteSeguridade();
                    System.out.print("ATENÇÃO!!! ERRO = " + ex);
                }
                
                System.out.print("Cliente definido");
                Bloco blocoB;
                System.out.print("Bloco b declarado");
                ProdutoSeguridade ps;
                System.out.print("Produto de Seguridade ps declarado");
                SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
                Date dateIni = null;
                Date dateFin = null;
                try
                {
                    dateIni = formatter.parse(datainicio);
                    dateFin = formatter.parse(datafim);
                    System.out.print("Datas definidas e formatadas");
                }
                catch(Exception e)
                {
                    System.out.print("Erro na conversão de data - Metodo Subir/Atualizar Arquivo : " + e);
                }
                System.out.print("Proximo passo --- definir produto seguridade");
                try{
                    ps = new ProdutoSeguridadeDao().findByNome(Produto);
                    System.out.print("ps definido - " + ps.getInfo());
                }
                catch(Exception e){
                    ps = new ProdutoSeguridade();
                    System.out.print("ps definido - vazio");
                }
                System.out.print("Proximo passo é definir bloco");
                try{
                    Bloco aux;
                    aux = new BlocoDao().findByNome(Bloco);
                    if(aux == null)
                    {
                        blocoB = new Bloco(Bloco);
                        new BlocoDao().makePersistent(blocoB).getNome(); 
                    }
                    System.out.print("bloco definido - " + aux.getNome());
                }
                catch(Exception e){
                    blocoB = new Bloco();
                    System.out.print("bloco definido --- vazio");
                }
                    
                            Logger.getLogger(ClienteLob.class.getName()).log(Level.INFO, "CHECKPOINT 1!");
                            Logger.getLogger(ClienteLob.class.getName()).log(Level.INFO, "CHECKPOINT 1!");
                            Logger.getLogger(ClienteLob.class.getName()).log(Level.INFO, "CHECKPOINT 1!");
                            Logger.getLogger(ClienteLob.class.getName()).log(Level.INFO, "CHECKPOINT 1!");
                            Logger.getLogger(ClienteLob.class.getName()).log(Level.INFO, "CHECKPOINT 1!");
//                ClienteProduto clienteAtualizado = new ClienteProdutoDao().findById(proposta, true);
                    ClienteProduto clienteAtualizado = new ClienteProduto();
                    try
                    {
                        clienteAtualizado = new ClienteProdutoDao().findByProposta(proposta);
                        Logger.getLogger(ClienteLob.class.getName()).log(Level.SEVERE, "clienteAtualizado definido = " + clienteAtualizado.getProposta());
                        
                    }
                    catch(Exception ex)
                    {
                        Logger.getLogger(ClienteLob.class.getName()).log(Level.SEVERE, "erro na linha: ");
                        Logger.getLogger(ClienteLob.class.getName()).log(Level.SEVERE, "'clienteAtualizado = new ClienteProdutoDao().findByProposta(proposta);'");
                    }
                    
                    try {
                        if (clienteAtualizado != null) {
                            if(ps.getCodProduto() == null){
                                Logger.getLogger(ClienteLob.class.getName()).log(Level.SEVERE, "Entramos no if ps.codigo for nulo");                                
                                ProdutoSeguridade p = new ProdutoSeguridade();
                                Logger.getLogger(ClienteLob.class.getName()).log(Level.SEVERE, "p declarado");  
                                p.setBloco(Bloco);
                                p.setNomeProduto(Produto);
                                Logger.getLogger(ClienteLob.class.getName()).log(Level.SEVERE, "p definido, ainda não feito persistente ");  
                                new ProdutoSeguridadeDao().makePersistent(p);
                                Logger.getLogger(ClienteLob.class.getName()).log(Level.SEVERE, "p definido e persistente = " + p.getCodProduto()); 
                                Logger.getLogger(ClienteLob.class.getName()).log(Level.SEVERE, "p definido e persistente = " + new ProdutoSeguridadeDao().findByNome(Produto).getInfo()); 
                                Logger.getLogger(ClienteLob.class.getName()).log(Level.SEVERE, "p virou persistente");  
                                clienteAtualizado.setCodproduto(p.getCodProduto());   
                                Logger.getLogger(ClienteLob.class.getName()).log(Level.SEVERE, "codigo do produto do cliente setado = " + clienteAtualizado.getCodproduto());  
                            }
                            else{
                                Logger.getLogger(ClienteLob.class.getName()).log(Level.SEVERE, "Entramos no if ps.codigo não for nulo"); 
                                clienteAtualizado.setCodproduto(ps.getCodProduto());
                            }
                            if(cliente.getMci() == null){
                                clienteSeguridade cAux = new clienteSeguridade();
                                cAux.setMci(mci);
                                cAux.setPrefixo(prefixo +"");
                                cAux.setNumCarteira(numCarteira+"");
                                clienteAtualizado.setMci(new ClienteSeguridadeDao().makePersistent(cAux).getMci());
                            }
                            else{
                                clienteAtualizado.setMci(cliente.getMci());
                            }
                           
                            clienteAtualizado.setDatainiciovigencia(dateIni);
                            clienteAtualizado.setDatavencimento(dateFin);
                            clienteAtualizado.setPrefixo(prefixo);
                            clienteAtualizado.setPremio(premio);
                            clienteAtualizado.setProposta(proposta);
                            Logger.getLogger(ClienteLob.class.getName()).log(Level.INFO, "CHECKPOINT 2!");
                            Logger.getLogger(ClienteLob.class.getName()).log(Level.INFO, "CHECKPOINT 2!");
                            Logger.getLogger(ClienteLob.class.getName()).log(Level.INFO, "CHECKPOINT 2!");
                            Logger.getLogger(ClienteLob.class.getName()).log(Level.INFO, "CHECKPOINT 2!");
                            Logger.getLogger(ClienteLob.class.getName()).log(Level.INFO, "CHECKPOINT 2!");
                            Operacao operacao = getNovaOperacao();
                            try {
                                Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
                                Type clienteType = new TypeToken<ClienteProduto>() {
                                }.getType();
                                String jsonOut = gson.toJson(clienteAtualizado, clienteType);
                                operacao.setJsonObj(jsonOut);
                            } catch (Exception e) {
                                System.out.println("Errro <<<<<<<<<<<<<<< " + e.getMessage() + " >>>>>>>>>>>>>>>>");
                            }
                            operacao = new OperacaoDAO().makePersistent(operacao);
                            clienteAtualizado.getOperacaoModificacao().add(operacao);
                            opSucess = new ClienteProdutoDao().makePersistent(clienteAtualizado) != null;
                            Logger.getLogger(ClienteLob.class.getName()).log(Level.INFO, "CHECKPOINT 3!");
                            Logger.getLogger(ClienteLob.class.getName()).log(Level.INFO, "CHECKPOINT 3!");
                            Logger.getLogger(ClienteLob.class.getName()).log(Level.INFO, "CHECKPOINT 3!");
                            Logger.getLogger(ClienteLob.class.getName()).log(Level.INFO, "CHECKPOINT 3!");
                            Logger.getLogger(ClienteLob.class.getName()).log(Level.INFO, "CHECKPOINT 3!");
                        } else {
                            Logger.getLogger(ClienteLob.class.getName()).log(Level.SEVERE, "Entramos no if clienteProduto for nulo");  
                            ClienteProduto clienteNovo = new ClienteProduto();
                                if(ps.getCodProduto() == null){
                                    Logger.getLogger(ClienteLob.class.getName()).log(Level.SEVERE, "Entramos no if ps.codigo for nulo");     
                                    ProdutoSeguridade p = new ProdutoSeguridade();
                                    Logger.getLogger(ClienteLob.class.getName()).log(Level.SEVERE, "p declarado");  
                                    p.setBloco(Bloco);
                                    p.setNomeProduto(Produto);
                                    Logger.getLogger(ClienteLob.class.getName()).log(Level.SEVERE, "p definido = " + p.getCodProduto());  
                                    new ProdutoSeguridadeDao().makePersistent(p);
                                    Logger.getLogger(ClienteLob.class.getName()).log(Level.SEVERE, "p definido = " + p.getCodProduto()); 
                                    Logger.getLogger(ClienteLob.class.getName()).log(Level.SEVERE, "p definido = " + new ProdutoSeguridadeDao().findByNome(Produto).getInfo());
                                    Logger.getLogger(ClienteLob.class.getName()).log(Level.SEVERE, "p virou persistente");  
                                    clienteNovo.setCodproduto(p.getCodProduto());   
                                    Logger.getLogger(ClienteLob.class.getName()).log(Level.SEVERE, "codigo do produto do cliente foi setado = " + clienteNovo.getCodproduto());  
                                }
                                else{
                                    Logger.getLogger(ClienteLob.class.getName()).log(Level.SEVERE, "Entramos no if ps.codigo não for nulo");   
                                    clienteNovo.setCodproduto(ps.getCodProduto());
                                }
                                if(cliente.getMci() == null){
                                    clienteSeguridade cAux = new clienteSeguridade();
                                    cAux.setMci(mci);
                                    cAux.setPrefixo(prefixo +"");
                                    cAux.setNumCarteira(numCarteira+"");
                                    clienteNovo.setMci(new ClienteSeguridadeDao().makePersistent(cAux).getMci());
                                }
                                else{
                                    clienteNovo.setMci(cliente.getMci());
                                }
                                clienteNovo.setDatainiciovigencia(dateIni);
                                clienteNovo.setDatavencimento(dateFin);
                                clienteNovo.setPrefixo(prefixo);
                                clienteNovo.setPremio(premio);
                                clienteNovo.setProposta(proposta);
                                Logger.getLogger(ClienteLob.class.getName()).log(Level.INFO, "CHECKPOINT 2!");
                                Logger.getLogger(ClienteLob.class.getName()).log(Level.INFO, "CHECKPOINT 2!");
                                Logger.getLogger(ClienteLob.class.getName()).log(Level.INFO, "CHECKPOINT 2!");
                                Logger.getLogger(ClienteLob.class.getName()).log(Level.INFO, "CHECKPOINT 2!");
                                Logger.getLogger(ClienteLob.class.getName()).log(Level.INFO, "CHECKPOINT 2!");
                            Operacao operacao = getNovaOperacao();
                            try {
                                Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
                                Type clienteType = new TypeToken<ClienteProduto>() {
                                }.getType();
                                String jsonOut = gson.toJson(clienteNovo, clienteType);
                                operacao.setJsonObj(jsonOut);
                            } catch (Exception e) {
                                System.out.println("Errro <<<<<<<<<<<<<<< " + e.getMessage() + " >>>>>>>>>>>>>>>>");
                            }
                            clienteNovo.setOperacaoCriacao(new OperacaoDAO().save(operacao));
                            clienteNovo.setOperacaoModificacao(new ArrayList<>());
                            clienteNovo.getOperacaoModificacao().add(new OperacaoDAO().save(operacao));
//                            opSucess = new ClienteProdutoDao().save(clienteNovo) != null;
                            opSucess = new ClienteProdutoDao().makePersistent(clienteNovo) != null;
                            Logger.getLogger(ClienteLob.class.getName()).log(Level.INFO, "CHECKPOINT 3!");
                            Logger.getLogger(ClienteLob.class.getName()).log(Level.INFO, "CHECKPOINT 3!");
                            Logger.getLogger(ClienteLob.class.getName()).log(Level.INFO, "CHECKPOINT 3!");
                            Logger.getLogger(ClienteLob.class.getName()).log(Level.INFO, "CHECKPOINT 3!");
                            Logger.getLogger(ClienteLob.class.getName()).log(Level.INFO, "CHECKPOINT 3!");
                        }

                            Logger.getLogger(ClienteLob.class.getName()).log(Level.INFO, "CHECKPOINT 4!");
                            Logger.getLogger(ClienteLob.class.getName()).log(Level.INFO, "CHECKPOINT 4!");
                            Logger.getLogger(ClienteLob.class.getName()).log(Level.INFO, "CHECKPOINT 4!");
                            Logger.getLogger(ClienteLob.class.getName()).log(Level.INFO, "CHECKPOINT 4!");
                            Logger.getLogger(ClienteLob.class.getName()).log(Level.INFO, "CHECKPOINT 4!");
                    } catch (HibernateException ex) {
                        Logger.getLogger(ClienteProduto.class.getName()).log(Level.SEVERE, "Erro na importação do CSV, favor fazer upload do arquivo csv novamente!", ex);
                    }
                }
                csvReader.close();
                            Logger.getLogger(ClienteLob.class.getName()).log(Level.INFO, "CHECKPOINT 5! LEU TUDO");
                            Logger.getLogger(ClienteLob.class.getName()).log(Level.INFO, "CHECKPOINT 5! LEU TUDO");
                            Logger.getLogger(ClienteLob.class.getName()).log(Level.INFO, "CHECKPOINT 5! LEU TUDO");
                            Logger.getLogger(ClienteLob.class.getName()).log(Level.INFO, "CHECKPOINT 5! LEU TUDO");
                            
                FacesMessage msg = new FacesMessage("SUCESSO!", "Base de Dados importado com sucesso!");
                FacesContext.getCurrentInstance().addMessage(null, msg);
                Logger.getLogger(ClienteLob.class.getName()).log(Level.INFO, "Importação dos dados concluída!");
            } else {
                FacesMessage msg = new FacesMessage("ATENÇÃO!", "Favor fazer upload do arquivo!");
                FacesContext.getCurrentInstance().addMessage(null, msg);
                Logger.getLogger(ClienteLob.class.getName()).log(Level.INFO, "Favor fazer upload do arquivo!");
            }
        } catch (IOException ex) {
            Logger.getLogger(ClienteLob.class.getName()).log(Level.SEVERE, "Erro CSVReader: " + ex.getMessage());
        }
        finally
        {
            return opSucess;
        }
    }
    
    public void lerArqCanc(FileUploadEvent event) {
        System.out.print("Entramos no método Ler Arquivos de Cancelados");
        try {
            System.out.print("Entramos no try maior");
            UploadedFile arq = event.getFile();
            if (arq != null) {
                System.out.print("Entramos no if arq != null");
                CsvReader csvReader = new CsvReader(arq.getInputstream(), Charset.forName("utf-8"));
                csvReader.setDelimiter(';');
                csvReader.readHeaders();
                while (csvReader.readRecord()) {
                    System.out.print("Iteração X do while que percorre o csv");
                    
                    Integer prefixo = Integer.parseInt(csvReader.get(2));
                    System.out.print("prefixo: " + prefixo);
                    System.out.print("*************************");
                    Integer numCarteira = Integer.parseInt(csvReader.get(4));
                    System.out.print("numCarteira: " + numCarteira);
                    System.out.print("*************************");
                    String data = csvReader.get(5);
                    System.out.print("data: " + data);
                    System.out.print("*************************");
                    Integer mci = Integer.parseInt(csvReader.get(6));
                    System.out.print("mci: " + mci);
                    System.out.print("*************************");
                    Integer proposta = Integer.parseInt(csvReader.get(7));
                    System.out.print("proposta: " + proposta);
                    System.out.print("*************************");
                    String descricao = csvReader.get(8);
                    System.out.print("descricao : " +descricao);
                    System.out.print("*************************");
                    String Produto = csvReader.get(10);
                    System.out.print("Produto: " + Produto);
                    System.out.print("*************************");
                    String premioStr = csvReader.get(11);
                    if(premioStr.contains(".")){
                        premioStr = premioStr.replace(".", "");
                    }
                    double premio = Double.parseDouble(premioStr);
                
                    System.out.print("premio: " + premio);
                    System.out.print("*************************");
                    
                    clienteSeguridade cliente = new clienteController().getOneManList(mci);
                    System.out.print("*************************");
                    ProdutoSeguridade ps;
                    SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
                    Date dateCanc = null;
                    try
                    {
                        System.out.print("*************************");
                        dateCanc = formatter.parse(data);
                        System.out.print("Data convertida: " + dateCanc.toString());
                        System.out.print("*************************");
                    }
                    catch(Exception e)
                    {
                        System.out.print("Erro na conversão de data - Metodo Subir/Atualizar Arquivo : " + e);
                    }
                    System.out.print("CHECKPOINT 1 - CHEGAMOS ATÉ AQUI!!!");
                    try{
                        try
                        {
                            System.out.print("*************************");
                            ps = new ProdutoSeguridadeDao().findByNome(Produto);
                            System.out.print("Produto = " + ps.getInfo());
                            System.out.print("*************************");
                        }
                        catch(Exception ex)
                        {
                            System.out.print("*************************");
                            ps = new ProdutoSeguridade();
                            System.out.print("Produto = " + ps.getInfo());
                            System.out.print("*************************");
                        }
                        ClienteProduto clienteAtualizado;
                        try
                        {
                            System.out.print("*************************");
                            clienteAtualizado = new ClienteProdutoDao().findByProposta(proposta);
                            System.out.print("cliente: " + clienteAtualizado.getMci());
                            System.out.print("*************************");
                        }
                        catch(Exception ex)
                        {
                            System.out.print("*************************");
                            clienteAtualizado = new ClienteProduto();
                            System.out.print("cliente: " + clienteAtualizado.getMci());
                            System.out.print("*************************");
                        }
                        try {
                            if (clienteAtualizado.getMci() != null) {
                                if(ps.getCodProduto() == null){
                                    System.out.print("Ao Subir uma tabela de produtos Cancelados, devem ser incluídos apenas produtos já existentes.");
                                }
                                else{
                                    clienteAtualizado.setCodproduto(ps.getCodProduto());
                                }
                                if(cliente.getMci() == null){
                                    clienteSeguridade cAux = new clienteSeguridade();
                                    cAux.setMci(mci);
                                    cAux.setPrefixo(prefixo +"");
                                    cAux.setNumCarteira(numCarteira+"");
                                    clienteAtualizado.setMci(new ClienteSeguridadeDao().makePersistent(cAux).getMci());
                                }
                                else{
                                    clienteAtualizado.setMci(cliente.getMci());
                                }
                                clienteAtualizado.setDatacancelamento(dateCanc);
                                clienteAtualizado.setCancelado(true);
                                clienteAtualizado.setPrefixo(prefixo);
                                clienteAtualizado.setPremio(premio);
                                clienteAtualizado.setProposta(proposta);
                                clienteAtualizado.setDescricaocancelamento(descricao);

                                Operacao operacao = getNovaOperacao();
                                try {
                                    Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
                                    Type clienteType = new TypeToken<ClienteProduto>() {
                                    }.getType();
                                    String jsonOut = gson.toJson(clienteAtualizado, clienteType);
                                    operacao.setJsonObj(jsonOut);
                                } catch (Exception e) {
                                    System.out.println("Errro <<<<<<<<<<<<<<< " + e.getMessage() + " >>>>>>>>>>>>>>>>");
                                }
                                operacao = new OperacaoDAO().makePersistent(operacao);
                                clienteAtualizado.getOperacaoModificacao().add(operacao);
                                opSucess = new ClienteProdutoDao().makePersistent(clienteAtualizado) != null;

                            } else {
                                ClienteProduto clienteNovo = new ClienteProduto();
                                    if(ps.getCodProduto() == null){
                                         System.out.print("Ao Subir uma tabela de produtos Cancelados, devem ser incluídos apenas produtos já existentes.");
                                    }
                                    else{
                                        clienteNovo.setCodproduto(ps.getCodProduto());
                                    }
                                    if(cliente.getMci() == null){
                                        clienteSeguridade cAux = new clienteSeguridade();
                                        cAux.setMci(mci);
                                        cAux.setPrefixo(prefixo +"");
                                        cAux.setNumCarteira(numCarteira+"");
                                        clienteNovo.setMci(new ClienteSeguridadeDao().makePersistent(cAux).getMci());
                                    }
                                    else{
                                        clienteNovo.setMci(cliente.getMci());
                                    }
                                    clienteNovo.setDatacancelamento(dateCanc);
                                    clienteNovo.setCancelado(true);
                                    clienteNovo.setPrefixo(prefixo);
                                    clienteNovo.setPremio(premio);
                                    clienteNovo.setProposta(proposta);
                                    clienteNovo.setDescricaocancelamento(descricao);

                                Operacao operacao = getNovaOperacao();
                                try {
                                    Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
                                    Type clienteType = new TypeToken<ClienteProduto>() {
                                    }.getType();
                                    String jsonOut = gson.toJson(clienteNovo, clienteType);
                                    operacao.setJsonObj(jsonOut);
                                } catch (Exception e) {
                                    System.out.println("Errro <<<<<<<<<<<<<<< " + e.getMessage() + " >>>>>>>>>>>>>>>>");
                                }
                                clienteNovo.setOperacaoCriacao(new OperacaoDAO().save(operacao));
                                clienteNovo.setOperacaoModificacao(new ArrayList<>());
                                clienteNovo.getOperacaoModificacao().add(new OperacaoDAO().save(operacao));
                                opSucess = new ClienteProdutoDao().save(clienteNovo) != null;
                            }

                        } catch (HibernateException ex) {
                            Logger.getLogger(ClienteProduto.class.getName()).log(Level.SEVERE, "Erro na importação do CSV, favor fazer upload do arquivo csv novamente!", ex);
                        }
                    }
                    catch(Exception e){
                        System.out.print("Erro ao encontrar Produto pelo nome: " + e);
                    }
                }

                FacesMessage msg = new FacesMessage("SUCESSO!", "Base de Dados importado com sucesso!");
                FacesContext.getCurrentInstance().addMessage(null, msg);
                Logger.getLogger(ClienteLob.class.getName()).log(Level.INFO, "Importação dos dados concluída!");
            } else {
                System.out.print("Entramos no if arq == null");
                FacesMessage msg = new FacesMessage("ATENÇÃO!", "Favor fazer upload do arquivo!");
                FacesContext.getCurrentInstance().addMessage(null, msg);
                Logger.getLogger(ClienteLob.class.getName()).log(Level.INFO, "Favor fazer upload do arquivo!");
            }
        } catch (IOException ex) {
            Logger.getLogger(ClienteLob.class.getName()).log(Level.SEVERE, "Erro CSVReader: " + ex.getMessage());
        }
        finally
        {
            System.out.print("SAINDO DO MÉTODO, VALOR DE OPSUCESS = " + opSucess);
            System.out.print("SAINDO DO MÉTODO, VALOR DE OPSUCESS = " + opSucess);
            System.out.print("SAINDO DO MÉTODO, VALOR DE OPSUCESS = " + opSucess);
            System.out.print("SAINDO DO MÉTODO, VALOR DE OPSUCESS = " + opSucess);
            System.out.print("SAINDO DO MÉTODO, VALOR DE OPSUCESS = " + opSucess);
            System.out.print("SAINDO DO MÉTODO, VALOR DE OPSUCESS = " + opSucess);
            System.out.print("SAINDO DO MÉTODO, VALOR DE OPSUCESS = " + opSucess);
            System.out.print("SAINDO DO MÉTODO, VALOR DE OPSUCESS = " + opSucess);
            System.out.print("SAINDO DO MÉTODO, VALOR DE OPSUCESS = " + opSucess);
        }
    }
}
