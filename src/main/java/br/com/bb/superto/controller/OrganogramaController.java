/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.controller;

import br.com.bb.superto.dao.OrganogramaDao;
import br.com.bb.superto.model.Funcionarios;
import br.com.bb.superto.model.Organograma;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author maycon
 */
@ManagedBean(name = "organograma")
@ViewScoped
public class OrganogramaController extends GenericController<Organograma> implements java.io.Serializable {

    private Organograma superitendente;
    private Organograma parent;
    private Funcionarios funcionarioSelecionado;
    private Integer idParent;
    private Integer idOrganograma;
    private boolean opSucess;

    public OrganogramaController() {
        superitendente = new OrganogramaDao().getList().get(0);
        opSucess = false;
    }

    public void delete(Integer id) {
        new OrganogramaDao().deletePorId(id);
    }

    public void add2() {
        parent = new OrganogramaDao().getPorId(idParent).get(0);
        System.out.println("testando");
        System.out.println(funcionarioSelecionado.getId());
        System.out.println(parent.getId());
        value = new Organograma();
        value.setFunc(funcionarioSelecionado);
        value.setParent(parent);
        value.setId(Integer.SIZE);
        new OrganogramaDao().save(value);
    }

    public List<Organograma> getGerentes() {
        return new OrganogramaDao().getChildrens(superitendente);
    }

    public Organograma getSuperitendente() {
        return superitendente;
    }

    public void setSuperitendente(Organograma superitendente) {
        this.superitendente = superitendente;
    }

    public List<Organograma> getChildrens(Integer id) {
        return new OrganogramaDao().getChildrens(id);
    }

    public Organograma getParent() {
        return parent;
    }

    public void setParent(Organograma parent) {
        this.parent = parent;
    }

    public Funcionarios getFuncionarioSelecionado() {
        return funcionarioSelecionado;
    }

    public void setFuncionarioSelecionado(Funcionarios funcionarioSelecionado) {
        this.funcionarioSelecionado = funcionarioSelecionado;
    }

    public Integer getIdParent() {
        return idParent;
    }

    public void setIdParent(Integer idParent) {
        this.idParent = idParent;
    }

    public Integer getIdOrganograma() {
        return idOrganograma;
    }

    public void setIdOrganograma(Integer idOrganograma) {
        this.idOrganograma = idOrganograma;
    }

    public boolean isOpSucess() {
        return opSucess;
    }

    public void setOpSucess(boolean opSucess) {
        this.opSucess = opSucess;
    }

    @Override
    public void add() {
        try {
            value = new Organograma();
            value.setFunc(funcionarioSelecionado);
            value.setParent(superitendente);
            value.setId(Integer.SIZE);
            opSucess = new OrganogramaDao().save(value) != null;
        } catch (Exception ex) {
            Logger.getLogger(OrganogramaController.class.getName()).log(Level.SEVERE, "Erro ao salvar arquivo!", ex);
        }

    }

    @Override
    public void edit() {
        try {
            parent.setFunc(funcionarioSelecionado);
            opSucess = new OrganogramaDao().makePersistent(parent) != null;
        } catch (Exception ex) {
            Logger.getLogger(OrganogramaController.class.getName()).log(Level.SEVERE, "Erro ao editar arquivo!", ex);
        }

    }

    @Override
    public void delete() {
    }
}
