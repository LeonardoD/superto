/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.controller;

import br.com.bb.superto.model.Actions;
import br.com.bb.superto.model.Mercado;
import br.com.bb.superto.model.SubCategoria;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.util.ArrayList;
import java.util.List;

/**
 * @author maycon
 */
@ManagedBean(name = "utilController2")
@ViewScoped
public class UtilController2 implements java.io.Serializable {

    Mercado categoria;
    List<Mercado> listCategorias;

    public UtilController2() {
        listCategorias = new ArrayList<>();
    }

    public Mercado getCategoria() {
        return categoria;
    }

    public void setCategoria(Mercado categoria) {
        this.categoria = categoria;
    }

    public Mercado[] getCategoryValues() {
        return Mercado.values();
    }

    public List<Mercado> getListCategorias() {
        if (listCategorias.isEmpty()) {
            listCategorias.add(Mercado.SUPER);
            listCategorias.add(Mercado.ADM);
            listCategorias.add(Mercado.PF);
            listCategorias.add(Mercado.PJ);
            listCategorias.add(Mercado.AGRO);
            listCategorias.add(Mercado.DS);
        }
        return listCategorias;
    }

    public void setListCategorias(List<Mercado> listCategorias) {
        this.listCategorias = listCategorias;
    }

    public void categorizar(Mercado categoria) {
        this.categoria = categoria;
    }

    public SubCategoria[] getSubCategorias() {
        ArrayList<SubCategoria> subCategorias = new ArrayList<SubCategoria>();
        if (categoria == Mercado.ADM) {
            subCategorias.add(SubCategoria.icc);
            subCategorias.add(SubCategoria.administ);
            subCategorias.add(SubCategoria.ambienc);
            subCategorias.add(SubCategoria.atend);
            subCategorias.add(SubCategoria.avali);
            subCategorias.add(SubCategoria.coban);
            subCategorias.add(SubCategoria.ajui);
            subCategorias.add(SubCategoria.contMov);
            subCategorias.add(SubCategoria.equip);
            subCategorias.add(SubCategoria.judici);
            subCategorias.add(SubCategoria.imprensa);
            subCategorias.add(SubCategoria.lob);
            subCategorias.add(SubCategoria.patroc);
            subCategorias.add(SubCategoria.regulFGO);
            subCategorias.add(SubCategoria.inscDAU);
            subCategorias.add(SubCategoria.siner);
            subCategorias.add(SubCategoria.prestCont);
            subCategorias.add(SubCategoria.capacit);
            subCategorias.add(SubCategoria.postal);
            subCategorias.add(SubCategoria.orfix);
            subCategorias.add(SubCategoria.sng);
            subCategorias.add(SubCategoria.taa);
            subCategorias.add(SubCategoria.procurac);
            subCategorias.add(SubCategoria.qualiCred);
            subCategorias.add(SubCategoria.renegDivid);
        } else if (categoria == Mercado.AGRO) {
            subCategorias.add(SubCategoria.mpo);
            subCategorias.add(SubCategoria.caf);
            subCategorias.add(SubCategoria.fies);
            subCategorias.add(SubCategoria.pronaf);
            subCategorias.add(SubCategoria.planDRS);
            subCategorias.add(SubCategoria.inadAGRO);
            subCategorias.add(SubCategoria.credAGRO);
            subCategorias.add(SubCategoria.cartAgro);
            subCategorias.add(SubCategoria.segurRural);
            subCategorias.add(SubCategoria.abcDCA);
        } else if (categoria == Mercado.DS) {
            subCategorias.add(SubCategoria.mpo);
            subCategorias.add(SubCategoria.caf);
            subCategorias.add(SubCategoria.fies);
            subCategorias.add(SubCategoria.pronaf);
            subCategorias.add(SubCategoria.planDRS);
            subCategorias.add(SubCategoria.inadAGRO);
            subCategorias.add(SubCategoria.credAGRO);
            subCategorias.add(SubCategoria.cartAgro);
            subCategorias.add(SubCategoria.segurRural);
            subCategorias.add(SubCategoria.abcDCA);
        } else if (categoria == Mercado.PF) {
            subCategorias.add(SubCategoria.gov);
            subCategorias.add(SubCategoria.creditoPF);
            subCategorias.add(SubCategoria.imob);
            subCategorias.add(SubCategoria.segurPF);
            subCategorias.add(SubCategoria.gestConv);
        } else if (categoria == Mercado.PJ) {
            subCategorias.add(SubCategoria.credPJG);
            subCategorias.add(SubCategoria.credPJI);
            subCategorias.add(SubCategoria.segurPJ);
            subCategorias.add(SubCategoria.limCred);
            subCategorias.add(SubCategoria.dicasPJ);
            subCategorias.add(SubCategoria.kitAberturaDeContasPJ);
            subCategorias.add(SubCategoria.credPJRecebiveis);
            subCategorias.add(SubCategoria.gestCart);
            subCategorias.add(SubCategoria.cobr);
            subCategorias.add(SubCategoria.relacionamento);
            subCategorias.add(SubCategoria.sinergia);
            subCategorias.add(SubCategoria.cartoes);
            subCategorias.add(SubCategoria.comex);
            subCategorias.add(SubCategoria.demaisArq);
        }
        SubCategoria vetor[] = new SubCategoria[subCategorias.size()];
        for (int i = 0; i < subCategorias.size(); i++) {
            vetor[i] = subCategorias.get(i);
        }
        return vetor;
    }

    public Actions[] getActionsValues() {
        return Actions.values();
    }

    public Mercado getMercadoADM() {
        return Mercado.ADM;
    }

    public Mercado getMercadoPF() {
        return Mercado.PF;
    }

    public Mercado getMercadoGOV() {
        return Mercado.GOV;
    }

    public Mercado getMercadoPJ() {
        return Mercado.PJ;
    }

    public Mercado getMercadoAGRO() {
        return Mercado.AGRO;
    }

    public Mercado getMercadoDS() {
        return Mercado.DS;
    }

    public Mercado getMercadoSuper() {
        return Mercado.SUPER;
    }
}
