/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.controller;

import br.com.bb.superto.dao.EventosDao;
import br.com.bb.superto.model.Eventos;
import br.com.bb.superto.model.Image;
import br.com.bb.superto.model.Mercado;
import br.com.bb.superto.util.FacesUtils;
import br.com.bb.superto.util.Util;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.util.List;

/**
 * @author maycon
 */
@ManagedBean(name = "eventoController")
@ViewScoped
public class EventosController extends GenericController<Eventos> implements java.io.Serializable {

    private Image image;

    public EventosController() {
    }

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }

    public void onSelect() {
        if (selected != null) {
            image = selected.getImagem();
        }
    }

    @Override
    public void edit() {
        if (image != null) {
            try {
                if (selected.getImagem().getId().intValue() != image.getId().intValue()) {
                    selected.setImagem(image);
                }
            } catch (NullPointerException e) {
                selected.setImagem(image);
            }
        }
        new EventosDao().makePersistent(selected);
        addFacesMsg("Evento editado com sucesso.", FacesMessage.SEVERITY_INFO);
    }

    @Override
    public void delete() {
        new EventosDao().makeTransient(selected);
        addFacesMsg("Evento deletado com sucesso.", FacesMessage.SEVERITY_INFO);
    }

    @Override
    public void add() {
        if (value != null) {
            if (image != null) {
                value.setImagem(image);
            }
            value.setDataDePublicacao(Util.getDate());
            new EventosDao().save(value);
            value = null;
            addFacesMsg("Evento adicionado com sucesso.", FacesMessage.SEVERITY_INFO);
        }
    }

    public void adicionarImagem(Image imagem) {
        if (imagem != null) {
            System.out.println("Imagem selecionada:\n\t" + imagem.getTitle());
            image = imagem;
        }
    }

    public void uploadImagem() {
        ImageController imageController = (ImageController) FacesUtils.getViewAttribute("imageController");
        imageController.add();
        adicionarImagem(imageController.getValue());
    }

    public List<Eventos> eventos(int first, int max) {
        return new EventosDao().getEventos(first, max, false, Mercado.SUPER);
    }
}
