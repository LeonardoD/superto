package br.com.bb.superto.controller;

import br.com.bb.superto.dao.AgenciasDao;
import br.com.bb.superto.dao.FuncionariosDao;
import br.com.bb.superto.model.Agencias;
import br.com.bb.superto.model.Funcionarios;
import com.csvreader.CsvReader;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Maycon
 */
@ManagedBean(name = "agenciasController")
@ViewScoped
public class AgenciasController extends GenericController<Agencias> {

    private UploadedFile file;
    private String fileName;
    private boolean opSucesso;

    private List<String> redes = new ArrayList<String>();

    public List<String> getRedes() 
    {
        for(Agencias a : this.getAgencias())
        {
            String y = a.getRede();
            boolean existe = false;
            for(String i : redes)
            {
                if(y.equals(i))
                {
                    existe = true; 
                }
            }
            if(existe == false)
            {
                redes.add(y);
            }
        }
        return redes;
    }
    
    public String formataPrefixo(Integer Prefixo)
    {
        if((Prefixo+"").length() == 3)
        {
            return "0"+Prefixo;
        }
        else
        {
            return Prefixo+"";
        }
    }

    public void setRedes(List<String> redes) {
        this.redes = redes;
    }
    public double pctRede(String rede)
    {
        double cont = 0;
        double somatorio = 0;
        for(Agencias a : this.getAgencias())
        {
            String y = a.getRede();
            if(y.equals(rede))
            {
                cont = cont + 1;
                somatorio = somatorio + this.calculaPct(a);
            }
        }
        return somatorio / cont;
    }
    
    public int qualisRede(String rede)
    {
        try
        {
            double cont = 0;
            double somatorio = 0;
            for(Agencias a : this.getAgencias())
            {
                String y = a.getRede();
                if(y.equals(rede))
                {
                    cont = cont + 1;
                    somatorio = somatorio + this.calculaPct(a);
                }
            }
            return (int)((somatorio / cont)/20);
        }
        catch(Exception ex)
        {
            return 0;
        }
    }
    
    
    public AgenciasController() {
    }

    public UploadedFile getFile() {
        return file;
    }

    public void setFile(UploadedFile file) {
        this.file = file;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public List<Agencias> getAgencias() {
        return new AgenciasDao().findAll();
    }
    
    public void fileUploadListener(FileUploadEvent event) {
        file = event.getFile();
    }

    public boolean isOpSucesso() {
        return opSucesso;
    }

    public void setOpSucesso(boolean opSucesso) {
        this.opSucesso = opSucesso;
    }

    public String normalizarMatricula(String fmatricula) {
        String zeros = "";
        for (int i = 0; i < (7 - fmatricula.length()); i++) {
            zeros += "0";
        }
        return "F" + zeros + fmatricula;
    }

    public void atualizar() {
        try {
            CsvReader csvReader = new CsvReader(file.getInputstream(), Charset.forName("iso-8859-1"));
            csvReader.setDelimiter(';');
            csvReader.readHeaders();
            while (csvReader.readRecord()) {

                Integer prefixo = Integer.parseInt(csvReader.get(0));
                String nomeRede = csvReader.get(1);
                String nomeAgencia = csvReader.get(2);
                String gerenteMatricula = csvReader.get(3);
                String gerenteNome = csvReader.get(4);

                Agencias agencia = new AgenciasDao().findById(prefixo, true);
                if (agencia != null) {
                    agencia.setRede(nomeRede);
                    agencia.setNomeAgencia(nomeAgencia);
                    Funcionarios gerenteAgencia = new FuncionariosDao().findByName(gerenteNome);
                    if (gerenteAgencia != null) {
                        agencia.setGerenteAgencia(gerenteAgencia);
                    } else {
                        Funcionarios novoGerente = new Funcionarios();
                        novoGerente.setNome(gerenteNome);
                        novoGerente.setFmatricula(normalizarMatricula(gerenteMatricula));
                        agencia.setGerenteAgencia(new FuncionariosDao().makePersistent(gerenteAgencia));
                    }
                    opSucesso = new AgenciasDao().makePersistent(agencia) != null;
                } else {
                    Agencias novaAgencia = new Agencias();
                    novaAgencia.setPrefixo(prefixo);
                    novaAgencia.setRede(nomeRede);
                    novaAgencia.setNomeAgencia(nomeAgencia);
                    Funcionarios gerenteAgencia = new FuncionariosDao().findById(Integer.parseInt(gerenteMatricula), true);
                    if (gerenteAgencia != null) {
                        novaAgencia.setGerenteAgencia(gerenteAgencia);
                    } else {
                        Funcionarios novoGerente = new Funcionarios();
                        novoGerente.setNome(gerenteNome);
                        novoGerente.setFmatricula(normalizarMatricula(gerenteMatricula));
                        novaAgencia.setGerenteAgencia(new FuncionariosDao().makePersistent(gerenteAgencia));
                    }
                    opSucesso = new AgenciasDao().makePersistent(novaAgencia) != null;
                }
            }
        } catch (NullPointerException | IOException ex) {
            Logger.getLogger(FuncionariosController2.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void edit() {
        if (selected != null) {
            new AgenciasDao().makePersistent(selected);
            addFacesMsg("Agencia editada com sucesso", FacesMessage.SEVERITY_INFO);
        }
    }

    @Override
    public void delete() {
        if (selected != null) {
            opSucesso = new AgenciasDao().makeTransient(selected) != null;
        }
    }

    @Override
    public void add() {
        if (value != null) {
            if (new AgenciasDao().getAgenciaByPrefixo(value.getPrefixo()) == null) {
                opSucesso = new AgenciasDao().save(value) != null;
            }
        }
    }
    
    public void limpaMetas()
    {
        List<Agencias> ags = new ArrayList<Agencias>();
        ags = new AgenciasDao().findAll();
        for(Agencias a : ags)
        {
            a.setMetaseguridade(0);
            a.setFeitoseguridade(0);
        }
    }
    public double calculaPct(Agencias a){
        try{
            double feitos = (double) a.getFeitoseguridade();
            double meta = (double) a.getMetaseguridade();
            if(meta > 0)
            {
                double result = feitos / meta * 100;
                if(result >= 0){
                    return Math.round(result);
                }
                else{
                    return 0;
                }
            }
            return 0;
        }
        catch(Exception e){
            return 0;
        }
    }
    public int calculaQualis(Agencias a){
        try{
            double auxiliar;
            auxiliar = this.calculaPct(a);
            if(auxiliar >= 0)
            {
                return (int) auxiliar/20;
            }
            else{
                return 0;
            }
  
        }
        catch(Exception e){
            return 0;
        }
    }
}
