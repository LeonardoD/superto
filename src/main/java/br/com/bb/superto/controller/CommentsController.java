/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.controller;

import br.com.bb.superto.model.Comments;
import br.com.bb.superto.model.Mercado;
import br.com.bb.superto.util.GenericLazyDataModel;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 * @author maycon
 */
@ManagedBean(name = "commentsController")
@ViewScoped
public class CommentsController extends GenericController<Comments> implements java.io.Serializable {

    private Mercado selectedCategory;
    private String selectedId;
    private GenericLazyDataModel<Comments, Integer> lazyData;

    public CommentsController() {
    }

    public Mercado getSelectedCategory() {
        return selectedCategory;
    }

    public void setSelectedCategory(Mercado selectedCategory) {
        this.selectedCategory = selectedCategory;
    }

    public String getSelectedId() {
        return selectedId;
    }

    public void setSelectedId(String selectedId) {
        this.selectedId = selectedId;
    }

    @Override
    public void edit() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void delete() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void add() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

}
