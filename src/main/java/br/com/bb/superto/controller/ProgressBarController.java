/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.controller;

import br.com.bb.superto.util.Util;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 * @author maycon
 */
@ManagedBean(name = "progressBarController")
@ViewScoped
public class ProgressBarController implements java.io.Serializable {

    private Integer maxValue = 100;
    private Integer currentValue = 1;

    public ProgressBarController() {
    }

    public Integer getProgress() {
        int e = (int) Util.porcent(maxValue, currentValue);
        System.out.println(e);
        return (int) Util.porcent(maxValue, currentValue);
    }

    public Integer getMaxValue() {
        return maxValue;
    }

    public void setMaxValue(Integer maxValue) {
        this.maxValue = maxValue;
    }

    public Integer getCurrentValue() {
        return currentValue;
    }

    public void setCurrentValue(Integer currentValue) {
        this.currentValue = currentValue;
    }
}
