package br.com.bb.superto.controller;

import br.com.bb.superto.dao.AgenciasDao;
import br.com.bb.superto.model.Funcionarios;
import br.com.bb.superto.model.Operacao;
import br.com.bb.superto.model.UserNotLogged;
import br.com.bb.superto.security.UserSecurityUtils;
import br.com.bb.superto.util.FacesUtils;
import br.com.bb.superto.util.GenericLazyDataModel;
import br.com.bb.superto.util.Util;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import java.lang.reflect.ParameterizedType;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Maycon
 * @param <T>
 */
public class GenericController<T> implements GenericControllerInterface {

    protected T value;
    protected T selected;
    protected T edit;
    protected boolean newValue;
    protected Class<T> Class;
    protected GenericLazyDataModel<T, Integer> lazyData;
    protected GenericLazyDataModel<T, Integer> lazyDataOrderData;

    public GenericController() {
        try {
            this.Class = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
            value = this.Class.newInstance();
            edit = this.Class.newInstance();
            newValue = false;
            lazyData = new GenericLazyDataModel<>(Util.getTableName(this.Class), this.Class);
        } catch (InstantiationException | IllegalAccessException ex) {
            Logger.getLogger(GenericController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public T getValue() {
        return value;
    }

    public void setValue(T value) {
        this.value = value;
    }

    public boolean isNewValue() {
        return newValue;
    }

    public void setNewValue(boolean newValue) {
        this.newValue = newValue;
    }

    public T getSelected() {
        return selected;
    }

    public void setSelected(T selected) {
        this.selected = selected;
    }

    public T getEdit() {
        return edit;
    }

    public void setEdit(T edit) {
        this.edit = edit;
    }

    public GenericLazyDataModel<T, Integer> getLazyData() {
        return lazyData;
    }

    public void setLazyData(GenericLazyDataModel<T, Integer> lazyData) {
        this.lazyData = lazyData;
    }

    public String checkSelection() {
        if (this.selected == null) {
            addFacesMsg("Nenhuma notícia selecionada.", FacesMessage.SEVERITY_ERROR);
            return "pretty:gerenciarnews";
        }
        return null;
    }

    @Override
    public void onSelectListener() {
        edit = selected;
    }

    @Override
    public void addFacesMsg(String msg, FacesMessage.Severity level) {
        FacesMessage t = new FacesMessage(level, msg, null);
        FacesContext.getCurrentInstance().addMessage(null, t);
    }

    public void clearValue() {
        try {
            value = this.Class.newInstance();
        } catch (InstantiationException | IllegalAccessException ex) {
            Logger.getLogger(GenericController.class.getName(), "Ocorreu um erro ao limpar o objeto da classe " + value.getClass()).log(Level.SEVERE, null, ex);
        }
    }

    public Object getController(String name) {
        return FacesUtils.getViewAttribute(name);
    }

    public Operacao getNovaOperacao() {
        Operacao operacao = new Operacao();
        try {
            Funcionarios user = UserSecurityUtils.getUserFuncionario().getFuncionario();
            operacao.setAutor(user);
            operacao.setAgencias(new AgenciasDao().findById(user.getPrefixo(), true));
            operacao.setDataOperacao(Util.getTimeStamp());
        } catch (UserNotLogged ex) {
            Logger.getLogger(GenericController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return operacao;
    }

    public int lengthArray(Object[] arrayObject) {
        return arrayObject.length;
    }

    @Override
    public void edit() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void delete() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void add() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
