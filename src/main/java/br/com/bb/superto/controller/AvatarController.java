/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.controller;

import br.com.bb.superto.dao.AvatarDAO;
import br.com.bb.superto.dao.FuncionariosDao;
import br.com.bb.superto.model.Avatar;
import br.com.bb.superto.model.Funcionarios;
import br.com.bb.superto.model.UserNotLogged;
import br.com.bb.superto.security.UserSecurityUtils;
import br.com.bb.superto.util.Util;
import org.primefaces.event.CaptureEvent;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.CroppedImage;
import org.primefaces.model.UploadedFile;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.imageio.stream.FileImageOutputStream;
import javax.servlet.ServletContext;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author maycon
 */
@ManagedBean(name = "avatarController")
@ViewScoped
public class AvatarController implements java.io.Serializable {

    private CroppedImage croppedImage;
    private UploadedFile file;
    private File imagemSelecionada;
    private boolean newFoto;

    public AvatarController() {
    }

    public File getImagemSelecionada() {
        return imagemSelecionada;
    }

    public void setImagemSelecionada(File imagemSelecionada) {
        this.imagemSelecionada = imagemSelecionada;
    }

    public CroppedImage getCroppedImage() {
        return croppedImage;
    }

    public void setCroppedImage(CroppedImage croppedImage) {
        this.croppedImage = croppedImage;
    }

    public UploadedFile getFile() {
        return file;
    }

    public void setFile(UploadedFile file) {
        this.file = file;
    }

    public boolean isNewFoto() {
        return newFoto;
    }

    public void setNewFoto(boolean newFoto) {
        this.newFoto = newFoto;
    }

    public void fileUploadListener(FileUploadEvent event) {
        file = event.getFile();
    }

    public void enviarFoto() {
        try {
            imagemSelecionada = new ArchiveController().createTemporaryFile(Util.createThumbnail(file.getContents(), 320, 240), file.getFileName());
            newFoto = true;
        } catch (FileNotFoundException ex) {
            Logger.getLogger(AvatarController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(AvatarController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void oncapture(CaptureEvent captureEvent) {
        try {
            imagemSelecionada = new ArchiveController().createTemporaryFile(captureEvent.getData(), "captura");
            newFoto = true;
        } catch (FileNotFoundException ex) {
            Logger.getLogger(AvatarController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(AvatarController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void salvarAvatar() {
        if (imagemSelecionada != null) {
            try {
                Avatar aAvatar = new Avatar();
                aAvatar.setImagem(new ArchiveController().add(imagemSelecionada));
                Avatar save = new AvatarDAO().save(aAvatar);
                Funcionarios funcionario = UserSecurityUtils.getUserFuncionario().getFuncionario();
                funcionario.setAvatar(aAvatar);
                new FuncionariosDao().updateAvatarFuncionarios(aAvatar, UserSecurityUtils.getUserFuncionario().getFuncionario());
            } catch (FileNotFoundException ex) {
                Logger.getLogger(AvatarController.class.getName()).log(Level.SEVERE, null, ex);
            } catch (UserNotLogged | IOException ex) {
                Logger.getLogger(AvatarController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public String crop() {
        System.out.println("Recortando a imagem");
        ServletContext servletContext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
        String newFileName = servletContext.getRealPath("") + File.separator + croppedImage.getOriginalFilename().substring(0, croppedImage.getOriginalFilename().lastIndexOf(".")) + "cropped.jpg";
        FileImageOutputStream imageOutput;
        try {
            imageOutput = new FileImageOutputStream(new File(newFileName));
            imageOutput.write(croppedImage.getBytes(), 0, croppedImage.getBytes().length);
            imageOutput.close();
        } catch (Exception e) {
        }
        return null;
    }

    public String getFotoUrl() {
        try {
            if (UserSecurityUtils.getUserFuncionario().getFuncionario().getAvatar() != null && !newFoto) {
                return Util.getMediaPath() + "/" + UserSecurityUtils.getUserFuncionario().getFuncionario().getAvatar().getImagem().getRadomName();
            } else if (!newFoto) {
                return "user";
            } else {
                return imagemSelecionada.getName();
            }
        } catch (UserNotLogged ex) {
            Logger.getLogger(AvatarController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}
