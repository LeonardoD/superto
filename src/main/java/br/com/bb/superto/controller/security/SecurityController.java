/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.controller.security;

import br.com.bb.superto.model.Actions;
import br.com.bb.superto.util.Util;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 * @author maycon
 */
@ManagedBean(name = "securityController")
@ViewScoped
public class SecurityController implements java.io.Serializable {

    Actions editar = Actions.EDIT;
    Actions deletar = Actions.DELETE;
    Actions visualizar = Actions.VIEW;
    Actions adicionar = Actions.ADD;
    Actions all = Actions.ALL;

    public SecurityController() {
    }

    public Actions getEditar() {
        return editar;
    }

    public void setEditar(Actions editar) {
        this.editar = editar;
    }

    public String getModulo() {
        return Util.previousPagUrl;
    }

}
