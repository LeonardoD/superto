/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.controller.security;

import br.com.bb.superto.model.Actions;

import java.util.HashMap;
import java.util.List;

/**
 * @author maycon
 */
public class PermissionController {

    HashMap<String, List<Actions>> permissionsList;

    public PermissionController() {
        permissionsList = new HashMap<>();
    }

    public HashMap<String, List<Actions>> getPermissionsList() {
        return permissionsList;
    }

    public void setPermissionsList(HashMap<String, List<Actions>> permissionsList) {
        this.permissionsList = permissionsList;
    }

}
