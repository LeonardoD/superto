/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.controller;

import br.com.bb.superto.dao.AgenciasDao;
import br.com.bb.superto.dao.UserHistoryDao;
import br.com.bb.superto.enums.Meses;
import br.com.bb.superto.model.Agencias;
import br.com.bb.superto.model.UserHistory;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import org.primefaces.event.CloseEvent;
import org.primefaces.model.chart.Axis;

import org.primefaces.model.chart.AxisType;
import org.primefaces.model.chart.BarChartModel;
import org.primefaces.model.chart.ChartSeries;

/**
 *
 * @author administrador
 */
@ManagedBean(name = "userhistory")
@ViewScoped
public class UserHistoryController extends GenericController<UserHistory> implements java.io.Serializable {

    private Integer tipoPesquisa;
    private List<UserHistory> lista;
    private String pesquisa = "";
    private String filtro = "Funcionário";
    private Date dataInicio;
    private Date dataFim;
    private int TotaldeAcessos, acessosDia, acessosMes;

    public void dia(ActionEvent event) {
        lista.clear();
        setLista(new UserHistoryDao().getTodosAcessosDoDia());
    }

    public void pesquisarAcessos(ActionEvent event) {
        lista.clear();
        FacesContext context = FacesContext.getCurrentInstance();
        if (getDataInicio() == null || getDataFim() == null) {
            context.addMessage(null, new FacesMessage("ERRO", "As datas devem ser preenchidas!"));
        } else {
            if (tipoPesquisa == 0) {
                setLista(new UserHistoryDao().getAcessosPorFuncionario(Integer.parseInt(pesquisa), dataInicio, dataFim));
            } else if (tipoPesquisa == 1) {
                setLista(new UserHistoryDao().getAcessosPorPrefixo(Integer.parseInt(pesquisa), dataInicio, dataFim));
            }
        }

    }

    public Integer getTipoPesquisa() {
        return tipoPesquisa;
    }

    public void setTipoPesquisa(Integer tipoPesquisa) {
        this.tipoPesquisa = tipoPesquisa;
    }

    public List<UserHistory> getLista() {
        if (lista == null) {
            lista = new ArrayList<>();
        }
        return lista;
    }

    public void setLista(List<UserHistory> lista) {
        this.lista = lista;
    }

    public String getPesquisa() {
        return pesquisa;
    }

    public void setPesquisa(String pesquisa) {
        this.pesquisa = pesquisa;
    }

    public String getFiltro() {
        return filtro;
    }

    public void setFiltro(String filtro) {
        this.filtro = filtro;
    }

    public String mudarFiltro(ActionEvent event) {
        if (tipoPesquisa == 0) {
            filtro = "Funcionário";
        } else {
            filtro = "Prefixo";
        }
        return filtro;
    }

    public Date getDataInicio() {
        return dataInicio;
    }

    public void setDataInicio(Date dataInicio) {
        this.dataInicio = dataInicio;
    }

    public Date getDataFim() {
        return dataFim;
    }

    public void setDataFim(Date dataFim) {
        this.dataFim = dataFim;
    }

    public void limpar(CloseEvent event) {
        setPesquisa(null);
        setDataInicio(null);
        setDataFim(null);
    }

    public int getTotaldeAcessos() {
        TotaldeAcessos = (int) new UserHistoryDao().contaTodos();// Consulta COUNT
        return TotaldeAcessos;
    }

    public void setTotaldeAcessos(int TotaldeAcessos) {
        this.TotaldeAcessos = TotaldeAcessos;
    }

    public int getAcessosDia() {
        acessosDia = (int) new UserHistoryDao().getAcessosDoDia(); //COUNT
        return acessosDia;
    }

    public void setAcessosDia(int acessosDia) {
        this.acessosDia = acessosDia;
    }

    public int getAcessosMes() {
        Date dtIni = new Date();
        Date dtFim = new Date();
        Calendar calendario = GregorianCalendar.getInstance();
        calendario.setTime(dtFim);
        dtIni.setDate(01);
        dtFim.setDate(calendario.getActualMaximum(Calendar.DAY_OF_MONTH)-1);
        acessosMes = (int) new UserHistoryDao().getAcessosPorIntervaloDeDatas(dtIni, dtFim);
        return acessosMes; 
    }

    public void setAcessosMes(int acessosMes) {
        this.acessosMes = acessosMes;
    }

    /**
     * ****************** SEÇÃO DE GRÁFICOS por meses****************************
     */
    private BarChartModel barModel, barModelByFuncionarios;
    private Meses chartMonth;
    private int QtdAcessosDia, QtdAcessoFuncs;

    public int getQtdAcessosDia() {
        return QtdAcessosDia;
    }

    public void setQtdAcessosDia(int QtdAcessosDia) {
        this.QtdAcessosDia = QtdAcessosDia;
    }

    public int getQtdAcessoFuncs() {
        return QtdAcessoFuncs;
    }

    public void setQtdAcessoFuncs(int QtdAcessoFuncs) {
        this.QtdAcessoFuncs = QtdAcessoFuncs;
    }

    public BarChartModel getBarModel() {
        if (barModel == null) {
            barModel = new BarChartModel();
        }
        createBarModelAcessosMes();
        return barModel;
    }

    private BarChartModel acessosPorMes() {
        BarChartModel model = new BarChartModel();
        Calendar calendario = GregorianCalendar.getInstance();
        Date day = new Date();
        ChartSeries dadosAcessos = new ChartSeries();

        day.setYear(Integer.parseInt(chartYear) -1900); 
        day.setMonth(chartMonth.getId() - 1);
       
        for (int i = 0; i < calendario.getActualMaximum(Calendar.DAY_OF_MONTH); i++) {
            day.setDate(i + 1);
            calendario.setTime(day);
            int diaDaSemana = calendario.get(Calendar.DAY_OF_WEEK);

            if (diaDaSemana != 1 && diaDaSemana != 7) {
                setQtdAcessosDia(new UserHistoryDao().getQtdAcessosDoDia(day));
                dadosAcessos.set(i + 1, getQtdAcessosDia());
            }
        }

        model.addSeries(dadosAcessos);
        return model;
    }

    private void createBarModelAcessosMes() {
        barModel = acessosPorMes();

        barModel.setTitle("Acessos no mês:");

        Axis xAxis = barModel.getAxis(AxisType.X);
        xAxis.setLabel("Dias");

        Axis yAxis = barModel.getAxis(AxisType.Y);
        yAxis.setLabel("Qtd. Acessos");

    }

    private void createBarModelFuncsMes() {
        barModelByFuncionarios = acessosPorFuncionario();

        barModelByFuncionarios.setTitle("Acessos no mês:");

        Axis xAxis = barModelByFuncionarios.getAxis(AxisType.X);
        xAxis.setLabel("Dias");

        Axis yAxis = barModelByFuncionarios.getAxis(AxisType.Y);
        yAxis.setLabel("Qtd. Funcionarios");

    }

    public Meses getChartMonth() {
        if (chartMonth == null) {
            setChartMonth(Meses.JANEIRO);
        }
        return chartMonth;
    }

    public void setChartMonth(Meses chartMonth) {
        this.chartMonth = chartMonth;
    }

    public BarChartModel getBarModelByFuncionarios() {
        if (barModelByFuncionarios == null) {
            barModelByFuncionarios = new BarChartModel();
        }
        createBarModelFuncsMes();
        return barModelByFuncionarios;
    }

    private BarChartModel acessosPorFuncionario() {
        BarChartModel modelFuncs = new BarChartModel();
        Calendar calendario = GregorianCalendar.getInstance();
        Date day = new Date();
        ChartSeries dadosAcessos = new ChartSeries();

        day.setYear(Integer.parseInt(chartYear) -1900);
        day.setMonth(chartMonth.getId() - 1);
        
        
        for (int i = 0; i < calendario.getActualMaximum(Calendar.DAY_OF_MONTH); i++) {
            day.setDate(i + 1);
            calendario.setTime(day);
            int diaDaSemana = calendario.get(Calendar.DAY_OF_WEEK);

            if (diaDaSemana != 1 && diaDaSemana != 7) {
                setQtdAcessoFuncs(new UserHistoryDao().getQtdFuncsDoDia(day));
                dadosAcessos.set(i + 1, getQtdAcessoFuncs());
            }
        }

        modelFuncs.addSeries(dadosAcessos);
        return modelFuncs;
    }

    /**
     * **************** SEÇÃO DE GRÁFICOS POR ANO **************
     */
    private BarChartModel barModelByAcessoAno, barModelByFuncionariosAno;
    private String chartYear;
    private ArrayList<String> listaAnos = new ArrayList<>();
    private int QtdAcessosAno, QtdAcessoFuncsAno;

    public int getQtdAcessosAno() {
        return QtdAcessosAno;
    }

    public void setQtdAcessosAno(int QtdAcessosAno) {
        this.QtdAcessosAno = QtdAcessosAno;
    }

    public int getQtdAcessoFuncsAno() {
        return QtdAcessoFuncsAno;
    }

    public void setQtdAcessoFuncsAno(int QtdAcessoFuncsAno) {
        this.QtdAcessoFuncsAno = QtdAcessoFuncsAno;
    }

    public List<String> getListaAnos() {
        listaAnos.clear();
        Date day = new Date();
        int ano = (day.getYear()) + (1900);
        listaAnos.add(ano + "");
        System.out.println("Ano atual dentro do getListaAnos(): " + ano);
        for (int i = 2015; i < ano; i++) {
            listaAnos.add(i + "");
        }
        System.err.println("<<<<<<<<<<<Lista anos:" + listaAnos);
        return listaAnos;
    }

    public String getChartYear() {
        if (chartYear == null) {
            setChartYear(2016 + "");
        }
        return chartYear;
    }

    public void setChartYear(String chartYear) {
        this.chartYear = chartYear;
    }

    public BarChartModel getBarModelByAcessoAno() {
        if (barModelByAcessoAno == null) {
            barModelByAcessoAno = new BarChartModel();
        }
        createBarModelAcessosAno();
        return barModelByAcessoAno;
    }

    public BarChartModel getBarModelByFuncionariosAno() {
        if (barModelByFuncionariosAno == null) {
            barModelByFuncionariosAno = new BarChartModel();
        }
        createBarModelFuncionariosAno();
        return barModelByFuncionariosAno;
    }
   
    private void createBarModelAcessosAno() {
        barModelByAcessoAno = acessosPorAno();

        barModelByAcessoAno.setTitle("Acessos no Ano:");

        Axis xAxis = barModelByAcessoAno.getAxis(AxisType.X);
        xAxis.setLabel("Meses");

        Axis yAxis = barModelByAcessoAno.getAxis(AxisType.Y);
        yAxis.setLabel("Qtd. Acessos");

    }

    private void createBarModelFuncionariosAno() {
        barModelByFuncionariosAno = acessosPorFuncionarioAno();

        barModelByFuncionariosAno.setTitle("Acessos no Ano:");

        Axis xAxis = barModelByFuncionariosAno.getAxis(AxisType.X);
        xAxis.setLabel("Meses");

        Axis yAxis = barModelByFuncionariosAno.getAxis(AxisType.Y);
        yAxis.setLabel("Qtd. Funcionarios");

    }

    private BarChartModel acessosPorFuncionarioAno() {
        BarChartModel modelFuncs = new BarChartModel();
        Date day = new Date();
        ChartSeries dadosAcessos = new ChartSeries();
        Calendar calendario = GregorianCalendar.getInstance();
        day.setYear(Integer.parseInt(chartYear) - 1900);
        day.setDate(01);

        for (int i = 0; i < 12; i++) {
            day.setMonth(i);
            Date dayFinal = new Date();
            dayFinal.setTime(day.getTime());
            dayFinal.setDate(calendario.getActualMaximum(Calendar.DAY_OF_MONTH) - 1);
            setQtdAcessoFuncsAno(new UserHistoryDao().getQtdFuncsDoMes(day, dayFinal));
            dadosAcessos.set(i + 1, getQtdAcessoFuncsAno());

        }

        modelFuncs.addSeries(dadosAcessos);
        return modelFuncs;
    }

    private BarChartModel acessosPorAno() {
        BarChartModel modelFuncs = new BarChartModel();
        Date day = new Date();
        ChartSeries dadosAcessos = new ChartSeries();
        Calendar calendario = GregorianCalendar.getInstance();

        day.setYear(Integer.parseInt(chartYear) - 1900);
        day.setDate(01);

        for (int i = 0; i < 12; i++) {
            day.setMonth(i);
            Date dayFinal = new Date();
            dayFinal.setTime(day.getTime());
            calendario.setTime(day);
            dayFinal.setDate(calendario.getActualMaximum(Calendar.DAY_OF_MONTH) - 1);
            QtdAcessosAno = new UserHistoryDao().getAcessosPorMes(day, dayFinal).size();
            dadosAcessos.set(i + 1, QtdAcessosAno);
        }

        modelFuncs.addSeries(dadosAcessos);
        return modelFuncs;
    }

     /**
     * ************ GRÁFICO DUPLA INFORMAÇÃO ***  Leonardo Testes
     */
     private BarChartModel barModelByAcessoVSFuncionarioAno;
    

    public BarChartModel getBarModelByAcessoAcessoVSFuncionarioAno() {
        if (barModelByAcessoVSFuncionarioAno == null) {
            barModelByAcessoVSFuncionarioAno= new BarChartModel();
        }
        createbarModelByAcessoVSFuncionarioAno();
        return barModelByAcessoVSFuncionarioAno;
    }
   
    private void createbarModelByAcessoVSFuncionarioAno() {
        barModelByAcessoVSFuncionarioAno = acessosPorAnoEfunc(); //

        barModelByAcessoVSFuncionarioAno.setTitle("Acessos do Ano:");

        Axis xAxis = barModelByAcessoVSFuncionarioAno.getAxis(AxisType.X);
        xAxis.setLabel("Meses");

        Axis yAxis = barModelByAcessoVSFuncionarioAno.getAxis(AxisType.Y);
        yAxis.setLabel("Qtd. Acessos");

    }
    
    private BarChartModel acessosPorAnoEfunc() {
        BarChartModel modelFuncs = new BarChartModel();
        Date day = new Date();
        ChartSeries dadosAcessos = new ChartSeries();
        ChartSeries dadosAcessos2 = new ChartSeries();
        Calendar calendario = GregorianCalendar.getInstance();

        day.setYear(Integer.parseInt(chartYear) - 1900);
        day.setDate(01);

        for (int i = 0; i < 12; i++) {
            day.setMonth(i);
            Date dayFinal = new Date();
            dayFinal.setTime(day.getTime());
            calendario.setTime(day);
            dayFinal.setDate(calendario.getActualMaximum(Calendar.DAY_OF_MONTH) - 1);
            QtdAcessosAno = new UserHistoryDao().getAcessosPorMes(day, dayFinal).size();
            dadosAcessos.set(i + 1, QtdAcessosAno);
        }
        
        for (int i = 0; i < 12; i++) {
            day.setMonth(i);
            Date dayFinal = new Date();
            dayFinal.setTime(day.getTime());
            dayFinal.setDate(calendario.getActualMaximum(Calendar.DAY_OF_MONTH) - 1);
            setQtdAcessoFuncsAno(new UserHistoryDao().getQtdFuncsDoMes(day, dayFinal));
            dadosAcessos2.set(i + 1, getQtdAcessoFuncsAno());

        }

        modelFuncs.addSeries(dadosAcessos);
        modelFuncs.addSeries(dadosAcessos2);
        return modelFuncs;
    }
    
    
    /**
     * ************ GRÁFICO POR PREFIXO ***
     */
    private BarChartModel modelByPrefixo;
    private int qtdAcessosByPrefixo;

    public BarChartModel getModelByPrefixo() {
        if (modelByPrefixo == null) {
            modelByPrefixo = new BarChartModel();
        }
        createBarModelByPrefixo();
        return modelByPrefixo;
    }

    private void createBarModelByPrefixo() {
        modelByPrefixo = acessosPorPrefixo();

        modelByPrefixo.setTitle("Acessos por Agência");

        Axis xAxis = barModelByAcessoAno.getAxis(AxisType.X);

        xAxis.setLabel("Agências");

        Axis yAxis = barModelByAcessoAno.getAxis(AxisType.Y);
        yAxis.setLabel("Qtd. Acessos");

    }

    private BarChartModel acessosPorPrefixo() {
        BarChartModel modelFuncs = new BarChartModel();
        Date dtInicio = new Date();
        Date dtFim = new Date();
        
        dtInicio.setYear(Integer.parseInt(chartYear) -1900);
        dtFim.setYear(Integer.parseInt(chartYear) -1900);
        
        ChartSeries dadosAcessos = new ChartSeries();
        modelFuncs.getAxis(AxisType.X).setTickAngle(-50);
        modelFuncs.setShowPointLabels(true);
        
        List<Agencias> agencias;
        agencias = new AgenciasDao().findAll();

        for (int i = 0; i < agencias.size(); i++) {
            dtInicio.setMonth(0);
            dtInicio.setDate(1);
            
            dtFim.setMonth(11);
            dtFim.setDate(31);
            
            System.out.println("Data de Inicio: " + dtInicio + "\nData: de Fim: " + dtFim);
            if (agencias.get(i).getPrefixo() != 8517) { 

                qtdAcessosByPrefixo = new UserHistoryDao().getAcessosPorPrefixo(agencias.get(i).getPrefixo(), dtInicio, dtFim).size();
                dadosAcessos.set(agencias.get(i).getPrefixo(), qtdAcessosByPrefixo);
            }
        }

        modelFuncs.addSeries(dadosAcessos);
        return modelFuncs;
    }

    public int getQtdAcessosByPrefixo() {
        return qtdAcessosByPrefixo;
    }

    public void setQtdAcessosByPrefixo(int qtdAcessosByPrefixo) {
        this.qtdAcessosByPrefixo = qtdAcessosByPrefixo;
    }

    @Override
    public void edit() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void delete() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void add() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void init() {
        getListaAnos();
    }

}
