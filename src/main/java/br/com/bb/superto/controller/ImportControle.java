/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.controller;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import java.util.Date;

/**
 * @author Maycon
 */
@Entity
@Table(name = "import_controle")
public class ImportControle implements java.io.Serializable {

    @Id
    private Integer id;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date dataImport;

    public ImportControle() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDataImport() {
        return dataImport;
    }

    public void setDataImport(Date dataImport) {
        this.dataImport = dataImport;
    }

}
