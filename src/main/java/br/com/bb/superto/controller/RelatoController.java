/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.controller;

import br.com.bb.superto.dao.RelatosDao;
import br.com.bb.superto.model.Relatos;
import br.com.bb.superto.model.UserNotLogged;
import br.com.bb.superto.security.UserSecurityUtils;
import br.com.bb.superto.util.FacesUtils;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author maycon
 */
@ManagedBean(name = "relatoController")
@ViewScoped
public class RelatoController implements java.io.Serializable {

    private Relatos newRelato;
    private Relatos selected;
    private List<Relatos> relatosFiltrados;

    public RelatoController() {
        this.newRelato = new Relatos();
    }

    public List<Relatos> getRelatosFiltrados() {
        return relatosFiltrados;
    }

    public void setRelatosFiltrados(List<Relatos> relatosFiltrados) {
        this.relatosFiltrados = relatosFiltrados;
    }

    public List<Relatos> getRelatos() {
        return new RelatosDao().findAllRelatos();
    }

    public List<Relatos> getRelatosADM() {
        return new RelatosDao().findAllRelatosADM();
    }

    public List<Relatos> relatos(Integer qtd) {
        return new RelatosDao().findAllRelatos(qtd);
    }

    public boolean getMenu() {
        if (selected == null) {
            return false;
        }
        return true;
    }

    public void add() {
        try {
            Date d = new Date();
            newRelato.setAprovado(false);
            newRelato.setDataEnvio(d);
            newRelato.setFuncionario(UserSecurityUtils.getUserFuncionario().getFuncionario());
            new RelatosDao().save(newRelato);
            FacesUtils.addFacesMsg("Relato enviado para aprovação. Você será notificado quando aprovado.", FacesMessage.SEVERITY_INFO);
            this.newRelato = new Relatos();
        } catch (UserNotLogged ex) {
            Logger.getLogger(RelatoController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void edit() {
        new RelatosDao().update(selected);
        FacesUtils.addFacesMsg("Relato editado.", FacesMessage.SEVERITY_INFO);
    }

    public void delete() {
        System.out.println(selected.getRelato());
        new RelatosDao().deleteRelato(selected);
        FacesUtils.addFacesMsg("Relato excluido", FacesMessage.SEVERITY_INFO);
    }

    public void aprovar() {
        selected.setAprovado(true);
        new RelatosDao().update(selected);
        FacesUtils.addFacesMsg("Relato Aprovado", FacesMessage.SEVERITY_INFO);
    }

    public Relatos getNewRelato() {
        return newRelato;
    }

    public void setNewRelato(Relatos newRelato) {
        this.newRelato = newRelato;
    }

    public Relatos getSelected() {
        return selected;
    }

    public void setSelected(Relatos selected) {
        this.selected = selected;
    }

}
