/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.controller;

import br.com.bb.superto.dao.AgenciasDao;
import br.com.bb.superto.dao.AvatarDAO;
import br.com.bb.superto.dao.FuncionariosDao;
import br.com.bb.superto.dao.UserHistoryDao;
import br.com.bb.superto.model.Agencias;
import br.com.bb.superto.model.Archive;
import br.com.bb.superto.model.Avatar;
import br.com.bb.superto.model.Funcionarios;
import br.com.bb.superto.model.UserNotLogged;
import br.com.bb.superto.security.UserSecurityUtils;
import com.csvreader.CsvReader;
import org.hibernate.exception.ConstraintViolationException;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.context.FacesContext;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;

/**
 * @author maycon
 */
@ManagedBean(name = "funcController")
@ViewScoped
public class FuncionariosController2 extends GenericController<Funcionarios> implements java.io.Serializable {

    private UploadedFile file;
    private String fileName;
    private String matricula;
    private FuncionariosDao funcionarioDAO;
    private boolean opSucesso;
    private List<Funcionarios> aniversariantesDoMes;
    private int sizeAniversariantes;
    private Archive arquivo;
    private List<Funcionarios> funcionarios;
    private List<Funcionarios> aniversariantes;
    private Agencias agencia;

    public List<Funcionarios> getFuncionarios() {
        return new FuncionariosDao().findByStatus();
    }

    public void setFuncionarios(List<Funcionarios> funcionarios) {
        this.funcionarios = funcionarios;
    }

    public FuncionariosController2() {
        aniversariantesDoMes = new FuncionariosDao().getAniversariantesDoMes();
        opSucesso = false;
    }
     
    public int getSizeAniversariantes() {
        return aniversariantesDoMes.size();
    }
    
    public void setSizeAniversariantes(int sizeAniversariantes) {
        this.sizeAniversariantes = sizeAniversariantes;
    }

    public void alterarAvatar() {
    }

    public void salvarAvatar() {
    }

    public void getFuncionario() {
    }

    public UploadedFile getFile() {
        return file;
    }

    public void setFile(UploadedFile file) {
        this.file = file;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    public boolean isOpSucesso() {
        return opSucesso;
    }

    public void setOpSucesso(boolean opSucesso) {
        this.opSucesso = opSucesso;
    }

    public Archive getArquivo() {
        return arquivo;
    }

    public void setArquivo(Archive arquivo) {
        this.arquivo = arquivo;
    }

    public void fileUploadListener(FileUploadEvent event) {
        try {
            UploadedFile file = event.getFile();
            arquivo = new ArchiveController().add(file);
        } catch (UserNotLogged | IOException e) {
            System.err.println("Erro no Upload dos Arquivos: " + e.getMessage());
        }

    }

    public void fileUploadListenerEdit(FileUploadEvent event) {

        try {
            UploadedFile file = event.getFile();
            arquivo = new ArchiveController().add(file);
            if (arquivo != null) {
                Avatar avatar = new Avatar();
                avatar.setImagem(arquivo);
                Avatar avatarpersist = new AvatarDAO().save(avatar);
                selected.setAvatar(avatarpersist);
            }

        } catch (UserNotLogged | IOException e) {
            System.err.println("Erro no Upload dos Arquivos: " + e.getMessage());
        }

    }

    public String selecionarFuncionario() {
        if (matricula != null && !matricula.isEmpty()) {
            this.selected = new FuncionariosDao().getByFmatricula(matricula);
            arquivo = selected.getAvatar().getImagem();
            if (this.selected != null) {
                return null;
            }
        }
        return "error";
    }

    public List<Funcionarios> getFuncs() {
        return new FuncionariosDao().findAll();
    }

    public Avatar getAvatarDoFuncionarioSelecionado() {
        if (selected != null) {
            List<Avatar> byFmatricula = new AvatarDAO().getByFmatricula(selected);
            if (byFmatricula != null && !byFmatricula.isEmpty()) {
                return byFmatricula.get(0);
            }
        }
        return null;
    }

    public List<Funcionarios> getAniversariantesDoMes() {
        if (aniversariantesDoMes == null) {
            aniversariantesDoMes = new FuncionariosDao().getAniversariantesDoMes();
        }
        return aniversariantesDoMes;
    }

    public void setAniversariantesDoMes(List<Funcionarios> aniversariantesDoMes) {
        this.aniversariantesDoMes = aniversariantesDoMes;
    }

    public String selectPerfil() {
        try {
            selected = UserSecurityUtils.getUserFuncionario().getFuncionario();
            return null;
        } catch (UserNotLogged ex) {
            Logger.getLogger(FuncionariosController2.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "error";
    }
    private List<Funcionarios> novosFuncionarios = new ArrayList<>();
    private List<Funcionarios> upFuncionarios = new ArrayList<>();

    public List<Funcionarios> getNovosFuncionarios() {
        return novosFuncionarios;
    }

    public void setNovosFuncionarios(List<Funcionarios> novosFuncionarios) {
        this.novosFuncionarios = novosFuncionarios;
    }

    public List<Funcionarios> getUpFuncionarios() {
        return upFuncionarios;
    }

    public void setUpFuncionarios(List<Funcionarios> upFuncionarios) {
        this.upFuncionarios = upFuncionarios;
    }

    public void atualizarFuncionarios() {
        int quantidadeNovos = 0;
        int quantidadeUps = 0;
        FacesContext context = FacesContext.getCurrentInstance();
        if (novosFuncionarios.isEmpty() || upFuncionarios.isEmpty()) {
            context.addMessage("ERRO", new FacesMessage("Nenhum funcionário foi salvo!", ""));
        } else {
            for (Funcionarios funcs : novosFuncionarios) {
                opSucesso = new FuncionariosDao().save(funcs) != null;
                quantidadeNovos++;
            }

            for (Funcionarios UPfuncs : upFuncionarios) {
                opSucesso = new FuncionariosDao().makePersistent(UPfuncs) != null;
                quantidadeUps++;
            }

        }
    }

    public void updateFuncionarios(FileUploadEvent event) {
        FacesContext context = FacesContext.getCurrentInstance();
        try {
            UploadedFile arq = event.getFile();
            if (arq != null) {
                CsvReader csvReader = new CsvReader(arq.getInputstream(), Charset.forName("ISO-8859-1"));
                csvReader.setDelimiter(';');
                csvReader.readHeaders();

                while (csvReader.readRecord()) {                    
                    int prefixo = Integer.parseInt(csvReader.get(0));
                    String fmatricula = csvReader.get(1);
                    int matriculaNum = Integer.parseInt(fmatricula.substring(1, fmatricula.length()));
                    String NomeCompleto = csvReader.get(2);
                    String NomeGuerra = csvReader.get(3);
                    String Funcao = csvReader.get(4);
                    boolean status;
                    status = csvReader.get(5).equalsIgnoreCase("SIM");
                    Funcionarios funcs = new FuncionariosDao().getByFmatricula(fmatricula);

                    if (funcs == null) {
                        funcs = new Funcionarios(matriculaNum, prefixo, fmatricula, NomeCompleto, NomeGuerra, Funcao, status);
                        novosFuncionarios.add(funcs);

                    } else {
                        funcs.setPrefixo(prefixo);
                        funcs.setNome(NomeCompleto);
                        funcs.setNomeGuerra(NomeGuerra);
                        funcs.setFuncao(Funcao);
                        funcs.setStatus(status);
                        upFuncionarios.add(funcs);
                    }
                    
                    System.out.println("Inserido!");
                }

            } else {
                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "INFO", "Arquivo importado está vazio!"));
            }

        } catch (IOException ex) {
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "ERROR", "Falha ao importar arquivo!\n" + ex.getMessage()));
        }
    }

    @Override
    public void edit() {
        try {
            
            Funcionarios f = new FuncionariosDao().getByFmatricula(selected.getFmatricula());
            f.setAvatar(selected.getAvatar());
            f.setPrefixo(selected.getPrefixo());
            f.setNome(selected.getNome());
            f.setNomeGuerra(selected.getNomeGuerra());
            
            opSucesso = new FuncionariosDao().makePersistent(f) != null;
            setFuncionarios(null);
            if (opSucesso) {
                arquivo = null;
            }
        } catch (Exception e) {
            System.out.println("Erro ao editar funcinario: " + e.getMessage());
        }
    }

    public String onRowSelect(SelectEvent event) {

        setSelected((Funcionarios) event.getObject());
        FacesMessage msg = new FacesMessage("Funcionário Selecionado", ((Funcionarios) event.getObject()).getNome());
        FacesContext.getCurrentInstance().addMessage(null, msg);
        
        return "erro";
    }

    public void alterInfos() {
        try {
            System.out.println(UserSecurityUtils.getUserFuncionario().getFuncionario().getNomeGuerra());
            getFuncionarioDAO().update(UserSecurityUtils.getUserFuncionario().getFuncionario());
            addFacesMsg("Alterações realizadas com sucesso.", FacesMessage.SEVERITY_INFO);

        } catch (UserNotLogged ex) {
            Logger.getLogger(FuncionariosController2.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void delete() {
        try {
            selected.setStatus(false);
            opSucesso = getFuncionarioDAO().makePersistent(selected) != null;

        } catch (Exception e) {
            System.out.println("Erro ao remover funcionario: " + e.getMessage());
        }
    }

    @Override
    public void add() {

        if (value != null && new FuncionariosDao().getByFmatricula(value.getFmatricula()) == null) {
            Funcionarios addFuncs = new Funcionarios();

            try {
                addFuncs.setPrefixo(value.getPrefixo());
                addFuncs.setFmatricula(value.getFmatricula().toUpperCase());
                addFuncs.setNome(value.getNome());
                addFuncs.setNomeGuerra(value.getNomeGuerra());
                addFuncs.setMatriculaNum(Integer.parseInt(addFuncs.getFmatricula().substring(1, addFuncs.getFmatricula().length())));

                if (arquivo != null) {
                    Avatar avatar = new Avatar();
                    avatar.setImagem(arquivo);
                    Avatar avatarpersist = new AvatarDAO().save(avatar);
                    addFuncs.setAvatar(avatarpersist);
                }
                addFuncs.setStatus(true);
                opSucesso = new FuncionariosDao().save(addFuncs) != null;

                if (opSucesso) {
                    arquivo = null;
                    value.setPrefixo(null);
                    value.setNome("");
                    value.setNomeGuerra("");
                    value.setFmatricula("");
                }

            } catch (ConstraintViolationException e) {
                System.out.println("Essa chave já esta adicionada: " + e.getMessage());
            }
        } else {
            // se a chave existir, e se o funcionário tiver apenas desativado, ele será atualizado e ativado...
            if(new FuncionariosDao().getByFmatricula(value.getFmatricula()).isStatus() == false){
                System.out.print("Entramos no método de recuperação de pessoas excluidas");
                Funcionarios addFuncs = new FuncionariosDao().getByFmatricula(selected.getFmatricula());
                addFuncs.setPrefixo(value.getPrefixo());
                addFuncs.setNome(value.getNome());
                addFuncs.setNomeGuerra(value.getNomeGuerra());
                addFuncs.setStatus(true);
                opSucesso = new FuncionariosDao().makePersistent(addFuncs) != null;             
                System.out.print("Deu certo: " + opSucesso);
            }
            else{
                addFacesMsg("Chave já existente!", FacesMessage.SEVERITY_ERROR);
            }
            System.err.println("CHAVE JA EXISTE");
        }
    }

    public FuncionariosDao getFuncionarioDAO() {
        if (this.funcionarioDAO == null) {
            this.funcionarioDAO = new FuncionariosDao();
        }
        return funcionarioDAO;
    }

    public boolean getMenu() {
        if (this.selected != null && (this.selected.getFmatricula().toLowerCase().startsWith("a") || this.selected.getFmatricula().toLowerCase().startsWith("c")
                || this.selected.getFmatricula().toLowerCase().startsWith("t") || this.selected.getFmatricula().toLowerCase().startsWith("f"))) {
            return true;
        }
        return false;
    }
    
    public String getAgencia(Integer prefixo) {
        AgenciasDao agenciasDao = new AgenciasDao();
        List<Agencias> agencias = agenciasDao.findAll();
        int index = 9999999;
        for(int i=0; i<agencias.size();i++){
            if(Objects.equals(agencias.get(i).getPrefixo(), prefixo)){
                index = i;
                break;
            }
        }
        System.out.println("Valor do index: "+index);
        if(index==9999999){
            return " ";
        }
        return "da equipe " + (agencias.get(index).getNomeAgencia());
    }
    
    public void setAgencia(Agencias agencia) {
        this.agencia = agencia;
    }
}
