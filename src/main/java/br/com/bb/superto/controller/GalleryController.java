/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.controller;

import br.com.bb.superto.dao.GalleryDao;
import br.com.bb.superto.dao.GalleryImagensDAO;
import br.com.bb.superto.dao.ImageDao;
import br.com.bb.superto.dao.OperacaoDAO;
import br.com.bb.superto.model.*;
import br.com.bb.superto.util.FacesUtils;
import br.com.bb.superto.util.Pagination;
import br.com.bb.superto.util.Util;
import org.hibernate.Query;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

/**
 * @author maycon
 */
@ManagedBean(name = "galleryController")
@ViewScoped
public class GalleryController extends GenericController<Gallery> implements java.io.Serializable {

    public String idImagem;
    public String idAlbumSelecionado;
    public String idAlbumDestino;
    public HashMap<Integer, Gallery> galerias;
    public List<GalleryImages> imagens = new ArrayList<>();
    private Pagination galeriaPaginacao;
    private Gallery galeriaSelecionada;
    private boolean opSucesso;

    public GalleryController() {
        opSucesso = false;
    }

    public String getIdAlbumSelecionado() {
        return idAlbumSelecionado;
    }

    public void setIdAlbumSelecionado(String idAlbumSelecionado) {
        this.idAlbumSelecionado = idAlbumSelecionado;
    }

    public Collection<Gallery> getGalerias() {
        if (galerias == null || isNewValue()) {
            galerias = new GalleryDao().getGallerys();
        }
        return new GalleryDao().findAll();
    }

    public void setGalerias(HashMap<Integer, Gallery> galerias) {
        this.galerias = galerias;
    }

    public String getIdImagem() {
        return idImagem;
    }

    public void setIdImagem(String idImagem) {
        this.idImagem = idImagem;
    }

    public List<Gallery> galerias(Integer first, Integer max) {
        return new GalleryDao().getGalerias(first, max, false, Mercado.SUPER);
    }

    public List<GalleryImages> getImagensDaGaleria() {
        return new GalleryImagensDAO().getImagensFromAlbum(selected);
    }

    public Gallery getGaleriaSelecionada() {
        return galeriaSelecionada;
    }

    public void setGaleriaSelecionada(Gallery galeriaSelecionada) {
        this.galeriaSelecionada = galeriaSelecionada;
    }

    public String getIdAlbumDestino() {
        return idAlbumDestino;
    }

    public void setIdAlbumDestino(String idAlbumDestino) {
        this.idAlbumDestino = idAlbumDestino;
    }

    public List<GalleryImages> getImagens() {
        return imagens;
    }

    public void setImagens(List<GalleryImages> imagens) {
        this.imagens = imagens;
    }

    public boolean isOpSucesso() {
        return opSucesso;
    }

    public void setOpSucesso(boolean opSucesso) {
        this.opSucesso = opSucesso;
    }

    @Override
    public void edit() {
        if (selected != null) {
            selected.getOperacaoModificacao().add(new OperacaoDAO().save(getNovaOperacao()));
            new GalleryDao().update(selected);
        }
    }

    @Override
    public void delete() {
        if (selected != null) {
            Query createQuery1 = new GalleryImagensDAO().getSession().createQuery("delete from GalleryImages where gallery.id = :id");
            createQuery1.setInteger("id", selected.getId());
            createQuery1.executeUpdate();
            Query createQuery = new GalleryImagensDAO().getSession().createQuery("delete from Gallery where id = :id");
            createQuery.setInteger("id", selected.getId());
            createQuery.executeUpdate();
            selected = null;
        }
    }

    @Override
    public void add() {
        try {
            Operacao op = getNovaOperacao();
            value.setOperacaoCriacao(new OperacaoDAO().save(op));
            value.setOperacaoModificacao(new ArrayList<>());
            value.getOperacaoModificacao().add(new OperacaoDAO().save(op)); 
            value.setVisible(true);
            value = new GalleryDao().save(value);
            setNewValue(true);
            addFacesMsg("Galeria '" + value.getTitle() + "' adicionada com sucesso!", FacesMessage.SEVERITY_INFO);
            clearValue();
        } catch (Exception ex) {
            System.out.println("Erro ao criar Album!");
        }
    }

    public void moverImagem() {
        System.out.println("Movendo imagem do album");
        Gallery album = galerias.get(Integer.parseInt(idAlbumDestino));
        GalleryImages findById = new GalleryImagensDAO().findById(Integer.parseInt(idImagem), false);
        findById.setGallery(album);
        new GalleryImagensDAO().makePersistent(findById);
    }

    public void trocarImagemDaCapa() {
        System.out.println("Alterando imagem da capa");
        Query createQuery = new GalleryImagensDAO().getSession().createQuery("update GalleryImages set capa = :capa where gallery.id = :id and capa = true");
        createQuery.setInteger("id", Integer.parseInt(idAlbumSelecionado));
        createQuery.setBoolean("capa", false);
        System.out.println("Quantidade de rows afetadas\n\t" + createQuery.executeUpdate());
        createQuery = new GalleryImagensDAO().getSession().createQuery("update GalleryImages set capa = :capa where id = :id");
        createQuery.setInteger("id", Integer.parseInt(idImagem));
        createQuery.setBoolean("capa", true);
        createQuery.executeUpdate();
    }

    public void removerImagem() {
        System.out.println("Removendo imagem");
        Query createQuery = new GalleryImagensDAO().getSession().createQuery("delete from GalleryImages where id = :id");
        createQuery.setInteger("id", Integer.parseInt(idImagem));
        createQuery.executeUpdate();
    }

    public void selectAlbum() {
        Gallery album = galerias.get(Integer.parseInt(idAlbumSelecionado));
        imagens = new GalleryImagensDAO().getImagensFromAlbum(album);
        selected = album;
    }

    public void addImagemSelected() {
        Gallery album = galerias.get(Integer.parseInt(idAlbumSelecionado));
        GalleryImages galleryImages = new GalleryImages();
        galleryImages.setGallery(album);
        galleryImages.setImage(new ImageDao().findById(Integer.parseInt(idImagem), false));
        new GalleryImagensDAO().save(galleryImages);
    }

    public void uploadForGallery() {

        ImageController imageController = (ImageController) FacesUtils.getViewAttribute("imageController");
        imageController.addAll();
        List<Image> listImage = imageController.getListaDeFotos();
        Gallery album = galerias.get(Integer.parseInt(idAlbumSelecionado));

        for (int i = 0; i < listImage.size(); i++) {
            GalleryImages galleryImages = new GalleryImages();
            galleryImages.setGallery(album);
            galleryImages.setImage(listImage.get(i));

            if (i == 0) {
                galleryImages.setCapa(true);
            }

            new GalleryImagensDAO().save(galleryImages);
        }
        imageController.getListaDeFotos().clear();
        imageController.getMapFotos().clear();
    }

    public Integer contarImagens(Gallery gallery) {
        return new GalleryImagensDAO().contarImagens(gallery);
    }

    public Pagination getGaleriaPaginacao() {
        if (galeriaPaginacao == null) {
            galeriaPaginacao = new Pagination(10) {

                @Override
                public List<Gallery> getPageDataModel() {
                    return new GalleryDao().getGalerias(getPageFirstItem(), getPageSize(), false, null);
                }

            };
            galeriaPaginacao.setRowCount(new GalleryDao().getRowCount());
        }
        return galeriaPaginacao;
    }

    public void setGaleriaPaginacao(Pagination galeriaPaginacao) {
        this.galeriaPaginacao = galeriaPaginacao;
    }

    public String selecionarGaleriaPeloId() {
        if (idAlbumSelecionado != null && !idAlbumSelecionado.isEmpty()) {
            List<Gallery> gallery = new GalleryDao().getGallery(Integer.parseInt(idAlbumSelecionado));
            if (gallery != null && !gallery.isEmpty()) {
                selected = gallery.get(0);
                return null;
            }
        }
        return "error";
    }

    public List<GalleryImages> imagensLista(Integer id) {
        if (id != null) {
            Query query = new GalleryImagensDAO().getSession().createQuery("from GalleryImages where (gallery.id = :id and capa = true) or (gallery.id = :id) order by capa desc");
            query.setInteger("id", id);
            query.setMaxResults(3);
            List<GalleryImages> list = (List<GalleryImages>) query.list();
            return list;
        }
        return null;
    }

    public String getCapa() {
        if (selected == null) {
            return "null";
        }
        Query query = new GalleryImagensDAO().getSession().createQuery("from GalleryImages where gallery.id = :id and capa = true");
        query.setInteger("id", selected.getId());
        query.setMaxResults(1);
        List<GalleryImages> list = (List<GalleryImages>) query.list();
        if (list != null && !list.isEmpty()) {
            return FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath() + "/ServidorImagens/ServidorImagens/archives/thumbnail/" + list.get(0).getImage().getArchive().getRadomName() + "_tb.jpg";
        }
        return "null";
    }

    public GalleryImages capa2(Integer id) {
        Query query = new GalleryImagensDAO().getSession().createQuery("from GalleryImages where gallery.id = :id and capa = true");
        query.setInteger("id", id);
        query.setMaxResults(1);
        List<GalleryImages> list = (List<GalleryImages>) query.list();
        if (list != null && !list.isEmpty()) {
            return list.get(0);
        }
        return null;
    }
}
