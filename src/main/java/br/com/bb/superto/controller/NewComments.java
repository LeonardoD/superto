/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.controller;

import br.com.bb.superto.dao.CommentsDao;
import br.com.bb.superto.model.Category;
import br.com.bb.superto.model.Comments;
import br.com.bb.superto.model.Funcionarios;
import br.com.bb.superto.model.UserNotLogged;
import br.com.bb.superto.security.UserSecurityUtils;
import br.com.bb.superto.util.FacesUtils;
import br.com.bb.superto.util.Util;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author maycon
 */
@ManagedBean(name = "newComments")
@ViewScoped
public class NewComments implements java.io.Serializable {

    Category category = Category.NEWS;
    NewsController newsController;
    String text = "";
    List<Comments> comentarios;

    public NewComments() {
    }

    private NewsController getNews() {
        if (newsController == null) {
            newsController = (NewsController) FacesUtils.getViewAttribute("newsController");
        }
        return newsController;
    }

    public List<Comments> getComments() {
        return new CommentsDao().getComentarios(category, getNews().getSelected().getId());
    }

    public Integer getQuantidadeDeComentarios() {
        return new CommentsDao().getQuantidadeDeComentarios(category, getNews().getSelected().getId());
    }

    public void like() {

    }

    public String teste(String t) {
        System.out.println(t);
        return t;
    }

    public Integer count(Integer id) {
        System.out.println("Valor do id é : " + id);
        return new CommentsDao().getQuantidadeDeComentarios(category, id);
    }

    public void comentar() {
        try {
            Funcionarios func = UserSecurityUtils.getUserFuncionario().getFuncionario();
            Comments comentario = new Comments();
            comentario.setAuthor(func);
            comentario.setCategory(category);
            comentario.setDatePublication(Util.getDate());
            comentario.setParent(null);
            comentario.setVisible(true);
            comentario.setText(text);
            comentario.setSourceId(getNews().getSelected().getId());
            new CommentsDao().save(comentario);
            this.text = "";
            FacesUtils.addFacesMsg("Comentario adicionado", FacesMessage.SEVERITY_INFO);
        } catch (UserNotLogged ex) {
            Logger.getLogger(NewComments.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void deletarComentario(Comments comentario) {
        new CommentsDao().deletarComentario(comentario);
        FacesUtils.addFacesMsg("Comentario deletado com sucesso.", FacesMessage.SEVERITY_INFO);
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

}
