package br.com.bb.superto.controller;

import br.com.bb.superto.dao.DestaquesDao;
import br.com.bb.superto.dao.OperacaoDAO;
import br.com.bb.superto.model.*;
import br.com.bb.superto.util.FacesUtils;
import br.com.bb.superto.dao.HibernateUtil;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * @author maycon
 */
@ManagedBean(name = "destaqueController")
@ViewScoped
public class DestaquesController extends GenericController<Destaques> implements Serializable {

    private News noticiaSelecionada;
    private Image image;
    private List<Destaques> vrDestaques;
    private boolean opSucesso;
    private boolean externo;

    public boolean isExterno() {
        return externo;
    }

    public void setExterno(boolean externo) {
        this.externo = externo;
    }

    public DestaquesController() {
        opSucesso = false;
    }

    public News getNoticiaSelecionada() {
        return noticiaSelecionada;
    }

    public void setNoticiaSelecionada(News noticiaSelecionada) {
        this.noticiaSelecionada = noticiaSelecionada;
    }

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }

    public List<Destaques> getVrDestaques() {
        return new DestaquesDao().getDestaquesIndex(0, 10, false, Mercado.SUPER);
    }

    public void setVrDestaques(List<Destaques> vrDestaques) {
        this.vrDestaques = vrDestaques;
    }

    public void adicionarImagem(Image imagem) {
        if (imagem != null) {
            image = imagem;
            if (selected != null) {
             selected.setImagem(imagem);
            }
        }
    }

    public boolean isOpSucesso() {
        return opSucesso;
    }

    public void setOpSucesso(boolean opSucesso) {
        this.opSucesso = opSucesso;
    }

    public void uploadImagem() {
        ImageController imageController = (ImageController) FacesUtils.getViewAttribute("imageController");
        imageController.add();
        adicionarImagem(imageController.getValue());
    }

    @Override
    public void edit() {
        try {
            selected.getOperacaoModificacao().add(new OperacaoDAO().save(getNovaOperacao()));
            if (image != null && !Objects.equals(selected.getImagem().getId(), image.getId())) {
                selected.setImagem(image);
            }
            if (noticiaSelecionada != null) {
                selected.setNoticia(noticiaSelecionada);
            }
            new DestaquesDao().update(selected);
            opSucesso = true;
        } catch (Exception e) {
            System.out.println("Erro ao atualizar imagem: " + e.getMessage());
        }
    }

    @Override
    public void delete() {
        opSucesso = new DestaquesDao().makeTransient(selected) != null;
    }

    @Override
    public void add() {
        if (value != null) {
            if (image != null) {
                value.setImagem(image);
            }
            if (value.getNoticia() == null && value.getUrl()== null){
                value.setUrl("#");
            }
            Operacao op = getNovaOperacao();
            value.setOperacaoCriacao(new OperacaoDAO().save(op));
            value.setOperacaoModificacao(new ArrayList<>());
            value.getOperacaoModificacao().add(new OperacaoDAO().save(op));
            if (noticiaSelecionada != null) {
                value.setNoticia(noticiaSelecionada);
            }
            opSucesso = new DestaquesDao().save(value) != null;
            ImageController imageController = (ImageController) FacesUtils.getViewAttribute("imageController");
            imageController.clearValue();
            image = null;
            clearValue();
        }
    }

    public List<Destaques> destaques(Integer first, Integer max) {
        vrDestaques = new DestaquesDao().getDestaquesIndex(first, max, false, Mercado.SUPER);
        return vrDestaques;
    }

    public List<Destaques> getDestaqueByMercado(Mercado mercado) {
        List<Destaques> listDestaques = new DestaquesDao().getDestaquesByMercado(true, mercado);
        return listDestaques;
    }

    public String enderecoUrl(Destaques destaque) {
        if (destaque.getNoticia() == null) {
            return destaque.getUrl();
        } else {
            String d = "#{facesContext.externalContext.requestContextPath}";
            return "/noticia/" + destaque.getNoticia().getId();
        }
    }
}
