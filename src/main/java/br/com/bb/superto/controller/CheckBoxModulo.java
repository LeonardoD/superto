/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.controller;

import java.util.ArrayList;
import java.util.List;

/**
 * @author maycon
 */
public class CheckBoxModulo implements java.io.Serializable {

    private String modulo;
    private List<String> permissoes;

    public CheckBoxModulo() {
        this.permissoes = new ArrayList<>();
    }

    public CheckBoxModulo(String modulo) {
        super();
        this.permissoes = new ArrayList<>();
        this.modulo = modulo;
    }

    public String getModulo() {
        return modulo;
    }

    public void setModulo(String modulo) {
        this.modulo = modulo;
    }

    public List<String> getPermissoes() {
        return permissoes;
    }

    public void setPermissoes(List<String> permissoes) {
        this.permissoes = permissoes;
    }

}
