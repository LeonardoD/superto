/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.controller;

import br.com.bb.superto.enums.Meses;
import br.com.bb.superto.model.Actions;
import br.com.bb.superto.model.Mercado;
import br.com.bb.superto.model.SubCategoria;
import br.com.bb.superto.dao.HibernateUtil; // no logger, deu falta disso, portanto botei ela aqui
import java.sql.Timestamp;
import java.time.LocalDateTime;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


/**
 * @author maycon
 */
@ManagedBean(name = "utilController")
@ViewScoped
public class UtilController implements java.io.Serializable {

    Mercado mercado;
    List<Mercado> listCategorias;

    public UtilController() {
        listCategorias = new ArrayList<>();
        listaMeses = new ArrayList<>();
    }
    
    public Timestamp dataCorrente(){
        Timestamp t = Timestamp.valueOf(LocalDateTime.now());
        return t;
    }
    
    public Timestamp localDateToTimestamp(LocalDateTime localDateTime){
        Timestamp t = Timestamp.valueOf(localDateTime);
        return t;
    }
    
    public LocalDateTime TimestampTolocalDate(Timestamp timestamp){
        LocalDateTime t = timestamp.toLocalDateTime();
        return t;
    }

    public Mercado getMercado() {
        return mercado;
    }

    public void setMercado(Mercado mercado) {
        this.mercado = mercado;
    }

    public Mercado[] getCategoryValues() {
        return Mercado.values();
    }

    public List<Mercado> getListCategorias() {
        if (listCategorias.isEmpty()) {
            listCategorias.add(Mercado.SUPER);
            listCategorias.add(Mercado.ADM);
            listCategorias.add(Mercado.PF);
            listCategorias.add(Mercado.PJ);
            listCategorias.add(Mercado.AGRO);
            listCategorias.add(Mercado.DS);
            listCategorias.add(Mercado.GOV);
            listCategorias.add(Mercado.ECOA);
        }
        return listCategorias;
    }

    public void setListCategorias(List<Mercado> listCategorias) {
        this.listCategorias = listCategorias;
    }

    public void categorizar(Mercado mercado) {
        this.mercado = mercado;
    }

    public SubCategoria[] getSubCategorias() {
        ArrayList<SubCategoria> subCategorias = new ArrayList<>();

        if (mercado == Mercado.ADM) {
            subCategorias.add(SubCategoria.adimplencia);
            subCategorias.add(SubCategoria.ajui);
            subCategorias.add(SubCategoria.ambienc);
            subCategorias.add(SubCategoria.atendimento);
            subCategorias.add(SubCategoria.avali);
            subCategorias.add(SubCategoria.postal);
            subCategorias.add(SubCategoria.capacit);
            subCategorias.add(SubCategoria.coban);
            subCategorias.add(SubCategoria.contMov);
            subCategorias.add(SubCategoria.judici);
            subCategorias.add(SubCategoria.administ);
            subCategorias.add(SubCategoria.equip);
            subCategorias.add(SubCategoria.funcionalismo);
            subCategorias.add(SubCategoria.icc);
            subCategorias.add(SubCategoria.imprensa);
            subCategorias.add(SubCategoria.inscDAU);
            subCategorias.add(SubCategoria.lob);
            subCategorias.add(SubCategoria.logistica);
            subCategorias.add(SubCategoria.orfix);
            subCategorias.add(SubCategoria.patroc);
            subCategorias.add(SubCategoria.prestCont);
            subCategorias.add(SubCategoria.procurac);
            subCategorias.add(SubCategoria.qualiCred);
            subCategorias.add(SubCategoria.rating);
            subCategorias.add(SubCategoria.regulFGO);
            subCategorias.add(SubCategoria.renegDivid);
            subCategorias.add(SubCategoria.siner);
            subCategorias.add(SubCategoria.sng);
            subCategorias.add(SubCategoria.taa);
            //subCategorias.add(SubCategoria.atend);
        } else if (mercado == Mercado.AGRO) {
            subCategorias.add(SubCategoria.mpo);
            subCategorias.add(SubCategoria.caf);
            subCategorias.add(SubCategoria.fies);
            subCategorias.add(SubCategoria.pronaf);
            subCategorias.add(SubCategoria.planDRS);
            subCategorias.add(SubCategoria.inadAGRO);
            subCategorias.add(SubCategoria.credAGRO);
            subCategorias.add(SubCategoria.cartAgro);
            subCategorias.add(SubCategoria.segurRural);
            subCategorias.add(SubCategoria.abcDCA);
        } else if (mercado == Mercado.DS) {
            subCategorias.add(SubCategoria.mpo);
            subCategorias.add(SubCategoria.caf);
            subCategorias.add(SubCategoria.fies);
            subCategorias.add(SubCategoria.pronaf);
            subCategorias.add(SubCategoria.planDRS);
            subCategorias.add(SubCategoria.inadAGRO);
            subCategorias.add(SubCategoria.credAGRO);
            subCategorias.add(SubCategoria.cartAgro);
            subCategorias.add(SubCategoria.segurRural);
            subCategorias.add(SubCategoria.abcDCA);
        } else if (mercado == Mercado.PF) {
            subCategorias.add(SubCategoria.gov);
            subCategorias.add(SubCategoria.creditoPF);
            subCategorias.add(SubCategoria.imob);
            subCategorias.add(SubCategoria.segurPF);
            subCategorias.add(SubCategoria.gestConv);
        } else if (mercado == Mercado.PJ) {
            subCategorias.add(SubCategoria.credPJG);
            subCategorias.add(SubCategoria.credPJI);
            subCategorias.add(SubCategoria.segurPJ);
            subCategorias.add(SubCategoria.limCred);
            subCategorias.add(SubCategoria.dicasPJ);
            subCategorias.add(SubCategoria.kitAberturaDeContasPJ);
            subCategorias.add(SubCategoria.credPJRecebiveis);
            subCategorias.add(SubCategoria.gestCart);
            subCategorias.add(SubCategoria.cobr);
            subCategorias.add(SubCategoria.relacionamento);
            subCategorias.add(SubCategoria.sinergia);
            subCategorias.add(SubCategoria.cartoes);
            subCategorias.add(SubCategoria.comex);
            subCategorias.add(SubCategoria.demaisArq);
        }
        SubCategoria vetor[] = new SubCategoria[subCategorias.size()];
        for (int i = 0; i < subCategorias.size(); i++) {
            vetor[i] = subCategorias.get(i);
        }
        return vetor;
    }

    public Mercado getMercadoADM() {
        return Mercado.ADM;
    }

    public Mercado getMercadoPF() {
        return Mercado.PF;
    }

    public Mercado getMercadoGOV() {
        return Mercado.GOV;
    }

    public Mercado getMercadoPJ() {
        return Mercado.PJ;
    }

    public Mercado getMercadoAGRO() {
        return Mercado.AGRO;
    }

    public Mercado getMercadoDS() {
        return Mercado.DS;
    }

    public Mercado getMercadoSuper() {
        return Mercado.SUPER;
    }
    
    public Mercado getMercadoECOA() {
        return Mercado.ECOA;
    }

    public Actions[] getActionsValues() {
        return Actions.values();
    }
    
    
    /********* meses do ano *********/
    private List<Meses> listaMeses;
    ArrayList<Meses> arrayList = new ArrayList<>();
    public List<Meses> getListaMeses() {
        if (listaMeses.isEmpty()) {
            arrayList = new ArrayList<>(Arrays.asList(Meses.values()));
        }
        return arrayList;
    }

    public void setListaMeses(List<Meses> listaMeses) {
        this.listaMeses = listaMeses;
    }
    
    
}
