/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.controller;

import br.com.bb.superto.dao.ArchiveDao;
import br.com.bb.superto.model.Archive;
import br.com.bb.superto.model.UserNotLogged;
import br.com.bb.superto.security.UserSecurityUtils;
import br.com.bb.superto.util.Util;
import org.primefaces.model.CroppedImage;
import org.primefaces.model.UploadedFile;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author maycon
 */
@ManagedBean(name = "archiveController")
@ViewScoped
public class ArchiveController extends GenericController<Archive> implements java.io.Serializable {

    private CroppedImage croppedImage;

    public ArchiveController() {
    }

    public CroppedImage getCroppedImage() {
        return croppedImage;
    }

    public void setCroppedImage(CroppedImage croppedImage) {
        this.croppedImage = croppedImage;
    }

    public void delete() {
    }

    public Archive add(File file) throws FileNotFoundException, UserNotLogged, IOException {
        if (file != null) {
            try {
                FileOutputStream fos = null;
                String path = Util.getMediaPath();
                String nameRandom = Util.getRandomName() + "_" + Util.removerAcentos(file.getName());
                String urlTemp = path + nameRandom;
                String url = File.separator + "ServidorImagens" + File.separator + "ServidorImagens" + File.separator + "archives" + File.separator + nameRandom;
                //    String url =  "http://superto.bb.com.br/intranet/resources/templates/resources/images" + File.separator+nameRandom;
                File outFile = new File(urlTemp);
                byte[] byteData = Util.fileToByte(file);
                File newFile = new File(path);
                if (!newFile.exists()) {
                    newFile.mkdirs();
                }
                fos = new FileOutputStream(outFile);
                fos.write(byteData);
                fos.close();
                Archive archive = new Archive();
                archive.setArchiveSize(file.getUsableSpace());
                //archive.setMimeType();
                archive.setName(file.getName());
                archive.setRadomName(nameRandom);
                archive.setUploadDate(Util.getDate());
                archive.setUploadHour(Util.getHour());
                archive.setUrl(url);
                archive.setUserUploaded(UserSecurityUtils.getUserFuncionario().getFuncionario());
                return new ArchiveDao().save(archive);
            } catch (Exception ex) {
                Logger.getLogger(ArchiveController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return null;
    }

    public Archive add(UploadedFile file) throws FileNotFoundException, UserNotLogged, IOException {
        if (file != null) {
            ArchiveDao archiveDao = new ArchiveDao();
            FileOutputStream fos = null;
            try {
                String path = Util.getMediaPath();
                String prefix = file.getFileName().substring(file.getFileName().lastIndexOf("."), file.getFileName().length());
                String name = file.getFileName().substring(0, file.getFileName().lastIndexOf("."));
                String nameRandom = Util.getRandomName() + "_" + Util.removerAcentos(name) + prefix;
                String url = path + nameRandom;
                String urlRelativa = File.separator + "archives" + File.separator + nameRandom;

                File outFile = new File(url);
                byte[] byteData = file.getContents();
                File newFile = new File(path);
                if (!newFile.exists()) {
                    newFile.mkdirs();
                }
                fos = new FileOutputStream(outFile);
                fos.write(byteData);
                fos.close();

                Archive archive = new Archive();
                archive.setArchiveSize(file.getSize());
                archive.setMimeType(file.getContentType());
                archive.setName(file.getFileName());
                archive.setRadomName(nameRandom);
                archive.setUploadDate(Util.getDate());
                archive.setUploadHour(Util.getHour());
                archive.setUrl(urlRelativa);
                archive.setUserUploaded(UserSecurityUtils.getUserFuncionario().getFuncionario());
                return new ArchiveDao().save(archive);
            } finally {
                try {
                    if (fos != null) {
                        fos.close();
                    }
                } catch (IOException ex) {
                    Logger.getLogger(ArchiveController.class.getName()).log(Level.SEVERE, null, ex);
                }
                archiveDao = null;
            }
        } else {
            System.out.println("Testando testando 4");
            throw new FileNotFoundException("null");
        }

    }

    @Override
    public void edit() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void add() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public File createTemporaryFile(byte[] data, String name) throws IOException {
        File file = File.createTempFile(name, Long.toString(System.nanoTime()));
        FileOutputStream fos = null;
        fos = new FileOutputStream(file);
        fos.write(data);
        fos.close();
        return file;
    }
}
