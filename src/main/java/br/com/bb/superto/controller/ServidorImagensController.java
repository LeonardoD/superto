/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.controller;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.io.File;
import java.io.Serializable;

/**
 * @author vinicius
 */
@ManagedBean
@ViewScoped
public class ServidorImagensController implements Serializable {

    public String getRestUrl() {
        return "/ws/file/img/";
    }

    public String getRestTumb() {
        return "/ws/file/tumb/";
    }

    public String getRestVideo() {
        return "/ws/file/video/";
    }

    public String getFileName(String url) {
        String link = url.replace(File.separator + "archives" + File.separator, "");
        link = link.replace(File.separator + "ServidorImagens" + File.separator + "ServidorImagens", "");
        link = link.replace(File.separator + "ServidorImagens" + File.separator + "ServidorImagens" + File.separator, "");
        link = link.replace(File.separator + "home" + File.separator, "");
        return link;
    }

    public String getFileTb(String url) {
        String link = url.replace(File.separator + "archives" + File.separator, "");
        link = link.replace(File.separator + "ServidorImagens" + File.separator + "ServidorImagens", "");
        link = link.replace(File.separator + "ServidorImagens" + File.separator + "ServidorImagens" + File.separator, "");
        link = link.replace(File.separator + "home" + File.separator, "");
        link = link.substring(0, link.length() - 4);
        link += "_tb.jpg";
        return link;
    }
}
