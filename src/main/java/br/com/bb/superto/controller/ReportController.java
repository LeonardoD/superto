/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.controller;

import br.com.bb.superto.dao.OperacaoDAO;
import br.com.bb.superto.dao.ReportsDao;
import br.com.bb.superto.model.*;
import org.hibernate.HibernateException;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.io.*;
import java.util.ArrayList;

import java.util.List;
import java.util.logging.Level;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;
import java.util.logging.Logger;

/**
 * @author maycon
 */
@ManagedBean(name = "reportController")
@ViewScoped
public class ReportController extends GenericController<Reports> implements java.io.Serializable {

    private UploadedFile file;
    private List<Reports> relatoriosHibernate;
    private List<Reports> relatoriosFiltrados;
    private boolean opSucess;

    public ReportController() {

    }

    public List<Reports> getRelatoriosHibernate() {
        relatoriosHibernate = getRelatorios();
        return relatoriosHibernate;
    }

    public void setRelatoriosHibernate(List<Reports> relatoriosHibernate) {
        this.relatoriosHibernate = relatoriosHibernate;
    }

    public List<Reports> getRelatoriosFiltrados() {
        return relatoriosFiltrados;
    }

    public void setRelatoriosFiltrados(List<Reports> relatoriosFiltrados) {
        this.relatoriosFiltrados = relatoriosFiltrados;
    }

    public boolean isOpSucess() {
        return opSucess;
    }

    public void setOpSucess(boolean opSucess) {
        this.opSucess = opSucess;
    }

    public void fileUploadListener(FileUploadEvent event) {
        file = event.getFile();
        value.setTitle(file.getFileName().substring(0, file.getFileName().lastIndexOf(".")));
    }

    public List<Reports> getUltimosRelatoriosAdicionados() {
        return new ReportsDao().getUltimosRelatorios();
    }

    public List<Reports> ultimosRelatorios(Integer quantidade) {
        return new ReportsDao().getUltimosRelatorios(quantidade);
    }

    public List<Reports> getRelatorios() {
        relatoriosHibernate = new ReportsDao().findAll();
        return new ReportsDao().findAll();
    }

    public List<Reports> getRelatorios(String category) {
        relatoriosHibernate = new ReportsDao().getUltimosRelatorios(category);
        return new ReportsDao().getUltimosRelatorios(category);
    }

    public void filtrar(String value) {
        System.out.println(value);
        relatoriosHibernate = new ReportsDao().getUltimosRelatorios(value);
        lazyData.setFilter("category = '" + value + "'");
    }

    public void filtroEspecifico(String value, SubCategoria subCategoria) {
        relatoriosHibernate = new ReportsDao().getUltimosRelatorios(value, subCategoria);
        lazyData.setFilter("category = '" + value + "'");
    }

    public UploadedFile getFile() {
        return file;
    }

    public void setFile(UploadedFile file) {
        this.file = file;
    }

    public StreamedContent download(Archive report) {
        try {
            InputStream stream = null;
            try {
                stream = new FileInputStream(File.separator + "home" + File.separator + report.getUrl());
            } catch (FileNotFoundException ex) {
                Logger.getLogger(ReportController.class.getName()).log(Level.SEVERE, null, ex);
            }
            addFacesMsg("A URL é essa aqui" + report.getUrl(), FacesMessage.SEVERITY_WARN);
            return new DefaultStreamedContent(stream, report.getMimeType(), report.getName());
        } catch (NullPointerException e) {
            throw new NullPointerException();
        }
    }

    @Override
    public void edit() {
        try {
            if (selected == null) {
                throw new NullPointerException();
            }
            selected.getOperacaoModificacao().add(new OperacaoDAO().save(getNovaOperacao()));
            opSucess = new ReportsDao().makePersistent(selected) != null;
            selected = null;
        } catch (NullPointerException | HibernateException ex) {
            Logger.getLogger(ReportController.class.getName()).log(Level.SEVERE, "Erro ao editar arquivo!", ex);
        }
    }

    @Override
    public void delete() {
        try {
            if (selected == null) {
                throw new NullPointerException();
            }
            opSucess = new ReportsDao().makeTransient(selected) != null;
            selected = null;
        } catch (Exception ex) {
            Logger.getLogger(ReportController.class.getName()).log(Level.SEVERE, "Erro ao deletar arquivo!", ex);
        }
    }

    @Override
    public void add() {
        try {
            Operacao op = getNovaOperacao();
            value.setOperacaoCriacao(new OperacaoDAO().save(op));
            value.setOperacaoModificacao(new ArrayList<>());
            value.getOperacaoModificacao().add(new OperacaoDAO().save(op));
            Archive archive = new ArchiveController().add(file);
            value.setReport(archive);
            opSucess = new ReportsDao().save(value) != null;
            value = new Reports();
        } catch (UserNotLogged | IOException ex) {
            Logger.getLogger(ReportController.class.getName()).log(Level.SEVERE, "Erro ao salvar arquivo!", ex);
        }
    }

    /* sssssssssssssssssssssssssssssssssssssssssss **/
    List<Reports> arquivosADM;
    List<Reports> arquivosAGRO;
    List<Reports> arquivosDS;
    List<Reports> arquivosGOV;
    List<Reports> arquivosPF;
    List<Reports> arquivosPJ;
    List<Reports> arquivosECOA;

    private StreamedContent file2;
    private List<Archive> listaDeArquivos;

    public List<Reports> getArquivosADM() {
        if (arquivosADM == null) {
            arquivosADM = new ArrayList<>();
        }
        return arquivosADM;
    }
    
    public List<Reports> buscaCartilhaAdimplencia() {
        setArquivosADM(new ReportsDao().findBySubcategoria("adimplencia"));
        return arquivosADM;
    }
    
    public List<Reports> buscaCartilhaAtendimento() {
        setArquivosADM(new ReportsDao().findBySubcategoria("atendimento"));
        return arquivosADM;
    }
    
    public List<Reports> buscaCartilhaFuncionalismo() {
        setArquivosADM(new ReportsDao().findBySubcategoria("funcionalismo"));
        return arquivosADM;
    }
    
    public List<Reports> buscaCartilhaLogistica() {
        setArquivosADM(new ReportsDao().findBySubcategoria("logistica"));
        return arquivosADM;
    }
    
    public List<Reports> buscaCartilhaRating() {
        setArquivosADM(new ReportsDao().findBySubcategoria("rating"));
        return arquivosADM;
    }

    public void setArquivosADM(List<Reports> arquivosADM) {
        this.arquivosADM = arquivosADM;
    }

    public List<Reports> getArquivosAGRO() {
        arquivosAGRO = new ArrayList<>();
        arquivosAGRO = new ReportsDao().findByMercado("AGRO");

        return arquivosAGRO;
    }

    public void setArquivosAGRO(List<Reports> arquivosAGRO) {
        this.arquivosAGRO = arquivosAGRO;
    }

    public List<Reports> getArquivosDS() {
        arquivosDS = new ArrayList<>();
        arquivosDS = new ReportsDao().findByMercado("DS");

        return arquivosDS;
    }

    public void setArquivosDS(List<Reports> arquivosDS) {
        this.arquivosDS = arquivosDS;
    }

    public void setArquivosGOV(List<Reports> arquivosGOV) {
        this.arquivosGOV = arquivosGOV;
    }

    public List<Reports> getarquivosGOV() {
        arquivosGOV = new ArrayList<>();
        arquivosGOV = new ReportsDao().findByMercado("GOV");

        return arquivosGOV;
    }

    public void setArquivosPF(List<Reports> arquivosPF) {
        this.arquivosPF = arquivosPF;
    }

    public List<Reports> getarquivosPF() {
        arquivosPF = new ArrayList<>();
        arquivosPF = new ReportsDao().findByMercado("PF");

        return arquivosPF;
    }

    public void setArquivosPJ(List<Reports> arquivosPJ) {
        this.arquivosPJ = arquivosPJ;
    }

    public List<Reports> getarquivosPJ() {
        arquivosPJ = new ArrayList<>();
        arquivosPJ = new ReportsDao().findByMercado("PJ");

        return arquivosPJ;
    }

    public List<Reports> getArquivosECOA() {
        arquivosECOA = new ArrayList<>();
        arquivosECOA = new ReportsDao().findByMercado("ECOA");

        return arquivosECOA;
    }

    public void setArquivosECOA(List<Reports> arquivosECOA) {
        this.arquivosECOA = arquivosECOA;
    }

    public StreamedContent getFile2() {
        return file2;
    }

    public void setFile(StreamedContent file2) {
        this.file2 = file2;
    }

    public StreamedContent realizarDownload(Archive a) {
        String name;
        System.out.println(a.getUrl());
        FileInputStream stream;
        try {
            System.out.println(a.getUrl());
            stream = new FileInputStream("/home" + a.getUrl());
            name = a.getUrl().substring(a.getUrl().lastIndexOf(File.separator), a.getUrl().length());
            file2 = new DefaultStreamedContent(stream, "", name);
        } catch (FileNotFoundException ex) {
            System.out.println("Erro no Download: " + ex.getMessage());
        }
        return file2;
    }

}
