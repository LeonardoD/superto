/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.controller;

import br.com.bb.superto.dao.ClienteLobDao;
import br.com.bb.superto.dao.ClienteMDDao;
import br.com.bb.superto.dao.ClienteSeguridadeDao;
import br.com.bb.superto.dao.OperacaoDAO;
import br.com.bb.superto.dao.ThingDao;
import br.com.bb.superto.model.ClienteLob;
import br.com.bb.superto.model.ClienteMD;
import br.com.bb.superto.model.Funcionarios;
import br.com.bb.superto.model.Operacao;
import br.com.bb.superto.model.Thing;
import br.com.bb.superto.model.UserNotLogged;
import br.com.bb.superto.security.UserSecurityUtils;
import com.csvreader.CsvReader;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Type;
import java.nio.charset.Charset;
import java.util.ArrayList;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.hibernate.HibernateException;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

/**
 *
 * @author administrador
 */
@ManagedBean(name = "clienteMdController")
@ViewScoped
public class ClienteMDController extends GenericController<ClienteMD> {

    private ClienteMD cliente;
    private List<ClienteMD> clientes;
    private List<Thing> coisas;
    private boolean opSucess;
    private Funcionarios func;
    private long feitosMD;
    private long totalMD;
    private boolean possui;
    
    public ClienteMDController() {
        clientes();
    }

    public Funcionarios getFunc() {
        return func;
    }

    public void setFunc(Funcionarios func) {
        this.func = func;
    }

    public ClienteMD getCliente() {
        return cliente;
    }

    public void setCliente(ClienteMD cliente) {
        this.cliente = cliente;
    }
    
    public ClienteMD pegarClientePorMci(Integer mci){
        return new ClienteMDDao().findByMci(mci);
    }

    public List<ClienteMD> getClientes() {
        return clientes;
    }

    public void setClientes(List<ClienteMD> clientes) {
        this.clientes = clientes;
    }

    public boolean isOpSucess() {
        return opSucess;
    }

    public void setOpSucess(boolean opSucess) {
        this.opSucess = opSucess;
    }
    
    public void contatar(Integer mci, boolean aux){
        this.pegarClientePorMci(mci).setContatado(aux);
    }

    public long getFeitosMD(int prefixo) {
        if(prefixo == 8517){
            feitosMD = new ClienteSeguridadeDao().CountFeitosComCMD();
        }
        else{
            feitosMD = new ClienteSeguridadeDao().CountFeitosByPrefixo(prefixo+"");
        }
        return feitosMD;
    }

    public void setFeitosMD(int feitosMD) {
        this.feitosMD = feitosMD;
    }

    public long getTotalMD(int prefixo) {
        if(prefixo == 8517){
            totalMD = new ClienteSeguridadeDao().CountComCMD();
        }
        else{
            totalMD = new ClienteSeguridadeDao().CountByPrefixo(prefixo+"");
        }
        return totalMD;
    }

    public void setTotalMD(int totalMD) {
        this.totalMD = totalMD;
    }

    public void pegarFuncionario() {
        try {
            func = UserSecurityUtils.getUserFuncionario().getFuncionario();
        } catch (UserNotLogged ex) {
            Logger.getLogger(ClienteMDController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void clientes() {
        pegarFuncionario();
        if (func.getPrefixo() == 8517) {
            setClientes(new ClienteMDDao().findAll());
        } else {
//            setClientes(new ClienteMDDao().findByPrefixo(func.getPrefixo().toString()));
        }
    }
    @Override
    public void delete() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void add() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void edit() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public List<Thing> getCoisas() {
        return coisas;
    }

    public void setCoisas(List<Thing> coisas) {
        this.coisas = coisas;
    }
    
    public List<Thing> pegarCoisasPorCliente(Integer MCI){
        try{
            System.out.print("MCI: " + MCI);
            ClienteMD cmd = new ClienteMDDao().findByMci(MCI);
            System.out.print("CMD selecionado: " + cmd.getId());
            String cpf = cmd.getCpf()+"";
            System.out.print("cpf: " + cpf);
            System.out.print("CHEKPOINT 1");
            System.out.print("CHEKPOINT 1");
            System.out.print("CHEKPOINT 1 - PRÓX = RETORNAR");
            return new ThingDao().findFiltradosCliente(cpf);
        }
        catch(Exception ex){
            System.out.print("Erro: " + ex);
            return new ArrayList<Thing>();
        }
    }
    
    public void limpaContatos(){
        List<ClienteMD> todos;
        todos = new ClienteMDDao().findAll();
        
        for(ClienteMD cmd : todos){
            cmd.setContatado(false);
            Operacao operacao = getNovaOperacao();
                            try {
                                System.out.print("Entramos no try de edição");
                                Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
                                Type clienteType = new TypeToken<ClienteMD>() {
                                }.getType();
                                String jsonOut = gson.toJson(cmd, clienteType);
                                System.out.print("cliente: " + jsonOut);
                                operacao.setJsonObj(jsonOut);
                            } catch (Exception e) {
                                System.out.println("Errro <<<<<<<<<<<<<<< " + e.getMessage() + " >>>>>>>>>>>>>>>>");
                            }
                            System.out.print("Próximo passo é fazê-los persistentes");
                            try{
                                operacao = new OperacaoDAO().makePersistent(operacao);
                                opSucess = new ClienteMDDao().makePersistent(cmd) != null;
                                System.out.print("Registramos operação");
                            }
                            catch(Exception ex){
                                System.out.print("Erro: " + ex);
                            }
        }
    }
    
    public boolean lerArq(FileUploadEvent event) {
        try {
            System.out.print("ENTRAMOS NO LERARQ");
            UploadedFile arq = event.getFile();
            if (arq != null) {
                System.out.print("Arquivo não é nulo");
                CsvReader csvReader = new CsvReader(arq.getInputstream(), Charset.forName("utf-8"));
                csvReader.setDelimiter(';');
                csvReader.readHeaders();
                while (csvReader.readRecord()) {
                    Integer Mci = Integer.parseInt(csvReader.get(0));
                    String Orgao = csvReader.get(1);
                    
                    String marAux = csvReader.get(2).replace(',', '.');
                    Double Margem = 0.0;
                    try{
                        Margem = Double.parseDouble(marAux);
                    }
                    catch(Exception ex){
                        System.out.print("Erro: " + ex);
                    }
                    
                    String consigAux = csvReader.get(3).replace(',', '.');
                    Double Consig = 0.0;
                    try{
                        Consig = Double.parseDouble(consigAux);
                    }
                    catch(Exception ex){
                        System.out.print("Erro: " + ex);
                    }
                    
                    String trocoAux = "";
                    Double Troco = 0.0;
                    String salarioAux = "";
                    Double Salario = 0.0;
                    boolean focoSalario = false;
                    String aux2 = csvReader.get(6);
                    String cpf = ((csvReader.get(7).replace(".", "").replace("-", "")));
                    try{
                        trocoAux = csvReader.get(4).replace(',', '.');
                        Troco = Double.parseDouble(trocoAux);
                        salarioAux = csvReader.get(5).replace(',', '.');
                        Salario = Double.parseDouble(salarioAux);
                        if(aux2.equalsIgnoreCase("sim")){
                            focoSalario = true;
                        }
                    }
                    catch(Exception ex){
                        System.out.print("Erro: " + ex);
                    }
                    ClienteMD clienteAtualizado = null;
                    try{
                        clienteAtualizado = new ClienteMDDao().findById(Mci, true);
                    }
                    catch(Exception ex){
                        System.out.print(ex);
                    }
                    System.out.print("CHECKPOINT 1");
                    System.out.print("CHECKPOINT 1");
                    System.out.print("CHECKPOINT 1");
                    System.out.print("CHECKPOINT 1");
                    try {
                        if (clienteAtualizado != null) {
                            System.out.print("Cliente Atualizado não é vazio");
                            clienteAtualizado.setMargem(Margem);
                            clienteAtualizado.setOrgao(Orgao);
                            clienteAtualizado.setConsig(Consig);
                            clienteAtualizado.setTrocomax(Troco);
                            clienteAtualizado.setSalario(Salario);
                            clienteAtualizado.setFocosalario(focoSalario);
                            clienteAtualizado.setCpf(cpf);
                            System.out.print("Cliente Atualizado setado");
                            
                            
                            Operacao operacao = getNovaOperacao();
                            try {
                                System.out.print("Entramos no try de edição");
                                Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
                                Type clienteType = new TypeToken<ClienteMD>() {
                                }.getType();
                                String jsonOut = gson.toJson(clienteAtualizado, clienteType);
                                System.out.print("cliente: " + jsonOut);
                                operacao.setJsonObj(jsonOut);
                            } catch (Exception e) {
                                System.out.println("Errro <<<<<<<<<<<<<<< " + e.getMessage() + " >>>>>>>>>>>>>>>>");
                            }
                            System.out.print("Próximo passo é fazê-los persistentes");
                            try{
                                operacao = new OperacaoDAO().makePersistent(operacao);
                                opSucess = new ClienteMDDao().makePersistent(clienteAtualizado) != null;
                                System.out.print("Registramos operação");
                            }
                            catch(Exception ex){
                                System.out.print("Erro: " + ex);
                            }
                        } else {
                            ClienteMD clienteNovo = new ClienteMD();
                            try{
                                System.out.print("Setando novo cliente");
                                clienteNovo.setMargem(Margem);
                                clienteNovo.setConsig(Consig);
                                clienteNovo.setTrocomax(Troco);
                                clienteNovo.setSalario(Salario);
                                clienteNovo.setFocosalario(focoSalario);
                                clienteNovo.setCpf(cpf);
                                System.out.print("Setado novo cliente");
                            }
                            catch(Exception ex){
                                clienteNovo.setMargem(0);
                                clienteNovo.setConsig(0);
                                clienteNovo.setTrocomax(0);
                                clienteNovo.setSalario(0);
                                clienteNovo.setFocosalario(false);
                                clienteNovo.setCpf("");
                                System.out.print("Erro: " + ex);
                            }
                            clienteNovo.setOrgao(Orgao);
                            Operacao operacao = getNovaOperacao();
                            try {
                                System.out.print("Entramos no try de salvar");
                                Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
                                Type clienteType = new TypeToken<ClienteMD>() {
                                }.getType();
                                String jsonOut = gson.toJson(clienteNovo, clienteType);
                                operacao.setJsonObj(jsonOut);
                                System.out.print("cliente: " + jsonOut);
                            } catch (Exception e) {
                                System.out.println("Errro <<<<<<<<<<<<<<< " + e.getMessage() + " >>>>>>>>>>>>>>>>");
                            }
                            clienteNovo.setMci(Mci);
                            System.out.print("Salvando cliente");
                            try{
                                ClienteMD aux = new ClienteMDDao().save(clienteNovo);
                                opSucess = aux != null;
                            }
                            catch(Exception ex){
                                System.out.print("Erro: " + ex);
                            }
                        }
                    } catch (HibernateException ex) {
                        Logger.getLogger(ClienteLob.class.getName()).log(Level.SEVERE, "Erro na importação do CSV, favor faver upload do arquivo csv novamente!", ex);
                    }
                }
                FacesMessage msg = new FacesMessage("SUCESSO!", "Base de Dados importada com sucesso!");
                FacesContext.getCurrentInstance().addMessage(null, msg);
                Logger.getLogger(ClienteLob.class.getName()).log(Level.INFO, "Importação dos dados concluída!");
            } else {
                FacesMessage msg = new FacesMessage("ATENÇÃO!", "Favor fazer upload do arquivo!");
                FacesContext.getCurrentInstance().addMessage(null, msg);
                Logger.getLogger(ClienteLob.class.getName()).log(Level.INFO, "Favor fazer upload do arquivo!");
            }
        } catch (IOException ex) {
            Logger.getLogger(ClienteLob.class.getName()).log(Level.SEVERE, "Erro CSVReader: " + ex.getMessage());
        }
        return opSucess;
    }
    
    public boolean lerArqDelete(FileUploadEvent event) {
        try {
            System.out.print("ENTRAMOS NO LERARQ");
            UploadedFile arq = event.getFile();
            if (arq != null) {
                System.out.print("Arquivo não é nulo");
                CsvReader csvReader = new CsvReader(arq.getInputstream(), Charset.forName("utf-8"));
                csvReader.setDelimiter(';');
                csvReader.readHeaders();
                while (csvReader.readRecord()) {
                    Integer Mci = Integer.parseInt(csvReader.get(0));
                    
                    String Orgao = csvReader.get(1);
                    
                    String marAux = csvReader.get(2).replace(',', '.');
                    Double Margem = 0.0;
                    try{
                        Margem = Double.parseDouble(marAux);
                    }
                    catch(Exception ex){
                        System.out.print("Erro: " + ex);
                    }
                    
                    String consigAux = csvReader.get(3).replace(',', '.');
                    Double Consig = 0.0;
                    try{
                        Consig = Double.parseDouble(consigAux);
                    }
                    catch(Exception ex){
                        System.out.print("Erro: " + ex);
                    }
                    
                    String trocoAux = "";
                    Double Troco = 0.0;
                    String salarioAux = "";
                    Double Salario = 0.0;
                    boolean focoSalario = false;
                    String aux2 = csvReader.get(6);
                    try{
                        trocoAux = csvReader.get(4).replace(',', '.');
                        Troco = Double.parseDouble(trocoAux);
                        salarioAux = csvReader.get(5).replace(',', '.');
                        Salario = Double.parseDouble(salarioAux);
                        if(aux2.equalsIgnoreCase("sim")){
                            focoSalario = true;
                        }
                    }
                    catch(Exception ex){
                        System.out.print("Erro: " + ex);
                    }
                    ClienteMD clienteAtualizado = null;
                    try{
                        clienteAtualizado = new ClienteMDDao().findById(Mci, true);
                    }
                    catch(Exception ex){
                        System.out.print(ex);
                    }
                    try {
                        if (clienteAtualizado != null) {
                            System.out.print("Cliente Atualizado não é vazio");                          
                            try{
                                new ClienteMDDao().makeTransient(clienteAtualizado);
                            }
                            catch(Exception ex){
                                System.out.print("Erro: " + ex);
                            }
                        }
                    } catch (HibernateException ex) {
                        Logger.getLogger(ClienteLob.class.getName()).log(Level.SEVERE, "Erro na importação do CSV, favor faver upload do arquivo csv novamente!", ex);
                    }
                }
            } else {
                FacesMessage msg = new FacesMessage("ATENÇÃO!", "Favor fazer upload do arquivo!");
                FacesContext.getCurrentInstance().addMessage(null, msg);
                Logger.getLogger(ClienteLob.class.getName()).log(Level.INFO, "Favor fazer upload do arquivo!");
            }
        } catch (IOException ex) {
            Logger.getLogger(ClienteLob.class.getName()).log(Level.SEVERE, "Erro CSVReader: " + ex.getMessage());
        }
        return opSucess;
    }
    
    // método que pega o CPF da pessoa.
    public static String pegaCPF(String Pessoa){
        String vetPraPegarOcpf[];
        vetPraPegarOcpf = Pessoa.split(";");
        return vetPraPegarOcpf[0];
    }
    
    // método que pega o Banco da Thing.
    public static String pegaBanco(String Pessoa){
        String vetPraPegarBanco2[] = null;
        String bancos = "";
        vetPraPegarBanco2 = Pessoa.split("Empréstimo");
        bancos = vetPraPegarBanco2[0];
        return modelaBanco(bancos);
    }
    
    // método que pega todas as parcelas de operações da pessoa
    public static String pegaParcelas(String Pessoa){
        String vetPraPegar[];
        String vetPraPegarPrazo[];
        String vetPraPegarPrazo2[];
        vetPraPegar = Pessoa.split(";");
        vetPraPegarPrazo = vetPraPegar[1].split("\"\"");
        
        int tamanho = vetPraPegarPrazo.length;
        String prazos = "";
        
        for(int i = 1; i < tamanho; i = i + 1){
            vetPraPegarPrazo2 = vetPraPegarPrazo[i].split("\",\"");
            if(prazos.equals("")){
                vetPraPegarPrazo2[2] = vetPraPegarPrazo2[2].replace(",", ".");
                prazos = vetPraPegarPrazo2[2];
            }
            else{
                vetPraPegarPrazo2[2] = vetPraPegarPrazo2[2].replace(",", ".");
                prazos = prazos +";"+ vetPraPegarPrazo2[2];    
            }
        }
        return prazos;
    }
    
    // método que pega todos os bancos de operações da pessoa
    public static String modelaBanco(String bancos){
        try{
            bancos = bancos.replace("0", "");
            bancos = bancos.replace("1", "");
            bancos = bancos.replace("2", "");
            bancos = bancos.replace("3", "");
            bancos = bancos.replace("4", "");
            bancos = bancos.replace("5", "");
            bancos = bancos.replace("6", "");
            bancos = bancos.replace("7", "");
            bancos = bancos.replace("8", "");
            bancos = bancos.replace("9", "");
            bancos = bancos.replace("BBEI", "BB");
            bancos = bancos.replace("BBE", "BB");
            bancos = bancos.replace("BBI", "BB");
            bancos = bancos.replace("PRODIVINOI", "PRODIVINO");
            bancos = bancos.replace("PRODIVINOE", "PRODIVINO");
            bancos = bancos.replace("BRADESCOE", "BRADESCO");
            bancos = bancos.replace("BRADESCOI", "BRADESCO");
            bancos = bancos.replace("BB ", "BB");
            return bancos;
        }
        catch(Exception ex){
            System.out.print("O número que se tentou excluir não existe na String: " + ex);
            return "";
        }
    }
    
    public boolean gera(FileUploadEvent event) throws IOException{
        UploadedFile arq = event.getFile();
        CsvReader csvReader = new CsvReader(arq.getInputstream(), Charset.forName("utf-8"));
        csvReader.setDelimiter(';');
        try{            
            while(csvReader.readRecord()){
                String cpf = csvReader.get(0);
                String banco = csvReader.get(1);
                String prazo = csvReader.get(2);
                String parcela = csvReader.get(3);
            
                Thing t = new Thing();
                t.setID(0);
                t.setBanco(banco);
                t.setCpf(cpf);
                t.setParcela(parcela);
                t.setPrazo(prazo);
                
                new ThingDao().save(t);
            }
        }
        catch(Exception ex){
            System.out.print("Erro ao processar informações: " + ex);
        }
        return true;
    }

    public boolean isPossui() {
        return possui;
    }

    public void setPossui(boolean possui) {
        this.possui = possui;
    }
    
    public boolean possuiThing(String cpf){
        try{
            return new ThingDao().countFiltradosCliente(cpf) > 0;
        }
        catch(Exception ex){
            System.out.print("Erro: " + ex);
            return false;
        }
    }
}
