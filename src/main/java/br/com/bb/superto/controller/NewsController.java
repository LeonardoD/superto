/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.controller;

import br.com.bb.superto.dao.NewsDao;
import br.com.bb.superto.dao.OperacaoDAO;
import br.com.bb.superto.model.Archive;
import br.com.bb.superto.model.Mercado;
import br.com.bb.superto.model.Image;
import br.com.bb.superto.model.News;
import br.com.bb.superto.model.Operacao;
import br.com.bb.superto.model.UserNotLogged;
import br.com.bb.superto.util.FacesUtils;
import br.com.bb.superto.util.Pagination;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.validation.ConstraintViolationException;
import org.primefaces.event.FileUploadEvent;

/**
 *
 * @author maycon
 */
@ManagedBean(name = "newsController")
@ViewScoped
public class NewsController extends GenericController<News> implements java.io.Serializable {

    private Pagination pagNews;
    private Mercado setorFilter;
    private String categoriaFilter;
    private Archive novaCapa;
    private boolean destaque;
    private boolean opSucesso;
    private List<News> listaNoticias;
    private int sizeListaNoticias;
    private String id; // Id para buscar a notícia

    public NewsController() {
        opSucesso = false;
    }

    public Mercado getSetorFilter() {
        return setorFilter;
    }

    public void setSetorFilter(Mercado setorFilter) {
        this.setorFilter = setorFilter;
    }

    public boolean isDestaque() {
        return destaque;
    }

    public void setDestaque(boolean destaque) {
        this.destaque = destaque;
    }

    public News getNewsById(Integer id) {
        News news = new NewsDao().findById(id, false);
        return news;
    }

    public String getCategoriaFilter() {
        return categoriaFilter;
    }

    public void setCategoriaFilter(String categoriaFilter) {
        this.categoriaFilter = categoriaFilter;
    }

    public void addVisualizacao(){
        selected.setQtVisualizacoes();
    }
    
    public List<News> getAllNews() {
        return new NewsDao().findAll();
    }

    public List<News> ultimasNoticias(Integer first, Integer max) {
        return new NewsDao().getNews(first, max);
    }

    public List<News> getListaNoticias(Mercado mercado) {
        listaNoticias = new NewsDao().getNewsByMercado(true, mercado);
        return listaNoticias;
    }

    public void setListaNoticias(List<News> listaNoticias) {
        this.listaNoticias = listaNoticias;
    }

    public int getSizeListaNoticias() {
        sizeListaNoticias = new NewsDao().getRowCountFiltro(true, new UtilController().getMercadoSuper());
        return sizeListaNoticias;
    }

    public void setSizeListaNoticias(int sizeListaNoticias) {
        this.sizeListaNoticias = sizeListaNoticias;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<News> ultimasNoticias(Integer quantidade) {
        return new NewsDao().getNews(0, quantidade);
    }

    public void adicionarCapa(Image image) {
        value.setCapa(image);
    }

    public void mudarCapa(Image image) {
        selected.setCapa(image);
    }

    public void uploadCapa() {
        ImageController imageController = (ImageController) FacesUtils.getViewAttribute("imageController");
        imageController.add();
        adicionarCapa(imageController.getValue());
    }

    public void uploadNovaCapa() {
        try {
            ImageController imageController = (ImageController) FacesUtils.getViewAttribute("imageController");
            imageController.add();
            mudarCapa(imageController.getValue());
            opSucesso = true;
        } catch (Exception ex) {
            System.out.println("Eroo " + ex.getMessage());
        }
    }

    public boolean isOpSucesso() {
        return opSucesso;
    }

    public void setOpSucesso(boolean opSucesso) {
        this.opSucesso = opSucesso;
    }

    @Override
    public void edit() {
        try {
            System.out.println("<<<<<<<<<<<< Antes da operação: "+selected+">>>>>>>>>>>>>>>");
            if (selected != null) {
                selected.getOperacaoModificacao().add(new OperacaoDAO().save(getNovaOperacao()));
                
                System.out.println("<<<<<<<<<<<< Depois da operação: "+selected+">>>>>>>>>>>>>>>");
                opSucesso = new NewsDao().makePersistent(selected) != null;
            }
            
        } catch (Exception ex) {
            System.out.println("Erro ao editar noticia: " + ex.getMessage());
        }
    }

    public String selectNewsById() {
        if (id != null && !id.isEmpty()) {
            try {
                Integer selectedId = Integer.parseInt(id);
                News news = new NewsDao().findById(selectedId, false);
                if (news != null) {
                    selected = news;
                    return null;
                }
            } catch (Exception ex) {
                addFacesMsg("Não foi possivel selecionar a noticia de id: '" + id + "'", FacesMessage.SEVERITY_ERROR);
                return "error";
            }
        }
        return "error";
    }

    @Override
    public void delete() {
        try {
            if (selected == null) {
                throw new NullPointerException();
            }
            opSucesso = new NewsDao().makeTransient(selected) != null;
            opSucesso = true;
            selected = null;
        } catch (NullPointerException | ConstraintViolationException ex) {
            System.out.println("Erro ao deletar noticia: " + ex.getMessage());
        }
    }

    @Override
    public void add() {
        try {
            String vrUrlTitle = value.getTitle().replace(" ", "_");
            value.setUrlTitle(vrUrlTitle);
            Operacao op = getNovaOperacao();
            value.setOperacaoCriacao(new OperacaoDAO().save(op));
            value.setOperacaoModificacao(new ArrayList<>());
            value.getOperacaoModificacao().add(new OperacaoDAO().save(op));
            opSucesso = new NewsDao().save(value) != null;
            value = new News();
        } catch (Exception ex) {
            System.out.println("Erro ao salvar a noticia: " + ex.getMessage());
        }
    }

    public void fileUploadCapa(FileUploadEvent event) {
        try {
            novaCapa = new ArchiveController().add(event.getFile());
        } catch (UserNotLogged | IOException e) {
            addFacesMsg("Falha no Upload da Capa:  '" + e.getMessage(), FacesMessage.SEVERITY_ERROR);
        }
    }

    public String getUltimaDataOperacao(News news) {
        if (news.getOperacaoModificacao().isEmpty()) {
            return null;
        }
        Operacao op = news.getOperacaoModificacao().get(news.getOperacaoModificacao().size() - 1);
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/mm/yyyy hh:mm:ss");
        return dateFormat.format(op.getDataOperacao());
    }

    public List<News> getNewsMercado(Mercado mercado) {
        return new NewsDao().getNewsByMercado(true, mercado);
    }
}
