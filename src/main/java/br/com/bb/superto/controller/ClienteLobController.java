/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.controller;

import br.com.bb.superto.dao.ClienteLobDao;
import br.com.bb.superto.dao.OperacaoDAO;
import br.com.bb.superto.model.ClienteLob;
import br.com.bb.superto.model.Funcionarios;
import br.com.bb.superto.model.Operacao;
import br.com.bb.superto.model.UserNotLogged;
import br.com.bb.superto.security.UserSecurityUtils;
import com.csvreader.CsvReader;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import java.io.IOException;
import java.lang.reflect.Type;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.hibernate.HibernateException;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;

/**
 *
 * @author administrador
 */
@ManagedBean(name = "clienteLob")
@ViewScoped
public class ClienteLobController extends GenericController<ClienteLob> {

    private ClienteLob cliente;
    private List<ClienteLob> clientes;
    private boolean opSucess;
    private Funcionarios func;

    public ClienteLobController() {
        clientes();
    }

    public Funcionarios getFunc() {
        return func;
    }

    public void setFunc(Funcionarios func) {
        this.func = func;
    }

    public ClienteLob getCliente() {
        return cliente;
    }
    
    public int getPosix(){
        return 500;
    }

    public void setCliente(ClienteLob cliente) {
        this.cliente = cliente;
    }

    public List<ClienteLob> getClientes() {
        return clientes;
    }

    public void setClientes(List<ClienteLob> clientes) {
        this.clientes = clientes;
    }

    public boolean isOpSucess() {
        return opSucess;
    }

    public void setOpSucess(boolean opSucess) {
        this.opSucess = opSucess;
    }

    public void pegarFuncionario() {
        try {
            func = UserSecurityUtils.getUserFuncionario().getFuncionario();
        } catch (UserNotLogged ex) {
            Logger.getLogger(ClienteLobController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void clientes() {
        pegarFuncionario();
        if (func.getPrefixo() == 8517) {
            setClientes(new ClienteLobDao().findAll());
        } else {
            setClientes(new ClienteLobDao().findByPrefixo(func.getPrefixo().toString()));
        }
    }

    public void onRowClienteLobSelect(SelectEvent event) {
        FacesMessage msg = new FacesMessage("Cliente Selecionado", ((ClienteLob) event.getObject()).getMci().toString());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    @Override
    public void edit() {
        if (selected != null) {
            try {
                Operacao operacao = getNovaOperacao();
                try {
                    Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
                    Type clienteLobType = new TypeToken<ClienteLob>() {
                    }.getType();
                    String jsonOut = gson.toJson(selected, clienteLobType);
                    System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>" + jsonOut);
                    operacao.setJsonObj(jsonOut);
                } catch (Exception e) {
                    System.out.println("Errro <<<<<<<<<<<<<<< " + e.getMessage() + " >>>>>>>>>>>>>>>>");
                }
                operacao = new OperacaoDAO().makePersistent(operacao);
                selected.getOperacaoModificacao().add(operacao);
                new ClienteLobDao().update(selected);
                opSucess = true;
                FacesMessage msg = new FacesMessage("SUCESSO!", "Alterações Realizadas!");
                FacesContext.getCurrentInstance().addMessage(null, msg);
            } catch (Exception ex) {
                Logger.getLogger(ClienteLobController.class.getName()).log(Level.SEVERE, "Erro na atualização do Cliente!", ex);
            }
        }
    }

    public ClienteLob clienteFromJson(Operacao opercao) {
        ClienteLob clienteL = null;
        if (opercao != null) {
            try {
                Gson gson = new Gson();
                clienteL = gson.fromJson(opercao.getJsonObj(), ClienteLob.class);
            } catch (Exception e) {
                System.out.println("Erro: " + e.getMessage());
            }
        }
        return clienteL;
    }

    private UploadedFile uploadedFile;
    private StreamedContent file;

    public StreamedContent getFile() {
        return file;
    }

    public void setFile(StreamedContent file) {
        this.file = file;
    }

    public UploadedFile getUploadedFile() {
        return uploadedFile;
    }

    public void setUploadedFile(UploadedFile uploadedFile) {
        this.uploadedFile = uploadedFile;
    }

    public void lerArq(FileUploadEvent event) {
        System.out.print("Ola mundo");
        System.out.print("Ola mundo");
        System.out.print("Ola mundo");
        System.out.print("Ola mundo");
        System.out.print("Ola mundo");
        System.out.print("Ola mundo");
        System.out.print("Ola mundo");
        System.out.print("Ola mundo");
        System.out.print("Ola mundo");
        try {
            UploadedFile arq = event.getFile();
            if (arq != null) {
                CsvReader csvReader = new CsvReader(arq.getInputstream(), Charset.forName("utf-8"));
                csvReader.setDelimiter(';');
                csvReader.readHeaders();
                while (csvReader.readRecord()) {
                    String prefixo = csvReader.get(0);
                    String NrCaretira = csvReader.get(1);
                    Integer Mci = Integer.parseInt(csvReader.get(2));
                    String Nome = csvReader.get(3);
                    String telefone = csvReader.get(4);
                    boolean Lob = csvReader.get(5).equalsIgnoreCase("SIM");
                    ClienteLob clienteAtualizado = new ClienteLobDao().findById(Mci, true);
                    try {
                        if (clienteAtualizado != null) {
                            clienteAtualizado.setPrefixo(prefixo);
                            clienteAtualizado.setNumeroCarteira(NrCaretira);
                            clienteAtualizado.setNome(Nome);
                            clienteAtualizado.setLob(Lob);
                            clienteAtualizado.setTelefone(telefone);

                            Operacao operacao = getNovaOperacao();
                            try {
                                Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
                                Type clienteType = new TypeToken<ClienteLob>() {
                                }.getType();
                                String jsonOut = gson.toJson(clienteAtualizado, clienteType);
                                operacao.setJsonObj(jsonOut);
                            } catch (Exception e) {
                                System.out.println("Errro <<<<<<<<<<<<<<< " + e.getMessage() + " >>>>>>>>>>>>>>>>");
                            }
                            operacao = new OperacaoDAO().makePersistent(operacao);
                            clienteAtualizado.getOperacaoModificacao().add(operacao);
                            opSucess = new ClienteLobDao().makePersistent(clienteAtualizado) != null;

                        } else {
                            ClienteLob clienteNovo = new ClienteLob();
                            clienteNovo.setMci(Mci);
                            clienteNovo.setPrefixo(prefixo);
                            clienteNovo.setNumeroCarteira(NrCaretira);
                            clienteNovo.setNome(Nome);
                            clienteNovo.setLob(Lob);
                            clienteNovo.setTelefone(telefone);

                            Operacao operacao = getNovaOperacao();
                            try {
                                Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
                                Type clienteType = new TypeToken<ClienteLob>() {
                                }.getType();
                                String jsonOut = gson.toJson(clienteNovo, clienteType);
                                operacao.setJsonObj(jsonOut);
                            } catch (Exception e) {
                                System.out.println("Errro <<<<<<<<<<<<<<< " + e.getMessage() + " >>>>>>>>>>>>>>>>");
                            }
                            clienteNovo.setOperacaoCriacao(new OperacaoDAO().save(operacao));
                            clienteNovo.setOperacaoModificacao(new ArrayList<>());
                            clienteNovo.getOperacaoModificacao().add(new OperacaoDAO().save(operacao));
                            opSucess = new ClienteLobDao().save(clienteNovo) != null;
                        }

                    } catch (HibernateException ex) {
                        Logger.getLogger(ClienteLob.class.getName()).log(Level.SEVERE, "Erro na importação do CSV, favor faver upload do arquivo csv novamente!", ex);
                    }
                }

                FacesMessage msg = new FacesMessage("SUCESSO!", "Base de Dados importada com sucesso!");
                FacesContext.getCurrentInstance().addMessage(null, msg);
                Logger.getLogger(ClienteLob.class.getName()).log(Level.INFO, "Importação dos dados concluída!");
            } else {
                FacesMessage msg = new FacesMessage("ATENÇÃO!", "Favor fazer upload do arquivo!");
                FacesContext.getCurrentInstance().addMessage(null, msg);
                Logger.getLogger(ClienteLob.class.getName()).log(Level.INFO, "Favor fazer upload do arquivo!");
            }
        } catch (IOException ex) {
            Logger.getLogger(ClienteLob.class.getName()).log(Level.SEVERE, "Erro CSVReader: " + ex.getMessage());
        }
    }

    @Override
    public void delete() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void add() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
