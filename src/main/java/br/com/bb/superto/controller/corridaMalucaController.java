package br.com.bb.superto.controller;

import br.com.bb.superto.dao.corridaMalucaDao;
import br.com.bb.superto.model.corridaMaluca;
import java.io.Serializable;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

@ManagedBean
@ViewScoped
public class corridaMalucaController implements Serializable{
    private List<corridaMaluca> listCorrida;
    private boolean opSucesso;

    public corridaMalucaController() {
        
    }

    public List<corridaMaluca> getListCorrida() {
        System.out.print("Chegamos no list Corrida");
        listCorrida = new corridaMalucaDao().findAll();
        System.out.print("List Corrida recebeu o valor do findAll");
        System.out.print(listCorrida.toString());
        for(corridaMaluca c : listCorrida){
            System.out.print("List Corrida: " + c.getNome());
        }
        return listCorrida;
    }
    
    public void setListCorrida(List<corridaMaluca> listCorrida) {
        this.listCorrida = listCorrida;
    }
    
    public corridaMaluca getCarroAt(int id){
        for(corridaMaluca c : listCorrida){
            if(c.getId() == id){
                return c;
            }
        }
        return null;
    }

    public boolean isOpSucesso() {
        return opSucesso;
    }

    public void setOpSucesso(boolean opSucesso) {
        this.opSucesso = opSucesso;
    }
    
}

