/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.controller;

import br.com.bb.superto.dao.*;
import br.com.bb.superto.model.*;
import br.com.bb.superto.util.Util;
import org.primefaces.context.RequestContext;
import org.primefaces.model.DualListModel;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author maycon
 */
@ManagedBean(name = "groupsController")
@ViewScoped
public class GroupsController extends GenericController<Groups> implements Serializable {

    List<Funcionarios> selecionados;
    private DualListModel<Funcionarios> listaDeFuncionario;
    private List<Roles> listaDeRoles;
    private List<Roles> rolesSelecionados;
    private boolean opSucesso;

    public GroupsController() {
        opSucesso = false;
    }

    public DualListModel<Funcionarios> getListaDeFuncionario() {
        List<Funcionarios> disponiveis;
        selecionados = new ArrayList<>();
        if (selected != null) {
            disponiveis = new GroupMembersDAO().getNotExists(selected);
            selecionados.addAll(new GroupMembersDAO().getExists(selected));
        } else {
            disponiveis = new FuncionariosDao().findAll();
        }
        listaDeFuncionario = new DualListModel<>(disponiveis, selecionados);
        return listaDeFuncionario;
    }

    public void setListaDeFuncionario(DualListModel<Funcionarios> listaDeFuncionario) {
        this.listaDeFuncionario = listaDeFuncionario;
    }

    public boolean isOpSucesso() {
        return opSucesso;
    }

    public void setOpSucesso(boolean opSucesso) {
        this.opSucesso = opSucesso;
    }

    public void setListaDeRoles(List<Roles> listaDeRoles) {
        this.listaDeRoles = listaDeRoles;
    }

    public void setListaDeFuncionarioParaEdicao() {
    }

    public List<Roles> getRolesDoGrupo(List<GroupRole> groupRoles) {
        List<Roles> roles = new ArrayList<>();
        groupRoles.stream().forEach((role) -> {
            roles.add(role.getRole());
        });
        return roles;
    }

    public List<Roles> getRoles() {
        return new RolesDAO().findAll();

    }

    public List<Roles> getListDeRoles() {
        if (listaDeRoles == null || isNewValue()) {
            listaDeRoles = new RolesDAO().findAll();
            setNewValue(false);
        }
        return listaDeRoles;
    }

    public List<Roles> getRolesSelecionados() {
        if (selected != null) {
            if (rolesSelecionados == null) {
                rolesSelecionados = new ArrayList<>();
            }
            selected.getGroupRoles().stream().forEach((roles) -> {
                rolesSelecionados.add(roles.getRole());
            });
        }
        return rolesSelecionados;
    }

    public void setRolesSelecionados(List<Roles> rolesSelecionados) {
        if (rolesSelecionados == null) {
            rolesSelecionados = new ArrayList<>();
        }

        this.rolesSelecionados = rolesSelecionados;
    }

    @Override
    public void edit() {
        try {
            List<Funcionarios> mergeTwoArrays = Util.mergeTwoArrays(selecionados, listaDeFuncionario.getTarget());
            GroupMembersDAO groupMembersDAO = new GroupMembersDAO();
            mergeTwoArrays.stream().forEach((e) -> {
                groupMembersDAO.deleteMemberFromGroup(selected, e);
            });
            for (Funcionarios e : listaDeFuncionario.getTarget()) {
                new GroupMembersDAO().save(new GroupMembers(e, selected));
            }
            opSucesso = new GroupsDAO().makePersistent(selected) != null;
            setNewValue(true);
        } catch (Exception ex) {
            Logger.getLogger(GroupsController.class.getName()).log(Level.SEVERE, "Erro ao editar grupo!", ex);
        } finally {
            RequestContext.getCurrentInstance().update("scriptAlert");
            RequestContext.getCurrentInstance().execute("verificaOperacao('Grupo editado com sucesso!');");
        }
    }

    @Override
    public void delete() {
        try {
            new GroupMembersDAO().deleteMembersFromGroup(selected);
            new GroupRoleDAO().deleteRolesFromGroup(selected);
            opSucesso = new GroupsDAO().makeTransient(selected) != null;
        } catch (Exception ex) {
            Logger.getLogger(GroupsController.class.getName()).log(Level.SEVERE, "Erro ao deletar grupo!", ex);
        }

    }

    @Override
    public void add() {
        try {
            opSucesso = new GroupsDAO().save(value) != null;
            for (Funcionarios f : listaDeFuncionario.getTarget()) {
                opSucesso = new GroupMembersDAO().save(new GroupMembers(f, value)) != null;
            }
            for (Roles r : rolesSelecionados) {
                opSucesso = new GroupRoleDAO().save(new GroupRole(r, value)) != null;
            }
            rolesSelecionados = new ArrayList<>();
            clearValue();
            setNewValue(true);
        } catch (Exception ex) {
            Logger.getLogger(GroupsController.class.getName()).log(Level.SEVERE, "Erro ao salvar novo grupo!", ex);
        }
    }

    public List<Funcionarios> funcionariosBtGroup(String nomeDoGrupo) {
        return new GroupMembersDAO().getFuncionarioByGroup(new GroupsDAO().getGroupMembers(nomeDoGrupo));
    }
}
