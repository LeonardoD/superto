package br.com.bb.superto.controller;

import br.com.bb.superto.dao.ClienteProdutoDao;
import br.com.bb.superto.model.ClienteProduto;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import org.primefaces.event.ScheduleEntryMoveEvent;
import org.primefaces.event.ScheduleEntryResizeEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultScheduleEvent;
import org.primefaces.model.LazyScheduleModel;
import org.primefaces.model.ScheduleEvent;
import org.primefaces.model.ScheduleModel;

@ManagedBean
@ViewScoped

public class calendarioView implements Serializable{
    private ScheduleModel eventModel;
     
    private ScheduleModel lazyEventModel;
 
    private ScheduleEvent event = new DefaultScheduleEvent();
//    private List<Date> datasCom = new ArrayList<Date>();
    private List<ClienteProduto> itensListaDoDia;
    private Date dataEvento = new Date();

    public Date getDataEvento() {
        return dataEvento;
    }

    public void setDataEvento(Date dataEvento) {
        this.dataEvento = dataEvento;
    }
    
    public List<Date> pegaDatasVinc(int prefixo){
        // cada agencia só deve ter acesso as datas deles
        // adicionar o condicional que fará esse controle aqui
        if(prefixo == 8517){
            return new ClienteProdutoDao().datasVincSuper();
        }
        else{
            return new ClienteProdutoDao().datasVinc(prefixo);
        }
    }
    
    public List<Date> pegaDatasCanc(int prefixo){
        // cada agencia só deve ter acesso as datas deles
        // adicionar o condicional que fará esse controle aqui
        if(prefixo == 8517){
            return new ClienteProdutoDao().datasCancSuper();
        }
        else{
            return new ClienteProdutoDao().datasCanc(prefixo);
        }
    }
    
    @PostConstruct
    public void init() {         
        lazyEventModel = new LazyScheduleModel() {
             
            @Override
            public void loadEvents(Date start, Date end) {
                List<Date> datas = new ArrayList<Date>();
                List<Date> datasCanc = new ArrayList<Date>();
                int pref = new LoginController().getFuncionario().getPrefixo();
                datas.addAll(pegaDatasVinc(pref));
                datasCanc.addAll(pegaDatasCanc(pref));
 
                for(Date dt: datas)
                {
                    String parametro = new ClienteProdutoController().qtVencDia(dt);
//                    addEvent(new DefaultScheduleEvent(parametro, dt, dt));
                    addEvent(new DefaultScheduleEvent(parametro, dt, dt, "azul"));

                }
                for(Date dt: datasCanc)
                {
                    String parametro = new ClienteProdutoController().qtCancDia(dt);
//                    addEvent(new DefaultScheduleEvent(parametro, dt, dt));
                    addEvent(new DefaultScheduleEvent(parametro, dt, dt, "amarelo"));
                }
            }   
        };
    }

    public Date getInitialDate() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(calendar.get(Calendar.YEAR), Calendar.FEBRUARY, calendar.get(Calendar.DATE), 0, 0, 0);
         
        return calendar.getTime();
    }
     
    public ScheduleModel getEventModel() {
        return eventModel;
    }
     
    public ScheduleModel getLazyEventModel() {
        return lazyEventModel;
    }
 
    private Calendar today() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DATE), 0, 0, 0);
 
        return calendar;
    }

     
    public ScheduleEvent getEvent() {
        return event;
    }
 
    public void setEvent(ScheduleEvent event) {
        this.event = event;
    }
     
    public void addEvent(ActionEvent actionEvent) {
        if(event.getId() == null)
            eventModel.addEvent(event);
        else
            eventModel.updateEvent(event);
         
        event = new DefaultScheduleEvent();
    }
     
    public void onEventSelect(SelectEvent selectEvent) {
        event = (ScheduleEvent) selectEvent.getObject();
        System.out.print(event.getStartDate());
        dataEvento = event.getStartDate();
    }
     
    public void onDateSelect(SelectEvent selectEvent) {
        event = new DefaultScheduleEvent("", (Date) selectEvent.getObject(), (Date) selectEvent.getObject());
        System.out.print(event.getStartDate());
        dataEvento = event.getStartDate();
    }
     
    public void onEventMove(ScheduleEntryMoveEvent event) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Evento movido", "Day delta:" + event.getDayDelta() + ", Minute delta:" + event.getMinuteDelta());
         
        addMessage(message);
    }
     
    public void onEventResize(ScheduleEntryResizeEvent event) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Event resized", "Day delta:" + event.getDayDelta() + ", Minute delta:" + event.getMinuteDelta());
         
        addMessage(message);
    }
     
    private void addMessage(FacesMessage message) {
        FacesContext.getCurrentInstance().addMessage(null, message);
    }
    
    public List<ClienteProduto> getItensListaDoDia(){      
        List<ClienteProduto> aux1 = new ArrayList<ClienteProduto>();
        List<ClienteProduto> aux2 = new ArrayList<ClienteProduto>();
        List<ClienteProduto> retorno = new ArrayList<ClienteProduto>();
        
        int pref = new LoginController().getFuncionario().getPrefixo();
        
        
        aux1 = new ClienteProdutoDao().cpDaData(dataEvento, pref);
        aux2 = new ClienteProdutoDao().cpDaDataCanc(dataEvento, pref);
        try{
            retorno.addAll(aux1);
            retorno.addAll(aux2);
            return retorno;
        }
        catch(Exception ex){
            System.out.print("Erro ao pegar lista de eventos do dia: " + ex);
        }
        return new ArrayList<ClienteProduto>();
    }
    
//    public List<ClienteProduto> getItensListaDoDia() {
//        try
//        {
//            itensListaDoDia = new ArrayList<ClienteProduto>();
//            List <ClienteProduto> aux =  new ClienteProdutoDao().findAll();
//            try
//            {
//                System.out.print("Entramos no metodo de pegar a lista");
//                aux =  new ClienteProdutoDao().findAll();
//                System.out.print("aux recebeu todos");
//                itensListaDoDia = new ArrayList<ClienteProduto>();
//                System.out.print("Limpamos a lista do dia");
//            }
//            catch(Exception e)
//            {
//                System.out.print("----------------");
//                System.out.print("Erro:" + e);
//                System.out.print("----------------");
//            }
//            try
//            {
//                for(ClienteProduto cp : aux)
//                {
//                    System.out.print("Entramos no for dos cps");
//                    if(cp.getDatavencimento() != null)
//                    {
//                        System.out.print("Entramos no if data vencimento != null");
//                        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
//                        System.out.print("Definimos SDF"); 
//                        String data = sdf.format(cp.getDatavencimento());
//                        System.out.print("Convertemos data de vencimento: " + data);
//                        System.out.print("Valor de data evento antes da conversão:");
//                        System.out.print("Valor de data evento antes da conversão: (como string)" + this.event.getStartDate());
//                        System.out.print("Valor de data evento antes da conversão:" + this.event.getStartDate());
//                        String data2 = sdf.format(this.event.getStartDate());
//                        System.out.print("Convertemos data de evento"); 
//                        System.out.print("Data vencimento: " + data);
//                        System.out.print("Data Evento: " + data2);
//                        if(data.equals(data2))
//                        {
//                            System.out.print("Entramos no if data vencimento == dataEvento");
//                            itensListaDoDia.add(cp);
//                            System.out.print("adicionamos cp na lista");  
//                        }
//                    }
//                    else
//                    {
//                        System.out.print("Entramos no IF DATA vencimento == null");
//                        if(cp.getDatacancelamento().equals(this.event.getStartDate()))
//                        {
//                            System.out.print("Entramos no IF DATA cancelamento == data do evento");   
//                            itensListaDoDia.add(cp);
//                            System.out.print("adicionamos cp na lista");   
//                        }
//                    }
//                }
//                System.out.print("Fim do For!!! Proximo passo == retorno"); 
//            }
//            catch(Exception ex)
//            {
//                System.out.print("----------------");
//                System.out.print("Erro:" + ex);
//                System.out.print("----------------");
//            }
//            
//        }
//        catch(Exception ex)
//        {
//            System.out.print("----------------");
//            System.out.print("Erro:" + ex);
//            System.out.print("----------------");
//        }
//        finally
//        {
//            return itensListaDoDia;
//        }
//    }
}
