/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.controller;

import br.com.bb.superto.ausencias.Ausencia;
import br.com.bb.superto.ausencias.AusenciaDAO;
import br.com.bb.superto.ausencias.StatusAusencia;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import br.com.bb.superto.ausencias.listaMotivosAusencia;
import br.com.bb.superto.dao.OperacaoDAO;
import br.com.bb.superto.model.Operacao;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author Leonardo
 */
@ManagedBean(name = "ausenciaController")
@ViewScoped
public class AusenciaController extends GenericController<Ausencia> implements Serializable{
    private boolean opSucess;
    private List<Ausencia> ausenciasPrefixo;
    
    public List<String> getBrands() {
        return listaMotivosAusencia.getBrands();
    }

    public boolean isOpSucess() {
        return opSucess;
    }

    public void setOpSucess(boolean opSucess) {
        this.opSucess = opSucess;
    }

    public List<Ausencia> getAusenciasPrefixo() {
        ausenciasPrefixo = new AusenciaDAO().getAusenciasPrefixo(value.getPrefixoSolicitante());
        System.out.println("Get pedidos de ausencias para o prefixo: " + value.getPrefixoSolicitante());
        return ausenciasPrefixo;
    }

    public void setAusenciasPrefixo(List<Ausencia> ausenciasPrefixo) {
        this.ausenciasPrefixo = ausenciasPrefixo;
    }
    
    public String verificarTextColor(StatusAusencia ausencia) {
        if (ausencia == StatusAusencia.PENDENTE) {
            return "none";
        } else if (ausencia == StatusAusencia.AUTORIZADO) {
            return "#297b2c";
        } else if (ausencia == StatusAusencia.NEGADO) {
            return "#8c2c37";
        } 
        return "none";
    }
    
    public String verificarBackGroundColor(StatusAusencia ausencia) {
        if (ausencia == StatusAusencia.PENDENTE) {
            return "none";
        } else if (ausencia == StatusAusencia.AUTORIZADO) {
            return "#c6efcd";
        } else if (ausencia == StatusAusencia.NEGADO) {
            return "#ffc8ce";
        } 
        return "none";
    }
    
    /**
     *
     */
    @Override
    public void add() {
        System.out.println("Adicionando:" + value);
        value.setStatusAusencia(StatusAusencia.PENDENTE);
        Operacao operacao = getNovaOperacao();
        try {
            Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
            Type ausenciaType = new TypeToken<Ausencia>() {
            }.getType();
            String jsonOut = gson.toJson(value, ausenciaType);
            operacao.setJsonObj(jsonOut);
        } catch (Exception e) {
            System.out.println("Errro <<<<<<<<<<<<<<< " + e.getMessage() + " >>>>>>>>>>>>>>>>");
        }
        value.setDataMudanca(new Date());
        value.setOperacaoCriacao(new OperacaoDAO().save(operacao));
        value.setOperacaoModificacao(new ArrayList<>());
        value.setNumOperacao(operacao.getID() + "");
        value.getOperacaoModificacao().add(new OperacaoDAO().save(operacao));
        opSucess = new AusenciaDAO().save(value) != null;
        System.out.println("Adicionando:" + value);
        clearValue();
    }
    
}
