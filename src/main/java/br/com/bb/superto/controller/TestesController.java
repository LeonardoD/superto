/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.controller;

import br.com.bb.superto.dao.FuncionariosDao;
import br.com.bb.superto.dao.OrganogramaDao;
import br.com.bb.superto.model.Organograma;
import br.com.bb.superto.model.OrganogramaList;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.component.html.HtmlOutputText;
import javax.faces.component.html.HtmlPanelGroup;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author maycon
 */
@ManagedBean(name = "testeController")
@ViewScoped
public class TestesController {

    String filterValue;
    String filter;
    OrganogramaDao dao = new OrganogramaDao();

    public TestesController() {
    }

    public String getFilterValue() {
        return filterValue;
    }

    public void setFilterValue(String filterValue) {
        this.filterValue = filterValue;
    }

    public String getFilter() {
        return filter;
    }

    public void setFilter(String filter) {
        this.filter = filter;
    }

    public OrganogramaList getT() {
        OrganogramaList k = new OrganogramaList();
        k.setFunc(new FuncionariosDao().getByFmatricula("F0090310"));
        k.setChildrens(new ArrayList<>());
        k.getChildrens().add(new OrganogramaList(new FuncionariosDao().getByFmatricula("F0093678")));
        k.getChildrens().add(new OrganogramaList(new FuncionariosDao().getByFmatricula("F0134229")));
        OrganogramaList organogramaList = new OrganogramaList(new FuncionariosDao().getByFmatricula("F0134229"));
        organogramaList.setChildrens(new ArrayList<>());
        organogramaList.getChildrens().add(new OrganogramaList(new FuncionariosDao().getByFmatricula("F0169382")));
        k.getChildrens().add(organogramaList);
        k.getChildrens().add(new OrganogramaList(new FuncionariosDao().getByFmatricula("F0169382")));
        return k;
    }

    public javax.faces.component.html.HtmlPanelGroup getPanel() {
        javax.faces.component.html.HtmlPanelGroup p = new HtmlPanelGroup();
        List<Organograma> list = dao.getList();
        if (list != null && !list.isEmpty()) {
            List<Organograma> childrens = dao.getChildrens(list.get(0));
            javax.faces.component.html.HtmlOutputText g = new HtmlOutputText();
            g.setEscape(false);
            String image;
            if (list.get(0).getFunc().getAvatar() != null) {
                image = "<img src=\"/ws/file/img/" + list.get(0).getFunc().getAvatar().getImagem().getUrl().replace("/archives/", "") + "\" height=\"100\" width=\"100\"><br/><span style=\"font-size: 12px;\">" + list.get(0).getFunc().getNome() + "</span>"
                        + "<br/><span style=\"font-size: 10px;\">" + list.get(0).getFunc().getFuncao() + "</span></a>";
            } else {
                image = "<img src=\"http://superto.bb.com.br/resources/templates/resources/images/comment_avatar.png\" height=\"100\" width=\"100\"> <br/><span style=\"font-size: 12px;\">" + list.get(0).getFunc().getNome() + "</span>"
                        + "<br/><span style=\"font-size: 10px;\">" + list.get(0).getFunc().getFuncao() + "</span></a>";
            }
            String rt = "<ul id=\"primaryNav\" class=\"col5\" align = \"center\">"
                    + "<li id=\"home\"><a href=\"/funcionario/" + list.get(0).getFunc().getFmatricula() + "\">" + image + "<br/></a></li>";

            rt += setPanel(new String(), dao.getChildrens(list.get(0)).iterator());
            rt += "</ul>";
            System.out.println("valor " + rt);
            g.setValue(rt);
            p.getChildren().add(g);
        } else {
            javax.faces.component.html.HtmlOutputText g = new HtmlOutputText();
            g.setValue("Organograma não definido.");
            p.getChildren().add(g);
        }
        return p;
    }

    public String setPanel(String p, Iterator r) {
        while (r.hasNext()) {
            Organograma next = (Organograma) r.next();
            String linkImage = (next.getFunc().getAvatar() != null) ? "/ws/file/img/" + next.getFunc().getAvatar().getImagem().getUrl().replace("/archives/", "") : "/resources/templates/resources/images/comment_avatar.png";
            p += "<li><a href=\"/funcionario/" + next.getFunc().getFmatricula() + "\"><img src=\"" + linkImage + "\" height=\"100\" width=\"100\"/>"
                    + "<br/><span style=\"font-size: 12px;\">" + next.getFunc().getNome() + "</span>"
                    + "<br/><span style=\"font-size: 10px;\">" + next.getFunc().getFuncao() + "</span></a>";
            List<Organograma> childrens = dao.getChildrens(next);
            if (childrens != null && !childrens.isEmpty()) {
                p += "<ul>";
                p += setPanel(new String(), childrens.iterator());
                p += "</ul>";
            }
            p += "</li>";
        }
        return p;
    }
}
