/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.controller;

import br.com.bb.superto.dao.AppModulesDAO;
import br.com.bb.superto.dao.RolesDAO;
import br.com.bb.superto.dao.RolesPermissionDAO;
import br.com.bb.superto.model.AppModules;
import br.com.bb.superto.model.Roles;
import br.com.bb.superto.model.RolesPermissions;
import org.hibernate.HibernateException;
import org.hibernate.exception.ConstraintViolationException;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.util.ArrayList;
import java.util.List;

/**
 * @author maycon
 */
@ManagedBean(name = "rolesController")
@ViewScoped
public class RolesController extends GenericController<Roles> implements java.io.Serializable {

    List<String> permissoes;
    List<CheckBoxModulo> modulos;
    List<Roles> listaDeRoles;
    List<CheckBoxModulo> modulosSelect;

    public RolesController() {
        this.permissoes = new ArrayList<String>();
    }

    public List<CheckBoxModulo> getModulos() {
        if (modulos == null) {
            modulos = new ArrayList<CheckBoxModulo>();
            for (AppModules t : new AppModulesDAO().findAll()) {
                modulos.add(new CheckBoxModulo(t.getName()));
            }
        }
        return modulos;
    }

    public List<CheckBoxModulo> getModulosSelect() {
        if (selected != null) {
            modulosSelect = new ArrayList<CheckBoxModulo>();
            System.out.println("Pegando modulos");
            List<AppModules> findAll = new AppModulesDAO().findAll();
            for (AppModules t : findAll) {
                System.out.println("Modulo : " + t.getName());
                CheckBoxModulo checkBoxModulo = new CheckBoxModulo(t.getName());
                List<RolesPermissions> rolesByFuncionario = new RolesPermissionDAO().getRolesByFuncionario(selected);
                for (RolesPermissions p : rolesByFuncionario) {
                    System.out.println("Role : " + p.getPermission());
                    if (p.getPermission().contains(t.getName()) && !checkBoxModulo.getPermissoes().contains(t.getName())) {
                        checkBoxModulo.getPermissoes().add(p.getPermission());
                    }
                }
                modulosSelect.add(checkBoxModulo);
            }
            System.out.println("Fim do modulos");
            return modulosSelect;
        }
        System.out.println("Fim do modulos2");
        return new ArrayList<CheckBoxModulo>();
    }

    public List<Roles> getListDeRoles() {
        if (listaDeRoles == null || newValue) {
            listaDeRoles = new RolesDAO().findAll();
            setNewValue(false);
        }
        return listaDeRoles;
    }

    public List<RolesPermissions> getPermissonOfSelectedRole() {
        return new RolesPermissionDAO().getRolesByFuncionario(selected);
    }

    public List<String> getPermissoes() {
        return permissoes;
    }

    public void setPermissoes(List<String> permissoes) {
        this.permissoes = permissoes;
    }

    @Override
    public void edit() {
        List<String> errors = new ArrayList<String>();
        try {
            if (selected == null) {
                throw new NullPointerException();
            }
            //selected = new RolesDAO().makePersistent(selected);
            List<RolesPermissions> rolesByFuncionario = new RolesPermissionDAO().getRolesByFuncionario(selected);
            List<RolesPermissions> forRemove = new ArrayList<RolesPermissions>();
            List<RolesPermissions> forAdd = new ArrayList<RolesPermissions>();

            for (CheckBoxModulo modulo : modulosSelect) {
                for (String p : modulo.getPermissoes()) {
                    RolesPermissions t = new RolesPermissions(p, selected);
                    if (!rolesByFuncionario.contains(t)) {
                        forAdd.add(t);
                    }
                }
                for (RolesPermissions r : rolesByFuncionario) {
                    System.out.println("Testando a permissao de valor: " + r.getPermission());
                    if (r.getPermission().contains(modulo.getModulo()) && !modulo.getPermissoes().contains(r.getPermission())) {
                        forRemove.add(r);
                    }
                }
            }

            for (RolesPermissions e : forAdd) {
                new RolesPermissionDAO().save(e);

            }
            for (RolesPermissions e : forRemove) {
                new RolesPermissionDAO().makeTransient(e);
            }

            addFacesMsg("Role '" + selected.getRoleName() + "' editado com sucesso.", FacesMessage.SEVERITY_INFO);
            newValue = true;
        } catch (NullPointerException ex) {
            errors.add("Selecione um modulo.");
        } finally {
            for (String e : errors) {
                addFacesMsg(e, FacesMessage.SEVERITY_ERROR);
            }
        }
    }

    @Override
    public void delete() {
        List<String> errors = new ArrayList<String>();
        try {
            if (selected == null) {
                throw new NullPointerException();
            }
            RolesDAO roleDao = new RolesDAO();
            roleDao.flush();
            selected = roleDao.makeTransient(selected);
            selected = null;
            newValue = true;
        } catch (NullPointerException ex) {
            errors.add("Role não selecionado.");
        } catch (ConstraintViolationException ex) {
            errors.add("Não foi possivel deletar o role selecionado.");
        } finally {
            for (String e : errors) {
                addFacesMsg(e, FacesMessage.SEVERITY_ERROR);
            }
        }
    }

    @Override
    public void add() {
        List<String> errors = new ArrayList<String>();
        try {
            value = new RolesDAO().save(value);
            addFacesMsg("Role '" + value.getRoleName() + "' salvo com sucesso.", FacesMessage.SEVERITY_INFO);
            System.out.println("Salvando permissões");
            for (CheckBoxModulo c : modulos) {
                for (String p : c.getPermissoes()) {
                    new RolesPermissionDAO().save(new RolesPermissions(p, value));
                }
            }
            System.out.println("Fim permissões");
            value = new Roles();
            permissoes = new ArrayList<String>();
            setNewValue(true);
        } catch (NullPointerException ex) {
            errors.add("Error ao adicionar a Role.");
        } catch (HibernateException e) {
            if (e instanceof ConstraintViolationException) {
                errors.add("Error ao adicionar, a role já extiste.");
            }
        } finally {
            for (String e : errors) {
                addFacesMsg(e, FacesMessage.SEVERITY_ERROR);
            }
        }
    }
}
