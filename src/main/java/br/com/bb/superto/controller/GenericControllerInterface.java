package br.com.bb.superto.controller;

import javax.faces.application.FacesMessage;
import java.io.Serializable;

/**
 * @author Maycon
 */
public interface GenericControllerInterface extends Serializable {

    public void edit();

    public void delete();

    public void add();

    public void onSelectListener();

    public void addFacesMsg(String msg, FacesMessage.Severity level);
}
