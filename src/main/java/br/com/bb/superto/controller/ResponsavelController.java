/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.controller;

import br.com.bb.superto.dao.ResponsavelDAO;
import br.com.bb.superto.model.Funcionarios;
import br.com.bb.superto.model.Responsavel;
import br.com.bb.superto.model.SubCategoria;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 * @author Administrador
 */
@ManagedBean(name = "responsavelController")
@ViewScoped
public class ResponsavelController extends GenericController<Responsavel> implements java.io.Serializable {

    public ResponsavelController() {

    }

    public Funcionarios getFuncionario(SubCategoria sub) {
        if (value != null) {
            if (new ResponsavelDAO().getResponsalveBySubCat(sub) != null) {
                return new ResponsavelDAO().getResponsalveBySubCat(sub).getFuncionarioResponsavel();
            }

        }
        return null;
    }

    @Override
    public void edit() {

        if (value != null) {
            if (new ResponsavelDAO().getResponsalveBySubCat(value.getSubcategoria()) != null) {
                new ResponsavelDAO().update(new Responsavel(value.getFuncionarioResponsavel(), value.getSubcategoria()));
            } else {
                add();
            }

        }
        addFacesMsg("Galeria '" + value.getFuncionarioResponsavel().getNome() + "' Responsavel pelo Setor " + value.getSubcategoria().asString(), FacesMessage.SEVERITY_INFO);
    }

    @Override
    public void delete() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void add() {
        new ResponsavelDAO().save(new Responsavel(value.getFuncionarioResponsavel(), value.getSubcategoria()));
    }

}
