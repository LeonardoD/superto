/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.controller;

import br.com.bb.superto.dao.AppModulesDAO;
import br.com.bb.superto.model.AppModules;
import br.com.bb.superto.util.Util;
import org.hibernate.HibernateException;
import org.hibernate.exception.ConstraintViolationException;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.util.ArrayList;
import java.util.List;

/**
 * @author maycon
 */
@ManagedBean(name = "appModulesController")
@ViewScoped
public class AppModulesController extends GenericController<AppModules> implements java.io.Serializable {

    List<AppModules> listaDeModulos;

    public AppModulesController() {
    }

    public List<AppModules> getListaDeModulos() {
        if (listaDeModulos == null || newValue) {
            listaDeModulos = new AppModulesDAO().findAll();
            newValue = false;
        }
        return listaDeModulos;
    }

    public void setListaDeModulos(List<AppModules> listaDeModulos) {
        this.listaDeModulos = listaDeModulos;
    }

    @Override
    public void edit() {
        List<String> errors = new ArrayList<String>();
        try {
            if (selected == null) {
                throw new NullPointerException();
            }
            selected = new AppModulesDAO().makePersistent(selected);
            addFacesMsg("Modulo '" + selected.getName() + "' editado com sucesso.", FacesMessage.SEVERITY_INFO);
            newValue = true;
        } catch (NullPointerException ex) {
            errors.add("Selecione um modulo.");
        } finally {
            for (String e : errors) {
                addFacesMsg(e, FacesMessage.SEVERITY_ERROR);
            }
        }
    }

    @Override
    public void delete() {
        List<String> errors = new ArrayList<String>();
        try {
            if (selected == null) {
                throw new NullPointerException();
            }
            selected = new AppModulesDAO().makeTransient(selected);
            selected = null;
            newValue = true;
        } catch (NullPointerException ex) {
            errors.add("Modulo não selecionado.");
        } catch (ConstraintViolationException ex) {
            errors.add("Não foi possivel deletar modulo selecionado.");
        } finally {
            for (String e : errors) {
                addFacesMsg(e, FacesMessage.SEVERITY_ERROR);
            }
        }
    }

    @Override
    public void add() {
        List<String> errors = new ArrayList<String>();
        try {
            value.setName(Util.removerAcentos(value.getName().toLowerCase()));
            value = new AppModulesDAO().save(value);
            addFacesMsg("Modulo '" + value.getName() + "' salvo com sucesso.", FacesMessage.SEVERITY_INFO);
            value = new AppModules();
            setNewValue(true);
        } catch (NullPointerException ex) {
            errors.add("Error ao adicionar o modulo.");
        } catch (HibernateException e) {
            if (e instanceof ConstraintViolationException) {
                errors.add("Error ao adicionar, o modulo já extiste.");
            }
        } finally {
            for (String e : errors) {
                addFacesMsg(e, FacesMessage.SEVERITY_ERROR);
            }
        }

    }
}
