package br.com.bb.superto.controller;

import br.com.bb.superto.dao.UserHistoryDao;
import br.com.bb.superto.model.Funcionarios;
import br.com.bb.superto.model.UserHistory;
import br.com.bb.superto.model.UserNotLogged;
import br.com.bb.superto.security.LdapConnection;
import br.com.bb.superto.security.UserSecurityUtils;
//import br.com.bb.superto.dao.HibernateUtil;
import br.com.bb.superto.util.Util;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.UsernamePasswordToken;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.servlet.ServletException;
import javax.validation.constraints.Size;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;

/**
 * @author maycon
 */
@RequestScoped
@Named("loginController")
public class LoginController implements java.io.Serializable {

    @Size(min = 8, max = 8, message = "Chave preenchida incorretamente")
    String name;
    @Size(min = 8, max = 8, message = "Senha preenchida incorretamente")
    String password;

    public LoginController() {
        this.name = "";
        this.password = "";
    }

    public Funcionarios getFuncionario() {
        try {
            return UserSecurityUtils.getUserFuncionario().getFuncionario();
        } catch (UserNotLogged ex) {
            Logger.getLogger(LoginController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String login() throws ServletException {
        LdapConnection conn = new LdapConnection();
        if (!name.toLowerCase().startsWith("c") && !name.equalsIgnoreCase("t1054607") && !name.toLowerCase().startsWith("t") && !conn.connect(name, password) && !name.toLowerCase().startsWith("a") && !name.toLowerCase().startsWith("e")){
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Matricula ou senha incorreto.", null);
            FacesContext.getCurrentInstance().addMessage(null, msg);
        } 
        else {
            System.out.println("Conectado: " + name); //Inserindo para verificar melhor sobre possiveis erros (Logs)
            if (!SecurityUtils.getSubject().isAuthenticated()) {
                UsernamePasswordToken userToken = new UsernamePasswordToken(name, "naoalterar");
                try {
                    SecurityUtils.getSubject().login(userToken);
                    if (SecurityUtils.getSubject().hasRole("funcionario")) {
                        UserHistory userHistory = new UserHistory();
                        userHistory.setLoginDate(Util.getDate());
                        userHistory.setUser(UserSecurityUtils.getUserFuncionario().getFuncionario());
                        UserSecurityUtils.getUserFuncionario().setUserHistory(new UserHistoryDao().save(userHistory));
                    }
                } catch (UserNotLogged ex) {
                    Logger.getLogger(LoginController.class.getName()).log(Level.SEVERE, null, ex);
                } catch (org.apache.shiro.authc.IncorrectCredentialsException exc) {
                    FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("ErrorLogin", exc);
                } catch (AuthenticationException e) {
                    FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("ErrorLogin", e);
                }
            }
        }
        return "pretty:super";
    }
    
/*   public String loginRedirect(String url) throws ServletException {
        System.out.print("***********************************");
       System.out.print("***********************************");
        System.out.print("***********************************");
        System.out.print("***********************************");
        System.out.print("***********************************");
        System.out.print("***********************************");
        System.out.print("***********************************");
        System.out.print("***********************************");
        System.out.print("***********************************");
        System.out.print("Url do parâmetro é = " + url);
        System.out.print("***********************************");
        System.out.print("***********************************");
        System.out.print("***********************************");
        System.out.print("***********************************");
        System.out.print("***********************************");
        System.out.print("***********************************");
        System.out.print("***********************************");
        LdapConnection conn = new LdapConnection();
        if (!name.toLowerCase().startsWith("c") && !name.equalsIgnoreCase("t1054607")
                && !name.toLowerCase().startsWith("t") && !conn.connect(name, password) && !name.toLowerCase().startsWith("a") && !name.toLowerCase().startsWith("e")) {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Matricula ou senha incorreto.", null);
            FacesContext.getCurrentInstance().addMessage(null, msg);
        } else {
            System.out.println("Conectado!");
            if (!SecurityUtils.getSubject().isAuthenticated()) {
                UsernamePasswordToken userToken = new UsernamePasswordToken(name, "naoalterar");
                try {
                    SecurityUtils.getSubject().login(userToken);
                    if (SecurityUtils.getSubject().hasRole("funcionario")) {
                        UserHistory userHistory = new UserHistory();
                        userHistory.setLoginDate(Util.getDate());
                        userHistory.setUser(UserSecurityUtils.getUserFuncionario().getFuncionario());
                        UserSecurityUtils.getUserFuncionario().setUserHistory(new UserHistoryDao().save(userHistory));
                    } else {
                            // não entendi o porque deste ELSE
                    }
                } catch (UserNotLogged ex) {
                    Logger.getLogger(LoginController.class.getName()).log(Level.SEVERE, null, ex);
                } catch (org.apache.shiro.authc.IncorrectCredentialsException exc) {
                   FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("ErrorLogin", exc);
               } catch (AuthenticationException e) {
                    FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("ErrorLogin", e);
                }
            }
        }
        
        try{
            System.out.print("Entramos no try");
            if(url.isEmpty() || url.equalsIgnoreCase("") || url.equals(null) || url == null){
                System.out.print("Entramos no if url é vazia");
                return "/";
           }
           else{
                System.out.print("URL não é vazia");
                return url;
            }
        }
        catch(Exception ex){
            System.out.print("Erro: " + ex);
            return "/";
        }

   }*/

    public String logout() throws ServletException, IOException {
        if (SecurityUtils.getSubject().isAuthenticated()) {
            try {
                UserHistory userHistory = UserSecurityUtils.getUserFuncionario().getUserHistory();
                userHistory.setLogoutDate(Util.getDate());
                new UserHistoryDao().makePersistent(userHistory);
                SecurityUtils.getSubject().logout();
            } catch (UserNotLogged ex) {
                Logger.getLogger(LoginController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return "/index.jsf";
    }
}
