/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.controller;

import br.com.bb.superto.dao.VideosDao;
import br.com.bb.superto.model.Archive;
import br.com.bb.superto.model.UserNotLogged;
import br.com.bb.superto.model.Videos;
import br.com.bb.superto.util.Pagination;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author maycon
 */
@ManagedBean(name = "videoController")
@ViewScoped
public class VideoController extends GenericController<Videos> implements java.io.Serializable {

    private UploadedFile file;
    private Pagination videosPaginacao;
    private boolean opSucess;

    public VideoController() {
        opSucess = false;
    }

    public void fileUploadListener(FileUploadEvent event) {
        file = event.getFile();
        value.setTitle(file.getFileName().substring(0, file.getFileName().lastIndexOf(".")));
    }

    @Override
    public void edit() {
        try {
            if (selected == null) {
                throw new NullPointerException();
            }
            opSucess = new VideosDao().makePersistent(selected) != null;
        } catch (Exception ex) {
            Logger.getLogger(VideoController.class.getName()).log(Level.SEVERE, "Erro ao editar Video!", ex);
        }
    }

    @Override
    public void delete() {
        try {
            if (selected == null) {
                throw new NullPointerException();
            }
            opSucess = new VideosDao().makeTransient(selected) != null;
            selected = null;
        } catch (Exception ex) {
            Logger.getLogger(VideoController.class.getName()).log(Level.SEVERE, "Erro ao deletar Video!", ex);
        }
    }

    @Override
    public void add() {
        Archive archive;
        try {
            archive = new ArchiveController().add(file);
            value.setVideo(archive);
            value = new VideosDao().save(value);
        } catch (UserNotLogged | IOException ex) {
            Logger.getLogger(VideoController.class.getName()).log(Level.SEVERE, "Erro ao salvar Video!", ex);
        }
    }

    public Pagination getVideosPaginacao() {
        if (videosPaginacao == null) {
            videosPaginacao = new Pagination(10) {
                @Override
                public List<Videos> getPageDataModel() {
                    return new VideosDao().getVideos(getPageFirstItem(), getPageSize(), false, null);
                }
            };
            videosPaginacao.setRowCount(new VideosDao().getRowCount());
        }
        return videosPaginacao;
    }

    public void setVideosPaginacao(Pagination videosPaginacao) {
        this.videosPaginacao = videosPaginacao;
    }

    public List<Videos> ultimosVideos(Integer first, Integer max) {
        return new VideosDao().getVideos(first, max, false, null);
    }

    public String videoUrl(Videos video) {
        if (video != null) {
            String newUrl = video.getVideo().getUrl().replaceAll("\\\\", "/");
            return newUrl;
        }
        System.out.println("this videos is null.");
        return "null";
    }
}
