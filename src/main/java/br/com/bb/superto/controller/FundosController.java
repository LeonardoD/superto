package br.com.bb.superto.controller;

import br.com.bb.superto.dao.FundosDao;
import br.com.bb.superto.model.Fundos;
import com.csvreader.CsvReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.validation.ConstraintViolationException;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

/**
 *
 * @author Jheemeson S. Mota
 */

@ManagedBean(name = "fundosController")
@ViewScoped
public class FundosController extends GenericController<Fundos> implements java.io.Serializable {

    private boolean opSucesso;
    private List<Fundos> listaDeFundosFilter;
    private List<Fundos> listaFiltrada;
    private List<Fundos> lista;
    private List<String> todos;
    private String escolhido;
    private List<String> segmentos;
    private UploadedFile arquivosSubidos;
    private boolean listaRenderizada;
    private double valor;
    private double tarifa;
    private int codigo;
    private String segmento;
    private String parametro;
    
    public FundosController() {
        opSucesso = false;
    }
    
    public boolean isOpSucesso() {
        return opSucesso;
    }

    public void setOpSucesso(boolean opSucesso) {
        this.opSucesso = opSucesso;
    }

    public List<Fundos> getListaDeFundosFilter() {
        try{
            return listaDeFundosFilter;
        }
        catch(Exception ex){
            System.out.print("Erro ao retornar lista de fundos filter: " + ex);
            return new ArrayList<Fundos>();
        }
    }

    public void setListaDeFundosFilter(List<Fundos> listaDeFundosFilter) {
        try{
            this.listaDeFundosFilter = listaDeFundosFilter;
            this.listaFiltrada = listaDeFundosFilter;
        }
        catch(Exception ex){
            System.out.print("Erro ao setar lista filtrada " + ex);
        }
    }

    public List<Fundos> getListaFiltrada() {
        listaFiltrada = new FundosDao().findTodos();
        return listaFiltrada;
    }

    public void setListaFiltrada(List<Fundos> listaFiltrada) {
        this.listaFiltrada = listaFiltrada;
    }

    public boolean isListaRenderizada() {
        return listaRenderizada;
    }

    public void setListaRenderizada(boolean listaRenderizada) {
        this.listaRenderizada = listaRenderizada;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public String getSegmento() {
        System.out.print("getSegmento: " + segmento);
        return segmento;
    }

    public void setSegmento(String segmento) {
        System.out.print("setSegmento: " + segmento);
        this.segmento = segmento;
    }

    public List<String> getSegmentos() {
        segmentos = new FundosDao().findSegmentos();
        return segmentos;
    }

    public void setSegmentos(List<String> segmentos) {
        this.segmentos = segmentos;
    }

    public String getParametro() {
        System.out.print("getParametro: " + parametro);
        return parametro;
    }

    public void setParametro(String parametro) {
        System.out.print("setParametro: " + parametro);
        this.parametro = parametro;
    }
    
    public void GeraLista(){
        try{
            this.setLista(new ArrayList<Fundos>());
            System.out.print("Entramos no Gera Lista");
            System.out.print("Aplicação: " + this.parametro);
            System.out.print("Segmento: " + segmento);
            System.out.print("Valor: " + valor);

            System.out.print("Entramos no parametro subsequente --- prox passo = setar lista");
            System.out.print("prox passo = setar lista: " + new FundosDao().findFiltradosSubsequente(segmento, valor).toString());
            this.setLista(new FundosDao().findFiltradosSubsequente(segmento, valor));
        }
        catch(Exception ex){
            System.out.print("Erro: " + ex);
        }
    }

    public List<Fundos> getLista() {
        try{
            return lista;
        }
        catch(Exception ex){
            System.out.print("Erro: " + ex);
            return new ArrayList<Fundos>();
        }
    }

    public void setLista(List<Fundos> lista) {
        this.lista = lista;
    }

    public int getCodigo() {
        try{
            return this.selected.getId();
        }
        catch(Exception ex){
            System.out.print("Erro: " + ex);
            return 0;
        }
    }

    public void setCodigo(int codigo) {
        try{
            this.selected = new FundosDao().findById(codigo, true);
            escolhido = selected.getNomefundo();
            this.codigo = codigo;
        }
        catch(Exception ex){
            System.out.print("Erro: " + ex);
        }
    }
    
    @Override
    public void edit() {
        try {
            System.out.println("<<<<<<<<<<<< Antes da operação: "+selected.getNomefundo() +">>>>>>>>>>>>>>>");
            if (selected != null) {                
                opSucesso = new FundosDao().makePersistent(selected) != null;
                System.out.print("Sucesso: " + opSucesso);
            }
            
        } catch (Exception ex) {
            System.out.println("Erro ao editar objeto de Fundos: " + ex.getMessage());
        }
    }

    public String getEscolhido() {
        return escolhido;
    }

    public void setEscolhido(String escolhido) {
        try{
            Fundos novo = new FundosDao().findByNome(escolhido);
            this.codigo = novo.getId();
            this.selected = novo;
            this.escolhido = escolhido;
        }
        catch(Exception ex){
            System.out.print("Erro: " + ex);
            this.escolhido = null;
        }
    }
    
    @Override
    public void delete() {
        try {
            if (selected == null) {
                throw new NullPointerException();
            }
            opSucesso = new FundosDao().makeTransient(selected) != null;
            opSucesso = true;
            selected = null;
        } catch (NullPointerException | ConstraintViolationException ex) {
            System.out.println("Erro ao deletar objeto de Fundos: " + ex.getMessage());
        }
    }
    
    @Override
    public void add() {
    }

    public UploadedFile getArquivosSubidos() {
        return arquivosSubidos;
    }

    public void setArquivosSubidos(UploadedFile arquivosSubidos) {
        this.arquivosSubidos = arquivosSubidos;
    }
    
    public void fileUploadListener(FileUploadEvent event) {
        arquivosSubidos = event.getFile();
    }

    public List<String> getTodos() {
        todos = new FundosDao().findFundos();
        return todos;
    }

    public void setTodos(List<String> todos) {
        this.todos = todos;
    }

    public double getTarifa() {
        System.out.print("Entramos no getTarifa");
        System.out.print("Entramos no getTarifa");
        System.out.print("Entramos no getTarifa");
        System.out.print("Entramos no getTarifa");
        try{
            System.out.print("Entramos no try");
            System.out.print("taxa: " + selected.getTaxaadm());
            System.out.print("valor: " + this.valor);
            tarifa = ((selected.getTaxaadm()*this.valor)/100)/12;
            return tarifa;        
        }
        catch(Exception ex){
            System.out.print("Erro: " + ex);
            return 0;
        }
    }

    public void setTarifa(double tarifa) {
        this.tarifa = tarifa;
    }
    
    public void subirFundos() throws ParseException {
        try {
            CsvReader CSV = new CsvReader(arquivosSubidos.getInputstream(), Charset.forName("UTF-8"));
            CSV.setDelimiter(';');
            CSV.readHeaders();

            while (CSV.readRecord()) {

                String idFundo = CSV.get(0);
                int id = 0;
                try{
                    id = Integer.parseInt(idFundo);
                }
                catch(Exception ex){
                    System.out.print("Erro ao converter idFundo para inteiro: " + ex);
                }
                String nomeFundo = CSV.get(1);
                Fundos fundo = new FundosDao().findById(id, true);
                
                if (fundo != null) {
                    fundo.setNomefundo(nomeFundo);
                    opSucesso = new FundosDao().makePersistent(fundo) != null;
                } else if(id != 0){
                    Fundos f = new Fundos();
                    f.setId(id);
                    f.setNomefundo(nomeFundo);
                    f.setSituacao(true);
                    opSucesso = new FundosDao().save(f) != null;
                }
            }
            CSV.close();

            addFacesMsg("Fundos atualizados com sucesso.", FacesMessage.SEVERITY_INFO);
        } catch (IOException ex) {
            Logger.getLogger(FuncionariosController2.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void subirDetalhes() throws ParseException {
        try {
            CsvReader CSV = new CsvReader(arquivosSubidos.getInputstream(), Charset.forName("UTF-8"));
            CSV.setDelimiter(';');
            CSV.readHeaders();
            while (CSV.readRecord()) {
                System.out.print("iteração do while");
                String idFundo = CSV.get(0);
                System.out.print("id: " + idFundo);
                String tipo = CSV.get(2);
                System.out.print("tipo: " + tipo);
                boolean situacao = CSV.get(3).equalsIgnoreCase("Ativo");
                System.out.print("Situação: " + situacao);
                String familia = CSV.get(4);
                String segmento= CSV.get(5);
                if(segmento.contains("PF")){
                        segmento = segmento.replace("PF", "Pessoa Fisica");
                    }
                    if(segmento.contains("P F")){
                        segmento = segmento.replace("P F", "Pessoa Fisica");
                    }
                    if(segmento.contains("PJ")){
                        segmento = segmento.replace("PJ", "Pessoa Juridica");
                    }
                    
                    if(segmento.contains("P J")){
                        segmento = segmento.replace("P J", "Pessoa Juridica");
                    }
                    if(segmento.equalsIgnoreCase("Governo")){
                        System.out.print("Igual a Governo");
                        int identifier = Integer.parseInt(idFundo);
                        try{
                            Fundos fundo = new FundosDao().findById(identifier, true);
                            new FundosDao().makeTransient(fundo);
                            idFundo = "0";
                        }
                        catch(Exception ex){
                            System.out.print("Erro ao excluir fundo: " + ex);
                        }
                        situacao = false;
                    }
                    if(segmento.contains("Governo")){
                        System.out.print("Contem governo");
                        segmento = segmento.replace("/ Governo", "");
                        segmento = segmento.replace("Governo /", "");
                        System.out.print("Segmento no final do condicional: " + segmento);
                    }
                System.out.print("Segmento: " + segmento);
                String nivelRel = CSV.get(6);
                String taxaAdministracao = CSV.get(7);
                String valorCota = CSV.get(8);
                String PatrimonioLiquido = CSV.get(9);
                String RentabilidadeDia = CSV.get(10);
                String RentabilidadeMes = CSV.get(11);
                String RentabilidadeAno = CSV.get(12);
                String aplicacaoInicial = CSV.get(13);
                String aplicacaoSubsequente = CSV.get(14);
                String resgateMinimo = CSV.get(15);
                String saldoMinimo = CSV.get(16);
                String cotaAplicacao = CSV.get(17);
                String debitoAplicacao = CSV.get(18);
                String cotaResgate = CSV.get(19);
                String creditoResgate = CSV.get(20);
                try{
                    creditoResgate = creditoResgate.replace("D + ", "");
                }
                catch(Exception ex){
                    System.out.print("Erro ao editar creditoResgate: " + ex);
                }
                boolean movautoaplic = CSV.get(21).equalsIgnoreCase("Permite");
                boolean movautoresgate = CSV.get(22).equalsIgnoreCase("Permite");
                int id = 0;
                double taxa = 0;
                double Valcota = 0;
                double patrimonio = 0;
                double rentDia = 0;
                double rentMes = 0;
                double rentAno = 0;
                double aplicIni = 0;
                double aplicSub = 0;
                double resgate = 0;
                double saldo = 0;
                
                try{
                    id = Integer.parseInt(idFundo);
                    taxa = Double.parseDouble(taxaAdministracao.replace(",", "."));
                    Valcota = Double.parseDouble(valorCota.replace(",", "."));
                    patrimonio = Double.parseDouble(PatrimonioLiquido.replace(",", "."));
                    rentDia = Double.parseDouble(RentabilidadeDia.replace(",", "."));
                    rentMes = Double.parseDouble(RentabilidadeMes.replace(",", "."));
                    rentAno = Double.parseDouble(RentabilidadeAno.replace(",", "."));
                    aplicIni = Double.parseDouble(aplicacaoInicial.replace(",", "."));
                    aplicSub = Double.parseDouble(aplicacaoSubsequente.replace(",", "."));
                    resgate = Double.parseDouble(resgateMinimo.replace(",", "."));
                    saldo = Double.parseDouble(saldoMinimo.replace(",", "."));
                }
                catch(Exception ex){
                    System.out.print("Erro ao converter Strings para Números: " + ex);
                }
                Fundos fundo = new FundosDao().findById(id, true);
                if (fundo != null) {
                        fundo.setSituacao(situacao);
                        fundo.setFamilia(familia);
                        fundo.setTipo(tipo);
                        fundo.setSegmento(segmento);
                        fundo.setNivelRel(nivelRel);
                        fundo.setTaxaadm(taxa);
                        fundo.setValorcota(Valcota);
                        fundo.setPatrimonio(patrimonio);
                        fundo.setRentabilidadedia(rentDia);
                        fundo.setRentabilidademes(rentMes);
                        fundo.setRentabilidadeano(rentAno);
                        fundo.setAplicacaoinicial(aplicIni);
                        fundo.setAplicacaosubs(aplicSub);
                        fundo.setResgateminimo(resgate);
                        fundo.setSaldominimo(saldo);
                        fundo.setCotaaplicacao(cotaAplicacao);
                        fundo.setDebitoaplicacao(debitoAplicacao);
                        fundo.setCotaresgate(cotaResgate);
                        fundo.setCreditoResgate(creditoResgate);
                        fundo.setMovautoaplicavao(movautoaplic);
                        fundo.setMovautoresgate(movautoresgate);
                        opSucesso = new FundosDao().makePersistent(fundo) != null;
                } else if(id != 0){
                    System.out.print("Segmento: *"+segmento+"*");
                                                                    System.out.print("************************");
                                                System.out.print("************************");
                                                System.out.print("************************");
                                                System.out.print("************************");
                                                System.out.print("************************");
                                                System.out.print("************************");
                                                System.out.print("************************");
                                                System.out.print("************************");
                                                System.out.print("************************");
                                                System.out.print("************************");
                                                System.out.print("************************");
                                                System.out.print("************************");
                                                System.out.print("************************");
                                                System.out.print("************************");
                                                System.out.print("************************");
                                                System.out.print("************************");
                                                System.out.print("************************");
                                                System.out.print("************************");
                                                System.out.print("************************");
                                                System.out.print("************************");
                                                System.out.print("************************");
                                                System.out.print("************************");
                                                System.out.print("************************");
                                                System.out.print("************************");
                                                System.out.print("************************");
                                                System.out.print("************************");
                                                System.out.print("************************");
                                                System.out.print("************************");
                                                System.out.print("************************");
                                                System.out.print("************************");
                                                System.out.print("************************");
                                                System.out.print("************************");
                                                System.out.print("************************");
                                                System.out.print("Segmento: " + segmento);
                                                
                    System.out.print("CHECKPOINT C");
                    System.out.print("CHECKPOINT C");
                    System.out.print("CHECKPOINT C");
                    System.out.print("CHECKPOINT C");
                    System.out.print("CHECKPOINT C");
                    Fundos f = new Fundos();
                    f.setSituacao(true);
                    f.setId(id);
                    f.setFamilia(familia);
                    f.setTipo(tipo);
                    f.setSegmento(segmento);
                    f.setNivelRel(nivelRel);
                    f.setTaxaadm(taxa);
                    f.setValorcota(Valcota);
                    f.setPatrimonio(patrimonio);
                    f.setRentabilidadedia(rentDia);
                    f.setRentabilidademes(rentMes);
                    f.setRentabilidadeano(rentAno);
                    f.setAplicacaoinicial(aplicIni);
                    f.setAplicacaosubs(aplicSub);
                    f.setResgateminimo(resgate);
                    f.setSaldominimo(saldo);
                    f.setCotaaplicacao(cotaAplicacao);
                    f.setDebitoaplicacao(debitoAplicacao);
                    f.setCotaresgate(cotaResgate);
                    f.setCreditoResgate(creditoResgate);
                    f.setMovautoaplicavao(movautoaplic);
                    f.setMovautoresgate(movautoresgate);
                    System.out.print("CHECKPOINT D");
                    System.out.print("CHECKPOINT D");
                    System.out.print("CHECKPOINT D");
                    System.out.print("CHECKPOINT D");
                    System.out.print("CHECKPOINT D");
                    opSucesso = new FundosDao().save(f) != null;
                    System.out.print("CHECKPOINT E");
                    System.out.print("CHECKPOINT E");
                    System.out.print("CHECKPOINT E");
                    System.out.print("CHECKPOINT E");
                    System.out.print("CHECKPOINT E");
                }
            }
            System.out.print("CHECKPOINT F");
            System.out.print("CHECKPOINT F");
            System.out.print("CHECKPOINT F");
            System.out.print("CHECKPOINT F");
            CSV.close();

            addFacesMsg("Fundos atualizados com sucesso.", FacesMessage.SEVERITY_INFO);
        } catch (IOException ex) {
            Logger.getLogger(FuncionariosController2.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
