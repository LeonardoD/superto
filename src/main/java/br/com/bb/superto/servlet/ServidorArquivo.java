package br.com.bb.superto.servlet;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;
import java.io.File;

@Path("/file")
public class ServidorArquivo {

    /**
     *
     * @param pathToImg
     * @return
     */
    @GET
    @Path("/img/{filePath}")
    @Produces("image/png")
    @Consumes(MediaType.TEXT_PLAIN)
    public Response getFile(@PathParam("filePath") String pathToImg) {
        File f = new File("/home/archives/" + pathToImg);
        if (f.exists()) {
            System.out.println("Carregando pacote de imagens em 'home/archives: " + f.exists());
            return Response.ok(f, "image/png").build();
        }
        return Response.status(Response.Status.NOT_FOUND).build();
    }

    /**
     *
     * @param pathToImg
     * @return
     */
    @GET
    @Path("/tumb/{filePath}")
    @Produces("image/png")
    @Consumes(MediaType.TEXT_PLAIN)
    public Response getTumb(@PathParam("filePath") String pathToImg) {
        File f = new File("/home/archives/thumbnail/" + pathToImg);
        if (f.exists()) {
            return Response.ok(f, "image/png").build();
        }
        return Response.status(Response.Status.NOT_FOUND).build();
    }

    /**
     *
     * @param pathToImg
     * @return
     */
    @GET
    @Path("/video/{filePath}")
    @Produces("video/mpeg4")
    @Consumes(MediaType.TEXT_PLAIN)
    public StreamingOutput getVideo(@PathParam("filePath") String pathToImg) {
        File f = new File("/home/archives/" + pathToImg);
        if (f.exists()) {
            return new FileStreamingOutput(f);
        }
        throw new WebApplicationException(Response.Status.NOT_FOUND);
    }

}
