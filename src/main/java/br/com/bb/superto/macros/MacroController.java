/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.macros;

import br.com.bb.superto.controller.AgenciasController;
import br.com.bb.superto.controller.GenericController;
import br.com.bb.superto.controller.LoginController;
import br.com.bb.superto.model.Macro;
import java.io.Serializable;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.Scanner;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import br.com.bb.superto.dao.GenericHibernateDAO;
import br.com.bb.superto.dao.MacrosDao;
import br.com.bb.superto.model.Agencias;
import java.io.File;
import java.lang.annotation.Annotation;
import java.net.URI;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import javax.ws.rs.core.EntityTag;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Link;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.NewCookie;
import javax.ws.rs.core.Response;


/**
 *
 * @author administrador
 */
@ManagedBean(name = "macroController")
@ViewScoped
public class MacroController extends GenericController<Macro> implements Serializable {
    private Date dataCadastro;
    private String nome;
    private String relatorio;
    private String motivo;
    private String chaveSolicitante;
    private String url;
    private boolean opSucesso;

    public boolean isOpSucesso() {
        return opSucesso;
    }

    public void setOpSucesso(boolean opSucesso) {
        this.opSucesso = opSucesso;
    }
    
    public String getUrl() {
        return url;
    }
    
    public java.util.List<Macro> getMacros() {
        return new MacrosDao().findAll();
    }
    
    public void setUrl(String url) {
        this.url = url;
    }
    
    public Date getDataCadastro() {
        return dataCadastro;
    }

    public void setDataCadastro(Date dataCadastro) {
        this.dataCadastro = dataCadastro;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getRelatorio() {
        return relatorio;
    }

    public void setRelatorio(String relatorio) {
        this.relatorio = relatorio;
    }

    public String getMotivo() {
        return motivo;
    }

    public void setMotivo(String motivo) {
        this.motivo = motivo;
    }

    public String getChaveSolicitante() {
        return chaveSolicitante;
    }

    public void setChaveSolicitante(String chaveSolicitante) {
        this.chaveSolicitante = chaveSolicitante;
    }
    
    
    
    @Override
    public void edit() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void delete() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void add() {
        System.out.print("ADD Macro");
        System.out.print("ADD Macro");
        System.out.print("ADD Macro");
        System.out.print("ADD Macro");
        System.out.print("ADD Macro");
        
        try {
//            loginController.funcionario.fmatricula
                    System.out.print("Entramos no try");
            if(url.contains("disap") || url.contains("dired")){
                    System.out.print("url válida: disap ou dired");

                if(new MacrosDao().getMacroByRelatorio(relatorio) == null){
                    System.out.print("não existe macro do relatorio ainda");

                    System.out.print("********************");
                    dataCadastro = new Date();
                    chaveSolicitante = new LoginController().getFuncionario().getFmatricula();
                    String instructions = escreve(url, relatorio);
                    System.out.print("--------------------");

                    Macro m = new Macro(0, nome, relatorio, instructions, motivo, url, dataCadastro, chaveSolicitante);
                    System.out.print("Gerou objeto Macro: " + nome);
                    opSucesso = new MacrosDao().save(m)!= null;
                    System.out.print("Sucesso: " + opSucesso);
                }
                else{
                    // NÃO ESTÁ UTILIZÁVEL
                    dataCadastro = new Date();
                    Macro m = new MacrosDao().getMacroByRelatorio(relatorio);
                    System.out.print("Gerou objeto Macro: " + nome);
                    String instructions = escreve(url, relatorio);
//                    set xxx
                    opSucesso = new GenericHibernateDAO().makePersistent(m) != null;
                }
            }    
        } catch (Exception ex) {
            System.out.println("Erro ao gerar macro: " + ex.getMessage());
        }
    }
    
    public String escreve(String url, String relatorio) throws IOException{
        String retorno = "VERSION BUILD=844 RECORDER=CR \n";
        for(Agencias a: new AgenciasController().getAgencias()){
            if(a.getPrefixo()!= 3615 && a.getPrefixo()!= 8320 && a.getPrefixo()!= 8517){
              retorno = retorno + "\nURL GOTO=";
                retorno = retorno + url.replaceAll("XXX", a.getPrefixo()+"");
                retorno = retorno + "\n";
                retorno = retorno + ("ONDOWNLOAD FOLDER=* FILE=XXX.xls WAIT=YES").replaceAll("XXX", a.getPrefixo()+"");
                retorno = retorno + "\nTAG POS=1 TYPE=A ATTR=TXT:Arquivo<SP>Excel\n";  
            }
        }
        System.out.print(retorno);
        
            FileWriter arquivo = new FileWriter(("/opt/glassfish4.1/glassfish4/glassfish/macroXXX.iim").replaceAll("XXX", relatorio));
//            ele escreve o arquivo, bonitinho... minha dúvida é: quando for pro servidor, ele vai escrever o arquivo na máquina virtual, ou no pc do cliente?
//                 na máquina virtual
//                 como fazer pra enviar isso pro cliente?
//                 Fará o download na lista de macros existentes do aquivo gerado. Este arquivo é apenas um arquivo txt com extensão de nome iim
            
            
//             tentar
//                 jogar os macros pra dentro da pasta do projeto (atenção, a estrutura de pastas do servidor pode não ser a mesma que no local)
//                 fazer botão de download no widget referente a cada linha da tabela, que direciona pra "macro#{selected.getRelatorio()}.iim"
//                     isso talvez funcione
            
            System.out.print(".............vou escrever.............");
            PrintWriter gravarArq = new PrintWriter(arquivo);
            gravarArq.print(retorno);
            System.out.print(".............escrevi.............");
        
            return retorno;
    }
    
}
