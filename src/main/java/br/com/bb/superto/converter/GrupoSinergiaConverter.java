/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.converter;

import br.com.bb.superto.sinergia.dao.SinergiaGrupoDAO;
import br.com.bb.superto.sinergia.model.SinergiaGrupo;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

/**
 * @author Administrador
 */
@FacesConverter("grupoSinergiaConverter")
public class GrupoSinergiaConverter implements Converter {

    @Override
    public Object getAsObject(FacesContext fc, UIComponent component, String value) {
        SinergiaGrupo grupo;
        if (value != null && value.trim().length() > 0 && !value.toLowerCase().contains("selecione")) {
            Integer id = Integer.parseInt(value);
            grupo = new SinergiaGrupoDAO().findGroupByID(id);
        } else {
            return null;
        }
        return grupo;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object object) {
//        if (object != null) {
//            SinergiaGrupo grupo;
//
//            if (object == Integer.class) {
//                Integer id = (Integer) object;
//                grupo = new SinergiaGrupoDAO().findGroupByID(id);
//                return grupo.getDescricao();
//            } else if (object == SinergiaGrupo.class) {
//                return String.valueOf(((SinergiaGrupo) object).getId());
//            }
//        }
//        return null;

        if (object != null) {
            return String.valueOf(((SinergiaGrupo) object).getId());
        } else {
            return null;
        }
    }

}
