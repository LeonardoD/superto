/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.converter;

import br.com.bb.superto.dao.GalleryDao;
import br.com.bb.superto.model.Gallery;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

/**
 * @author maycon
 */
public class GalleryConverter implements Converter {

    @Override
    public Gallery getAsObject(FacesContext context, UIComponent component, String value) {
        System.out.println("Entrou para converter para a classe tag");
        return new GalleryDao().findByName(value);
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        return ((Gallery) (value)).getTitle();
    }

}
