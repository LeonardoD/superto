/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.converter;

import br.com.bb.superto.dao.MercadoGeralDao;
import br.com.bb.superto.model.MercadoGeral;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

/**
 * @author Administrador
 */
@FacesConverter(value = "mercadoGeralConverter", forClass = MercadoGeral.class)
public class MercadoGeralConverter implements Converter {

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        if (value == null || value.isEmpty()) {
            return null;
        }
        Integer id = Integer.valueOf(value);
        MercadoGeral mercado = new MercadoGeralDao().findById(id, true);
        System.out.println("entrou no conversor getObject");
        return mercado;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        MercadoGeral mercado = (MercadoGeral) value;
        if (mercado == null || mercado.getId() == null) {
            return null;
        }
        System.out.println("entrou no conversor getString");
        return String.valueOf(mercado.getId());
    }

}
