/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.converter;

import br.com.bb.superto.dao.FuncionariosDao;
import br.com.bb.superto.model.Funcionarios;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

/**
 * @author rodrigo
 */
@FacesConverter(value = "funcionarioConverter", forClass = Funcionarios.class)
public class FuncionarioConverter implements Converter {

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        if (value == null || value.isEmpty()) {
            return null;
        }

        Integer id = Integer.valueOf(value);
        Funcionarios funcionario = new FuncionariosDao().findById(id, true);
        return funcionario;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        Funcionarios funcionario = (Funcionarios) value;
        if (funcionario == null || funcionario.getId() == null) {
            return null;
        }
        return String.valueOf(funcionario.getId());
    }

}

