/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.converter;

import br.com.bb.superto.dao.AssuntoDao;
import br.com.bb.superto.model.Assunto;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

/**
 * @author Administrador
 */
@FacesConverter(value = "assuntoConverter", forClass = Assunto.class)
public class AssuntoConverter implements Converter {

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        if (value == null || value.isEmpty()) {
            return null;
        }

        Integer id = Integer.valueOf(value);
        Assunto assunto = new AssuntoDao().findById(id, true);
        System.out.println("entrou no conversor getObject");
        return assunto;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        Assunto assunto = (Assunto) value;
        if (assunto == null || assunto.getId() == null) {
            return null;
        }
        System.out.println("entrou no conversor getString");
        return String.valueOf(assunto.getId());
    }

}
