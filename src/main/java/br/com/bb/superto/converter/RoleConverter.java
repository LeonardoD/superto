/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.converter;

import br.com.bb.superto.dao.RolesDAO;
import br.com.bb.superto.model.Roles;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

/**
 * @author maycon
 */
public class RoleConverter implements Converter {

    @Override
    public Roles getAsObject(FacesContext context, UIComponent component, String value) {
        System.out.println("Entrou para converter para a classe role");
        return new RolesDAO().findByName(value);
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        return ((Roles) (value)).getRoleName();
    }

}
