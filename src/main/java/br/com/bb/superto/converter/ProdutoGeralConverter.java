/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.converter;

import br.com.bb.superto.dao.ProdutoGeralDao;
import br.com.bb.superto.model.ProdutoGeral;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

/**
 * @author Administrador
 */
@FacesConverter(value = "produtoGeralConverter", forClass = ProdutoGeral.class)
public class ProdutoGeralConverter implements Converter {

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        if (value == null || value.isEmpty()) {
            return null;
        }

        Integer id = Integer.valueOf(value);
        ProdutoGeral produto = new ProdutoGeralDao().findById(id, true);
        System.out.println("entrou no conversor getObject");
        return produto;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        ProdutoGeral produto = (ProdutoGeral) value;
        if (produto == null || produto.getId() == null) {
            return null;
        }
        System.out.println("entrou no conversor getString");
        return String.valueOf(produto.getId());
    }

}
