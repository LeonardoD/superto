/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.converter;

import br.com.bb.superto.dao.ProdutoDao;
import br.com.bb.superto.model.Produto;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

/**
 * @author Administrador
 */
@FacesConverter("produtoConverter")
public class ProdutoConverter implements Converter {

    @Override
    public Object getAsObject(FacesContext fc, UIComponent component, String value) {
        Produto produto;
        if (value != null && value.trim().length() > 0 && !value.equalsIgnoreCase("null")) {
            Integer id = Integer.parseInt(value);
            produto = new ProdutoDao().findById(id, false);
        } else {
            return null;
        }
        return produto;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object object) {
        if (object != null) {
            return String.valueOf(((Produto) object).getId());
        } else {
            return null;
        }
    }
}
