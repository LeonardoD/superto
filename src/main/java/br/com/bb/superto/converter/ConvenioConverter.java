/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.converter;

import br.com.bb.superto.plataformaDivida.dao.ConvenioDAO;
import br.com.bb.superto.plataformaDivida.model.Convenio;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

/**
 * @author Administrador
 */

@FacesConverter(value = "convenioConverter", forClass = Convenio.class)
public class ConvenioConverter implements Converter {

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        if (value == null || value.isEmpty()) return null;
        
        Integer id = Integer.valueOf(value);
        Convenio convenio = new ConvenioDAO().findById(id, true);
         System.out.println("entrou no conversor getObject");
                return convenio;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        Convenio convenio = (Convenio) value;
        if (convenio == null || convenio.getID() == null) return null;
        System.out.println("entrou no conversor getString");
        return String.valueOf(convenio.getID());
    }

}
