/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.converter;

import br.com.bb.superto.model.Mercado;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

/**
 * @author maycon
 */
@FacesConverter("mercadoConverter")
public class MercadoConverter implements Converter {

    @Override
    public Mercado getAsObject(FacesContext context, UIComponent component, String value) {
        return Mercado.valueOf(value);
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        try {
            return ((Mercado) (value)).getLabel();
        } catch (Exception e) {
            return "";
        }
    }

}
