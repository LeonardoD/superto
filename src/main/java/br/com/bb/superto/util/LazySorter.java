/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.util;

import org.primefaces.model.SortOrder;

import java.util.Comparator;

/**
 * @author maycon
 */
public class LazySorter<T> implements Comparator<T> {

    private String sortField;
    private SortOrder sortOrder;
    private Class aClass;

    public LazySorter(String sortField, SortOrder sortOrder, Class aClass) {
        this.sortField = sortField;
        this.sortOrder = sortOrder;
        this.aClass = aClass;
    }

    @Override
    public int compare(T t, T t1) {
        try {

            Object value1 = aClass.getClass().getField(this.sortField).get(t);
            Object value2 = aClass.getClass().getField(this.sortField).get(t1);

            int value = ((Comparable) value1).compareTo(value2);

            return SortOrder.ASCENDING.equals(sortOrder) ? value : -1 * value;
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException();
        }
    }
}
