/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.util;

import br.com.bb.superto.model.GenericEntity;
import org.hibernate.exception.spi.TemplatedViolatedConstraintNameExtracter;
import org.hibernate.exception.spi.ViolatedConstraintNameExtracter;
import org.hibernate.internal.util.JdbcExceptionHelper;

import javax.faces.FactoryFinder;
import javax.faces.component.UIViewRoot;
import javax.faces.context.FacesContext;
import javax.faces.context.FacesContextFactory;
import javax.faces.lifecycle.Lifecycle;
import javax.faces.lifecycle.LifecycleFactory;
import javax.imageio.ImageIO;
import javax.persistence.Table;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.swing.*;
import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.*;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.text.Normalizer;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Classe diversas funções uteis para o sistema
 *
 * @author maycon
 * @version 1.0
 */
public class Util {

    public static String previousPagUrl = "";
    private static ViolatedConstraintNameExtracter EXTRACTER = new TemplatedViolatedConstraintNameExtracter() {
        /**
         * * Extract the name of the violated constraint from the given
         * SQLException.
         *
         * * @param sqle The exception that was the result of the constraint
         * violation.
         * @return The extracted constraint name.
         */
        public String extractConstraintName(SQLException sqle) {
            String constraintName = null;
            int sqlError = Integer.valueOf(JdbcExceptionHelper.extractSqlState(sqle));
            if (sqlError == 23505) {
                constraintName = extractUsingTemplate("violates unique constraint \"", "\"", sqle.getMessage());
            }
            return constraintName;
        }
    };

    /**
     * Metodo que tem como função retornar o nome da tabela de uma classe que
     * está anotada como uma entidade.
     *
     * @param clazz
     * @return o nome da tabela
     * @since 1.0
     */
    public static String getTableName(Class clazz) {
        //System.out.println(clazz.getSimpleName());
        Table annotation = (Table) clazz.getAnnotation(javax.persistence.Table.class);
        if (annotation != null) {
            //System.out.println(annotation.name());
            return annotation.name();
        }
        return null;
    }

    public static <T> List<T> mergeTwoArrays(List<T> array, List<T> otherArray) {
        boolean match = false;
        List<T> temp = array.subList(0, array.size());
        for (int i = 0; i < array.size(); i++) {
            for (int j = 0; j < otherArray.size(); j++) {
                if (((GenericEntity) array.get(i)).getId().intValue() == ((GenericEntity) otherArray.get(j)).getId()) {
                    otherArray.remove(j);
                    match = true;
                    break;
                }
            }
            if (match) {
                temp.remove(array.get(i));
                match = false;
            }
        }
        return temp;
    }

    /**
     * Metodo que retorna a data atual
     *
     * @return a data atual
     * @since 1.0
     */
    public static Date getDate() {
        GregorianCalendar calendar = new GregorianCalendar(TimeZone.getTimeZone("GMT-3"), new Locale("pt_BR"));
        return calendar.getTime();
    }

    public static String getHour() {
        return new SimpleDateFormat("HH:mm").format(new Date());
    }

    public static Date getTimeStamp() {
        return Calendar.getInstance().getTime();
    }

    /**
     * Formata a data
     *
     * @param date
     * @return a data formatada
     */
    public static String dateFormat(Date date) {
        return null;

    }

    /**
     * Metodo responsavel por retornar o caminho da pasta de arquivos do sistema
     *
     * @return o caminho da pasta dos arquivos
     * @since 1.0
     */
    public static String getMediaPath() {
        return File.separator + "home" + File.separator + "archives" + File.separator;
    }

    /**
     * Esse metodo retorna um numero randomico
     *
     * @return um numero randomico
     * @since 1.0
     */
    public static String getRandomName() {
        int i = (int) (Math.random() * 10000000);
        return String.valueOf(i);
    }

    /**
     * Metodo responsavel por criptografar uma string em md5
     *
     * @param aToken
     * @return string criptogradada
     * @since 1.0
     */
    public static String generateMD5(String aToken) {
        MessageDigest aMessageDigest = null;
        try {
            aMessageDigest = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            System.out.println("Algoritmo solicitado não encontrado.");
        }
        BigInteger hash = new BigInteger(1, aMessageDigest.digest(aToken.getBytes()));
        String aMD5 = hash.toString(16);
        return aMD5;
    }

    /**
     * Este metodo e responsavel por retirar os acentos de um string
     *
     * @param acentuada
     * @return string sem acentos
     * @since 1.0
     */
    public static String removerAcentos(String acentuada) {
        CharSequence cs = new StringBuilder(acentuada);
        String normalizada = Normalizer.normalize(cs, Normalizer.Form.NFKD).replaceAll("\\p{InCombiningDiacriticalMarks}+", "");
        normalizada = normalizada.replace(" ", "_");
        normalizada = normalizada.replace(".", "");
        normalizada = normalizada.replace("/", "");
        normalizada = normalizada.replace("+", "");
        normalizada = normalizada.replace(":", "");
        normalizada = normalizada.replace("=", "");
        normalizada = normalizada.replace("ç", "c");
        normalizada = normalizada.replace("[", "_");
        normalizada = normalizada.replace("]", "_");
        normalizada = normalizada.toLowerCase();
        return normalizada;
    }

    /**
     * Este metodo retorna a extensão de um arquivo
     *
     * @param acentuada
     * @return extensão de um arquivo
     * @since 1.0
     */
    public static String getextension(String aFilename) {
        return aFilename.substring(aFilename.lastIndexOf('.') + 1);
    }

    public static List<String> readerCSV(FileInputStream csv) {
        return readerCSV(csv);
    }

    public static List<String> readerCSV(InputStream stream) {
        BufferedReader bin = null;
        List<String> data = new ArrayList<>();
        try {
            bin = new BufferedReader(new InputStreamReader(stream, "UTF-8"));
            String line = null;
            while ((line = bin.readLine()) != null) {
                String[] split = line.split(";");
                for (String value : split) {
                    if (!value.isEmpty()) {
                        data.add(value);
                    } else {
                        data.add(" ");
                    }
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(Util.class.getName()).log(Level.SEVERE, null, ex);
        }
        return data;
    }

    public static byte[] createThumbnail(byte[] orig, int maxDim, int t) {
        try {
            ImageIcon imageIcon = new ImageIcon(orig);
            java.awt.Image inImage = imageIcon.getImage();
            double scale = (double) maxDim / (double) inImage.getWidth(null);
            double scale2 = (double) t / (double) inImage.getHeight(null);

            int scaledW = (int) (scale * inImage.getWidth(null));
            int scaledH = (int) (scale2 * inImage.getHeight(null));

            BufferedImage outImage = new BufferedImage(scaledW, scaledH, BufferedImage.TYPE_INT_RGB);
            AffineTransform tx = new AffineTransform();

            if (scale < 1.0d) {
                tx.scale(scale, scale2);
            }

            Graphics2D g2d = outImage.createGraphics();
            g2d.drawImage(inImage, tx, null);
            g2d.dispose();

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ImageIO.write(outImage, "JPG", baos);
            byte[] bytesOut = baos.toByteArray();

            return bytesOut;
        } catch (IOException e) {
            System.out.println("Erro: " + e.getMessage());
            e.printStackTrace();
        }
        return null;
    }

    public static double porcent(double total, double value) {
        return ((value * 100) / total);
    }

    public static ViolatedConstraintNameExtracter getViolatedConstraintNameExtracter() {
        return EXTRACTER;
    }

    public static FacesContext getFacesContext(
            HttpServletRequest request, HttpServletResponse response) {
        // Get current FacesContext.
        FacesContext facesContext = FacesContext.getCurrentInstance();

        // Check current FacesContext.
        if (facesContext == null) {

            // Create new Lifecycle.
            LifecycleFactory lifecycleFactory = (LifecycleFactory) FactoryFinder.getFactory(FactoryFinder.LIFECYCLE_FACTORY);
            Lifecycle lifecycle = lifecycleFactory.getLifecycle(LifecycleFactory.DEFAULT_LIFECYCLE);

            // Create new FacesContext.
            FacesContextFactory contextFactory = (FacesContextFactory) FactoryFinder.getFactory(FactoryFinder.FACES_CONTEXT_FACTORY);
            facesContext = contextFactory.getFacesContext(
                    request.getSession().getServletContext(), request, response, lifecycle);

            // Create new View.
            UIViewRoot view = facesContext.getApplication().getViewHandler().createView(
                    facesContext, "");
            facesContext.setViewRoot(view);

            // Set current FacesContext.
            FacesContextWrapper.setCurrentInstance(facesContext);
        }

        return facesContext;
    }

    public static byte[] fileToByte(File file) throws Exception {
        return Util.fileToByte(new FileInputStream(file));
    }

    public static byte[] fileToByte(InputStream fis) throws Exception {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        byte[] buf = new byte[1024];
        try {
            for (int readNum; (readNum = fis.read(buf)) != -1;) {
                bos.write(buf, 0, readNum);
            }
        } catch (IOException ex) {
        }
        return bos.toByteArray();
    }

    /**
     * TODO Fazer um leitor de CSV Genérico
     */
    public static void readCSV() {

    }

    /**
     * TODO Fazer um gerador de CSV Genérico
     */
    public static void writeCSV() {

    }

    public static Double retirarMascaraDinheiro(String money) {
        money = money.replaceAll("\\.", "");
        money = money.replace(",", ".");
        return Double.parseDouble(money);
    }

    public static String normalizarFMatricula(String fmatricula) {
        int sizeString = fmatricula.length();
        if (sizeString < 7 && !fmatricula.contains("F")) {
            String zeros = "";
            for (int i = 0; i < (sizeString - 7); i++) {
                zeros += 0;
            }
            return "F" + zeros + fmatricula;
        }
        return "F" + fmatricula;
    }

    // Helpers -----------------------------------------------------------------------------------
    // Wrap the protected FacesContext.setCurrentInstance() in a inner class.
    private static abstract class FacesContextWrapper extends FacesContext {

        protected static void setCurrentInstance(FacesContext facesContext) {
            FacesContext.setCurrentInstance(facesContext);
        }
    }

    public static String normalizarPrefixo(String prefixo) {
        int sizePrefixo = prefixo.length();
        String zeros = "";
        if (sizePrefixo < 4) {
            for (int i = 0; i < (4 - sizePrefixo); i++) {
                zeros += "0";
            }
        }
        return (zeros + prefixo);
    }
}
