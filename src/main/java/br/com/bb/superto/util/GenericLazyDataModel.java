/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
*/
package br.com.bb.superto.util;

import br.com.bb.superto.dao.HibernateUtil;
import br.com.bb.superto.model.GenericEntity;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import java.math.BigInteger;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * @author maycon
 * @param <T>
 * @param <ID>
 */

public class GenericLazyDataModel<T, ID> extends LazyDataModel<T> {

    List<T> dataSource;
    String tableName;
    String byId = "";
    String filter = "";
    private final Class<T> persistentClass;

    public GenericLazyDataModel(String tablename, Class<T> aClass) {
        persistentClass = aClass;
        tableName = tablename;
        System.out.println(tablename);
        System.out.println(persistentClass);
    }

    public GenericLazyDataModel(String tablename, Class<T> aClass, String filter) {
        persistentClass = aClass;
        tableName = tablename;
        this.filter = filter;
    }

    public String getById() {
        return byId;
    }

    public void setById(String byId) {
        this.byId = byId;
    }
    
    @Override
    public ID getRowKey(T entity) {
       System.out.println("Erro ROWKEY"); 
       return (ID) ((GenericEntity) entity).getId();
    }
 
    @Override
    public T getRowData(String id) {
        if (dataSource == null) {
            return null;
        }
        for (T value : dataSource) {
            if ((((GenericEntity) value).getId()+"").contains(id)) {
                return value;
            }
        }
        System.out.println("Erro ID------------------------------------");
        return null;
    }

    /**
     * @param first
     * @param pageSize
     * @param sortField
     * @param sortOrder
     * @param filters
     * @return
     */
    @Override
    public List<T> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
        String initQuery = "SELECT * FROM " + tableName + " ";
        String where = "";
        String id = "";
        QueryModel k = null;
        if (sortField != null) {
            filters.put(sortField, null);
        }
        for (Iterator<String> it = filters.keySet().iterator(); it.hasNext(); ) {
            String filterProperty = it.next();
            String filterValue = (String) filters.get(filterProperty);
            if (filterProperty.contains(".")) {
                String[] split = filterProperty.split("\\.");
                k = QueryModel.setModel(persistentClass, split[0], null);
                for (int i = 1; i < split.length; i++) {
                    QueryModel.setModel(k.lastChildren.getType().getReturnedClass(), split[i], k);
                }
            } else {
                k = QueryModel.setModel(persistentClass, filterProperty, null);
            }
            initQuery += "" + QueryModel.createQuery(k);
            if (filterValue != null) {
                if (k.getLastChildren().getType().getReturnedClass().getSimpleName().equalsIgnoreCase("String")) {
                    where += k.getLastChildren().getNomeDaTabela() + "." + k.getLastChildren().getNomeDaColuna() + " ~* '" + filterValue + "'";
                } else {
                    where += "CAST(" + k.getLastChildren().getNomeDaTabela() + "." + k.getLastChildren().getNomeDaColuna() + " AS text) ~* '" + filterValue + "'";
                }
                if (it.hasNext()) {
                    where += " and ";
                }
            }
        }
        String query = initQuery;
        if (!where.isEmpty()) {
            query += " where " + where;
        }
        if (!filter.isEmpty() && !where.isEmpty()) {
            query += " and " + tableName + "." + filter;
        } else if (!filter.isEmpty()) {
            query += " where " + tableName + "." + filter;
        }
        String orderBy = null;
        if ("resolve".equals(tableName)) {
           orderBy = " order by resolve.dataresolve ";
        }
        else
           orderBy = " order by " + tableName + "." + QueryModel.getIdentifierName(persistentClass);

        if (sortField != null && k != null) {
            orderBy = " order by " + k.lastChildren.getNomeDaTabela() + "." + k.lastChildren.getNomeDaColuna();
        }
        String querycount = query.replaceFirst("\\*", "count(*) as value");
        query += orderBy + " desc offset " + (first) + " limit " + (first + pageSize);
        System.out.println("Query:\n\t" + query);
        dataSource = HibernateUtil.getSessionFactory().getCurrentSession().createSQLQuery(query).addEntity(persistentClass).list();
        setRowCount(((BigInteger) HibernateUtil.getSessionFactory().getCurrentSession().createSQLQuery(querycount).uniqueResult()).intValue());
        System.out.println("Query Count:\n\t" + querycount);

        return dataSource;
    }
    
    public List<T> load2(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
        String initQuery = "SELECT * FROM " + tableName + " ";
        String where = "";
        String id = "";
        QueryModel k = null;
        if (sortField != null) {
            filters.put(sortField, null);
        }
        for (Iterator<String> it = filters.keySet().iterator(); it.hasNext(); ) {
            String filterProperty = it.next();
            String filterValue = (String) filters.get(filterProperty);
            if (filterProperty.contains(".")) {
                String[] split = filterProperty.split("\\.");
                k = QueryModel.setModel(persistentClass, split[0], null);
                for (int i = 1; i < split.length; i++) {
                    QueryModel.setModel(k.lastChildren.getType().getReturnedClass(), split[i], k);
                }
            } else {
                k = QueryModel.setModel(persistentClass, filterProperty, null);
            }
            initQuery += "" + QueryModel.createQuery(k);
            if (filterValue != null) {
                if (k.getLastChildren().getType().getReturnedClass().getSimpleName().equalsIgnoreCase("String")) {
                    where += k.getLastChildren().getNomeDaTabela() + "." + k.getLastChildren().getNomeDaColuna() + " ~* '" + filterValue + "'";
                } else {
                    where += "CAST(" + k.getLastChildren().getNomeDaTabela() + "." + k.getLastChildren().getNomeDaColuna() + " AS text) ~* '" + filterValue + "'";
                }
                if (it.hasNext()) {
                    where += " and ";
                }
            }
        }
        String query = initQuery;
        if (!where.isEmpty()) {
            query += " where " + where;
        }
        if (!filter.isEmpty() && !where.isEmpty()) {
            query += " and " + tableName + "." + filter;
        } else if (!filter.isEmpty()) {
            query += " where " + tableName + "." + filter;
        }

        String orderBy = " order by " + tableName + "." + QueryModel.getIdentifierName(persistentClass);

        if (sortField != null && k != null) {
            orderBy = " order by " + k.lastChildren.getNomeDaTabela() + "." + k.lastChildren.getNomeDaColuna();
        }
        String querycount = query.replaceFirst("\\*", "count(*) as value");
        query += orderBy + " desc offset " + (first) + " limit " + (first + pageSize);
        System.out.println("Query:\n\t" + query);
        dataSource = HibernateUtil.getSessionFactory().getCurrentSession().createSQLQuery(query).addEntity(persistentClass).list();
        setRowCount(((BigInteger) HibernateUtil.getSessionFactory().getCurrentSession().createSQLQuery(querycount).uniqueResult()).intValue());
        System.out.println("Query Count:\n\t" + querycount);

        return dataSource;
    }
    
    public String getFilter() {
        return filter;
    }

    public void setFilter(String filter) {
        this.filter = filter;
    }
}