/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.humanograma.controller;

import br.com.bb.superto.controller.GenericController;
import br.com.bb.superto.dao.AssuntoDao;
import br.com.bb.superto.dao.FuncionariosDao;
import br.com.bb.superto.dao.MercadoGeralDao;
import br.com.bb.superto.dao.ProdutoGeralDao;
import br.com.bb.superto.model.Assunto;
import br.com.bb.superto.model.Funcionarios;
import br.com.bb.superto.model.MercadoGeral;
import br.com.bb.superto.model.ProdutoGeral;
import br.com.bb.superto.plataformaDivida.controller.AverbacaoController;
import com.csvreader.CsvReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import org.hibernate.HibernateException;
import org.hibernate.exception.ConstraintViolationException;
import org.primefaces.event.CloseEvent;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;

/**
 *
 * @author administrador
 */
@ManagedBean(name = "humanogramacontroller")
@ViewScoped
public class HumanogramaController extends GenericController<Assunto> implements java.io.Serializable {

    private List<MercadoGeral> mercados;
    private List<Assunto> listaassuntos;
    private List<Assunto> assuntosSelecionados;
    private List<Assunto> assuntosMercado;
    private List<ProdutoGeral> produtos;
    private MercadoGeral mercado;
    private MercadoGeral mercadoselecionado;
    private ProdutoGeral produto;
    private ProdutoGeral produtoSelecionado;
    private List<ProdutoGeral> produtosAssunto;
    private String descricao;
    private Assunto assunto;
    private List<Funcionarios> allFuncionarios;

    private List<Funcionarios> funcionariosSuper;

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public List<MercadoGeral> getMercados() {
        mercados = new MercadoGeralDao().findAll();
        return mercados;
    }

    public void setMercados(List<MercadoGeral> mercados) {
        this.mercados = mercados;
    }

    public MercadoGeral getMercado() {
        if (mercado == null) {
            mercado = new MercadoGeral();
        }
        return mercado;
    }

    public void setMercado(MercadoGeral mercado) {
        this.mercado = mercado;
    }

    public MercadoGeral getMercadoselecionado() {
        if (mercadoselecionado == null) {
            mercadoselecionado = new MercadoGeral();
        }
        return mercadoselecionado;
    }

    public void setMercadoselecionado(MercadoGeral mercadoselecionado) {
        this.mercadoselecionado = mercadoselecionado;
    }

    public ProdutoGeral getProduto() {
        if (produto == null) {
            produto = new ProdutoGeral();
        }
        return produto;
    }

    public void setProduto(ProdutoGeral produto) {
        this.produto = produto;
    }

    public ProdutoGeral getProdutoSelecionado() {
        if (produtoSelecionado == null) {
            produtoSelecionado = new ProdutoGeral();
        }
        return produtoSelecionado;
    }

    public void setProdutoSelecionado(ProdutoGeral produtoSelecionado) {
        this.produtoSelecionado = produtoSelecionado;
    }

    public Assunto getAssunto() {
        if (assunto == null) {
            assunto = new Assunto();
        }
        return assunto;
    }

    public void setAssunto(Assunto assunto) {
        this.assunto = assunto;
    }

    public List<Assunto> getListaassuntos() {
        listaassuntos = new AssuntoDao().findAll();
        return listaassuntos;
    }

    public void setListaassuntos(List<Assunto> listaassuntos) {
        this.listaassuntos = listaassuntos;
    }

    public List<Assunto> getAssuntosSelecionados() {
        if (assuntosSelecionados == null) {
            assuntosSelecionados = new ArrayList<>();
        }
        return assuntosSelecionados;
    }

    public void setAssuntosSelecionados(List<Assunto> assuntosSelecionados) {
        this.assuntosSelecionados = assuntosSelecionados;
    }

    public List<Assunto> getAssuntosMercado() {
        if (assuntosMercado == null) {
            assuntosMercado = new ArrayList<>();
        }
        return assuntosMercado;
    }

    public void setAssuntosMercado(List<Assunto> assuntosMercado) {
        this.assuntosMercado = assuntosMercado;
    }

    public List<ProdutoGeral> getProdutos() {
        produtos = new ProdutoGeralDao().findAll();
        return produtos;
    }

    public void setProdutos(List<ProdutoGeral> produtos) {
        this.produtos = produtos;
    }

    public List<ProdutoGeral> getProdutosAssunto() {
        if (produtosAssunto == null) {
            produtosAssunto = new ArrayList<>();
        }
        return produtosAssunto;
    }

    public void setProdutosAssunto(List<ProdutoGeral> produtosAssunto) {
        this.produtosAssunto = produtosAssunto;
    }

    public List<Funcionarios> getFuncionariosSuper() {
        funcionariosSuper = new FuncionariosDao().findByPrefixo();
        return funcionariosSuper;
    }

    public void setFuncionariosSuper(List<Funcionarios> funcionariosSuper) {
        this.funcionariosSuper = funcionariosSuper;
    }

    public List<Funcionarios> getAllFuncionarios() {
        return allFuncionarios = new FuncionariosDao().findAll();
    }

    public void setAllFuncionarios(List<Funcionarios> allFuncionarios) {
        this.allFuncionarios = allFuncionarios;
    }

    @Override
    public void edit() {
        FacesContext context = FacesContext.getCurrentInstance();
        boolean opSucesso = false;
        try {
            getSelected().setDescricao(value.getDescricao());
            getSelected().setMercadogeral(mercado);

            if (selected.getDescricao() == null || selected.getDescricao().isEmpty()) {
                context.addMessage(null, new FacesMessage("ERRO", "Edição de assunto nao realizada!"));
            } else {
                new AssuntoDao().makePersistent(selected);
                context.addMessage(null, new FacesMessage("EDITADO", "Assunto   editado com sucesso!"));
                opSucesso = true;
            }

        } catch (Exception ex) {
            System.out.println("Erro ao editar assunto: " + ex.getMessage());
        }
    }

    @Override
    public void delete() {
        boolean opSucesso;
        try {
            if (selected == null) {
                throw new NullPointerException();
            }
            opSucesso = new AssuntoDao().makeTransient(selected) != null;
            opSucesso = true;
            selected = null;
        } catch (NullPointerException | ConstraintViolationException ex) {
            System.out.println("Erro ao deletar noticia: " + ex.getMessage());
        }
    }

    @Override
    public void add() {
        FacesContext context = FacesContext.getCurrentInstance();
        try {
            Assunto novoAssunto = new Assunto();
            novoAssunto.setDescricao(value.getDescricao());
            novoAssunto.setMercadogeral(mercado);
            if (value.getDescricao() == null || value.getDescricao().isEmpty()) {
                context.addMessage(null, new FacesMessage("ERRO", "Cadstro de assunto nao realizado!"));
                opSucess = false;
            } else {
                new AssuntoDao().save(novoAssunto);
                opSucess = true;
                System.out.println("Assunto salvo com sucesso!");
                context.addMessage(null, new FacesMessage("CADASTRADO", "Assunto salvo com sucesso!"));
            }

            if (opSucess) {
                value.setDescricao("");
            }

        } catch (Exception ex) {
            System.out.println("Erro ao salvar o assunto do mercado: " + ex.getMessage());
            context.addMessage(null, new FacesMessage("ERRO", "Cadstro de assunto nao realizado!"));
        }
    }

    /* ADD REMOVER EDITAR PRODUTO ************************ */
    public void editProduto() {
        FacesContext context = FacesContext.getCurrentInstance();
        boolean opSucesso;
        try {
            produtoSelecionado.setAssunto(value);
            produtoSelecionado.setDescricao(produto.getDescricao());

            if (produtoSelecionado.getDescricao() == null || produtoSelecionado.getDescricao().isEmpty()) {
                context.addMessage(null, new FacesMessage("ERRO", "Edição de produto nao realizada!"));
            } else {
                new ProdutoGeralDao().makePersistent(produtoSelecionado);
                context.addMessage(null, new FacesMessage("EDITADO", "Produto editado com sucesso!"));
            }

            setProduto(null);
        } catch (Exception ex) {
            System.out.println("Erro ao editar produto: " + ex.getMessage());
        }
    }

    public void deleteProduto() {
        boolean opSucesso;
        try {
            if (produtoSelecionado == null) {
                throw new NullPointerException();
            }
            opSucesso = new ProdutoGeralDao().makeTransient(produtoSelecionado) != null;
            opSucesso = true;
            setProduto(null);
            produtoSelecionado = null;
        } catch (NullPointerException | ConstraintViolationException ex) {
            System.out.println("Erro ao deletar noticia: " + ex.getMessage());
        }
    }

    public void addProduto() {
        FacesContext context = FacesContext.getCurrentInstance();
        try {
            produto.setAssunto(value);
            if (produto.getDescricao() == null || produto.getDescricao().isEmpty()) {
                context.addMessage(null, new FacesMessage("ERRO", "Cadstro de assunto nao realizado!"));
            } else {
                new ProdutoGeralDao().save(produto);
                System.out.println("Produto salvo com sucesso!");
                context.addMessage(null, new FacesMessage("CADASTRADO", "Produto salvo com sucesso!"));
            }
            setProduto(null);
        } catch (Exception ex) {
            System.out.println("Erro ao salvar o assunto do mercado: " + ex.getMessage());
            context.addMessage(null, new FacesMessage("ERRO", "Cadstro de assunto nao realizado!"));
        }
    }

    public String onRowSelect(SelectEvent event) {

        setValue(selected);
        setMercado(selected.getMercadogeral());

        FacesMessage msg = new FacesMessage("Assunto Selecionado", ((Assunto) event.getObject()).getDescricao());
        FacesContext.getCurrentInstance().addMessage(null, msg);

        return "erro";
    }

    public void addAssuntos() {
        boolean opSucesso;
        try {
            FacesContext context = FacesContext.getCurrentInstance();
            if (assuntosSelecionados.isEmpty()) {
                context.addMessage("ERRO", new FacesMessage("Selecione um assunto para atribuir ao funcionário!", ""));
            } else {
                for (Assunto a : assuntosSelecionados) {
                    if (!getFuncionario().getAssuntos().contains(a)) {
                        getFuncionario().getAssuntos().add(a);
                        new FuncionariosDao().makePersistent(funcionario);
                        a.getFuncionarios().add(funcionario);
                        new AssuntoDao().makePersistent(a);
                    }
                }

                context.addMessage("SUCESSO", new FacesMessage("Assuntos Adicionados com sucesso!", ""));
            }

            setAssuntosSelecionados(null);

            opSucesso = true;

        } catch (NullPointerException | ConstraintViolationException ex) {
            System.out.println("Erro ao inserir Assunto: " + ex.getMessage());
        }
    }

    public void excluirAssunto() {
        boolean opSucesso;
        FacesContext context = FacesContext.getCurrentInstance();
        try {

            if (selected != null) {
                getFuncionario().getAssuntos().remove(selected);
                new FuncionariosDao().makePersistent(funcionario);
                new AssuntoDao().makePersistent(selected);

                opSucesso = true;
                context.addMessage(null, new FacesMessage("EXCLUSÃO", "Assunto excluido com sucesso!"));

            } else {
                context.addMessage(null, new FacesMessage("EXCLUSÃO", "Selecione um Assunto para ser Excluído!"));
            }
        } catch (NullPointerException | ConstraintViolationException ex) {
            System.out.println("Erro ao deletar Assunto: " + ex.getMessage());
        }
    }

    public String onRowAssuntoFunicionarioSelect(SelectEvent event) {

        FacesMessage msg = new FacesMessage("Assunto Selecionado", ((Assunto) event.getObject()).getDescricao());
        FacesContext.getCurrentInstance().addMessage(null, msg);

        return "erro";
    }

    public String onRowProdutoSelect(SelectEvent event) {

        setProduto(produtoSelecionado);
        setValue(produtoSelecionado.getAssunto());

        FacesMessage msg = new FacesMessage("Produto Selecionado", ((ProdutoGeral) event.getObject()).getDescricao());
        FacesContext.getCurrentInstance().addMessage(null, msg);

        return "erro";
    }

    private UploadedFile uploadedFile;
    private StreamedContent file;
    private boolean opSucess;

    public boolean isOpSucess() {
        return opSucess;
    }

    public void setOpSucess(boolean opSucess) {
        this.opSucess = opSucess;
    }

    public StreamedContent getFile() {
        return file;
    }

    public void setFile(StreamedContent file) {
        this.file = file;
    }

    public UploadedFile getUploadedFile() {
        return uploadedFile;
    }

    public void setUploadedFile(UploadedFile uploadedFile) {
        this.uploadedFile = uploadedFile;
    }
    private List<Assunto> assuntosUP = new ArrayList<>();
    private int qtdLidos, qtdSalvos;

    public List<Assunto> getAssuntosUP() {
        return assuntosUP;
    }

    public void setAssuntosUP(List<Assunto> assuntosUP) {
        this.assuntosUP = assuntosUP;
    }
    
    public void lerArqAssunto(FileUploadEvent event) {
        FacesContext context = FacesContext.getCurrentInstance();
        try {
            UploadedFile arq = event.getFile();
            if (arq != null) {
                CsvReader csvReader = new CsvReader(arq.getInputstream(), Charset.forName("utf-8"));
                csvReader.setDelimiter(';');
                csvReader.readHeaders();
                assuntosUP.clear();
                while (csvReader.readRecord()) {
                    String descricaoassunto = csvReader.get(0);
                    Integer idMercado = Integer.parseInt(csvReader.get(1));

                    MercadoGeral mercadoatualizado = new MercadoGeralDao().findById(idMercado, true);
                    try {
                        Assunto assuntoUP = new Assunto();
                        assuntoUP.setDescricao(descricaoassunto);
                        assuntoUP.setMercadogeral(mercadoatualizado);
                        qtdLidos++;

                        if (verificaAssunto(assuntoUP)) {

                            assuntosUP.add(assuntoUP);
                            qtdSalvos++;
                        }

                    } catch (HibernateException ex) {
                        Logger.getLogger(AverbacaoController.class
                                .getName()).log(Level.SEVERE, "Erro na importação do CSV, favor fazer upload do arquivo csv novamente!", ex);
                    }

                }
                context.addMessage(null, new FacesMessage("CARREGADO", qtdLidos + " assunto(s) carregado(s) e " + qtdSalvos + " será(ão) salvo(s)!"));

            }

        } catch (IOException ex) {
            Logger.getLogger(AverbacaoController.class
                    .getName()).log(Level.SEVERE, "Erro CSVReader: " + ex.getMessage());
            context.addMessage(
                    null, new FacesMessage("ERRO", "Cadstro de assunto nao realizado!"));
        }
    }

    public void salvarAssuntos() {
        int quantidade = 0;
        try {
            FacesContext context = FacesContext.getCurrentInstance();
            if (assuntosUP.isEmpty()) {
                context.addMessage("ERRO", new FacesMessage("Nenhum assunto foi salvo!", ""));
            } else {
                for (Assunto a : assuntosUP) {
                    opSucess = new AssuntoDao().makePersistent(a) != null;
                    quantidade++;
                }
            }

            if (opSucess) {
                context.addMessage("SUCESSO", new FacesMessage(quantidade + " assuntos Adicionados com sucesso!", ""));
            }
            assuntosUP = null;

        } catch (NullPointerException | ConstraintViolationException ex) {
            System.out.println("Erro ao inserir Assunto: " + ex.getMessage());
        }
    }

    public void lerArqProduto(FileUploadEvent event) {
        FacesContext context = FacesContext.getCurrentInstance();
        try {
            UploadedFile arq = event.getFile();
            if (arq != null) {
                CsvReader csvReader = new CsvReader(arq.getInputstream(), Charset.forName("utf-8"));
                csvReader.setDelimiter(';');
                csvReader.readHeaders();
                while (csvReader.readRecord()) {
                    String descricaoproduto = csvReader.get(0);
                    Integer idAssunto = Integer.parseInt(csvReader.get(1));

                    Assunto assuntoatualizado = new AssuntoDao().findById(idAssunto, true);
                    try {

                        ProdutoGeral produto = new ProdutoGeral();
                        produto.setDescricao(descricaoproduto);
                        produto.setAssunto(assuntoatualizado);

                        if (verificaProduto(produto)) {
                            opSucess = new ProdutoGeralDao().makePersistent(produto) != null;
                        } else {
                            context.addMessage(null, new FacesMessage("ERRO", "Verifique se o produto inserido já existe!"));

                        }

                    } catch (HibernateException ex) {
                        Logger.getLogger(AverbacaoController.class
                                .getName()).log(Level.SEVERE, "Erro na importação do CSV, favor faver upload do arquivo csv novamente!", ex);
                    }
                }
                if (opSucess) {
                    context.addMessage(null, new FacesMessage("CADASTRADO", "Produto(s) salvo(s) com sucesso!"));

                }
                Logger
                        .getLogger(AverbacaoController.class
                                .getName()).log(Level.INFO, "Importação dos dados concluída!");
            } else {
                Logger.getLogger(AverbacaoController.class
                        .getName()).log(Level.INFO, "Favor fazer upload do arquivo!");
            }

        } catch (IOException ex) {
            Logger.getLogger(AverbacaoController.class
                    .getName()).log(Level.SEVERE, "Erro CSVReader: " + ex.getMessage());
            context.addMessage(
                    null, new FacesMessage("ERRO", "Cadstro de produtos nao realizado!"));
        }
    }

    public void lerArqFuncs_Assunto(FileUploadEvent event) {
        FacesContext context = FacesContext.getCurrentInstance();
        try {
            UploadedFile arq = event.getFile();
            if (arq != null) {
                CsvReader csvReader = new CsvReader(arq.getInputstream(), Charset.forName("utf-8"));
                csvReader.setDelimiter(';');
                csvReader.readHeaders();
                while (csvReader.readRecord()) {

                    Integer idFuncionario = Integer.parseInt(csvReader.get(0));
                    Integer idAssunto = Integer.parseInt(csvReader.get(1));

                    Funcionarios funcionario = new FuncionariosDao().findById(idFuncionario, true);
                    Assunto assuntoatualizado = new AssuntoDao().findById(idAssunto, true);
                    try {

                        if (funcionario != null && assuntoatualizado != null) {
                            funcionario.getAssuntos().add(assuntoatualizado);
                            assuntoatualizado.getFuncionarios().add(funcionario);

                            new AssuntoDao().makePersistent(assuntoatualizado);
                            new FuncionariosDao().makePersistent(funcionario);
                            opSucess = true;

                        } else {
                            context.addMessage(null, new FacesMessage("ERRO", "Verifique se o id do assunto e funcionário estão corretos!"));

                        }

                    } catch (HibernateException ex) {
                        Logger.getLogger(AverbacaoController.class
                                .getName()).log(Level.SEVERE, "Erro na importação do CSV, favor faver upload do arquivo csv novamente!", ex);
                    }
                }
                if (opSucess) {
                    context.addMessage(null, new FacesMessage("CADASTRADO", "Assunto(s) de funcionário(s) salvo(s) com sucesso!"));

                }
                Logger
                        .getLogger(AverbacaoController.class
                                .getName()).log(Level.INFO, "Importação dos dados concluída!");
            } else {
                Logger.getLogger(AverbacaoController.class
                        .getName()).log(Level.INFO, "Favor fazer upload do arquivo!");
            }

        } catch (IOException ex) {
            Logger.getLogger(AverbacaoController.class
                    .getName()).log(Level.SEVERE, "Erro CSVReader: " + ex.getMessage());
            context.addMessage(
                    null, new FacesMessage("ERRO", "Cadstro de produtos nao realizado!"));
        }
    }

    public List<Assunto> completeAssuntos(String query) {
        List<Assunto> allAssuntos = new AssuntoDao().findAll();
        List<Assunto> filteredAssuntos = new ArrayList<>();

        for (Assunto temp : allAssuntos) {
            if (temp.getDescricao().toLowerCase().contains(query.toLowerCase())) {
                filteredAssuntos.add(temp);
            }
        }
        return filteredAssuntos;
    }

    public List<ProdutoGeral> completeProdutos(String query) {
        List<ProdutoGeral> allProdutos = new ProdutoGeralDao().findAll();
        List<ProdutoGeral> filteredProdutos = new ArrayList<>();

        for (ProdutoGeral temp : allProdutos) {
            if (temp.getDescricao().toLowerCase().startsWith(query.toLowerCase())) {
                filteredProdutos.add(temp);
            }
        }
        return filteredProdutos;
    }

    public void pesquisarAssuntos(ActionEvent event) {
        try {
            assuntosMercado = new AssuntoDao().findByMercado(mercado.getId());
        } catch (Exception e) {
            e.getMessage();
        }
    }

    public void pesquisarProdutos(ActionEvent event) {
        try {
            produtosAssunto = new ProdutoGeralDao().findByAssunto(value.getId());
        } catch (Exception e) {
            e.getMessage();
        }
    }

    public void limparListagens(CloseEvent event) {
        setListaassuntos(null);
        setProdutos(null);
    }

    private void acaoLimpar() {
        setValue(null);
        setMercado(null);
        setListaassuntos(null);
        setSelected(null);
    }

    private boolean verificaAssunto(Assunto assunto) {
        List<Assunto> assuntos = new AssuntoDao().findAll();

        for (Assunto ass : assuntos) {
            if (ass.getDescricao().toLowerCase().equals(assunto.getDescricao().toLowerCase())) {
                return false;
            }
        }
        return true;
    }

    private boolean verificaProduto(ProdutoGeral produto) {
        List<ProdutoGeral> produtos = new ProdutoGeralDao().findAll();

        for (ProdutoGeral prod : produtos) {
            if (prod.getDescricao().toLowerCase().equals(produto.getDescricao().toLowerCase())) {
                return false;
            }
        }
        return true;
    }

    private Funcionarios funcionario;
    private List<Funcionarios> listaFuncionarios;
    private Funcionarios gerente;

    public Funcionarios getGerente() {
        return gerente;
    }

    public void setGerente(Funcionarios gerente) {
        this.gerente = gerente;
    }

    public Funcionarios getFuncionario() {
        return funcionario;
    }

    public void setFuncionario(Funcionarios funcionario) {
        this.funcionario = funcionario;
    }

    public List<Funcionarios> getListaFuncionarios() {
        if (listaFuncionarios == null) {
            listaFuncionarios = new ArrayList<>();
        }
        return listaFuncionarios;
    }

    public void setListaFuncionarios(List<Funcionarios> listaFuncionarios) {
        this.listaFuncionarios = listaFuncionarios;
    }

    public void buscarFuncionariosAssunto() {
        setListaFuncionarios(null);
        listaFuncionarios = value.getFuncionarios();
        for (Funcionarios f : listaFuncionarios) {
            if (f.getFuncao().contains("GER")) {
                gerente = f;
                listaFuncionarios.remove(f);
                break;
            }
        }
    }

    public void limparFuncionarios(CloseEvent event) {
        setListaFuncionarios(null);
    }

    public void buscarFuncionariosProduto() {
        setListaFuncionarios(null);
        listaFuncionarios = produto.getAssunto().getFuncionarios();
        for (Funcionarios f : listaFuncionarios) {
            if (f.getFuncao().contains("GER")) {
                gerente = f;
                listaFuncionarios.remove(f);
                break;
            }
        }
    }
}
