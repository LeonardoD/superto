/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.enums;

/**
 * @author Rodrigo/Jhonatan
 */
public enum Meses {
    
    JANEIRO("Janeiro", 1),
    FEVEREIRO("Fevereiro", 2),
    MARÇO("Março", 3),
    ABRIL("Abril", 4),
    MAIO("Maio", 5),
    JUNHO("Junho", 6),
    JULHO("Julho", 7),
    AGOSTO("Agosto", 8),
    SETEMBRO("Setembro", 9),
    OUTUBRO("Outubro", 10),
    NOVEMBRO("Novembro", 11),
    DEZEMBRO("Dezembro", 12);
    
    private final String mes;
    private final int id;
    
    Meses(String mes, int id) {
        this.mes = mes;
        this.id = id;
    }

    public String getMes() {
        return mes;
    }

    public int getId() {
        return id;
    }
    
}
