/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.dao;

import br.com.bb.superto.model.Funcionarios;
import br.com.bb.superto.model.GroupMembers;
import br.com.bb.superto.model.Groups;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;

import java.util.ArrayList;
import java.util.List;

/**
 * @author maycon
 */
public class GroupMembersDAO extends GenericHibernateDAO<GroupMembers, Integer> {

    public List<GroupMembers> getGroupMembers(Groups group) {
        Criteria criteria = getSession().createCriteria(getPersistentClass());
        List<GroupMembers> list = criteria.add(Restrictions.eq("groups", group)).list();
        return list;
    }

    public List<GroupMembers> getGroupsByFunc(Funcionarios func) {
        Criteria criteria = getSession().createCriteria(getPersistentClass());
        criteria.add(Restrictions.eq("funcionario", func));
        return criteria.list();
    }

    public List<Funcionarios> getFuncionarioByGroup(Groups group) {
        List<GroupMembers> groupMembers = getGroupMembers(group);
        List<Funcionarios> funcionarios = new ArrayList<Funcionarios>();
        for (GroupMembers m : groupMembers) {
            funcionarios.add(m.getFuncionario());
        }
        return funcionarios;
    }

    public Integer deleteMemberFromGroup(Groups group, Funcionarios funcionario) {
        String query = "delete from GroupMembers where groups.id = :groupId and funcionario.matriculaNum = :matricula";
        Query createQuery = getSession().createQuery(query);
        createQuery.setInteger("groupId", group.getId());
        createQuery.setInteger("matricula", funcionario.getMatriculaNum());
        return createQuery.executeUpdate();
    }

    public Integer deleteMembersFromGroup(Groups group) {
        String query = "delete from GroupMembers where groups.id = :groupId";
        Query createQuery = getSession().createQuery(query);
        createQuery.setInteger("groupId", group.getId());
        return createQuery.executeUpdate();
    }

    public List<Funcionarios> getExists(Groups group) {
        if (group != null) {
            System.out.println(group.getId());
        }
        List<Funcionarios> list = (List<Funcionarios>) getSession().createQuery("from Funcionarios as func where exists(from GroupMembers as g where g.groups.id = :id and func.matriculaNum = g.funcionario.matriculaNum)")
                .setInteger("id", group.getId()).list();

        return list;
    }

    public List<Funcionarios> getNotExists(Groups group) {
        if (group != null) {
            System.out.println(group.getId());
        }
        List<Funcionarios> list = (List<Funcionarios>) getSession().createQuery("from Funcionarios as func where not exists(from GroupMembers as g where g.groups.id = :id and func.matriculaNum = g.funcionario.matriculaNum)")
                .setInteger("id", group.getId()).list();

        return list;
    }
}
