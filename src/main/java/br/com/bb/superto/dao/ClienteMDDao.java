package br.com.bb.superto.dao;

import br.com.bb.superto.model.ClienteMD;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Query;

/**
 * @author Jhemeson Mota
 */
public class ClienteMDDao extends GenericHibernateDAO<ClienteMD, Integer> {
    public List<ClienteMD> findComInfo(){
        Query q = getSession().createQuery("SELECT cmd FROM ClienteMD cmd WHERE cmd.orgao != ''");
        return q.list();
    }
    
//    public List<ClienteMD> findByPrefixo(String prefixo){
//        Query q = getSession().createQuery("SELECT cmd FROM clienteSeguridade cs INNER JOIN ClienteMD cmd ON cs.mci = cmd.mci");
//        return q.list();
//    }
    
    public ClienteMD findByMci(Integer mci) {
        Query q = getSession().createQuery("SELECT c FROM ClienteMD c WHERE c.mci = :mci");
        q.setParameter("mci", mci);
        return (ClienteMD) q.uniqueResult();
    }
    
}