/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.dao;

import br.com.bb.superto.model.Agencias;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import java.util.List;
import org.hibernate.Query;

/**
 * @author maycon
 */
public class AgenciasDao extends GenericHibernateDAO<Agencias, Integer> {

    public Agencias getAgenciaByPrefixo(Integer prefixo) {
        Criteria criteria = getSession().createCriteria(getPersistentClass());
        criteria.add(Restrictions.eq("prefixo", prefixo));
        criteria.setMaxResults(1);
        List list = criteria.list();
        if (list != null && !list.isEmpty()) {
            return (Agencias) list.get(0);
        }
        return null;
    }
    public List<Agencias> findByPrefixo(Integer prefixo) {
        Query q = getSession().createQuery("SELECT a FROM Agencias a where a.prefixo = "+prefixo);
        System.out.println(q);
        return (List<Agencias>)q.list();
    }
    
}
