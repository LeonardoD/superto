/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.dao;

import br.com.bb.superto.model.Destaques;
import br.com.bb.superto.model.Mercado;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * @author maycon
 */
public class DestaquesDao extends GenericHibernateDAO<Destaques, Integer> {

    public List<Destaques> getDestaquesIndex(int first, int last, boolean visivel, Mercado mercado) {
        Criteria criteria = getSession().createCriteria(getPersistentClass());
        criteria.add(Restrictions.eq("visivel", visivel));
        criteria.add(Restrictions.eq("category", mercado));
        criteria.setFirstResult(first);
        criteria.setMaxResults(last);
        return criteria.list();
    }

    public List<Destaques> getDestaquesByMercado(boolean visivel, Mercado mercado) {
        Criteria criteria = getSession().createCriteria(getPersistentClass());
        criteria.add(Restrictions.eq("visivel", visivel));
        criteria.add(Restrictions.eq("mercado", mercado));
        criteria.createAlias("operacaoCriacao", "op").addOrder(Order.desc("op.dataOperacao"));
        return criteria.list();
    }
    
    //METODO DEVE RETORNAR O ULTIMO DESTAQUE DE CADA MERCADO (SE EXISTIR) 
    public List<Destaques> getUltimoDestaqueByMercado() {
         Criteria criteria = getSession().createCriteria(getPersistentClass());

         return criteria.list();
    }
}
