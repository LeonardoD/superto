/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.dao;

import br.com.bb.superto.model.Responsavel;
import br.com.bb.superto.model.SubCategoria;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * @author Administrador
 */
public class ResponsavelDAO extends GenericHibernateDAO<Responsavel, Integer> {

    public Responsavel getResponsalveBySubCat(SubCategoria subCategoria) {
        List<Responsavel> buscarPor = null;
        try {
            buscarPor = findByCriteria(Restrictions.eq("subcategoria", subCategoria));
            if (buscarPor == null || buscarPor.isEmpty()) {
                return null;
            } else {
                return buscarPor.get(0);
            }
        } catch (Exception er) {
            er.printStackTrace();

        }
        return null;
    }

}
