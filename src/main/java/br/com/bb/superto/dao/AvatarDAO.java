/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.dao;

import br.com.bb.superto.model.Avatar;
import br.com.bb.superto.model.Funcionarios;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * @author maycon
 */
public class AvatarDAO extends GenericHibernateDAO<Avatar, Integer> implements java.io.Serializable {

    public List<Avatar> getByFmatricula(Funcionarios func) {
        Criteria criteria = getSession().createCriteria(getPersistentClass());
        criteria.add(Restrictions.eq("matricula", func));
        return criteria.list();
    }

    public List<Avatar> getByFmatricula(Integer matricula) {
        Criteria criteria = getSession().createCriteria(getPersistentClass());
        criteria.createAlias("matricula", "a");
        criteria.add(Restrictions.eq("a.matriculaNum", matricula));
        return criteria.list();
    }

}
