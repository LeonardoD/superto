package br.com.bb.superto.dao;

import br.com.bb.superto.model.Macro;
import org.hibernate.Query;

/**
 * @author Jhemeson
 */
public class MacrosDao extends GenericHibernateDAO<Macro, Integer> {
    public Macro getMacroByRelatorio(String relatorio) {
        Query q = getSession().createQuery("SELECT d FROM Macro d WHERE d.Relatorio = :relatorio");
        q.setParameter("relatorio", relatorio);
        return (Macro) q.uniqueResult();
    }
}
