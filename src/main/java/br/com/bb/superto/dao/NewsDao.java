package br.com.bb.superto.dao;

import br.com.bb.superto.model.Mercado;
import br.com.bb.superto.model.News;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * @author maycon
 */
public class NewsDao extends GenericHibernateDAO<News, Integer> {

    public List<News> getNews(int first, int last) {

        Query q = getSession().createQuery("SELECT a FROM News a WHERE a.visible = TRUE ORDER BY a.operacaoCriacao.dataOperacao DESC");
        q.setFirstResult(first);
        q.setMaxResults(last);

        return q.list();
    }

    public int getRowCountFiltro(boolean visivel, Mercado mercado) {
        Criteria criteria = getSession().createCriteria(getPersistentClass());
        criteria.add(Restrictions.eq("visible", visivel));
        criteria.add(Restrictions.eq("mercado", mercado));
        criteria.setProjection(Projections.rowCount());
        Integer rowCount = ((Long) criteria.uniqueResult()).intValue();
        return rowCount;
    }

    public List<News> getNewsByMercado(boolean visivel, Mercado mercado) {
        Criteria criteria = getSession().createCriteria(getPersistentClass());
        criteria.add(Restrictions.eq("visible", visivel));
        criteria.add(Restrictions.eq("mercado", mercado));
        criteria.createAlias("operacaoCriacao", "op").addOrder(Order.desc("op.dataOperacao"));
        criteria.setMaxResults(10);
        return criteria.list();
    }
    
    public List<News> getUltimasNoticias() {

        Query q = getSession().createQuery("SELECT a FROM News a ORDER BY a.id DESC");
        q.setMaxResults(6);
        return q.list();
    }
    
    public List<News> getUltimasNoticiasPaginacao() {

        Query q = getSession().createQuery("SELECT a FROM News a ORDER BY a.id DESC");
        q.setMaxResults(8);
        return q.list();
    }

}
