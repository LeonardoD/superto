package br.com.bb.superto.dao;

import br.com.bb.superto.model.Bloco;
import br.com.bb.superto.model.ClienteProduto;
import java.util.List;
import org.hibernate.Query;
/**
 * @author Jhemeson S. Mota
 */
public class BlocoDao extends GenericHibernateDAO<Bloco, Integer> {
    
    public List<Bloco> findTodos() {
        Query q = getSession().createQuery("SELECT c FROM Bloco c ORDER BY c.nome");
        return q.list();
    }
    
    public Bloco findByNome(String nome){
        Query q = getSession().createQuery("SELECT b FROM Bloco b WHERE b.nome = :nome");
        q.setParameter("nome", nome);
        return (Bloco) q.uniqueResult();
    }
}
