/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.dao;

import br.com.bb.superto.model.UserHistory;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import org.hibernate.Query;

/**
 * @author maycon
 */
public class UserHistoryDao extends GenericHibernateDAO<UserHistory, Integer>{

    //retorna numero de acessos do dia
    public long getAcessosDoDia() {
        Date diaAtual = new Date();
        diaAtual.setHours(00);
        diaAtual.setMinutes(00);
        Query q = getSession().createQuery("SELECT COUNT(*) FROM UserHistory u WHERE u.loginDate >= :data");
        q.setParameter("data", diaAtual);

        return (long) q.uniqueResult();
    }
    
    public List<UserHistory> getTodosAcessosDoDia() {
        Date diaAtual = new Date();
        diaAtual.setHours(00);
        diaAtual.setMinutes(00);
        Query q = getSession().createQuery("SELECT u FROM UserHistory u WHERE u.loginDate >= :data");
        q.setParameter("data", diaAtual);

        return q.list();
    }
    
    public long contaTodos(){
        Query q = getSession().createQuery("SELECT COUNT(*) FROM UserHistory");
        return (long) q.uniqueResult();
    }

    //retorna numero de acessos por funcionario informado
    public List<UserHistory> getAcessosPorFuncionario(Integer funcionario, Date inicio, Date fim) {
//        SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy HH:mm"); //locahost
        SimpleDateFormat formato = new SimpleDateFormat("MM/dd/yyyy HH:mm"); //produção
        String i = formato.format(inicio);
        String f = formato.format(fim);

        Query q = getSession().createQuery("SELECT u FROM UserHistory u WHERE u.user = " + funcionario + " AND u.loginDate >= \'" + i + "\' AND loginDate <= \'" + f + "\'");
        System.err.println(q);
        System.err.println(q.list());
        return (List<UserHistory>) q.list();
    }

    public long getAcessosPorIntervaloDeDatas(Date inicio, Date fim) {
//        SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy HH:mm"); //locahost
        SimpleDateFormat formato = new SimpleDateFormat("MM/dd/yyyy HH:mm"); //produção
        String i = formato.format(inicio);
        String f = formato.format(fim);

        Query q = getSession().createQuery("SELECT count(*) FROM UserHistory u WHERE u.loginDate >= \'" + i + "\' AND loginDate <= \'" + f + "\'");
        System.err.println(q);
        System.err.println(q.list());
        return (long) q.uniqueResult();
    }

    public List<UserHistory> getAcessosPorMes(Date inicio, Date fim) {
//        SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy HH:mm"); //locahost
        SimpleDateFormat formato = new SimpleDateFormat("MM/dd/yyyy HH:mm"); //produção
        String i = formato.format(inicio);
        String f = formato.format(fim);

        Query q = getSession().createQuery("SELECT u FROM UserHistory u WHERE u.loginDate >= \'" + i + "\' AND loginDate <= \'" + f + "\'");
        System.err.println(q);
        System.err.println(q.list());
        return (List<UserHistory>) q.list();
    }

    //retorna numero de acessos por prefixo informado
    public List<UserHistory> getAcessosPorPrefixo(Integer prefixo, Date inicio, Date fim) {
//        SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy HH:mm"); //locahost
        SimpleDateFormat formato = new SimpleDateFormat("MM/dd/yyyy HH:mm"); //produção
        String i = formato.format(inicio);
        String f = formato.format(fim);

        Query q = getSession().createQuery("SELECT u FROM UserHistory u WHERE u.user.prefixo = " + prefixo + " AND u.loginDate >= \'" + i + "\' AND loginDate <= \'" + f + "\'");
        return (List<UserHistory>) q.list();
    }

    public int getQtdAcessosDoDia(Date dia) {
        Date diaInicio = new Date();
        Date diaFim = new Date();

        diaInicio.setDate(dia.getDate());
        diaInicio.setMonth(dia.getMonth());
        diaInicio.setYear(dia.getYear());
        diaInicio.setHours(00);
        diaInicio.setMinutes(00);

        diaFim.setDate(dia.getDate());
        diaFim.setMonth(dia.getMonth());
        diaFim.setYear(dia.getYear());
        diaFim.setHours(23);
        diaFim.setMinutes(59);

        Query q = getSession().createQuery("SELECT u FROM UserHistory u WHERE u.loginDate >= \'" + diaInicio + "\' AND u.loginDate <= \'" + diaFim + "\'");

        return q.list().size();
    }
   
    public int getQtdFuncsDoDia(Date dia) {
        Date diaInicio = new Date();
        Date diaFim = new Date();

        diaInicio.setDate(dia.getDate());
        diaInicio.setMonth(dia.getMonth());
        diaInicio.setYear(dia.getYear());
        diaInicio.setHours(00);
        diaInicio.setMinutes(00);

        diaFim.setDate(dia.getDate());
        diaFim.setMonth(dia.getMonth());
        diaFim.setYear(dia.getYear());
        diaFim.setHours(23);
        diaFim.setMinutes(59);

        Query q = getSession().createQuery("SELECT DISTINCT(u.user.matriculaNum) FROM UserHistory u WHERE u.loginDate >= \'" + diaInicio + "\' AND u.loginDate <= \'" + diaFim + "\'");
        return q.list().size();
    }
    
    public int getQtdFuncsDoMes(Date inicio, Date fim) {
//      SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy HH:mm"); //locahost
        SimpleDateFormat formato = new SimpleDateFormat("MM/dd/yyyy HH:mm"); //produção
        String i = formato.format(inicio);
        String f = formato.format(fim);

        Query q = getSession().createQuery("SELECT DISTINCT(u.user.matriculaNum) FROM UserHistory u WHERE u.loginDate >= \'" + i + "\' AND u.loginDate <= \'" + f + "\'");
        return q.list().size();
    }
}