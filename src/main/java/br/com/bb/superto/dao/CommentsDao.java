/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.dao;

import br.com.bb.superto.model.Category;
import br.com.bb.superto.model.Comments;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * @author maycon
 */
public class CommentsDao extends GenericHibernateDAO<Comments, Integer> {

    public List<Comments> getComentarios(Category category, Integer id) {
        Criteria criteria = getSession().createCriteria(getPersistentClass());
        criteria.addOrder(Order.desc("datePublication"));
        criteria.add(Restrictions.eq("category", category));
        criteria.add(Restrictions.eq("sourceId", id));
        return criteria.list();
    }

    public Integer getQuantidadeDeComentarios(Category category, Integer id) {
        Criteria criteria = getSession().createCriteria(getPersistentClass());
        criteria.add(Restrictions.eq("category", category));
        criteria.add(Restrictions.eq("sourceId", id));
        criteria.setProjection(Projections.rowCount());
        Integer rowCount = ((Long) criteria.uniqueResult()).intValue();
        return rowCount;
    }

    public Integer deletarComentario(Comments comentario) {
        String query = "delete from Comments where id = :id";
        Query createQuery = getSession().createQuery(query);
        createQuery.setInteger("id", comentario.getId());
        return createQuery.executeUpdate();
    }
}
