/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.dao;

import br.com.bb.superto.model.Assunto;
import java.io.Serializable;
import java.util.List;
import org.hibernate.Query;


/**
 * @author filipe
 */
public class AssuntoDao extends GenericHibernateDAO<Assunto, Serializable>{

    @Override
    public List<Assunto> findAll(){
        Query q = getSession().createQuery("SELECT a FROM Assunto a ORDER BY a.descricao ASC");
        return (List<Assunto>) q.list();
    }

    public List<Assunto> findByMercado(Integer mercado) {
        Query q = getSession().createQuery("SELECT a FROM Assunto a where a.mercadogeral.ID = "+mercado+" ORDER BY a.id ASC");
        return (List<Assunto>) q.list();
    }
}
