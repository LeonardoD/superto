/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.dao;

import br.com.bb.superto.model.ClienteLob;
import org.hibernate.Query;
import java.io.Serializable;
import java.util.List;

/**
 * @author administrador
 */
public class ClienteLobDao extends GenericHibernateDAO<ClienteLob, Serializable> {

    public ClienteLob findByMci(Integer mci) {
        Query q = getSession().createQuery("SELECT c FROM ClienteLob c WHERE c.mci = :mci");
        q.setParameter("mci", mci);
        return (ClienteLob) q.uniqueResult();
    }
    
    public List<ClienteLob> findByPrefixo (String prefixo) {
        Query q = getSession().createQuery("SELECT c FROM ClienteLob c WHERE c.prefixo = :prefixo");
        q.setParameter("prefixo", prefixo);
        return q.list();
    }
}
