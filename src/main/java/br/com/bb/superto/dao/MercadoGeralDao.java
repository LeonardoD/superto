/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.dao;

import br.com.bb.superto.model.MercadoGeral;
import java.util.List;
import org.hibernate.Query;

import java.io.Serializable;

/**
 * @author filipe
 */
public class MercadoGeralDao extends GenericHibernateDAO<MercadoGeral, Serializable> {

    public MercadoGeral findByDescricao(String descricao) {
        Query q = getSession().createQuery("SELECT p FROM MercadoGeral p WHERE p.descricao = :descricao");
        q.setParameter("descricao", descricao);
        return (MercadoGeral) q.uniqueResult();
    }
    
    @Override
    public List<MercadoGeral> findAll(){
        Query q = getSession().createQuery("SELECT a FROM MercadoGeral a ORDER BY a.id ASC");
        return (List<MercadoGeral>) q.list();
    }
}
