package br.com.bb.superto.dao;

import br.com.bb.superto.model.Thing;
import java.util.List;
import org.hibernate.Query;
/**
 * @author Jhemeson S. Mota
 */
public class ThingDao extends GenericHibernateDAO<Thing, Integer> {

    public List<Thing> findFiltradosCliente(String cpf) {
            Query q = getSession().createQuery("SELECT t FROM Thing t WHERE t.cpf = :cpf");
            q.setParameter("cpf", cpf);
            return q.list();
    }
    
    public long countFiltradosCliente(String cpf) {
            Query q = getSession().createQuery("SELECT COUNT(*) FROM Thing t WHERE t.cpf = :cpf");
            q.setParameter("cpf", cpf);
            return (long) q.uniqueResult();
    }
}
