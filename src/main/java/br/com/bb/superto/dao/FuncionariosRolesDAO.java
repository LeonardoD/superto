/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.dao;

import br.com.bb.superto.model.FuncionarioRole;
import br.com.bb.superto.model.Funcionarios;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * @author maycon
 */
public class FuncionariosRolesDAO extends GenericHibernateDAO<FuncionarioRole, Integer> {

    public List<FuncionarioRole> getRolesByFuncionario(Funcionarios func) {
        Criteria criteria = getSession().createCriteria(getPersistentClass());
        criteria.add(Restrictions.eq("funcionario", func));
        return criteria.list();
    }
}
