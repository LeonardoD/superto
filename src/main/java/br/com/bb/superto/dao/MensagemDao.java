/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.dao;

import br.com.bb.superto.model.Funcionarios;
import br.com.bb.superto.model.Mensagens;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * @author maycon
 */
public class MensagemDao extends GenericHibernateDAO<Mensagens, Integer> implements java.io.Serializable {

    public List<Mensagens> getMensagensNaoLidas(Funcionarios funcionario, Integer qtd, boolean all) {
        Criteria criteria = getSession().createCriteria(getPersistentClass());
        criteria.addOrder(Order.desc("dataDeEnvio"));
        criteria.add(Restrictions.eq("destinatario", funcionario));
        criteria.add(Restrictions.eq("visualizada", false));
        if (!all) {
            criteria.setMaxResults(qtd);
        }
        return criteria.list();
    }

    public List<Mensagens> getMensagens(Funcionarios func, Integer first, Integer max, String query) {
        Criteria criteria = getSession().createCriteria(getPersistentClass());
        criteria.addOrder(Order.desc("dataDeEnvio"));
        criteria.add(Restrictions.eq("destinatario", func));
        if (query != null && !query.isEmpty()) {
            System.out.println("Estamos pesquisando...");
            criteria.add(Restrictions.like("mensagem", "%" + query + "%"));
        }
        criteria.setFirstResult(first);
        criteria.setMaxResults(max);
        return criteria.list();
    }

    public Integer rowCount(Funcionarios func, String query) {
        Criteria criteria = getSession().createCriteria(getPersistentClass());
        criteria.add(Restrictions.eq("destinatario", func));
        if (query != null && !query.isEmpty()) {
            criteria.add(Restrictions.like("mensagem", "%" + query + "%"));
        }
        criteria.setProjection(Projections.rowCount());
        Integer rowCount = ((Long) criteria.uniqueResult()).intValue();
        return rowCount;
    }
}
