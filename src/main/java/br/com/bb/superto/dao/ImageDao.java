/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.dao;

import br.com.bb.superto.model.Image;
import br.com.bb.superto.model.Mercado;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * @author maycon
 */
public class ImageDao extends GenericHibernateDAO<Image, Integer> {

    public List<Image> getImages(int first, int last, boolean filter, Mercado category) {
        Criteria criteria = getSession().createCriteria(getPersistentClass());
        criteria.add(Restrictions.eq("visible", true));
        if (filter) {
            criteria.add(Restrictions.eq("category", category.getLabel()));
        }
        criteria.setFirstResult(first);
        criteria.setMaxResults(last);
        return criteria.list();
    }
}
