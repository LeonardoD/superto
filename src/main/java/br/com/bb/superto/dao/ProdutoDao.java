/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.dao;

import br.com.bb.superto.model.Produto;
import java.util.List;
import org.hibernate.Query;

import java.io.Serializable;

/**
 * @author filipe
 */
public class ProdutoDao extends GenericHibernateDAO<Produto, Serializable> {

    public Produto findByDescricao(String descricao) {
        Query q = getSession().createQuery("SELECT p FROM Produto p WHERE p.descricao = :descricao");
        q.setParameter("descricao", descricao);
        return (Produto) q.uniqueResult();
    }
    
    @Override
    public List<Produto> findAll(){
        Query q = getSession().createQuery("SELECT a FROM Produto a ORDER BY a.id ASC");
        return (List<Produto>) q.list();
    }
}
