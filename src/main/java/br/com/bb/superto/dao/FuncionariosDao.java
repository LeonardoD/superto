/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.dao;

import br.com.bb.superto.model.Avatar;
import br.com.bb.superto.model.Funcionarios;
import br.com.bb.superto.model.Mercado;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import java.time.LocalDate;
import java.util.List;

/**
 * @author maycon
 */
public class FuncionariosDao extends GenericHibernateDAO<Funcionarios, Integer> {

    public Funcionarios getByFmatricula(String fmatricula) {
        List<Funcionarios> findByCriteria = findByCriteria(Restrictions.eq("fmatricula", fmatricula.toUpperCase()));
        try {
            if (findByCriteria == null || findByCriteria.isEmpty()) {
                return null;
            }
        } catch (Exception e) {
        }
        return findByCriteria.get(0);
    }
    
    public List<Funcionarios> findByStatus() {
        Query q = getSession().createQuery("SELECT f FROM Funcionarios f where f.status = TRUE ORDER BY f.nome ASC");
        return (List<Funcionarios>)q.list();
    }

    public Funcionarios findByName(String nome) {
        List<Funcionarios> findByCriteria = findByCriteria(Restrictions.eq("nome", nome));
        if (findByCriteria == null || findByCriteria.isEmpty()) {
            return null;
        }
        return findByCriteria.get(0);

    }

    public List<Funcionarios> getNotExist() {
        List<Funcionarios> list;

        list = findByCriteria(Restrictions.eq("mercado", null));

        return list;
    }

    public List<Funcionarios> getExist(Mercado categoria) {
        List<Funcionarios> list = null;
        if (categoria != null) {
            list = findByCriteria(Restrictions.eq("categoria", categoria));
        }
        return list;
    }

    public List<Funcionarios> pesquisarFuncionarios(String query) {
        Criteria criteria = getSession().createCriteria(getPersistentClass());
        criteria.addOrder(Order.desc("nome").ignoreCase());
        criteria.add(Restrictions.like("nome", query + "%"));
        return criteria.list();
    }

    public Integer updateAvatarFuncionarios(Avatar avatar, Funcionarios func) {
        String query = "update Funcionarios set avatar = :avatarId where id = :id";
        Query createQuery = getSession().createQuery(query);
        createQuery.setEntity("avatarId", avatar);
        createQuery.setInteger("id", func.getId());
        return createQuery.executeUpdate();
    }

    public List<Funcionarios> getAniversariantesDoMes() {
        LocalDate diaAtual = LocalDate.now();
        Query q = getSession().createQuery("SELECT a FROM Funcionarios a WHERE a.status=TRUE and (SELECT  EXTRACT(DAY FROM datanascimento) AS x FROM Funcionarios b WHERE b.matriculaNum = a.matriculaNum)=:dia AND (SELECT  EXTRACT(MONTH FROM datanascimento) AS x FROM Funcionarios b WHERE b.matriculaNum = a.matriculaNum)=:mes ORDER BY dataNascimento");
        q.setParameter("dia", diaAtual.getDayOfMonth());
        q.setParameter("mes", diaAtual.getMonthValue());
        return q.list();
    }
    
    public List<Funcionarios> findByPrefixo() {
        Query q = getSession().createQuery("SELECT a FROM Funcionarios a where a.prefixo = 8517 ORDER BY a.nome ASC");
        return (List<Funcionarios>) q.list();
    }
}
