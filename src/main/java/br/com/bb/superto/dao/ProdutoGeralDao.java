/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.dao;

import br.com.bb.superto.model.ProdutoGeral;
import java.io.Serializable;
import java.util.List;
import org.hibernate.Query;

/**
 * @author filipe
 */
public class ProdutoGeralDao extends GenericHibernateDAO<ProdutoGeral, Serializable> {

    @Override
    public List<ProdutoGeral> findAll() {
        Query q = getSession().createQuery("SELECT a FROM ProdutoGeral a ORDER BY a.id ASC");
        return (List<ProdutoGeral>) q.list();
    }
    
    public List<ProdutoGeral> findByAssunto(Integer assunto) {
        Query q = getSession().createQuery("SELECT a FROM ProdutoGeral a where a.assunto.ID = "+assunto+" ORDER BY a.id ASC");
        return (List<ProdutoGeral>) q.list();
    }
    
}
