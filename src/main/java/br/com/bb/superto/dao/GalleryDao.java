/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.dao;

import br.com.bb.superto.model.Gallery;
import br.com.bb.superto.model.Mercado;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import java.util.HashMap;
import java.util.List;

/**
 * @author maycon
 */
public class GalleryDao extends GenericHibernateDAO<Gallery, Integer> {

    public List<Gallery> getNews(Mercado category) {
        Criteria criteria = getSession().createCriteria(getPersistentClass());
        criteria.addOrder(Order.desc("publicationDate"));
        criteria.add(Restrictions.eq("visible", true));
        criteria.add(Restrictions.eq("category", category));
        return criteria.list();
    }

    public List<Gallery> getGallery(Integer id) {
        Criteria criteria = getSession().createCriteria(getPersistentClass());
        criteria.add(Restrictions.eq("id", id));
        return criteria.list();
    }

    public Gallery findByName(String title) {
        List<Gallery> findByCriteria = findByCriteria(Restrictions.eq("title", title));
        if (findByCriteria == null || findByCriteria.isEmpty()) {
            return null;
        }
        return findByCriteria.get(0);

    }

    public HashMap<Integer, Gallery> getGallerys() {
        HashMap<Integer, Gallery> g = new HashMap<Integer, Gallery>();
        List<Gallery> findAll = findAll();
        for (Gallery f : findAll) {
            g.put(f.getId(), f);
        }
        return g;
    }

    public List<Gallery> getGalerias(int first, int last, boolean filter, Mercado category) {
        Criteria criteria = getSession().createCriteria(getPersistentClass());
        criteria.addOrder(Order.desc("publicationDate"));
        criteria.add(Restrictions.eq("visible", true));
        if (filter) {
            criteria.add(Restrictions.eq("category", category));
        }
        criteria.setFirstResult(first);
        criteria.setMaxResults(last);
        return criteria.list();
    }
}
