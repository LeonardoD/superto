/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.dao;

import br.com.bb.superto.model.Eventos;
import br.com.bb.superto.model.Mercado;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;

import java.util.List;

public class EventosDao extends GenericHibernateDAO<Eventos, Integer> {

    public List<Eventos> getEventos(int first, int last, boolean filter, Mercado category) {
        Criteria criteria = getSession().createCriteria(getPersistentClass());
        criteria.addOrder(Order.desc("dataDePublicacao"));
        criteria.setFirstResult(first);
        criteria.setMaxResults(last);
        return criteria.list();
    }
}
