package br.com.bb.superto.dao;

import br.com.bb.superto.model.Fundos;
import java.util.List;
import org.hibernate.Query;
/**
 * @author Jhemeson S. Mota
 */
public class FundosDao extends GenericHibernateDAO<Fundos, Integer> {
    
    public List<Fundos> findTodos() {
        Query q = getSession().createQuery("SELECT f FROM Fundos f WHERE f.nomefundo != null AND f.situacao = true AND f.taxaadm != null ORDER BY f.id DESC");
        return q.list();
    }
    
    public List<Fundos> findFiltradosSubsequente(String segmento, double valor) {
        if(segmento.equalsIgnoreCase("Todos")){
            Query q = getSession().createQuery("SELECT f FROM Fundos f WHERE f.segmento <> '0' AND f.aplicacaosubs <= :valor AND f.situacao=true ORDER BY f.taxaadm DESC");
            q.setParameter("valor", valor);
            return q.list();
        }
        if(segmento.equalsIgnoreCase("Pessoa Fisica")){
            Query q = getSession().createQuery("SELECT f FROM Fundos f WHERE f.segmento <> 'Pessoa Juridica' AND f.segmento <> '0' AND f.aplicacaosubs <= :valor AND f.situacao=true ORDER BY f.taxaadm DESC");
            q.setParameter("valor", valor);
            return q.list();
        }
        else{
            Query q = getSession().createQuery("SELECT f FROM Fundos f WHERE f.segmento <> 'Pessoa Fisica' AND f.segmento <> '0' AND f.aplicacaosubs <= :valor AND f.situacao=true ORDER BY f.taxaadm DESC");
            q.setParameter("valor", valor);
            return q.list();
        }
    }
    
    public List<Fundos> findFiltradosInicial(String segmento, double valor) {
        if(segmento.equalsIgnoreCase("Todos")){
            Query q = getSession().createQuery("SELECT f FROM Fundos f WHERE f.aplicacaoinicial <= :valor AND f.situacao=true ORDER BY f.taxaadm DESC");
            q.setParameter("valor", valor);
            return q.list();
        }
        if(segmento.equalsIgnoreCase("Pessoa Fisica")){
            Query q = getSession().createQuery("SELECT f FROM Fundos f WHERE f.segmento <> 'Pessoa Juridica' AND f.segmento <> '0' AND f.aplicacaoinicial <= :valor AND f.situacao=true ORDER BY f.taxaadm DESC");
            q.setParameter("valor", valor);
            return q.list();
        }
        else{
            Query q = getSession().createQuery("SELECT f FROM Fundos f WHERE f.segmento <> 'Pessoa Fisica' AND f.segmento <> '0' AND f.aplicacaoinicial <= :valor AND f.situacao=true ORDER BY f.taxaadm DESC");
            q.setParameter("valor", valor);
            return q.list();
        }
    }

    public Fundos findByNome(String nome){
        Query q = getSession().createQuery("SELECT f FROM Fundos f WHERE f.nomefundo = :nome AND f.situacao=true");
        q.setParameter("nome", nome);
        return (Fundos) q.uniqueResult();
    }

    public List<String> findSegmentos() {
        Query q = getSession().createQuery("SELECT f.segmento FROM Fundos f WHERE f.situacao=true GROUP BY f.segmento");
        return q.list();
    }
    
    public List<String> findFundos() {
        Query q = getSession().createQuery("SELECT f.nomefundo FROM Fundos f WHERE f.situacao=true GROUP BY f.nomefundo");
        return q.list();
    }
}
