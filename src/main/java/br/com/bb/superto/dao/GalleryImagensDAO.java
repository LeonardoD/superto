/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.dao;

import br.com.bb.superto.model.Gallery;
import br.com.bb.superto.model.GalleryImages;
import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import java.util.List;
import org.hibernate.Query;

/**
 * @author maycon
 */
public class GalleryImagensDAO extends GenericHibernateDAO<GalleryImages, Integer> {

    public List<GalleryImages> getImagensFromAlbum(Gallery album) {
        Criteria criteria = getSession().createCriteria(getPersistentClass());
        criteria.add(Restrictions.eq("gallery", album));
        return criteria.list();
    }

    public void deleteGaleria(Gallery gallery){
        int I = gallery.getId();
        
        Query createQuery1 = getSession().createQuery("delete from gallery_images as a where a.gallery_id = :id");
        createQuery1.setInteger("id", I);
        createQuery1.executeUpdate();
            
        Query createQuery2 = new GalleryImagensDAO().getSession().createQuery("delete from gallery_operacao as b where b.gallery_id = :id");
        createQuery2.setInteger("id", I);
        createQuery2.executeUpdate();
        
        Query createQuery = getSession().createQuery("delete from gallery as g where g.id = :id");
        createQuery.setInteger("id", I);
        createQuery.executeUpdate();
    }
    
    public Integer contarImagens(Gallery gallery) {
        Criteria criteria = getSession().createCriteria(getPersistentClass());
        criteria.add(Restrictions.eq("gallery", gallery));
        criteria.setProjection(Projections.rowCount());
        Integer rowCount = ((Long) criteria.uniqueResult()).intValue();
        return rowCount;
    }
}
