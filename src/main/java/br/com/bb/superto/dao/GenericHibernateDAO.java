/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.dao;

import br.com.bb.superto.model.Operacao;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.LockOptions;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Projections;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Maycon
 * @param <T>
 * @param <ID>
 */
public class GenericHibernateDAO<T, ID extends Serializable> implements GenericDAO<T, ID> {

    private Class<T> persistentClass;
    private Session session;

    public GenericHibernateDAO() {
        this.persistentClass = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
        this.session = HibernateUtil.getSessionFactory().getCurrentSession();
    }

    public GenericHibernateDAO(Class<T> persistentClass) {
        this.persistentClass = persistentClass;
        this.session = HibernateUtil.getSessionFactory().getCurrentSession();
    }

    public Session getSession() {
        if (session == null) {
            throw new IllegalStateException("Session error");
        }
        if (!session.isOpen()) {
//            session = HibernateUtil.getSessionFactory().getCurrentSession();
        }
        return session;
    }

    public Class<T> getPersistentClass() {
        return persistentClass;
    }

    @SuppressWarnings("unchecked")
    @Override
    public T findById(ID id, boolean lock) {
        T entity = null;
        try {
            if (lock) {
                entity = (T) getSession().load(getPersistentClass(), id, LockOptions.UPGRADE);
            } else {

                entity = (T) getSession().load(getPersistentClass(), id);
            }
        } catch (Exception ex) {
            return null;
        }
        return entity;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<T> findAll() {
        return findByCriteria();
    }

    @Override
    public T makePersistent(T entity) {
        try {
            getSession().saveOrUpdate(entity);
            return entity;
        } catch (Exception e) {
            System.out.println("Erro ao atualizar ou salvar: " + e.getMessage());
            return null;
        }

    }

    public List<T> makePersistentList(List<T> entities) {
        List<T> list = new ArrayList<>();
        T aux;
        for (T entity : entities) {
            aux = makePersistent(entity);
            if (aux != null) {
                list.add(aux);
            } else {
                Logger.getLogger(persistentClass.getName()).log(Level.SEVERE, "Objeto não atualizado ---> ", entity.toString());
            }
        }
        return list;

    }

    public void update(T entity) {
        getSession().merge(entity);
    }

    public T save(T entity) throws HibernateException {
        try {
            getSession().save(entity);
            return entity;
        } catch (HibernateException e) {
            throw e;
        }
    }

    public T save(T entity, Operacao operacao) throws HibernateException {
        try {
            getSession().save(entity);
            getSession().save(operacao);
            return entity;
        } catch (HibernateException e) {
            throw e;
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public T makeTransient(T entity) {
        try {
            getSession().delete(entity);
            return entity;
        } catch (Exception e) {
            return null;
        }
    }

    public void removeAll(List<T> list) {
        list.forEach(element -> {
            getSession().delete(element);
        });
    }

    public void flush() {
        getSession().flush();
    }

    public void clear() {
        getSession().clear();
    }

    @SuppressWarnings("unchecked")
    public List<T> findByCriteria(Criterion... criterion) {
        Criteria criteria = getSession().createCriteria(getPersistentClass());
        for (Criterion c : criterion) {
            criteria.add(c);
        }
        return criteria.list();
    }

    @SuppressWarnings("unchecked")
    public List<T> findByCriteria2(int first, int size, Criterion... criterion) {
        Criteria criteria = getSession().createCriteria(getPersistentClass());
        for (Criterion c : criterion) {
            criteria.add(c);
        }
        criteria.setFirstResult(first);
        criteria.setMaxResults(size);
        return criteria.list();
    }

    public int getRowCount2(Criterion... criterion) {
        Criteria criteria = getSession().createCriteria(getPersistentClass());
        for (Criterion c : criterion) {
            criteria.add(c);
        }
        criteria.setProjection(Projections.rowCount());
        Integer rowCount = ((Long) criteria.uniqueResult()).intValue();
        return rowCount;

    }

    @Override
    public List<T> findAll(int size, int first) {
        Criteria criteria = getSession().createCriteria(getPersistentClass());
        criteria.setFirstResult(first);
        criteria.setMaxResults(size);
        return criteria.list();
    }

    public int getRowCount() {
        Criteria criteria = getSession().createCriteria(getPersistentClass());
        criteria.setProjection(Projections.rowCount());
        Integer rowCount = ((Long) criteria.uniqueResult()).intValue();
        return rowCount;
    }

    @Override
    public void saveList(List<T> entities) {
        for (T entity : entities) {
            entity = save(entity);
        }
    }

    public List<T> saveListReturn(List<T> entities) {
        List<T> list = new ArrayList<>();
        T aux;
        for (T entity : entities) {
            aux = save(entity);
            if (aux != null) {
                list.add(aux);
            } else {
                Logger.getLogger(persistentClass.getName()).log(Level.SEVERE, "Objeto não adicionado ---> ", entity.toString());
            }
        }
        return list;
    }
}
