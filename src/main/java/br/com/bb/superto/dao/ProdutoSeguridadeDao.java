/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.dao;

import br.com.bb.superto.model.ProdutoSeguridade;
import java.util.List;
import org.hibernate.Query;

/**
 * @author Jhemeson S. Mota
 */
public class ProdutoSeguridadeDao extends GenericHibernateDAO<ProdutoSeguridade, Integer> {

    public List<ProdutoSeguridade> findLista(){
        Query q = getSession().createQuery("SELECT a FROM ProdutoSeguridade As a ORDER BY a.nomeProduto ASC");
        return (List<ProdutoSeguridade>) q.list();
    }
    
    public static ProdutoSeguridade findByCodProduto() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public ProdutoSeguridade findByCodProduto(Integer cod) {
        Query q = getSession().createQuery("SELECT p FROM ProdutoSeguridade p WHERE p.codProduto = :cod");
        q.setParameter("cod", cod);
        return (ProdutoSeguridade) q.uniqueResult();
    }
    
    public ProdutoSeguridade findByNome(String nome) {
        Query q = getSession().createQuery("SELECT p FROM ProdutoSeguridade p WHERE p.nomeProduto = :nome");
        q.setParameter("nome", nome);
        return (ProdutoSeguridade) q.uniqueResult();
    }

}
