package br.com.bb.superto.dao;

import br.com.bb.superto.model.Curiosidade;
import org.hibernate.Query;

import java.util.List;

/**
 * @author Jhemeson
 */
public class CuriosidadesDao extends GenericHibernateDAO<Curiosidade, Integer> {
    public List<Curiosidade> getAtivos() {
        Query q = getSession().createQuery("SELECT a FROM Curiosidade a WHERE a.visible = TRUE ORDER BY a.operacaoCriacao.dataOperacao DESC");
        return q.list();
    }
}
