package br.com.bb.superto.dao;
import br.com.bb.superto.model.ClienteProduto;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.hibernate.Query;
import java.util.List;

/**
 * @author Jhemeson S. Mota
 */

public class ClienteProdutoDao extends GenericHibernateDAO<ClienteProduto, Integer> {
    
    public List<ClienteProduto> findByMci(Integer mci) {
        Query q = getSession().createQuery("SELECT c FROM ClienteProduto c WHERE c.mci = :mci");
        q.setParameter("mci", mci);
        return q.list();
    }

    public ClienteProduto findByCodProduto(Integer id) {
        Query q = getSession().createQuery("SELECT c FROM ClienteProduto c WHERE c.id = :id");
        q.setParameter("id", id);
        return (ClienteProduto) q.uniqueResult();
    }
    
    public List<ClienteProduto> cpDaDataCanc(Date dataSelecionada, Integer prefixo) {
//        SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy"); //locahost
        SimpleDateFormat formato = new SimpleDateFormat("MM/dd/yyyy"); //produção
        String f = formato.format(dataSelecionada);
        try
        {
            if(prefixo == 8517){
                Query q = getSession().createQuery("SELECT c FROM ClienteProduto c WHERE c.datacancelamento = \'" + f + "\' AND c.codproduto IS NOT NULL");
                return q.list();
            }
            else{
                Query q = getSession().createQuery("SELECT c FROM ClienteProduto c WHERE c.datacancelamento = \'" + f + "\' AND c.prefixo = " + prefixo + " AND c.codproduto IS NOT NULL");
                return q.list();
            }
        }
        catch(Exception ex)
        {
            System.out.print("Erro: " + ex);
            return null;
        } 
    }
    
    public List<ClienteProduto> cpDaData(Date dataSelecionada, Integer prefixo) {
//        SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy"); //locahost
        SimpleDateFormat formato = new SimpleDateFormat("MM/dd/yyyy"); //produção
        String f = formato.format(dataSelecionada);
        try
        {
            if(prefixo == 8517){
                Query q = getSession().createQuery("SELECT c FROM ClienteProduto c WHERE c.datavencimento = \'" + f + "\' AND c.codproduto IS NOT NULL");
                return q.list();
            }
            else{
                Query q = getSession().createQuery("SELECT c FROM ClienteProduto c WHERE c.datavencimento = \'" + f + "\' AND c.prefixo = " + prefixo + " AND c.codproduto IS NOT NULL");
                return q.list();
            }
        }
        catch(Exception ex)
        {
            System.out.print("Erro: " + ex);
            return null;
        } 
    }
    
        // se o parâmetro do método for 1, ele irá pegar os vencidos/vincendos que já foram contatados (1 == feito)
        // se o parâmetro do método for 0, ele irá pegar os vencidos/vincendos
    public long qtVincVenc(int param, Date dataSelecionada, int prefixo) {
//        SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy"); //locahost
        SimpleDateFormat formato = new SimpleDateFormat("MM/dd/yyyy"); //produção
        String f = formato.format(dataSelecionada);
        if(param == 0){
            try
            {
                if(prefixo == 8517){
                    Query q = getSession().createQuery("SELECT COUNT(*) FROM ClienteProduto c WHERE c.codproduto IS NOT NULL AND c.datavencimento = \'" + f + "\'");
                    return (long) q.uniqueResult();   
                }
                else{
                    Query q = getSession().createQuery("SELECT COUNT(*) FROM ClienteProduto c WHERE c.codproduto IS NOT NULL AND c.prefixo = " + prefixo + " AND c.datavencimento = \'" + f + "\'");
                    return (long) q.uniqueResult();
                }
            }
            catch(Exception ex)
            {
                System.out.print("Erro: " + ex);
                return 0;
            } 
        }
        else{
            try
            {
                if(prefixo == 8517){
                    Query q = getSession().createQuery("SELECT COUNT(*) FROM ClienteProduto c WHERE c.contatado = true AND c.datavencimento = \'" + f + "\'");
                    return (long) q.uniqueResult();   
                }
                else{
                    Query q = getSession().createQuery("SELECT COUNT(*) FROM ClienteProduto c WHERE c.prefixo = " + prefixo + " AND c.contatado = true AND c.datavencimento = \'" + f + "\'");
                    return (long) q.uniqueResult();
                }
            }
            catch(Exception ex)
            {
                System.out.print("Erro: " + ex);
                return 0;
            } 
        }
    }
    
        // se o parâmetro do método for 1, ele irá pegar os cancelados que já foram contatados (1 == feito)
        // se o parâmetro do método for 0, ele irá pegar os cancelados
    public long qtCanc(int param, Date dataSelecionada, int prefixo) {
//        SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy"); //locahost
        SimpleDateFormat formato = new SimpleDateFormat("MM/dd/yyyy"); //produção
        String f = formato.format(dataSelecionada);
        if(param == 0){
                    try
                    {
                        if(prefixo == 8517){
                            Query q = getSession().createQuery("SELECT COUNT(*) FROM ClienteProduto c WHERE c.codproduto IS NOT NULL AND c.datacancelamento = \'" + f + "\'");
                            return (long) q.uniqueResult();
                        }
                        else{
                            Query q = getSession().createQuery("SELECT COUNT(*) FROM ClienteProduto c WHERE c.codproduto IS NOT NULL AND c.prefixo = " + prefixo + " AND c.datacancelamento = \'" + f + "\'");
                            return (long) q.uniqueResult();    
                        }
                    }
                    catch(Exception ex)
                    {
                        System.out.print("Erro: " + ex);
                        return 0;
                    } 
        }
        else{
                    try
                    {
                        if(prefixo == 8517){
                            Query q = getSession().createQuery("SELECT COUNT(*) FROM ClienteProduto c WHERE c.contatado = true AND c.datacancelamento = \'" + f + "\'");
                            return (long) q.uniqueResult();    
                        }
                        else{
                            Query q = getSession().createQuery("SELECT COUNT(*) FROM ClienteProduto c WHERE c.prefixo = " + prefixo + " AND c.contatado = true AND c.datacancelamento = \'" + f + "\'");
                            return (long) q.uniqueResult();    
                        }
                        
                    }
                    catch(Exception ex)
                    {
                        System.out.print("Erro: " + ex);
                        return 0;
                    } 
        }
    }

    public List<Date> datasVinc(int prefixo){
        Query q = getSession().createQuery("SELECT cp.datavencimento FROM ClienteProduto cp WHERE cp.datavencimento IS NOT NULL AND cp.datavencimento < '12/12/2017' AND cp.prefixo = " + prefixo + " GROUP BY cp.datavencimento");
        return q.list();
    }
    
    public List<Date> datasVincSuper(){
        Query q = getSession().createQuery("SELECT cp.datavencimento FROM ClienteProduto cp WHERE cp.datavencimento IS NOT NULL AND cp.datavencimento < '12/12/2017' GROUP BY cp.datavencimento");
        return q.list();
    }
    
    public List<Date> datasCanc(int prefixo){
        Query q = getSession().createQuery("SELECT cp.datacancelamento FROM ClienteProduto cp WHERE cp.codproduto IS NOT NULL AND cp.datacancelamento IS NOT NULL AND cp.prefixo = " + prefixo + " GROUP BY cp.datacancelamento");
        return q.list();
    }
    
    public List<Date> datasCancSuper(){
        Query q = getSession().createQuery("SELECT cp.datacancelamento FROM ClienteProduto cp WHERE cp.codproduto IS NOT NULL AND cp.datacancelamento IS NOT NULL GROUP BY cp.datacancelamento");
        return q.list();
    }
    
    public ClienteProduto findByProposta(Integer proposta) {
        Query q = getSession().createQuery("SELECT c FROM ClienteProduto c WHERE c.proposta = :proposta");
        q.setParameter("proposta", proposta);
        return (ClienteProduto) q.uniqueResult();
    }
    
    public boolean PossuiCancelados(Integer mci) {
        Query q = getSession().createQuery("SELECT c FROM ClienteProduto c WHERE c.mci = :mci AND c.cancelado = true");
        q.setParameter("mci", mci);
        return q.list().isEmpty() == false;
    }    
    
    public String PossuiNaoContatados(Integer mci) {
        Query q = getSession().createQuery("SELECT c FROM ClienteProduto c WHERE c.mci = :mci AND c.contatado = false");
        q.setParameter("mci", mci);
        if(q.list().isEmpty()){
            return "Não";
        }
        else{
            return "Sim";
        }
    }
    
    public boolean VincAteh30dias(Integer mci) {
        Date a = new Date();
        Date b = new Date();
        try
        {
            b.setMonth(b.getMonth()+1);
        }
        catch(Exception ex)
        {
            System.out.print("Erro: " + ex);
        }
        SimpleDateFormat sdf = new SimpleDateFormat("DD/mm/yyyy");
        String i = sdf.format(a);
        String f = sdf.format(b);
        Query q = getSession().createQuery("SELECT c FROM ClienteProduto c WHERE c.mci = :mci AND c.contatado = false AND c.datavencimento >= \'" + i + "\' AND c.datavencimento <= \'" + f + "\'");
        q.setParameter("mci", mci);
        return q.list().isEmpty() == false;
    }
       
    public boolean VincsPorIntervaloDeDatas(Date inicio, Date fim, Integer mci) {
//        SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy HH:mm"); //locahost
        SimpleDateFormat formato = new SimpleDateFormat("MM/dd/yyyy HH:mm"); //produção
        inicio = new Date();
        String i = formato.format(inicio);
        String f = formato.format(fim);
        try
        {
            Query q = getSession().createQuery("SELECT c.mci FROM ClienteProduto c WHERE c.mci = :mci AND c.contatado = false AND c.datavencimento >= \'" + i + "\' AND c.datavencimento <= \'" + f + "\'");
            q.setParameter("mci", mci);
            return q.list().isEmpty() == false;
        }
        catch(Exception ex)
        {
            System.out.print("Erro: " + ex);
            return false;
        } 
    }
    public boolean VencsPorIntervaloDeDatas(Date inicio, Date fim, Integer mci) {
//        SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy HH:mm"); //locahost
        SimpleDateFormat formato = new SimpleDateFormat("MM/dd/yyyy HH:mm"); //produção
        fim = new Date();
        String i = formato.format(inicio);
        String f = formato.format(fim);
        try
        {
            Query q = getSession().createQuery("SELECT c.mci FROM ClienteProduto c WHERE c.mci = :mci AND c.contatado = false AND c.datavencimento >= \'" + i + "\' AND c.datavencimento <= \'" + f + "\'");
            q.setParameter("mci", mci);
            return q.list().isEmpty() == false;
        }
        catch(Exception ex)
        {
            Query q = getSession().createQuery("SELECT c.mci FROM ClienteProduto c WHERE c.mci = :mci AND c.contatado = false AND c.datacancelamento >= \'" + i + "\' AND c.datacancelamento <= \'" + f + "\'");
            q.setParameter("mci", mci);
            System.err.println(q);
            return q.list().isEmpty() == false;
        } 
    }
    
    public int QTNaoContatados(Integer mci) {
        Query q = getSession().createQuery("SELECT c.mci FROM ClienteProduto c WHERE c.mci = :mci AND c.contatado = false");
        q.setParameter("mci", mci);
        return q.list().size();
    }
    
    public double PremNaoContatados(Integer mci) {
        Query q = getSession().createQuery("SELECT SUM(c.premio) FROM ClienteProduto c WHERE c.mci = :mci AND c.contatado = false");
        q.setParameter("mci", mci);
        return (double) q.uniqueResult();
    }
    
    public boolean PossuiVencidos(Integer mci) {
        Query q = getSession().createQuery("SELECT c.mci FROM ClienteProduto c WHERE c.mci = :mci AND c.datavencimento <= CURRENT_DATE");
        q.setParameter("mci", mci);
        return q.list().isEmpty() == false;
    }
    
    public boolean PossuiVincendos(Integer mci) {
        Query q = getSession().createQuery("SELECT c.mci FROM ClienteProduto c WHERE c.mci = :mci AND c.datavencimento > CURRENT_DATE");
        q.setParameter("mci", mci);
        return q.list().isEmpty() == false;
    }
    
    public boolean PossuiVincendos7Dias(Integer mci) {
        SimpleDateFormat formato = new SimpleDateFormat("MM/dd/yyyy");
        Date aux = new Date();
        String j = formato.format(aux);
        aux.setDate(aux.getDate()+7);
        String i = formato.format(aux);
        Query q = getSession().createQuery("SELECT c FROM ClienteProduto c WHERE c.mci = :mci AND c.datavencimento BETWEEN Date("+j+") AND DATE("+i+")");
        q.setParameter("mci", mci);
        if(q.list().isEmpty()){
            return false;
        }
        else{
            return true;
        }
    }
}