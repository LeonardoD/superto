/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.dao;

import br.com.bb.superto.model.GroupRole;
import br.com.bb.superto.model.Groups;
import org.hibernate.Query;

/**
 * @author maycon
 */
public class GroupRoleDAO extends GenericHibernateDAO<GroupRole, Integer> {

    public Integer deleteRolesFromGroup(Groups group) {
        String query = "delete from GroupRole where group.id = :groupId";
        Query createQuery = getSession().createQuery(query);
        createQuery.setInteger("groupId", group.getId());
        return createQuery.executeUpdate();
    }
}
