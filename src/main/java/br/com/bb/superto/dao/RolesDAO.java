/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.dao;

import br.com.bb.superto.model.Roles;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * @author maycon
 */
public class RolesDAO extends GenericHibernateDAO<Roles, Integer> {

    public Roles findByName(String name) {
        List<Roles> findByCriteria = findByCriteria(Restrictions.eq("roleName", name));
        if (findByCriteria == null || findByCriteria.isEmpty()) {
            return null;
        }
        return findByCriteria.get(0);

    }
}
