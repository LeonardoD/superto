/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.dao;

import br.com.bb.superto.model.Agencias;
import br.com.bb.superto.model.Cliente;
import br.com.bb.superto.model.clienteSeguridade;
import org.hibernate.Query;
import java.util.ArrayList;
import java.util.List;

/**
 * @author administrador
 */
public class ClienteDao extends GenericHibernateDAO<Cliente, Integer> {

    public List<Cliente> getAgenciaCliente(Agencias agencia) {
        Query q = getSession().createQuery("SELECT c FROM Cliente c WHERE c.agenciaDeOrigem = :agencia");
        q.setParameter("agencia", agencia);
        List<Cliente> listAgencias = (List<Cliente>) q.list();
        return listAgencias;
    }

    public Cliente findByMci(Integer mci) {
        Query q = getSession().createQuery("SELECT c FROM Cliente c WHERE c.mci = :mci");
        q.setParameter("mci", mci);
        return (Cliente) q.uniqueResult();
    }
    
    public ArrayList<Cliente> findByPrefixo (String prefixo) {
        ArrayList <Cliente> aux = new ArrayList<>();
        Query q = getSession().createQuery("SELECT c FROM Cliente c WHERE c.prefixo = :prefixo"); 
        q.setParameter("prefixo", prefixo);
        aux = (ArrayList<Cliente>) q.list();
        return aux;
    }

    public List<clienteSeguridade> findAllSeguridade(){
        Query q = getSession().createQuery("SELECT c FROM clienteSeguridade c WHERE c.mci IN(SELECT mci FROM ClienteProduto)");
        return (List<clienteSeguridade>) q.list();
    }
    
    public clienteSeguridade findByMciSeg(Integer mci) {
        Query q = getSession().createQuery("SELECT c FROM clienteSeguridade c WHERE c.mci = :mci");
        q.setParameter("mci", mci);
        if(q.list() != null)
        {
            return (clienteSeguridade) q.uniqueResult();
        }
        else
        {
            return null;
        }
    }
}