/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.dao;

import br.com.bb.superto.model.Relatos;
import br.com.bb.superto.model.UserNotLogged;
import br.com.bb.superto.security.UserSecurityUtils;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author maycon
 */
public class RelatosDao extends GenericHibernateDAO<Relatos, Integer> {

    public List<Relatos> findAllRelatos() {
        try {
            Criteria criteria = getSession().createCriteria(getPersistentClass());
            criteria.add(Restrictions.eq("funcionario", UserSecurityUtils.getUserFuncionario().getFuncionario()));
            criteria.addOrder(Order.desc("dataEnvio"));
            return criteria.list();
        } catch (UserNotLogged ex) {
            Logger.getLogger(RelatosDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public List<Relatos> findAllRelatosADM() {

        Criteria criteria = getSession().createCriteria(getPersistentClass());

        criteria.addOrder(Order.desc("dataEnvio"));
        return criteria.list();

    }

    public List<Relatos> findAllRelatos(Integer qtd) {
        Criteria criteria = getSession().createCriteria(getPersistentClass());
        criteria.add(Restrictions.eq("aprovado", true));
        criteria.addOrder(Order.desc("dataEnvio"));
        criteria.setMaxResults(qtd);
        return criteria.list();
    }

    public Integer deleteRelato(Relatos relato) {
        Query createQuery = getSession().createQuery("delete from Relatos where id = :id");
        createQuery.setInteger("id", relato.getId());
        return createQuery.executeUpdate();
    }
}
