/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.dao;

import br.com.bb.superto.model.Roles;
import br.com.bb.superto.model.RolesPermissions;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * @author maycon
 */
public class RolesPermissionDAO extends GenericHibernateDAO<RolesPermissions, Integer> {

    public List<RolesPermissions> getRolesByFuncionario(Roles role) {
        Criteria criteria = getSession().createCriteria(getPersistentClass());
        criteria.add(Restrictions.eq("role", role));
        return criteria.list();
    }
}
