/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.dao;

import br.com.bb.superto.model.Mercado;
import br.com.bb.superto.model.Reports;
import br.com.bb.superto.model.SubCategoria;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * @author maycon
 */
public class ReportsDao extends GenericHibernateDAO<Reports, Integer> {

    public List<Reports> getUltimosRelatorios() {
        return getUltimosRelatorios(10);
    }

    public List<Reports> getUltimosRelatorios(Integer quantidade) {
        Criteria criteria = getSession().createCriteria(getPersistentClass());
        criteria.createAlias("report", "r");
        criteria.addOrder(Order.desc("r.uploadDate"));
        criteria.add(Restrictions.eq("visible", true));
        criteria.setFirstResult(0);
        criteria.setMaxResults(quantidade);
        return criteria.list();
    }

    public List<Reports> getUltimosRelatorios(String category) {

        Mercado categoria = Mercado.SUPER;
        Criteria criteria = getSession().createCriteria(getPersistentClass());
        criteria.createAlias("report", "r");
        if (category.equalsIgnoreCase("ADM")) {
            categoria = Mercado.ADM;
        } else if (category.equalsIgnoreCase("PF")) {
            categoria = Mercado.PF;
        } else if (category.equalsIgnoreCase("PJ")) {
            categoria = Mercado.PJ;
        } else if (category.equalsIgnoreCase("AGRO")) {
            categoria = Mercado.AGRO;
        }

        criteria.add(Restrictions.eq("Category", categoria));
        return criteria.list();
    }

    public List<Reports> getUltimosRelatorios(String category, SubCategoria subCategoria) {

        Mercado categoria = Mercado.SUPER;
        Criteria criteria = getSession().createCriteria(getPersistentClass());
        criteria.createAlias("report", "r");
        if (category.equalsIgnoreCase("ADM")) {
            categoria = Mercado.ADM;
        } else if (category.equalsIgnoreCase("PF")) {
            categoria = Mercado.PF;
        } else if (category.equalsIgnoreCase("PJ")) {
            categoria = Mercado.PJ;
        } else if (category.equalsIgnoreCase("AGRO")) {
            categoria = Mercado.AGRO;
        }

        criteria.add(Restrictions.eq("Category", categoria));
        criteria.add(Restrictions.eq("subCategoria", subCategoria));
        return criteria.list();
    }
    
    public List<Reports> findByMercado (String mercado) {
        Query q = getSession().createQuery("SELECT a FROM Reports a where a.mercado = '"+mercado+"'");
        return (List<Reports>) q.list();
    }
    
    public List<Reports> findBySubcategoria (String subcategoria) {
        Query q = getSession().createQuery("SELECT a FROM Reports a where a.subCategoria = '"+subcategoria+"'");
        return (List<Reports>) q.list();
    }
}
