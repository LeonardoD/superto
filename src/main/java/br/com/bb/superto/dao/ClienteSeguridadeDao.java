package br.com.bb.superto.dao;

import br.com.bb.superto.model.clienteSeguridade;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Query;

/**
 * @author Jhemeson Mota
 */
public class ClienteSeguridadeDao extends GenericHibernateDAO<clienteSeguridade, Integer> {
    public List<clienteSeguridade> findComInfo(){
        Query q = getSession().createQuery("SELECT cs FROM clienteSeguridade cs WHERE cs.nomeCliente != ''");
        return q.list();
    }
    
    public List<clienteSeguridade> findComCMD(){
        Query q = getSession().createQuery("SELECT cs FROM clienteSeguridade cs WHERE cs.nomeCliente != '' AND cs.mci IN (SELECT cmd.mci FROM ClienteMD cmd ORDER BY margem DESC)");
        return q.list();
    }
    
    public long CountComCMD(){
        Query q = getSession().createQuery("SELECT COUNT(*) FROM clienteSeguridade cs WHERE cs.nomeCliente != '' AND cs.mci IN (SELECT cmd.mci FROM ClienteMD cmd)");
        return (long) q.uniqueResult();
    }
    
    public long CountFeitosComCMD(){
        Query q = getSession().createQuery("SELECT COUNT(*) FROM clienteSeguridade cs WHERE cs.nomeCliente != '' AND cs.mci IN (SELECT cmd.mci FROM ClienteMD cmd WHERE cmd.contatado = true)");
        return (long) q.uniqueResult();
    }
    
    public List<clienteSeguridade> findComCMDByPrefixo(String prefixo){
        Query q = getSession().createQuery("SELECT cs FROM clienteSeguridade cs WHERE cs.nomeCliente != '' AND cs.mci IN (SELECT cmd.mci FROM ClienteMD cmd ORDER BY margem DESC) AND cs.prefixo = :pref");
        q.setParameter("pref", prefixo);
        return q.list();
    }
    
//    public int CountByPrefixo(String prefixo){
//        Query q = getSession().createQuery("SELECT cs FROM clienteSeguridade cs WHERE cs.nomeCliente != '' AND cs.mci IN (SELECT cmd.mci FROM ClienteMD cmd) AND cs.prefixo = :pref");
//        q.setParameter("pref", prefixo);
//        return q.list().size();
//    }
    
    public long CountByPrefixo(String prefixo){
        Query q = getSession().createQuery("SELECT COUNT(*) FROM clienteSeguridade cs WHERE cs.nomeCliente != '' AND cs.mci IN (SELECT cmd.mci FROM ClienteMD cmd) AND cs.prefixo = :pref");
        q.setParameter("pref", prefixo);
        return (long) q.uniqueResult();
    }
    
    public long CountFeitosByPrefixo(String prefixo){
        Query q = getSession().createQuery("SELECT COUNT(*) FROM clienteSeguridade cs WHERE cs.nomeCliente != '' AND cs.mci IN (SELECT cmd.mci FROM ClienteMD cmd WHERE cmd.contatado = true) AND cs.prefixo = :pref");
        q.setParameter("pref", prefixo);
        return (long) q.uniqueResult();
    }
    
    public List<clienteSeguridade> findSemInfo(){
        try{
            Query q = getSession().createQuery("SELECT cc FROM clienteSeguridade cc WHERE cc.mci NOT IN (SELECT mci FROM clienteSeguridade cs WHERE cs.nomeCliente != '')");
            return q.list();
        }
        catch(Exception ex){
            System.out.print("Erro no findSemInfo: " + ex);
            List<clienteSeguridade> cs = new ArrayList<clienteSeguridade>();
            return cs;
        }
    }
    
    public List<clienteSeguridade> findWithProdVinc(Integer prefixo){
        String pref = prefixo+"";
        Query q = getSession().createQuery("SELECT c FROM clienteSeguridade c WHERE c.prefixo = '" + pref + "' AND c.mci IN (SELECT cl.mci FROM ClienteProduto cl WHERE cl.datavencimento >= NOW())");
        return q.list();
    }
    
    public List<clienteSeguridade> findWithProdVincSuper(){
        Query q = getSession().createQuery("SELECT c FROM clienteSeguridade c WHERE c.mci IN (SELECT cl.mci FROM ClienteProduto cl WHERE cl.datavencimento >= NOW())");
        return q.list();
    }
    
    public List<clienteSeguridade> findWithProdVenc(Integer prefixo){
        String pref = prefixo+"";
        Query q = getSession().createQuery("SELECT c FROM clienteSeguridade c WHERE c.prefixo = '" + pref + "' AND c.mci IN (SELECT cl.mci FROM ClienteProduto cl WHERE cl.datavencimento <= NOW())");
        return q.list();
    }
    
    public List<clienteSeguridade> findWithProdVencSuper(){
        Query q = getSession().createQuery("SELECT c FROM clienteSeguridade c WHERE c.mci IN (SELECT cl.mci FROM ClienteProduto cl WHERE cl.datavencimento <= NOW())");
        return q.list();
    }
    
    public List<clienteSeguridade> findWithProdCanc(Integer prefixo){
        String pref = prefixo+"";
        Query q = getSession().createQuery("SELECT c FROM clienteSeguridade c WHERE c.prefixo = '" + pref + "' AND c.mci IN (SELECT cl.mci FROM ClienteProduto cl WHERE cl.cancelado = true))");
        return q.list();
    }
    
    public List<clienteSeguridade> findWithProdCancSuper(){
        Query q = getSession().createQuery("SELECT c FROM clienteSeguridade c WHERE c.mci IN (SELECT cl.mci FROM ClienteProduto cl WHERE cl.cancelado = true))");
        return q.list();
    }
}