/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.dao;

import br.com.bb.superto.model.Organograma;

import java.util.List;

/**
 * @author maycon
 */
public class OrganogramaDao extends GenericHibernateDAO<Organograma, Integer> {

    public List<Organograma> getList() {
        String query = "from Organograma as o where o.parent = null";
        return (List<Organograma>) getSession().createQuery(query).list();
    }

    public List<Organograma> getChildrens(Organograma organograma) {
        String query = "from Organograma as o where o.parent = :teste";
        return (List<Organograma>) getSession().createQuery(query).setInteger("teste", organograma.getId()).list();
    }

    public List<Organograma> getChildrens(Integer id) {
        String query = "from Organograma as o where o.parent = :teste";
        return (List<Organograma>) getSession().createQuery(query).setInteger("teste", id).list();
    }

    public List<Organograma> getPorId(Integer id) {
        String query = "from Organograma as o where o.id = :teste";
        return (List<Organograma>) getSession().createQuery(query).setInteger("teste", id).list();
    }

    public void deletePorId(Integer id) {
        String query = "delete from Organograma as o where o.id = :teste";
        String query2 = "delete from Organograma as o where o.parent = :teste";
        getSession().createQuery(query2).setInteger("teste", id).executeUpdate();
        getSession().createQuery(query).setInteger("teste", id).executeUpdate();
    }
}
