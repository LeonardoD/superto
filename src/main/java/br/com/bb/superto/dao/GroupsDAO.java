/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.dao;

import br.com.bb.superto.model.Groups;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

/**
 * @author maycon
 */
public class GroupsDAO extends GenericHibernateDAO<Groups, Integer> {

    public Groups getGroupMembers(String name) {
        Criteria criteria = getSession().createCriteria(getPersistentClass());
        return (Groups) criteria.add(Restrictions.eq("groupName", name)).uniqueResult();
    }
}
