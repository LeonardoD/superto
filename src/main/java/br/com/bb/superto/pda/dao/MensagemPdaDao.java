/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.pda.dao;

import br.com.bb.superto.dao.GenericHibernateDAO;
import br.com.bb.superto.model.Funcionarios;
import br.com.bb.superto.pda.model.MensagemPda;
import br.com.bb.superto.pda.model.PdaAgencias;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * @author maycon
 */
public class MensagemPdaDao extends GenericHibernateDAO<MensagemPda, Integer> {

    public List<MensagemPda> getMensagens(PdaAgencias pda) {
        Criteria criteria = getSession().createCriteria(getPersistentClass());
        criteria.addOrder(Order.desc("dataEnvio"));
        criteria.add(Restrictions.eq("pdaAgencia", pda));
        return criteria.list();
    }

    public Integer deleteMensagens(PdaAgencias pdaAgencia) {
        String query = "delete from MensagemPda where pdaAgencia.id = :pda";
        Query createQuery = getSession().createQuery(query);
        createQuery.setInteger("pda", pdaAgencia.getId());
        return createQuery.executeUpdate();
    }

    public List<Funcionarios> getMensagens2(PdaAgencias pda) {
        Criteria criteria = getSession().createCriteria(getPersistentClass());
        criteria.add(Restrictions.eq("pdaAgencia", pda));
        criteria.setProjection(Projections.distinct(Projections.property("funcionario")));
        return criteria.list();
    }

}
