/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.pda.dao;

import br.com.bb.superto.dao.GenericHibernateDAO;
import br.com.bb.superto.model.UserNotLogged;
import br.com.bb.superto.security.UserSecurityUtils;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author maycon
 */
public class NotificacoesTemporariaDao extends GenericHibernateDAO<Object, Integer> {

    public List<Object> getNotificicacoesParcialByFunc(int first, int last) {
        try {
            Criteria criteria = getSession().createCriteria(getPersistentClass());
            criteria.createAlias("notificicao", "a");
            criteria.add(Restrictions.eq("func", UserSecurityUtils.getUserFuncionario().getFuncionario()));
            criteria.addOrder(Order.desc("a.dataDeEnvio"));
            return criteria.list();
        } catch (UserNotLogged ex) {
            Logger.getLogger(NotificacoesTemporariaDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

}
