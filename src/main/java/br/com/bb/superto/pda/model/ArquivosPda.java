/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.pda.model;

import br.com.bb.superto.model.Archive;
import br.com.bb.superto.model.GenericEntity;

import javax.persistence.*;

/**
 * @author maycon
 */
@Entity
@Table(name = "pda_anexos")
public class ArquivosPda implements java.io.Serializable, GenericEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @OneToOne
    private Archive anexo;
    @OneToOne(fetch = FetchType.LAZY)
    private Pda pda;

    public ArquivosPda() {
    }

    public ArquivosPda(Archive anexo, Pda pda) {
        this.anexo = anexo;
        this.pda = pda;
    }

    @Override
    public Integer getId() {
        return id;
    }

    public Archive getAnexo() {
        return anexo;
    }

    public void setAnexo(Archive anexo) {
        this.anexo = anexo;
    }

    public Pda getPda() {
        return pda;
    }

    public void setPda(Pda pda) {
        this.pda = pda;
    }

}
