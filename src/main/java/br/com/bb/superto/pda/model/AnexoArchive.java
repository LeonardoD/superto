/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.pda.model;

import br.com.bb.superto.model.Archive;
import br.com.bb.superto.pda.model.interfaces.AnexoInterface;
import org.primefaces.model.UploadedFile;

/**
 * @author maycon
 */
public class AnexoArchive implements AnexoInterface {

    ArquivosPda archivePda;
    Archive archive;
    boolean add;

    public AnexoArchive(Archive archive, boolean aBoolean) {
        this.archive = archive;
        this.add = aBoolean;
    }

    public AnexoArchive(ArquivosPda archive, boolean aBoolean) {
        this.archivePda = archive;
        this.archive = archive.getAnexo();
        this.add = aBoolean;
    }

    @Override
    public String getName() {
        return archive.getName();
    }

    @Override
    public void setArchive(Archive archive) {
        this.archive = archive;
    }

    @Override
    public Archive getArchive() {
        return this.archive;
    }

    @Override
    public void setArchive(UploadedFile file) {
    }

    @Override
    public boolean isTemp() {
        return add;
    }

    @Override
    public ArquivosPda getAnexo() {
        return archivePda;
    }

}
