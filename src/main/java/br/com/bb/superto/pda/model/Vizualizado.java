/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.pda.model;

import br.com.bb.superto.model.Funcionarios;
import br.com.bb.superto.model.GenericEntity;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author Harlley
 */
@Entity
@Table(name = "Vizualizado")
public class Vizualizado implements GenericEntity, Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer ID;

    @ManyToOne
    private Notificacoes id;
    @ManyToOne
    private Funcionarios Id;

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 19 * hash + (this.ID != null ? this.ID.hashCode() : 0);
        hash = 19 * hash + (this.id != null ? this.id.hashCode() : 0);
        hash = 19 * hash + (this.Id != null ? this.Id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Vizualizado other = (Vizualizado) obj;
        if (this.ID != other.ID && (this.ID == null || !this.ID.equals(other.ID))) {
            return false;
        }
        if (this.id != other.id && (this.id == null || !this.id.equals(other.id))) {
            return false;
        }
        if (this.Id != other.Id && (this.Id == null || !this.Id.equals(other.Id))) {
            return false;
        }
        return true;
    }

    @Override
    public Integer getId() {
        return ID;
    }

}
