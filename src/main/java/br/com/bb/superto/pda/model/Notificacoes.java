/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.pda.model;

import br.com.bb.superto.model.GenericEntity;
import br.com.bb.superto.model.Mercado;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * @author Harlley
 */
@Entity
@Table(name = "notificacoes")
public class Notificacoes implements GenericEntity, Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String descricao;
    private String URL;
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date dataDeEnvio;
    @Enumerated(value = EnumType.STRING)
    private Mercado tipoPrio;
    private boolean todos;

    //MensagemPda novo;
    public Notificacoes() {
    }

    public Date getDataDeEnvio() {
        return dataDeEnvio;
    }

    public void setDataDeEnvio(Date dataDeEnvio) {
        this.dataDeEnvio = dataDeEnvio;
    }

    public boolean isTodos() {
        return todos;
    }

    public void setTodos(boolean todos) {
        this.todos = todos;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Mercado getTipoPrio() {
        return tipoPrio;
    }

    public void setTipoPrio(Mercado tipoPrio) {
        this.tipoPrio = tipoPrio;
    }

    public String getURL() {
        return URL;
    }

    public void setURL(String URL) {
        this.URL = URL;
    }

    @Override
    public String toString() {
        return "" + id;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 23 * hash + (this.id != null ? this.id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Notificacoes other = (Notificacoes) obj;
        if (this.id != other.id && (this.id == null || !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public Integer getId() {
        return id;
    }
}
