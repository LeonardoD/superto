/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.pda.model;

import br.com.bb.superto.model.Funcionarios;
import br.com.bb.superto.model.GenericEntity;
import br.com.bb.superto.pda.enumeration.DemandaCategoria;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * @author maycon
 */
@Entity
@Table(name = "pda")
public class Pda implements java.io.Serializable, GenericEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @OneToMany(mappedBy = "pda")
    private List<PdaAgencias> dependencias;
    private String documento;
    private String titulo;
    private String demandaDescricao;
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date dataInclusao;
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date dataAlteracao;
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date dataPrazo;
    @Enumerated(value = EnumType.STRING)
    private DemandaCategoria categoria;
    @OneToOne
    private Funcionarios responsavel;

    @Override
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<PdaAgencias> getDependencias() {
        return dependencias;
    }

    public void setDependencias(List<PdaAgencias> dependencias) {
        this.dependencias = dependencias;
    }

    public String getDocumento() {
        return documento;
    }

    public void setDocumento(String documento) {
        this.documento = documento;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDemandaDescricao() {
        return demandaDescricao;
    }

    public void setDemandaDescricao(String demandaDescricao) {
        this.demandaDescricao = demandaDescricao;
    }

    public Date getDataInclusao() {
        return dataInclusao;
    }

    public void setDataInclusao(Date dataInclusao) {
        this.dataInclusao = dataInclusao;
    }

    public Date getDataPrazo() {
        return dataPrazo;
    }

    public void setDataPrazo(Date dataPrazo) {
        this.dataPrazo = dataPrazo;
    }

    public DemandaCategoria getCategoria() {
        return categoria;
    }

    public void setCategoria(DemandaCategoria categoria) {
        this.categoria = categoria;
    }

    public Funcionarios getResponsavel() {
        return responsavel;
    }

    public void setResponsavel(Funcionarios responsavel) {
        this.responsavel = responsavel;
    }

    public Date getDataAlteracao() {
        return dataAlteracao;
    }

    public void setDataAlteracao(Date dataAlteracao) {
        this.dataAlteracao = dataAlteracao;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 71 * hash + (this.id != null ? this.id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Pda other = (Pda) obj;
        if (this.id != other.id && (this.id == null || !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "" + id;
    }

}
