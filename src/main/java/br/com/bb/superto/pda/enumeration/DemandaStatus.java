/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.pda.enumeration;

/**
 * @author maycon
 */
public enum DemandaStatus implements java.io.Serializable {

    ANDAMENTO("Em andamento"),
    EMCONCLUSAO("Em conclusão"),
    CONCLUIDO("Concluído");

    private String value;

    private DemandaStatus(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String asString() {
        return this.value;
    }

}
