/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.pda.model;

import br.com.bb.superto.model.Archive;
import br.com.bb.superto.model.GenericEntity;

import javax.persistence.*;

/**
 * @author maycon
 */
@Entity
@Table(name = "agencias_pda_anexos")
public class AgenciasArquivosPda implements java.io.Serializable, GenericEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @OneToOne
    private Archive anexo;
    @OneToOne(fetch = FetchType.LAZY)
    private PdaAgencias pdaAgencia;

    public AgenciasArquivosPda() {
    }

    public AgenciasArquivosPda(Archive anexo, PdaAgencias pdaAgencia) {
        this.anexo = anexo;
        this.pdaAgencia = pdaAgencia;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Archive getAnexo() {
        return anexo;
    }

    public void setAnexo(Archive anexo) {
        this.anexo = anexo;
    }

    public PdaAgencias getPdaAgencia() {
        return pdaAgencia;
    }

    public void setPdaAgencia(PdaAgencias pdaAgencia) {
        this.pdaAgencia = pdaAgencia;
    }

}
