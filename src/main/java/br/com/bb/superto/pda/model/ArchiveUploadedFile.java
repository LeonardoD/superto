/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.pda.model;

import br.com.bb.superto.controller.ArchiveController;
import br.com.bb.superto.model.Archive;
import br.com.bb.superto.model.UserNotLogged;
import br.com.bb.superto.pda.model.interfaces.AnexoInterface;
import org.primefaces.model.UploadedFile;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author maycon
 */
public class ArchiveUploadedFile implements AnexoInterface {

    UploadedFile anexo;

    public ArchiveUploadedFile() {
    }

    public ArchiveUploadedFile(UploadedFile anexo) {
        this.anexo = anexo;
    }

    @Override
    public String getName() {
        return anexo.getFileName();
    }

    @Override
    public void setArchive(Archive archive) {
    }

    @Override
    public Archive getArchive() {
        try {
            return new ArchiveController().add(anexo);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ArchiveUploadedFile.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UserNotLogged ex) {
            Logger.getLogger(ArchiveUploadedFile.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ArchiveUploadedFile.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public void setArchive(UploadedFile file) {
        this.anexo = file;
    }

    @Override
    public boolean isTemp() {
        return false;
    }

    @Override
    public ArquivosPda getAnexo() {
        throw new UnsupportedOperationException("Operação não suportada.");
    }

}
