/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.pda.dao;

import br.com.bb.superto.dao.GenericHibernateDAO;
import br.com.bb.superto.pda.model.AgenciasArquivosPda;
import br.com.bb.superto.pda.model.PdaAgencias;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * @author maycon
 */
public class ArquivosAgenciasPdaDao extends GenericHibernateDAO<AgenciasArquivosPda, Integer> {

    public List<AgenciasArquivosPda> getAnexosByPda(PdaAgencias pda) {
        return findByCriteria(Restrictions.eq("pdaAgencia", pda));
    }

    public void delete(AgenciasArquivosPda arquivo) {
        String query = "delete from AgenciasArquivosPda where id = :id";
        Query createQuery = getSession().createQuery(query);
        createQuery.setInteger("id", arquivo.getId());
        createQuery.executeUpdate();
    }
}
