/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.pda.model.interfaces;

import br.com.bb.superto.model.Archive;
import br.com.bb.superto.pda.model.ArquivosPda;
import org.primefaces.model.UploadedFile;

/**
 * @author maycon
 */
public interface AnexoInterface {

    public String getName();

    public void setArchive(Archive archive);

    public Archive getArchive();

    public void setArchive(UploadedFile file);

    public ArquivosPda getAnexo();

    public boolean isTemp();
}
