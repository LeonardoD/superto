/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.pda.dao;

import br.com.bb.superto.dao.GenericHibernateDAO;
import br.com.bb.superto.pda.model.Vizualizado;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import java.io.Serializable;
import java.util.List;

public class VizualizadoDao extends GenericHibernateDAO<Vizualizado, Serializable> {

    public List<Vizualizado> getJaViu(Integer ID) {
        Criteria criteria = getSession().createCriteria(getPersistentClass());
        criteria.addOrder(Order.desc(null));
        criteria.add(Restrictions.eq("ID", ID));

        return criteria.list();
    }

}
