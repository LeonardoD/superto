/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.pda.controller;

import br.com.bb.superto.controller.ArchiveController;
import br.com.bb.superto.controller.GenericController;
import br.com.bb.superto.controller.ImageController;
import br.com.bb.superto.dao.AgenciasDao;
import br.com.bb.superto.model.Agencias;
import br.com.bb.superto.model.Archive;
import br.com.bb.superto.model.GenericEntity;
import br.com.bb.superto.model.UserNotLogged;
import br.com.bb.superto.pda.dao.*;
import br.com.bb.superto.pda.enumeration.DemandaCategoria;
import br.com.bb.superto.pda.enumeration.DemandaStatus;
import br.com.bb.superto.pda.model.*;
import br.com.bb.superto.pda.model.interfaces.AnexoInterface;
import br.com.bb.superto.security.UserSecurityUtils;
import br.com.bb.superto.util.FacesUtils;
import br.com.bb.superto.util.Util;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.hibernate.validator.constraints.NotEmpty;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.ToggleEvent;
import org.primefaces.model.UploadedFile;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author maycon
 */
@ManagedBean(name = "pdaController")
@ViewScoped
public class PdaController extends GenericController<Pda> implements java.io.Serializable {

    NotificacaoControle not;
    boolean loaded = false;
    private List<UploadedFile> files;
    private List<Agencias> agencias;
    private List<Agencias> agenciasSelecionadas;
    private List<Agencias> aListAgenciasFromPda = new ArrayList<>();
    private List<AnexoInterface> anexosList;
    private List<AnexoInterface> anexosParaRemocao = new ArrayList<>();
    private List<PdaAgencias> selectedList = new ArrayList<>();
    private PdaAgencias agPdaSelected;
    private String status;
    private String categoria;
    @NotEmpty(message = "Mensagem não enviada, entre com o resumo.")
    private String resumo;
    private AgenciasArquivosPda anexoSelected;

    public PdaController() {
        files = new ArrayList<>();
    }

    public List<AnexoInterface> getAnexosList() {
        if (anexosList == null) {
            anexosList = new ArrayList<>();
        }
        return anexosList;
    }

    public void setAnexosList(List<AnexoInterface> anexosList) {
        this.anexosList = anexosList;
    }

    public void temp() {
        List<ArquivosPda> anexosPda = anexosPda(selected);
        for (ArquivosPda aAnexo : anexosPda) {
            if (anexosList == null) {
                anexosList = new ArrayList<>();
            }
            anexosList.add(new AnexoArchive(aAnexo, true));
        }
    }

    //    public String selectPda() {
//        if (id != null && !id.isEmpty()) {
//            try {
//                Integer selectedId = Integer.parseInt(id);
//                System.out.println();
//                this.selected = new PdaDao().getById(selectedId);
//                if (this.selected != null) {
//                    agenciasSelecionadas = new PdaAgenciasDao().getAgenciasFromPda(this.selected);
//                    aListAgenciasFromPda = agenciasSelecionadas;
//                    if (!loaded) {
//                        temp();
//                        loaded = true;
//                    }
//                    return null;
//                }
//            } catch (Exception ex) {
//                addFacesMsg("Não possivel selecionar a noticia de id: '" + id + "'", FacesMessage.SEVERITY_ERROR);
//                return "error";
//            }
//        }
//        return "error";
//    }
    public List<Agencias> getAgencias() {
        if (agencias == null) {
            agencias = new AgenciasDao().findAll();
        }
        return agencias;
    }

    public void fileUploadListener(FileUploadEvent event) {
        if (anexosList == null) {
            anexosList = new ArrayList<>();
        }
        anexosList.add(new ArchiveUploadedFile(event.getFile()));
    }

    public void fileUploadListenerAg(FileUploadEvent event) {
        try {
            new ArquivosAgenciasPdaDao().save(new AgenciasArquivosPda(new ArchiveController().add(event.getFile()), agPdaSelected));
        } catch (UserNotLogged | IOException ex) {
            Logger.getLogger(PdaController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public List<ArquivosPda> anexosPda(Pda pda) {
        return new ArquivosPdaDao().getAnexosByPda(pda);
    }

    public List<AgenciasArquivosPda> anexosPdaAg(PdaAgencias pda) {
        return new ArquivosAgenciasPdaDao().getAnexosByPda(pda);
    }

    public List mergeTwoArrays(List array, List otherArray) {
        boolean match;
        List remover = new ArrayList();
        for (int i = 0; i < array.size(); i++) {
            match = false;
            for (int j = 0; j < otherArray.size(); j++) {
                if (((GenericEntity) array.get(i)).getId().intValue() == ((GenericEntity) otherArray.get(j)).getId()) {
                    otherArray.remove(j);
                    match = true;
                    break;
                }
            }
            if (!match) {
                remover.add(array.get(i));
            }
        }
        return remover;
    }

    @Override
    public void edit() {
        if (selected != null) {
            ArquivosPdaDao aArquivoPdaDao = new ArquivosPdaDao();
            if (!getAnexoParaRemocao().isEmpty()) {
                removerAnexosDoBanco(getAnexoParaRemocao());
            }
            for (AnexoInterface aAnexo : anexosList) {
                if (!aAnexo.isTemp()) {
                    aArquivoPdaDao.save(new ArquivosPda(aAnexo.getArchive(), selected));
                }
            }

            List<Agencias> mergeTwoArrays = mergeTwoArrays(aListAgenciasFromPda, agenciasSelecionadas);
            PdaAgenciasDao aPdaAgenciasDao = new PdaAgenciasDao();
            MensagemPdaDao aMensagemDao = new MensagemPdaDao();
            for (Agencias aAgencia : mergeTwoArrays) {
                aMensagemDao.deleteMensagens(aPdaAgenciasDao.getPdaAgencia(selected, aAgencia));
                aPdaAgenciasDao.deleteMemberFromGroup(selected, aAgencia);
            }
            for (Agencias aAgencia : agenciasSelecionadas) {
                aPdaAgenciasDao.save(new PdaAgencias(aAgencia, selected, null, DemandaStatus.ANDAMENTO));
            }
            new PdaDao().makePersistent(selected);
            setNewValue(true);
            addFacesMsg("Demanda editada com sucesso.", FacesMessage.SEVERITY_INFO);
        }
    }

    public void createFromPda() {
        try {
            Pda pda = new Pda();
            pda.setCategoria(selected.getCategoria());
            pda.setDataInclusao(Util.getDate());
            pda.setDemandaDescricao(selected.getDemandaDescricao());
            pda.setResponsavel(UserSecurityUtils.getUserFuncionario().getFuncionario());
            pda.setTitulo(selected.getTitulo());
            Pda aPda = new PdaDao().save(pda);
            PdaAgenciasDao aPdaAgenciasDao = new PdaAgenciasDao();
            ArquivosPdaDao aArquivoPdaDao = new ArquivosPdaDao();
            for (AnexoInterface aAnexo : anexosList) {
                aArquivoPdaDao.save(new ArquivosPda(aAnexo.getArchive(), aPda));
            }
            for (Agencias aAgencia : agenciasSelecionadas) {
                aPdaAgenciasDao.save(new PdaAgencias(aAgencia, aPda, null, DemandaStatus.ANDAMENTO));
            }
            setNewValue(true);
            addFacesMsg("Demanda adicionada com sucesso.", FacesMessage.SEVERITY_INFO);
        } catch (UserNotLogged ex) {
            Logger.getLogger(PdaController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @Override
    public void delete() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void add() {
        try {
            ArquivosPdaDao aArquivoPdaDao = new ArquivosPdaDao();
            value.setDataInclusao(Util.getDate());
            value = new PdaDao().save(value);
            for (Agencias aAgencia : agenciasSelecionadas) {
                new PdaAgenciasDao().save(new PdaAgencias(aAgencia, value, DemandaStatus.ANDAMENTO));
            }
            for (AnexoInterface aAnexo : anexosList) {
                if (!aAnexo.isTemp()) {
                    aArquivoPdaDao.save(new ArquivosPda(aAnexo.getArchive(), value));
                }
            }
            setNewValue(true);

            Notificacoes aNotification = new Notificacoes();
            aNotification.setDataDeEnvio(Util.getDate());
            aNotification.setDescricao("Novo pda adicionado");
            aNotification.setTodos(true);
            aNotification.setURL(FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath() + "/adm/pda/");
            new NotificacoesDAO().save(aNotification);
        } catch (NullPointerException e) {
            Logger.getLogger(this.getClass().getName()).log(Level.WARNING, "null");
        }
    }

    public void addArchive(Archive archive) {
        this.getAnexosList().add(new AnexoArchive(archive, false));
    }

    public void removerAnexo(AnexoInterface fileSelected) {
        if (fileSelected != null) {
            if (fileSelected.isTemp()) {
                this.getAnexoParaRemocao().add(fileSelected);
            }
            anexosList.remove(fileSelected);
        }
    }

    public void removerAnexoBanco() {
        if (anexoSelected != null) {
            new ArquivosAgenciasPdaDao().delete(anexoSelected);
        }
    }

    public void removerAnexosDoBanco(List<AnexoInterface> aList) {
        ArquivosPdaDao arquivosPdaDao = new ArquivosPdaDao();
        for (AnexoInterface aAnexo : aList) {
            arquivosPdaDao.makeTransient(aAnexo.getAnexo());
        }
    }

    public List<UploadedFile> getFiles() {
        return files;
    }

    public void setFiles(List<UploadedFile> files) {
        this.files = files;
    }

    public List<Agencias> getAgenciasSelecionadas() {
        return agenciasSelecionadas;
    }

    public void setAgenciasSelecionadas(List<Agencias> agenciasSelecionadas) {
        this.agenciasSelecionadas = agenciasSelecionadas;
    }

    private List<AnexoInterface> getAnexoParaRemocao() {
        if (anexosParaRemocao == null) {
            anexosParaRemocao = new ArrayList<>();
        }
        return anexosParaRemocao;
    }

    public Integer contarQuantidade(DemandaCategoria demandaCategoria, DemandaStatus status, String prefixo) {
        Subject subject = SecurityUtils.getSubject();
        if (subject.hasRole("pda")) {
            return new PdaDao().contar(demandaCategoria.name(), status.name(), prefixo, true);
        }
        return new PdaDao().contar(demandaCategoria.name(), status.name(), prefixo, false);
    }

    public void selectionPda(DemandaCategoria demandaCategoria, DemandaStatus status, String prefixo) {
    }

    public List<PdaAgencias> getSelectedList() {
        return selectedList;
    }

    public void setSelectedList(List<PdaAgencias> selectedList) {
        this.selectedList = selectedList;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public String filtrar() {
        if (this.status != null && !this.status.isEmpty() && this.categoria != null && !this.categoria.isEmpty() && !this.status.equals(
                DemandaStatus.CONCLUIDO.name())) {
            try {
                System.out.println("Status :\n\t " + status);
                System.out.println("Categoria :\n\t " + categoria);
                if (SecurityUtils.getSubject().hasRole("pda")) {
                    selectedList = new PdaAgenciasDao().selectionarListaDePda(categoria, status, UserSecurityUtils.getUserFuncionario().getFuncionario().getPrefixo().toString(), true);
                } else {
                    selectedList = new PdaAgenciasDao().selectionarListaDePda(categoria, status, UserSecurityUtils.getUserFuncionario().getFuncionario().getPrefixo().toString(), false);
                }
                return null;
            } catch (UserNotLogged ex) {
            }
        }
        return "error";
    }
//
//    public String selPdaAgencia() {
//        if (id != null && !id.isEmpty()) {
//            try {
//                Integer selectedId = Integer.parseInt(id);
//                this.agPdaSelected = new PdaAgenciasDao().getById(selectedId);
//                if (this.agPdaSelected != null) {
//                    return null;
//                }
//            } catch (Exception ex) {
//            }
//        }
//        return "error";
//    }

    public void redirect() {
        try {
            System.out.println("\t" + agPdaSelected.getPda().getTitulo());
            FacesContext.getCurrentInstance().getExternalContext().redirect("/superto/mercado/adm/pda/" + agPdaSelected.getId());
        } catch (IOException ex) {
            Logger.getLogger(PdaController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void onRowToggle(ToggleEvent event) {
        PdaAgencias sel = (PdaAgencias) event.getData();
    }

    public PdaAgencias getAgPdaSelected() {
        return agPdaSelected;
    }

    public void setAgPdaSelected(PdaAgencias agPdaSelected) {
        this.agPdaSelected = agPdaSelected;
    }

    public void enviarParaConclusao() {
        this.agPdaSelected.setStatus(DemandaStatus.EMCONCLUSAO);
        new PdaAgenciasDao().makePersistent(agPdaSelected);
        addFacesMsg("Demanda enviada para conclusão.", FacesMessage.SEVERITY_INFO);
    }

    public void concluir() {
        if (resumo != null && !resumo.isEmpty()) {
            this.agPdaSelected.setStatus(DemandaStatus.CONCLUIDO);
            new PdaAgenciasDao().makePersistent(agPdaSelected);
            addFacesMsg("Demanda concluída com sucesso.", FacesMessage.SEVERITY_INFO);
            addFacesMsg("Você será redirecionado", FacesMessage.SEVERITY_INFO);
            RequestContext requestContext = RequestContext.getCurrentInstance();
            requestContext.execute("redirecionar()");
        }
    }

    public void voltarParaEdicao() {
        this.agPdaSelected.setStatus(DemandaStatus.ANDAMENTO);
        new PdaAgenciasDao().makePersistent(agPdaSelected);
        addFacesMsg("Demanda em andamento.", FacesMessage.SEVERITY_INFO);
    }

    public String getResumo() {
        return resumo;
    }

    public void setResumo(String resumo) {
        this.resumo = resumo;
    }

    public boolean verificarPermissaoParaEdicao() {
        try {
            if (UserSecurityUtils.getUserFuncionario().getFuncionario().getFuncao().equalsIgnoreCase(categoria) || SecurityUtils.getSubject().hasRole("pda")) {
                return true;
            }
        } catch (UserNotLogged ex) {
            Logger.getLogger(PdaController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public AgenciasArquivosPda getAnexoSelected() {
        return anexoSelected;
    }

    public void setAnexoSelected(AgenciasArquivosPda anexoSelected) {
        this.anexoSelected = anexoSelected;
    }

    public void upImagem() {
        ImageController imageController = (ImageController) FacesUtils.getViewAttribute("imageController");
        imageController.add();
        RequestContext requestContext = RequestContext.getCurrentInstance();
        requestContext.execute("addImage('" + imageController.getValue().getArchive().getUrl() + "')");
    }

}
