/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.pda.controller;

import br.com.bb.superto.model.Funcionarios;
import br.com.bb.superto.model.UserNotLogged;
import br.com.bb.superto.pda.dao.MensagemPdaDao;
import br.com.bb.superto.pda.dao.NotificacoesDAO;
import br.com.bb.superto.pda.dao.NotificacoesTemporariaDao;
import br.com.bb.superto.pda.model.MensagemPda;
import br.com.bb.superto.pda.model.Notificacoes;
import br.com.bb.superto.pda.model.PdaAgencias;
import br.com.bb.superto.security.UserSecurityUtils;
import br.com.bb.superto.util.FacesUtils;
import br.com.bb.superto.util.Util;
import org.hibernate.validator.constraints.NotEmpty;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author maycon
 */
@ManagedBean(name = "mensagemPdaController")
@ViewScoped
public class MensagemPdaController implements java.io.Serializable {

    @NotEmpty(message = "Mensagem em branco, por favor preencha o campo mensagem.")
    String texto;

    public MensagemPdaController() {
    }

    public List<MensagemPda> mensagens(PdaAgencias pda) {
        return new MensagemPdaDao().getMensagens(pda);
    }

    public void enviarMensagem(PdaAgencias pda) {
        if (texto != null && !texto.isEmpty()) {
            try {
                new MensagemPdaDao().save(new MensagemPda(texto, Util.getDate(), UserSecurityUtils.getUserFuncionario().getFuncionario(), pda));

                Notificacoes aNotification = new Notificacoes();
                aNotification.setDataDeEnvio(Util.getDate());
                aNotification.setDescricao("Novo mensagem adicionada");
                aNotification.setTodos(true);
                aNotification.setURL(FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath() + "/adm/pda/" + pda.getId());
                aNotification = new NotificacoesDAO().save(aNotification);
                List<Funcionarios> mensagens2 = new MensagemPdaDao().getMensagens2(pda);
                NotificacoesTemporariaDao notTempDao = new NotificacoesTemporariaDao();
//                for(Funcionarios aFunc : mensagens2){
//                    notTempDao.save(new NotificacaoTemporaria(aNotification, aFunc));
//                }                                
                FacesUtils.addFacesMsg("Mensagem enviada com sucesso.", FacesMessage.SEVERITY_INFO);
            } catch (UserNotLogged ex) {
                System.out.println("Entramos 4");
                Logger.getLogger(MensagemPdaController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }
}
