/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.pda.dao;

import br.com.bb.superto.dao.GenericHibernateDAO;
import br.com.bb.superto.model.Agencias;
import br.com.bb.superto.pda.model.Pda;
import br.com.bb.superto.pda.model.PdaAgencias;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * @author maycon
 */
public class PdaAgenciasDao extends GenericHibernateDAO<PdaAgencias, Integer> implements java.io.Serializable {

    public List<Agencias> getAgenciasFromPda(Pda pda) {
        List<Agencias> aAgencias = getSession().createSQLQuery(
                "select agencias.prefixo, agencias.nomeagencia, agencias.rede from pda_agencias inner join agencias on agencias.prefixo = dependecia_prefixo where pda_id = " + pda.getId())
                .addEntity(Agencias.class).list();
        return aAgencias;

    }

    public PdaAgencias getPdaAgencia(Pda pda, Agencias e) {
        List<PdaAgencias> findByCriteria = this.findByCriteria(Restrictions.eq("pda", pda), Restrictions.eq("dependecia", e));
        if (findByCriteria != null && !findByCriteria.isEmpty()) {
            return findByCriteria.get(0);
        }
        return null;
    }

    public int deleteMemberFromGroup(Pda selected, Agencias e) {
        String query = "delete from PdaAgencias where pda.id = :pdaId and dependecia.prefixo = :prefixo";
        Query createQuery = getSession().createQuery(query);
        createQuery.setInteger("pdaId", selected.getId());
        createQuery.setInteger("prefixo", e.getPrefixo());
        return createQuery.executeUpdate();
    }

    public int deleteAgencia(Pda selected) {
        String query = "delete from PdaAgencias where pda.id = :pdaId";
        Query createQuery = getSession().createQuery(query);
        createQuery.setInteger("pdaId", selected.getId());
        return createQuery.executeUpdate();
    }

    public List<PdaAgencias> selectionarListaDePda(String demandaCategoria, String status, String prefixo, boolean all) {
        String where = "a.categoria = '" + demandaCategoria + "' and pda_agencias.status = '" + status + "'";
        if (!all) {
            where += " and pda_agencias.dependecia_prefixo = " + prefixo;
        }
        return getSession().createSQLQuery(" SELECT * from pda_agencias "
                + "inner join pda as a on a.id = pda_agencias.pda_id "
                + "where "
                + where).addEntity(getPersistentClass()).list();
        //+ "a.categoria = '" + demandaCategoria + "' and pda_agencias.status = '" + status + "' and pda_agencias.dependecia_prefixo = " + prefixo).addEntity(getPersistentClass()).list();

    }

    public PdaAgencias getById(Integer id) {
        List<PdaAgencias> findByCriteria = this.findByCriteria(Restrictions.eq("id", id));
        if (findByCriteria != null && !findByCriteria.isEmpty()) {
            return findByCriteria.get(0);
        }
        return null;
    }
}
