/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.pda.model;

import br.com.bb.superto.model.Funcionarios;

import javax.persistence.*;
import java.util.Date;

/**
 * @author maycon
 */
@Entity
@Table(name = "mensagem_pda")
public class MensagemPda implements java.io.Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String texto;
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date dataEnvio;
    @OneToOne
    private Funcionarios funcionario;
    @OneToOne
    private PdaAgencias pdaAgencia;

    public MensagemPda() {
    }

    public MensagemPda(String texto, Date dataEnvio, Funcionarios funcionario, PdaAgencias pdaAgencia) {
        this.texto = texto;
        this.dataEnvio = dataEnvio;
        this.funcionario = funcionario;
        this.pdaAgencia = pdaAgencia;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    public Date getDataEnvio() {
        return dataEnvio;
    }

    public void setDataEnvio(Date dataEnvio) {
        this.dataEnvio = dataEnvio;
    }

    public Funcionarios getFuncionario() {
        return funcionario;
    }

    public void setFuncionario(Funcionarios funcionario) {
        this.funcionario = funcionario;
    }

    public PdaAgencias getPdaAgencia() {
        return pdaAgencia;
    }

    public void setPdaAgencia(PdaAgencias pdaAgencia) {
        this.pdaAgencia = pdaAgencia;
    }

}
