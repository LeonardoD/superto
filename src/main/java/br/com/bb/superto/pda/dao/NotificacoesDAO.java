/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.pda.dao;

import br.com.bb.superto.dao.GenericHibernateDAO;
import br.com.bb.superto.pda.model.Notificacoes;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import java.io.Serializable;
import java.util.List;

public class NotificacoesDAO extends GenericHibernateDAO<Notificacoes, Serializable> {

    public List<Notificacoes> getNotificacoesTotal() {
        Criteria criteria = getSession().createCriteria(getPersistentClass());
        criteria.addOrder(Order.desc("dataDeEnvio"));
        //criteria.add(Restrictions.eq(null, last));
        return criteria.list();
    }

    public List<Notificacoes> getNotificicacoesParcial(int first, int last) {
        Criteria criteria = getSession().createCriteria(getPersistentClass());
        criteria.addOrder(Order.desc("dataDeEnvio"));
        criteria.setFirstResult(first);
        criteria.setMaxResults(last);
        return criteria.list();
    }

    public List<Notificacoes> getNotificacoesMatricula(Integer id) {
        Criteria criteria = getSession().createCriteria(getPersistentClass());
        criteria.addOrder(Order.desc("dataDeEnvio"));
        criteria.add(Restrictions.eq("matricula", id));

        return criteria.list();
    }

}
