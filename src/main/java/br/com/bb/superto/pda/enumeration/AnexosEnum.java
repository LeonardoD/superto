/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.pda.enumeration;

/**
 * @author maycon
 */
public enum AnexosEnum {

    DOWN("Download"),
    UP("Upload");
    private String value;

    private AnexosEnum(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}
