/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.pda.dao;

import br.com.bb.superto.model.GenericEntity;
import br.com.bb.superto.pda.model.MensagemPda;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author Harlley
 */
@Entity
@Table(name = "MensPDA")
public class NotifMensPDA implements Serializable, GenericEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer ID;
    @ManyToOne
    private MensagemPda id;

    @Override
    public String toString() {
        return "MensPDA{" + "ID=" + ID + '}';
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 79 * hash + (this.ID != null ? this.ID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final NotifMensPDA other = (NotifMensPDA) obj;
        if (this.ID != other.ID && (this.ID == null || !this.ID.equals(other.ID))) {
            return false;
        }
        return true;
    }

    @Override
    public Integer getId() {
        return ID;
    }

}
