/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.pda.controller;

import br.com.bb.superto.controller.GenericController;
import br.com.bb.superto.model.Funcionarios;
import br.com.bb.superto.pda.dao.NotificacoesDAO;
import br.com.bb.superto.pda.dao.VizualizadoDao;
import br.com.bb.superto.pda.model.MensagemPda;
import br.com.bb.superto.pda.model.Notificacoes;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.util.List;

@ManagedBean(name = "notificacaoControle")
@ViewScoped

public class NotificacaoControle<N> extends GenericController<Notificacoes> implements java.io.Serializable {

    String desc;
    String url;
    Funcionarios mat;
    MensagemPda mensagem;

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public List<Notificacoes> notificar(Notificacoes n) {
        return new NotificacoesDAO().getNotificacoesTotal();

    }

    public void atribuir(MensagemPda mensagem) {
        desc = mensagem.getTexto();

    }

    @Override
    public void add() {
        //selected.setMatricula(mat.getFmatricula());
        selected.setDescricao(desc);
//        selected.setTipoPrio(Mercado.PRIO);
    }

    public boolean vizualizarNot(VizualizadoDao aVizualizado) {
        return true;
    }

    @Override
    public void edit() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void delete() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

//    public List<NotificacaoTemporaria> notificacoes(Integer qtd){
//        //return new NotificacoesTemporariaDao().getNotificicacoesParcialByFunc(0, 10);
//    }
}
