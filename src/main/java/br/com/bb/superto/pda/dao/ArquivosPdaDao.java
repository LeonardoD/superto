/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.pda.dao;

import br.com.bb.superto.dao.GenericHibernateDAO;
import br.com.bb.superto.pda.model.ArquivosPda;
import br.com.bb.superto.pda.model.Pda;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * @author maycon
 */
public class ArquivosPdaDao extends GenericHibernateDAO<ArquivosPda, Integer> {

    public List<ArquivosPda> getAnexosByPda(Pda pda) {
        return findByCriteria(Restrictions.eq("pda", pda));
    }

    public void delete(ArquivosPda arquivo) {
        String query = "delete from ArquivosPda where id = :id";
        Query createQuery = getSession().createQuery(query);
        createQuery.setInteger("id", arquivo.getId());
        createQuery.executeUpdate();
    }
}
