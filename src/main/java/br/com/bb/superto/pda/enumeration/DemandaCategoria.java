/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.pda.enumeration;

/**
 * @author maycon
 */
public enum DemandaCategoria {

    RATING("Rating"),
    ADIMPLENCIA("Adimplência"),
    FUNCIONALISMO("Funcionalismo"),
    ATENDIMENTO("Atendimendo"),
    LOGISTICA("logística");
    private final String type;

    private DemandaCategoria(String type) {
        this.type = type;
    }

    private String type() {
        return this.type;
    }

    public String getType() {
        return this.type;
    }

    public String asString() {
        return this.type;
    }
}
