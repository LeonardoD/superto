/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.pda.model;

import br.com.bb.superto.model.Agencias;
import br.com.bb.superto.model.Funcionarios;
import br.com.bb.superto.model.GenericEntity;
import br.com.bb.superto.pda.enumeration.DemandaStatus;

import javax.persistence.*;
import java.util.Date;

/**
 * @author maycon
 */
@Entity
@Table(name = "pda_agencias")
public class PdaAgencias implements java.io.Serializable, GenericEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne
    private Agencias dependecia;
    @ManyToOne
    private Pda pda;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date dataConclusao;
    @Enumerated(value = EnumType.STRING)
    private DemandaStatus status;
    private String resumoConclusao;
    @OneToOne
    private Funcionarios gerenteResponsavel;

    public PdaAgencias() {
    }

    public PdaAgencias(Agencias dependecia, Pda pda, Date dataConclusao, DemandaStatus status) {
        this.dependecia = dependecia;
        this.pda = pda;
        this.dataConclusao = dataConclusao;
        this.status = status;
    }

    public PdaAgencias(Agencias dependecia, Pda pda, DemandaStatus status) {
        this.dependecia = dependecia;
        this.pda = pda;
        this.status = status;
    }

    @Override
    public Integer getId() {
        return id;
    }

    public Agencias getDependecia() {
        return dependecia;
    }

    public void setDependecia(Agencias dependecia) {
        this.dependecia = dependecia;
    }

    public Pda getPda() {
        return pda;
    }

    public void setPda(Pda pda) {
        this.pda = pda;
    }

    public Date getDataConclusao() {
        return dataConclusao;
    }

    public void setDataConclusao(Date dataConclusao) {
        this.dataConclusao = dataConclusao;
    }

    public DemandaStatus getStatus() {
        return status;
    }

    public void setStatus(DemandaStatus status) {
        this.status = status;
    }

    public String getResumoConclusao() {
        return resumoConclusao;
    }

    public void setResumoConclusao(String resumoConclusao) {
        this.resumoConclusao = resumoConclusao;
    }

    public Funcionarios getGerenteResponsavel() {
        return gerenteResponsavel;
    }

    public void setGerenteResponsavel(Funcionarios gerenteResponsavel) {
        this.gerenteResponsavel = gerenteResponsavel;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + (this.dependecia != null ? this.dependecia.hashCode() : 0);
        hash = 97 * hash + (this.pda != null ? this.pda.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PdaAgencias other = (PdaAgencias) obj;
        if (this.dependecia != other.dependecia && (this.dependecia == null || !this.dependecia.equals(other.dependecia))) {
            return false;
        }
        if (this.pda != other.pda && (this.pda == null || !this.pda.equals(other.pda))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "" + id;
    }

}
