/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bb.superto.pda.dao;

import br.com.bb.superto.dao.GenericHibernateDAO;
import br.com.bb.superto.model.UserNotLogged;
import br.com.bb.superto.pda.model.Pda;
import br.com.bb.superto.security.UserSecurityUtils;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import java.math.BigInteger;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author maycon
 */
public class PdaDao extends GenericHibernateDAO<Pda, Integer> {

    public Pda getById(Integer id) {
        List<Pda> findByCriteria = this.findByCriteria(Restrictions.eq("id", id));
        if (findByCriteria != null && !findByCriteria.isEmpty()) {
            return findByCriteria.get(0);
        }
        return null;
    }

    public void teste() {
        try {
            Criteria createCriteria = getSession().createCriteria(getPersistentClass());
            createCriteria.add(Restrictions.eq("responsavel", UserSecurityUtils.getUserFuncionario().getFuncionario()));
        } catch (UserNotLogged ex) {
            Logger.getLogger(PdaDao.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public Integer contar(String demandaCategoria, String status, String prefixo, boolean all) {
        String where = "categoria = '" + demandaCategoria + "' and status = '" + status + "'";
        if (!all) {
            where += " and prefixo = " + prefixo;
        }
        return ((BigInteger) getSession().createSQLQuery("SELECT COUNT(*) from pda "
                + "inner join pda_agencias ag on ag.pda_id = pda.id "
                + "inner join agencias ag2 on ag2.prefixo = ag.dependecia_prefixo "
                + "where "
                + where).uniqueResult()).intValue();
    }

}
