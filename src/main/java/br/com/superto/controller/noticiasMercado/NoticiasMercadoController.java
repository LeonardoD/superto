package br.com.superto.controller.noticiasMercado;

import br.com.bb.superto.controller.*;
import br.com.bb.superto.dao.DestaquesDao;
import br.com.bb.superto.dao.NewsDao;
import br.com.bb.superto.model.*;
import br.com.bb.superto.controller.*;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.io.Serializable;
import java.util.List;

/**
 * @author Rodrigo e Jhonatan Controller para o portal de noticias do site.
 */
@ManagedBean(name = "noticiasMercadoController")
@ViewScoped
public class NoticiasMercadoController extends GenericController<News> implements Serializable {

    private List<News> ultimasNoticias;
    private List<News> noticiasMercado;

    public List<News> getUltimasNoticias() {
        ultimasNoticias = new NewsDao().getUltimasNoticias();
        return ultimasNoticias;
    }

    public List<News> getTodasAsNoticias() {
        ultimasNoticias = new NewsDao().getUltimasNoticiasPaginacao();
        return ultimasNoticias;
    }

    public List<Destaques> getDestaques() {
        List<Destaques> listDestaques = new DestaquesDao().findAll();
        return listDestaques;
    }

    public List<News> getNoticiasMercado(Mercado mercado) {
        noticiasMercado = new NewsDao().getNewsByMercado(true, mercado);
        return noticiasMercado;
    }

    @Override
    public void edit() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void delete() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void add() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
