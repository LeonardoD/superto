/* 
 * Autor: Rodrigo Magalhaes
 * 
 * Este bloco de código se refere à barra de navegação 
 * quando ela deve ser fixa mesmo sem o scrum
 */
$(document).ready(function () {
    $('#div_menus').css("background-color", "rgba(38,38,38,1)");
    $('#div_menus #menu_opcoes div').css("color", "white");
    $('#div_menus').css("height", "65px");
    $('#div_menus').addClass("sombra-menu");
//    $('#menu_logobb').show(250);
    $('#div_menus #menu_opcoes .item-menu').addClass("item-menu-border");
    $('#div_menus #menu_opcoes .item-menu').addClass("item-menu-fundo");

});

$(window).scroll(function () {
    if (window.scrollY > 10) {
        $('#div_logo_bb').fadeOut(500);
        $('#menu_logobb').fadeIn(500);
    } else {
        $('#div_logo_bb').fadeIn(500);
        $('#menu_logobb').fadeOut(500);
    }
});