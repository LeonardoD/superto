/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function mostraAtencao(titulo, mensagem, duracao) {
    if (duracao > 0) {
        swal({
            title: titulo,
            text: mensagem,
            type: "warning",
            timer: duracao
        });
    } else {
        swal({
            title: titulo,
            text: mensagem,
            type: "warning"
        });
    }
}
function mostraSucesso(titulo, mensagem, duracao) {
    if (duracao > 0) {
        swal({
            title: titulo,
            text: mensagem,
            type: "success",
            timer: duracao
        });
    } else {
        swal({
            title: titulo,
            text: mensagem,
            type: "success"
        });
    }
}
function mostraError(titulo, mensagem, duracao) {
    if (duracao > 0) {
        swal({
            title: titulo,
            text: mensagem,
            type: "error",
            timer: duracao
        });
    } else {
        swal({
            title: titulo,
            text: mensagem,
            type: "error"
        });
    }
}