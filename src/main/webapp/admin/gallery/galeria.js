//Clicks
$("#buttonAdd").live('click', function () {
    if ($(this).hasClass('disable'))
        return false;
    imgDialog.show();
});


//Funções
function imgRow() {
    var maxrow = $('.albumpics').width();
    if (maxrow) {
        maxItem = Math.floor(maxrow / 160);
        maxW = maxItem * 160;
        mL = (maxrow - maxW) / 2;
        $('.albumpics ul').css({
            'width': maxW,
            'marginLeft': mL
        })
    }
}

function atualizar() {
    $("#albumsList").find("#" + albumSelected).addClass('selected', 30);

    $(".preview").delegate('img', 'mouseenter', function () {
        if ($(this).hasClass('stackphotos')) {
            var $parent = $(this).parent();
            $parent.find('img#photo1').addClass('rotate1');
            $parent.find('img#photo2').addClass('rotate2');
            $parent.find('img#photo3').addClass('rotate3');
        }
    }).delegate('img', 'mouseleave', function () {
        $('img#photo1').removeClass('rotate1');
        $('img#photo2').removeClass('rotate2');
        $('img#photo3').removeClass('rotate3');
    });

    $('.album').droppable({
        hoverClass: 'over',
        activeClass: 'dragging',
        drop: function (event, ui) {

            if ($(this).hasClass('selected'))
                return false;
            loading('Moving');
            var draggableId = ui.draggable.attr("id");
            var droppableId = $(this).attr("id");
            ui.helper.fadeOut(400);
            setTimeout("unloading()", 1500);
            console.log(draggableId + "teste" + droppableId);

            document.getElementById('id').value = draggableId;
            document.getElementById('iddestino').value = droppableId;

            moveImage();
        },
        tolerance: 'pointer'
    });

    $("#sortable").sortable({
        opacity: 0.6,
        revert: true,
        cursor: "move",
        zIndex: 9000
    });

    $('.albumImage').dblclick(function () {
        $("a[rel=glr]").fancybox({
            'showCloseButton': true,
            'overlayOpacity': 0.8,
            'padding': 0,
            'autoCenter ': true,
            'centerOnScroll': true,
            helpers: {
                title: {
                    type: 'outside'
                }
            }
        });
        $(this).find('a').trigger('click');
    });
    $('.picPreview').droppable({
        hoverClass: 'picPreview-hover',
        activeClass: 'picPreview-hover',
        drop: function (event, ui) {
            $('#image-albumPreview').attr('src', ui.draggable.find('img').attr('src'));
            var draggableId = ui.draggable.attr("id");
            document.getElementById('id').value = draggableId;
            trocarImagemCapa();
        }
    });
    $('.deletezone').droppable({
        hoverClass: 'deletezoneover',
        activeClass: 'deletezonedragging',
        drop: function (event, ui) {
            ui.helper.fadeOut(function () {
                ui.helper.remove();
                setTimeout("unloading()", 1500);
                var draggableId = ui.draggable.attr("id");
                document.getElementById('id').value = draggableId;
                removerImagem();
            });
        },
        tolerance: 'pointer'
    });
}


